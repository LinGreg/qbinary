
\documentclass{article}
\usepackage{bm}

\begin{document}

\title{Constraining Properties of Binary Star Populations using a Hierarchical 
Bayesian Model}
\date{}
\maketitle

\section{Introduction}

Here I develop a Bayesian methodology to infer the statistical properties of 
binary star populations from a multi-epoch radial velocity survey, either in 
clusters, dwarf galaxies or in the Milky Way field. These properties include 
the binary fraction and the distribution of periods, eccentricities, and mass 
ratios. This approach relies on radial velocity variations and hence is 
currently only sensitive to binaries with sufficiently short periods ($\sim$10 
years or less).  However, binary spectral fitting (El-Badry et al. 2017) can 
identify binaries and constrain their mass ratios even if the period is too long 
for velocity variations to be detected. Thus, it may be possible to extend the 
methodology to include more observables, and hence constrain binary properties 
even at longer periods.

The idea is to use Bayesian hierarchical modeling with two levels of inference: 
first, we infer the binary properties (and probability of binarity) for each 
star; next, we infer properties of the entire star population using the 
marginal likelihoods generated from the first level of inference for each star.  
Using this approach, we squeeze the maximum amount of information we can get 
from the data. Being entirely statisical in nature, it does not rely on 
definitively confirming any particular star as a binary, or nailing down the 
binary properties of a particulary star (although it certainly helps to do so 
for stars that show large velocity variations). At present, Greg Martinez and I 
are starting to apply this methodology to mock data to answer the question 
``how well can we nail down binary population properties using a 
high-resolution spectrograph and a large multi-epoch survey?'' In lieu of a 
definitive answer to this question, below I describe the method itself.

In Section \ref{sec:likelihood}, I show how to construct the likelihood for an 
individual star under the binary model. By marginalizing over this likelihood, 
we will generate the first-level Bayesian evidence, the procedure for which is 
described in Section \ref{sec:bayes}, and we will likewise also generate the 
Bayesian evidence under the assumption of non-binarity.  The first-level 
Bayesian evidences will then be used in Section \ref{sec:bayes2} to generate 
the marginal likelihood of the entire star population (for the second level of 
inference), over which we marginalize to infer the binary fraction and 
distribution of binary properties.

\section{First level of inference: likelihood for individual binary stars}\label{sec:likelihood}

At the first level of inference, we are constraining the binary orbital 
parameters of an individual star from radial velocity measurements under the 
assumption of binarity. The parameters we will marginalize over are the 
line-of-sight component of the barycenter velocity $v_{cm}$, and the orbital 
parameters $\mathcal{P} = (\log T,q,e,\theta,\psi,\eta_1)$. These parameters 
are, respectively: the log of the orbital period $T$ (in log-years), mass ratio 
$q$, eccentricity $e$, the second and third Euler angles\footnote{The first 
Euler angle is not needed here because the radial velocity is invariant to 
rotations about the $z$-axis, which we take to be along the line-of-sight.} 
$\theta,\psi$, and the initial eccentric anomaly of the orbit $\eta_1$. We 
assume here that the mass of the primary star $m_p$ can be estimated from its 
magnitude and color using a stellar population synthesis model, and hence do 
not include it as a model parameter (although in principle, it could be 
included as a parameter).

The binary star likelihood for radial velocity data can now be written as 
follows:

\begin{equation}
\mathcal{L}_b(v_i;\sigma_i,t_i,m_p|v_{cm},\mathcal{P}) = \prod_{i=1}^n \frac{1}{\sqrt{2\pi\sigma_i^2}} e^{-(v_{cm}+V_{b,i}-v_i)^2/2\sigma_i^2}
\label{eq:like0}
\end{equation}
where the $v_i, \sigma_i$ are the radial velocity measurements and 
corresponding errors measured at epochs $t_i$, and the binary radial velocities 
$V_{b,i}$ are given by the following functions,
\begin{equation}
V_{b,i} = \frac{g(\theta,\psi,\eta_i;e)}{\sqrt{1-e^2}} \frac{2\pi q}{(1+q)^{2/3}} \left(\frac{m_p}{T}\right)^{1/3},
\end{equation}
\begin{equation}
g(\theta,\psi,\eta_i;e) \equiv \sin \theta\left[\cos(\psi - \phi(\eta_i)) + e\cos(\phi(\eta_i))\right],
\end{equation}
where $\theta$ and $\psi$ are the second and third Euler angles, respectively, and
$\phi(\eta_i)$ are the orbital phases (the ``true anomaly'') determined from 
the eccentric anomaly at each time $t_i$. The initial eccentric anomaly 
$\eta_1$ is a parameter to be marginalized over, while the anomaly at later 
times $\eta_i$ are obtained by evolving the binary's orbit by the time 
differences $t_i-t_{i-1}$.

Before we proceed, it is useful to cast the likelihood in a slightly different 
form.  With some algebraic manipulations, it can be shown that 
eq.~\ref{eq:like0} can be rewritten as

\begin{equation}
\mathcal{L}_b(v_i;\sigma_i|v_{cm},\mathcal{P}) = \mathcal{D}(v_i,\sigma_i|\mathcal{P})\frac{e^{-(v_{cm}-\langle v-V_b \rangle)^2/2\sigma_m^2}}{\sqrt{2\pi\sigma_m^2}},
\label{eq:lfac}
\end{equation}
where we are now suppressing the $t_i$ and $m_p$ in our notation for compactness' sake, and
\begin{equation}
\langle v-V_b \rangle = \sigma_m^2 \sum_{i=1}^n \frac{v_i - V_{b,i}}{\sigma_i^2},
\label{eq:vmean}
\end{equation}

\begin{equation}
\sigma_m^2 = \left(\sum_{i=1}^{n}\frac{1}{\sigma_i^2}\right)^{-1},
\end{equation}

\begin{equation}
\mathcal{D}(v_i,\sigma_i|\mathcal{P}) = \frac{\sqrt{2\pi\sigma_m^2}}{\prod_{i=1}^n \sqrt{2\pi\sigma_i^2}} e^{-\left(\langle (v-V_b)^2\rangle - \langle v - V_b\rangle^2\right)/2\sigma_m^2}.
\label{eq:dfac}
\end{equation}

This form is useful because we have factored out the part that depends on 
$v_{cm}$, so that if one is not interested in constraining $v_{cm}$ or its 
distribution (i.e. galaxy/cluster kinematics), it can be easily marginalized 
out analytically to reduce the parameter space.  It can be shown that the 
remaining factor $\mathcal{D}(v_i,\sigma_i|\mathcal{P})$ in fact depends only 
on the \emph{differences} between the measured velocities, and thus carries 
information about binary variation that is independent of the barycenter 
velocity. However, for the sake of full generality, we will keep $v_{cm}$ as a 
model parameter in the following.

\section{Priors and First-Level Bayesian evidence}\label{sec:bayes}

For our priors in the parameters $\theta$, $\psi$ and $\eta_1$, we assume 
random orientations and a uniform prior in the time traversed since periastron 
(when $\eta=0$). This corresponds to priors $P(\theta) = 
\frac{1}{2}\sin\theta$, $P(\psi) = \frac{1}{2\pi}$, and $P(\eta_1) = 
(1-e\cos\eta_1)/2\pi$ over their respective ranges.  Alternatively, one could 
also transform parameters to get uniform priors; this is accomplished by 
transforming from $\theta$ to $x = \cos\theta$ and from $\eta_1$ to $\tau_1$ 
where $\tau_1$ is the time since periastron.

The form of the priors in the binary parameters may be taken from previous 
studies (typically from a census of binaries in the solar neighborhood, e.g.  
Duquennoy \& Mayor 1991).  More generally, the prior distributions can be 
parametrized such that the parameters will be constrained at the second level 
of inference, when we are studying the entire population of stars. Thus, in 
general we will write the priors as $P(\log 
T,q,e|\boldsymbol\alpha_T,\boldsymbol\alpha_q,\boldsymbol\alpha_e)$ and 
$P(v_{cm}|\boldsymbol\alpha_{v})$, where the $\boldsymbol\alpha$'s denote the 
sets of \emph{hyperparameters}, i.e.  the parameters in each prior that will be 
constrained in the next level of inference. For example, if we assume a 
log-normal distribution of periods, then the prior in $\log T$ takes the form 
of a Gaussian, so the set of hyperparameters $\boldsymbol\alpha_T$ would be the 
corresponding mean log-period $\mu_{\log T}$ and dispersion of periods 
$\sigma_{\log T}$. For the sake of compactness, we denote the set of all 
hyperparameters as $\boldsymbol{\alpha} \equiv 
[\boldsymbol\alpha_{v},\boldsymbol\alpha_T,\boldsymbol\alpha_q,\boldsymbol\alpha_e]$.

Using the above notation, according to Bayes' theorem we can now write

\begin{eqnarray}
\lefteqn{P_b(v_i;\sigma_i|\boldsymbol{\alpha}) 
P_b(\mathcal{P},v_{cm}|\boldsymbol{\alpha})} && \nonumber \\
& = & \mathcal{L}_b(v_i;\sigma_i|v_{cm},\mathcal{P}) P(\theta,\psi,\eta_1) P(v_{cm}|\boldsymbol\alpha_{v}) P(\log T,q,e|\boldsymbol\alpha_T,\boldsymbol\alpha_q,\boldsymbol\alpha_e).
\label{eq:bayes}
\end{eqnarray}

By integrating both sides over the model parameters $v_{cm},\mathcal{P}$, we 
arrive at the first-level Bayesian evidence $P_b(v_i;\sigma_i|\boldsymbol\alpha)$
(which will become part of the marginal likelihood for the second level of 
inference), which is the first factor on the left-hand side of 
eq.~\ref{eq:bayes} (the second factor being the first-level posterior).  This 
integration can be performed using nested sampling\footnote{In cases where the 
observed velocities are entirely consistent with being single stars, simple 
Monte Carlo integration would probably suffice since the posterior will not be 
strongly peaked; convergence can be evaluated using the standard error in the 
mean.}.  However, there is a serious obstacle at this stage: if we carry out 
the integration with specific values chosen for the hyperparameters 
$\boldsymbol\alpha$, then at the second level of inference, we would have to do 
the integral again every time these hyperparameters are varied, which is 
computationally far too expensive.  One way to solve this is to use importance 
sampling. We first do the integration assuming \emph{flat} priors in 
$v_{cm},\log T,q,e$:

\begin{eqnarray}
P_{b,f}(v_i;\sigma_i) & = & \int d\Omega_\mathcal{P}dv_{cm}\mathcal{L}(v_i;\sigma_i|v_{cm},\mathcal{P}) P(\theta,\psi,\eta_1) \nonumber \\
& = & \int d\Omega_\mathcal{P}dv_{cm}\mathcal{D}(v_i,\sigma_i|\mathcal{P})\frac{e^{-(v_{cm}-\langle v-V_b \rangle)^2/2\sigma_m^2}}{\sqrt{2\pi\sigma_m^2}}\frac{\sin\theta(1-e\cos\eta_1)}{8\pi^2}  \nonumber \\
\label{eq:pflat}
\end{eqnarray}
where $d\Omega_\mathcal{P}$ is a differential volume in the binary parameters 
(that is, $d\Omega_\mathcal{P} = d\theta d\psi d\eta_1 d(\log T)dqde$). The 
range of $\log T$ to be marginalized over should be wide enough to include all 
conceivable periods; we find that $\log T \in [-7,7.5]$ is sufficient to cover 
the full range of periods observed in the solar neighborhood and in very close 
binaries.

In addition to the first-level Bayesian evidence, the Monte Carlo integration 
produces a set of $N_p$ points with corresponding weights $w_i$ and values 
($\mathcal{P}_i,v_{cm,i}$) that sample the posterior 
$P_{b,f}(\mathcal{P},v_{cm})$, normalized such that $\sum w_i = 1$. To get the 
first-level evidence we are after, we then sum the weights but weight them 
further by the \emph{actual} priors in these parameters:

\begin{equation}
P_b(v_i;\sigma_i|\boldsymbol{\alpha}) = P_{b,f}(v_i;\sigma_i) \sum_{i=1}^{N_p} w_i P(v_{cm,i}|\boldsymbol\alpha_{v}) P(\log T_i,q_i,e_i|\boldsymbol\alpha_T,\boldsymbol\alpha_q,\boldsymbol\alpha_e).
\label{eq:importance_sampling}
\end{equation}

This summation can be done during the second level of inference every time the 
hyperparameters $\boldsymbol\alpha$ are varied. As long as the priors are not 
very strongly peaked and there are enough points to sample the posterior (which 
will require careful consideration), this should produce the correct 
first-level evidence to good accuracy.\footnote{Other sampling priors are 
possible that could make the sampling more efficient, for example a Jeffrey's 
prior. For the log-period, a good option would be a ``Milky Way'' prior, which 
is log-normal with parameters determined for Milky Way field binaries, which is 
as broad a distribution as one could expect. If we are choosing sampling priors 
that are not flat, then we must weight the points by the \emph{ratio} of the 
``true'' prior over the sampling prior (i.e. we must divide 
eq.~\ref{eq:importance_sampling} by the sampling prior).} Again, note that if 
we are only interested in binary properties, we can marginalize out $v_{cm}$ 
analytically beforehand, in which case the exponential factor is no longer 
present in eq.~\ref{eq:pflat}.

We will also need the first-level evidence under the assumption of 
non-binarity.  This is relatively simple to calculate, as the only parameter to 
be marginalized over is the star's true velocity $v_{cm}$:

\begin{equation}
P_{nb}(v_i;\sigma_i|\boldsymbol\alpha_v) = \int_{-\infty}^{\infty}\mathcal{D}(v_i,\sigma_i)\frac{e^{-(v_{cm}-\langle v \rangle)^2/2\sigma_m^2}}{\sqrt{2\pi\sigma_m^2}} P(v_{cm}|\boldsymbol\alpha_v)dv_{cm}
\end{equation}
Note that we have performed the same factorization as in eq.~\ref{eq:lfac}; the 
only difference is that the binary velocity terms $V_{b,i}$ are omitted in 
eqs.~\ref{eq:lfac}, \ref{eq:vmean}, and \ref{eq:dfac}. If we are only 
interested in binary properties, we can simply use a flat prior in $v_{cm}$ and 
marginalize it out, leaving only the $\mathcal{D}$ factor. In the case of star 
clusters or dwarf galaxies, the distribution of $v_{cm}$ is usually modeled as 
Gaussian, in which case the hyperparameters $\boldsymbol\alpha_v$ are the systemic velocity 
$\mu$ and dispersion $\sigma$; here, too, the integral can be done 
analytically, so no Monte Carlo integration is required.

Incidentally, we can use the first-level Bayesian evidences to infer the 
probability of each star being part of a binary system, given the data: this is 
simply given by the ratio of evidences, $P_b/(P_{nb} + P_b)$. This binary 
probability will of course depend on the hyperparameters $\boldsymbol\alpha$, 
which is inferred in the next level for the whole population; afterword the 
best-fit values for $\boldsymbol\alpha$ can be plugged in to infer a best-fit 
binary probability for each star.  Note that the binary probability is unlikely 
to be very low for any star, because a star that shows no discernable variation 
beyond the measurement error could simply be a long-period binary. On the other 
hand, a star with large variations will have a binary probability very close to 
1.

\section{Second level of inference: constraining binary population 
properties}\label{sec:bayes2}

After the first-level evidence is generated for a large sample of $N_s$ stars, 
we can now proceed to the second level of inference. Our likelihood for the 
entire population of stars can be written

\begin{equation}
\mathcal{L}(v_i;\sigma_i|B,\boldsymbol\alpha) = \prod_{j=1}^{N_s}\mathcal{L}_j(v_i;\sigma_i|B,\boldsymbol\alpha),
\end{equation}
where the marginal likelihood for the $j$'th star is built from the first-level 
Bayesian evidences we have generated for each star using the binary and 
non-binary models:
\begin{equation}
\mathcal{L}_j(v_i;\sigma_i|B,\boldsymbol\alpha) = (1-B)P_{nb,j}(v_i;\sigma_i|\boldsymbol\alpha_v) + BP_{b,j}(v_i;\sigma_i|\boldsymbol\alpha)
\end{equation}
where $B$ is the binary fraction. Now we can marginalize over the binary 
fraction $B$ and hyperparameters $\boldsymbol\alpha$ using a Monte Carlo sampler 
(nested sampling is probably best) to produce posterior distributions in each 
parameter. If the population is a star cluster or dwarf galaxy and membership 
is a concern, one can also extend the likelihood to include member vs.  
nonmember stars, possibly including metallicity and position as observables, as 
we did in Martinez et al. 2011.

The benefit of this method compared to what we did in previous papers (Martinez 
et al. 2011 and Minor et al. 2013) is twofold: first, we are no longer 
generating binary likelihoods by binning in velocities, and thus a large number 
of velocity measurements can be accommodated as long as the Monte Carlo sampler 
is able to find the peaked region(s) of parameter space (if a peak indeed 
exists for a given star).  Thus, for stars that obviously show large velocity 
variations, a large number of repeat measurements can be made and these can be 
used to better constrain its orbital parameters (period, eccentricity etc.).  
Second, how we parameterize the prior distributions in the binary orbital 
parameters (using the set of hyperparameters $[\boldsymbol\alpha_T, 
\boldsymbol\alpha_q,\boldsymbol\alpha_e]$) can be done during the second level 
of inference, using importance sampling. Thus, it is straightforward and 
feasible to constrain the distributions in binary parameters over a 
considerable range of periods provided there is sufficient data (e.g. many 
stars, good spectral resolution, large number of epochs with appropriate 
cadence).

In principle, other observables besides just the radial velocity of the primary 
star could be brought under the same umbrella. For example, binary spectrum 
fitting for main sequence stars can produce measurements of the mass ratio $q$ 
and velocity of the secondary star, if resolvable (see El-Badry et al. 2017).  
To incorporate these additional observables, the likelihood at the first level 
of inference can be modified. Another possibility would be to include proper 
motion measurements (in which case a few more model parameters must be 
marginalized over).  Individual stars could have a likelihood in proper 
motions, radial velocities, or both. A more ambitious program would be to 
incorporate visual binaries by defining a likelihood based on separation and 
common radial velocities and proper motions, which would provide a probe of 
long-period binaries.  Ultimately by including more observables, we can 
constrain binary properties over a wider range of periods and hence probe the 
distribution of binary properties with more completeness.

\end{document}
