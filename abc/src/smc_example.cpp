#include <iostream>
#include <iomanip>
#include <cmath>
#include <boost/math/special_functions/erf.hpp>
#include <algorithm>
#include "sgravar.hpp"
#include "functions.hpp"


class prior : public prior_utils::prior_base<prior>
{
public:
    template <typename T>
    void transform(std::unordered_map<std::string, double> &map, T &&vec)
    {
    	auto vec_it = vec.begin();
    	
        map["a1"] = 3.5 - (3.5 - 1.2) * std::sqrt((*vec_it++));
        map["a2"] = (10.0 - map["a1"]) * (*vec_it++) + map["a1"];

        map["b1"] = 0.6 - (0.6 - 0.001) * std::sqrt((*vec_it++));
        map["b2"] = (0.6 - map["b1"]) * (*vec_it++) + map["b1"];
        
        map["delta"] = 0.1;
        
        
        double sigma_noise = 0.2, mu_noise = 0.65;
        map["noise"] = M_SQRT2*sigma_noise*boost::math::erf_inv(2.0*(*vec_it++) - 1.0) + mu_noise;

        map["mu"] = 6.0 - (6.0 + 6.0) * std::sqrt((*vec_it++));
        map["sig"] = 4.0 - (4.0 - 0.001) * std::sqrt((*vec_it++));

	map["offset"] = 0.0;

        
        double sigma_k_vlt_noise = 0.08, mu_k_vlt_noise = 0.33;
        map["vlt_noise"] =  M_SQRT2*sigma_k_vlt_noise*boost::math::erf_inv(2.0*(*vec_it++) - 1.0) + mu_k_vlt_noise;
        
        map["vlt_mu"] = 6.0 - (6.0 + 6.0) * std::sqrt((*vec_it++));
        map["vlt_sig"] = 4.0 - (4.0 - 0.001) * std::sqrt((*vec_it++));
        
	map["vlt_offset"] = 0.0;

        double sigma_k_keck_noise = 0.08, mu_k_keck_noise = 0.16;
        map["keck_noise"] =  M_SQRT2*sigma_k_keck_noise*boost::math::erf_inv(2.0*(*vec_it++) - 1.0) + mu_k_keck_noise;
        
        map["keck_mu"] = map["vlt_mu"];
        map["keck_sig"] = map["vlt_sig"];

	map["keck_offset"] = 0.0;
    }
    
    std::string info() const
    {
        return "bla";
    }
};

int main(int argc, char *argv[])
{
    prior pr;
    
    int NThreads = 2;
    ABC::init_mpi(argc, argv);
    

    std::vector<std::string> files = {"data/SgrAFlux_epoch_1.dat", "data/SgrAFlux_epoch_2.dat", "data/SgrAFlux_epoch_3.dat", "data/SgrAFlux_epoch_4.dat", "data/SgrAFlux_epoch_5.dat", "data/SgrAFlux_epoch_6.dat", "data/SgrAFlux_epoch_7.dat", "data/SgrAFlux_epoch_8.dat"};
    

    std::vector<std::string> files_keck = {"data/snippets_keck/file_1.dat", "data/snippets_keck/file_3.dat", "data/snippets_keck/file_5.dat", "data/snippets_keck/file_6.dat", "data/snippets_keck/file_7.dat", "data/snippets_keck/file_8.dat", "data/snippets_keck/file_9.dat", "data/snippets_keck/file_10.dat", "data/snippets_keck/file_11.dat", "data/snippets_keck/file_12.dat", "data/snippets_keck/file_13.dat", "data/snippets_keck/file_14.dat", "data/snippets_keck/file_15.dat", "data/snippets_keck/file_16.dat", "data/snippets_keck/file_17.dat", "data/snippets_keck/file_18.dat", "data/snippets_keck/file_19.dat", "data/snippets_keck/file_20.dat", "data/snippets_keck/file_21.dat", "data/snippets_keck/file_22.dat", "data/snippets_keck/file_23.dat", "data/snippets_keck/file_24.dat", "data/snippets_keck/file_25.dat", "data/snippets_keck/file_26.dat", "data/snippets_keck/file_27.dat", "data/snippets_keck/file_28.dat", "data/snippets_keck/file_29.dat", "data/snippets_keck/file_30.dat", "data/snippets_keck/file_31.dat"};
    

    std::vector<std::string> files_vlt = {"data/snippets_vlt/file_1.dat", "data/snippets_vlt/file_2.dat", "data/snippets_vlt/file_3.dat", "data/snippets_vlt/file_4.dat", "data/snippets_vlt/file_5.dat", "data/snippets_vlt/file_6.dat", "data/snippets_vlt/file_7.dat", "data/snippets_vlt/file_8.dat", "data/snippets_vlt/file_9.dat", "data/snippets_vlt/file_10.dat", "data/snippets_vlt/file_11.dat", "data/snippets_vlt/file_12.dat", "data/snippets_vlt/file_13.dat", "data/snippets_vlt/file_14.dat", "data/snippets_vlt/file_15.dat", "data/snippets_vlt/file_16.dat", "data/snippets_vlt/file_17.dat", "data/snippets_vlt/file_18.dat", "data/snippets_vlt/file_19.dat", "data/snippets_vlt/file_20.dat", "data/snippets_vlt/file_21.dat", "data/snippets_vlt/file_22.dat", "data/snippets_vlt/file_23.dat", "data/snippets_vlt/file_24.dat", "data/snippets_vlt/file_25.dat", "data/snippets_vlt/file_26.dat", "data/snippets_vlt/file_27.dat", "data/snippets_vlt/file_28.dat", "data/snippets_vlt/file_29.dat", "data/snippets_vlt/file_30.dat", "data/snippets_vlt/file_31.dat", "data/snippets_vlt/file_32.dat", "data/snippets_vlt/file_33.dat", "data/snippets_vlt/file_34.dat", "data/snippets_vlt/file_35.dat", "data/snippets_vlt/file_36.dat", "data/snippets_vlt/file_37.dat", "data/snippets_vlt/file_38.dat", "data/snippets_vlt/file_39.dat", "data/snippets_vlt/file_40.dat", "data/snippets_vlt/file_41.dat", "data/snippets_vlt/file_42.dat", "data/snippets_vlt/file_43.dat", "data/snippets_vlt/file_44.dat", "data/snippets_vlt/file_45.dat", "data/snippets_vlt/file_46.dat", "data/snippets_vlt/file_47.dat", "data/snippets_vlt/file_48.dat", "data/snippets_vlt/file_49.dat", "data/snippets_vlt/file_50.dat", "data/snippets_vlt/file_51.dat", "data/snippets_vlt/file_52.dat", "data/snippets_vlt/file_53.dat", "data/snippets_vlt/file_54.dat", "data/snippets_vlt/file_55.dat", "data/snippets_vlt/file_56.dat", "data/snippets_vlt/file_57.dat", "data/snippets_vlt/file_58.dat", "data/snippets_vlt/file_59.dat", "data/snippets_vlt/file_60.dat", "data/snippets_vlt/file_61.dat", "data/snippets_vlt/file_62.dat", "data/snippets_vlt/file_63.dat", "data/snippets_vlt/file_64.dat", "data/snippets_vlt/file_65.dat", "data/snippets_vlt/file_66.dat", "data/snippets_vlt/file_67.dat", "data/snippets_vlt/file_68.dat", "data/snippets_vlt/file_69.dat", "data/snippets_vlt/file_70.dat", "data/snippets_vlt/file_71.dat", "data/snippets_vlt/file_72.dat", "data/snippets_vlt/file_73.dat", "data/snippets_vlt/file_74.dat", "data/snippets_vlt/file_75.dat", "data/snippets_vlt/file_76.dat", "data/snippets_vlt/file_77.dat", "data/snippets_vlt/file_78.dat", "data/snippets_vlt/file_79.dat", "data/snippets_vlt/file_80.dat", "data/snippets_vlt/file_81.dat", "data/snippets_vlt/file_82.dat", "data/snippets_vlt/file_83.dat", "data/snippets_vlt/file_84.dat", "data/snippets_vlt/file_85.dat", "data/snippets_vlt/file_86.dat", "data/snippets_vlt/file_87.dat", "data/snippets_vlt/file_88.dat", "data/snippets_vlt/file_89.dat", "data/snippets_vlt/file_90.dat", "data/snippets_vlt/file_91.dat", "data/snippets_vlt/file_92.dat", "data/snippets_vlt/file_93.dat", "data/snippets_vlt/file_94.dat"};
    
  //nreal must be larger than NThreads!!!
    std::unordered_map<std::string, double> options_map_spitzer = {{"n_real", 25}, {"samp", 0.1}, {"lag_bin_size", 0.1}, {"log_shift", 0.35}, {"lag_max", 170.0}, {"lag_max_cutoff", 700.0}};
    std::unordered_map<std::string, double> options_map_vlt = {{"n_real", 25}, {"samp", 0.1}, {"lag_bin_size", 0.1}, {"log_shift", 1.7}, {"lag_max", 50.0}, {"lag_max_cutoff", 1000.0}};
    std::unordered_map<std::string, double> options_map_keck = {{"n_real", 25}, {"samp", 0.1}, {"lag_bin_size", 0.12}, {"log_shift", 2.2}, {"lag_max", 10.5}, {"lag_max_cutoff", 40.0}};

    sgravar::SgrAVar <Spec::smooth_pwr, Conv::log_norm, Noise::normal> spitzer(options_map_spitzer, files, "");
    sgravar::SgrAVar <Spec::smooth_pwr, Conv::log_norm, Noise::normal> vlt(options_map_vlt, files_vlt, "vlt_");
    sgravar::SgrAVar <Spec::smooth_pwr, Conv::log_norm, Noise::normal> keck(options_map_keck, files_keck, "keck_");
    sgravar::SyncData <Spec::smooth_pwr, Conv::log_norm, Conv::log_norm, Noise::normal, Noise::normal> sync_data(options_map_keck, files_keck, "keck_", "");
    //sgravar::ABC_Ran(band_data(&vlt), 10000, NThreads, pr, "run_10000_prior_s_6/run_six_nights");
    //sgravar::ABC_MPI(band_data(&spitzer, &vlt, &keck), 0.000055, 10000, 20000, pr, "run_800_prior_s_6/run_six_nights", true);
    sgravar::ABC(band_data(&spitzer, &vlt, &keck, &sync_data), 0.000055,100,100, NThreads, pr, "test_run", sgravar::SMC);
    
    //nreal must be larger than NThreads!!!
//
    //std::unordered_map<std::string, double> input_map =
      //{{"delta", 0.01},{"a1", 2.2}, {"a2", 3.8}, {"b1", 0.0038}, {"b2", 0.06},
       //{"cutoff", -3.57}, {"slope", 4.3}, {"fac", 0.65}, {"offset", -3.57},
       //{"noise", 0.68}, {"upper_cut", 3000.}};
      //spitzer.CalcFlux(NThreads, input_map);
      //spitzer.PrintStruct("run_10000_prior_s_6/test_struct.dat");
//      
      //std::unordered_map<std::string, double> input_map_vlt =
      //{{"delta", 0.01},{"a1", 2.2}, {"a2", 3.8}, {"b1", 0.0038}, {"b2", 0.06},
       //{"k_vlt_cutoff", -3.57}, {"k_vlt_slope", 4.22}, {"k_vlt_fac", 1.000}, {"k_vlt_offset", -3.57},
       //{"k_vlt_noise", 0.1}, {"k_vlt_upper_cut", 3000.}};
      //vlt.CalcFlux(NThreads, input_map_vlt);
       //vlt.PrintStruct("run_10000_prior_s_6/test_struct_k_vlt.dat");
//       
       //std::unordered_map<std::string, double> input_map_keck =
      //{{"delta", 0.01},{"a1", 2.2}, {"a2", 3.8}, {"b1", 0.0038}, {"b2", 0.06},
       //{"k_keck_cutoff", -3.57}, {"k_keck_slope", 4.22}, {"k_keck_fac", 1.000}, {"k_keck_offset", -3.57},
       //{"k_keck_noise", 0.1}, {"k_keck_upper_cut", 3000.}};
      //keck.CalcFlux(NThreads, input_map_keck);
       //keck.PrintStruct("run_10000_prior_s_6/test_struct_k_keck.dat");
     
    // spitzer.PrintFlux("run_100_test36_ixchel/flux.dat", false);
 //   vlt.PrintFlux("run_100_test36_ixchel/flux.dat", false);
    //spitzer.PrintCDF("test_CDF.dat");

    ABC::end_mpi();
}
