//#define CHECK
#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <algorithm>
#include "sgravar.hpp"
#include "functions.hpp"

using namespace std::chrono;

inline std::vector<std::string> make_files(const std::string &name, int N)
{
    std::vector<std::string> files;
    
    for (int i = 1; i <= N; i++)
    {
        std::stringstream ss;
        ss << i;
        files.push_back(name + "_" + ss.str() + ".dat");
    }
    
    return files;
}

class prior : public prior_utils::prior_base<prior>
{
public:
    template <typename T>
    void transform(std::unordered_map<std::string, double> &map, T &&vec)
    {
        auto vec_it = vec.begin();
        map["a0"] = 0.0 + 0.3*(*vec_it++);
        map["a1"] = 1.9 + (2.1 - 1.9)*(*vec_it++);
        map["a2"] = 3.4 + (4.2-3.4)*(*vec_it++);
        map["b1"] = 0.0015 + (0.0018 - 0.0015)*(*vec_it++);
        map["b2"] = 0.03 + (0.07 - 0.03)*(*vec_it++);
        map["delta"] = 0.1;
        //below are the one that are that will get an offset
        map["fac"] = 0.9 + (1.2 - 0.9)*(*vec_it++);
        map["offset"] = -3.5 + (-4.8 - -3.5)*(*vec_it++);
        map["noise"] = 0.3 + (0.9 - 0.3)*(*vec_it++);
        map["slope"] = 4.22;
        map["cutoff"] = -3.57;
        map["upper_cut"] = 1000.0;
        map["k_fac"] = map["fac"];
        map["k_offset"] = map["offset"];
        map["k_noise"] = map["noise"];
        map["k_slope"] = map["slope"];
        map["k_cutoff"] = map["cutoff"];
        map["k_upper_cut"] = map["upper_cut"];
        
    }
    
    std::string info() const
    {
        return "";
    }
};

int main(int argc, char *argv[])
{
    ABC::init_mpi(argc, argv);
    prior pr;

    int NThreads = 4;
    std::mt19937_64 engine;
    std::uniform_real_distribution<double> dist;

    //old inputs: nreal, samp, length, toff, lag_bin_size, lag_max, file
    steady_clock::time_point t1 = steady_clock::now();//, "SgrAFlux6n_min.dat"
    //old inputs: nreal, samp, length, toff, lag_bin_size, lag_max, file
    //sgravar::SgrAVar spitzer(20, 0.1, 1638.4*16.0, -3000, 0.1, 160.0, "SgrAFlux6n_min.dat", 1000);
    //sgravar::SgrAVar spitzer(20, 0.1, 0.1, 160.0, "SgrAFlux6n_min.dat", 1000);
    
    /*------->  new inputs: nreal, samp, lag_bin_size, lag_max, files, max_lag, step_size <--------*/
    //SgrAVar(int nreal, double samp, double lag_bin_size, double log_shift, double lag_max, const std::vector<std::string> &files, double lag_max_cutoff = -1, const std::string &prefix = "", int step = 1)
    
    std::unordered_map<std::string, double> options_map = {{"n_real", 20}, {"samp", 0.1}, {"lag_bin_size", 0.1}, {"log_shift", 0.35}, {"lag_max", 160.0}, {"lag_max_cutoff", 1000}};
    sgravar::SgrAVar<Spec::smooth_pwr, Conv::pwr_law, Noise::normal> spitzer(options_map, make_files("SgrAFlux_epoch", 6), "");
    //sgravar::SgrAVar<Spec::smooth_pwr, Conv::pwr_law, Noise::normal> spitzer(20, 0.1, 0.1, 0.35, 160.0, make_files("SgrAFlux_epoch", 6), 1000, "");
    //sgravar::SgrAVar<Spec::smooth_pwr, Conv::pwr_law, Noise::normal> spitzer2(20, 0.1, 0.1, 0.35, 160.0, make_files("SgrAFlux_epoch", 6), 1000, "k_");
                                         //|^ name of prefix
    steady_clock::time_point t2 = steady_clock::now();
    std::unordered_map<std::string, double> input_map = 
    {{"delta", 1}, {"a0", 0.0}, {"a1", 2.0}, {"a2", 3.8}, {"b1", 0.0017}, {"b2", 0.05}, 
     {"cutoff", -3.57}, {"slope", 4.22}, {"fac", 1.002}, {"offset", -3.57},
        {"noise", 0.0}, {"upper_cut", 3000.}};
    spitzer.CalcFlux(NThreads, input_map);
    steady_clock::time_point t3 = steady_clock::now();
    duration<double> time_span1 = duration_cast<duration<double>>(t2 - t1);
    duration<double> time_span2 = duration_cast<duration<double>>(t3 - t2);
    std::cout << "overhead: " << time_span1.count() << std::endl;
    std::cout << "time: " << time_span2.count() << std::endl;
    //spitzer.PrintFlux("test_flux.dat");
    //spitzer.PrintStruct("test_struct.dat");
    //spitzer.PrintCDF("test_CDF.dat");
    //sgravar::ABC(band_data(&spitzer), 0.1, 50, 100, NThreads, pr, "stuff", false);
    //sgravar::ABC_Data(band_data(&spitzer, &spitzer2), 10, NThreads, pr, "stuff");
    //sgravar::ABC_Ran(band_data(&spitzer), 10, NThreads, pr, "stuff");
    ABC::end_mpi();
}
