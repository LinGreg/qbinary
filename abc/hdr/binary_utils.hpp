#ifndef __SGRAVAR_UTILS__
#define __SGRAVAR_UTILS__

#include <cmath>
#include <vector>
#include <algorithm>
#include <tuple>
#include <utility>

namespace binary
{
    inline void find_moments(const std::vector<double> points, std::vector<double> &moments)
    {
        double temp, avg = 0.0;
        
        for (auto &&pt : points)
        {
            avg += pt;
        }
        avg /= points.size();
        
        for (auto &&m : moments) m = 0.0;
        
        for (auto &&pt : points)
        {
            temp = (pt - avg);
            for (int i = 1, end = moments.size(); i < end; i++)
            {
                temp *= (pt - avg);
                moments[i] += temp;
            }
        }
        
        for (auto &&m : moments)
            m /= points.size();
        
        moments[0] = avg;
        
        return;
    }
    
    template <typename mat>
    inline void find_var(mat &&pts, double &var, double &avg, int N)
    {
        avg = var = 0.0;
        
        for (std::vector<double> &vec : pts)
        {
            for (auto &&pt : vec)
            {
                avg += pt;
                var += pt*pt;
            }
        }
        
        avg /= double(N);
        var /= double(N);
        var -= avg*avg;
    }
    
    inline double find_median(const std::vector<double> &vec)
    {
        return (vec.size() % 2 == 0) ? vec[vec.size()/2 - 1] + vec[vec.size()/2] : vec[vec.size()/2];
    }
    
    inline std::vector<double> lc_cdf(std::vector<double> &lc)
    {
        int n = lc.size();
        std::vector<double> cf(n);
        
        std::sort (lc.begin(), lc.end());
        for (int i = 0; i < n; i++)
        {
            cf[i] = double(i)/double(n-1);//(n - i + 1)/n;
        }
        
        return cf;
    }

    inline void make_generators(std::vector<std::mt19937_64> &engines, const int &NThreads)
    {
        std::random_device rng;
        std::vector<unsigned int> random_data(NThreads);
        std::generate(random_data.begin(), random_data.end(), [&](){return rng();});
        random_data.push_back(time(0));
        random_data.push_back(ABC::get_rank());
        std::seed_seq seq(random_data.begin(), random_data.end());
        std::vector<std::uint32_t> seeds(NThreads);
        seq.generate(seeds.begin(), seeds.end());
        for (auto &&seed : seeds)
            engines.emplace_back(seed);
    }
    
    inline std::vector<std::uint32_t> make_seeds(const int &NThreads)
    {
        std::random_device rng;
        std::vector<unsigned int> random_data(NThreads);
        std::generate(random_data.begin(), random_data.end(), [&](){return rng();});
        random_data.push_back(time(0));
        random_data.push_back(ABC::get_rank());
        std::seed_seq seq(random_data.begin(), random_data.end());
        std::vector<std::uint32_t> seeds(NThreads);
        seq.generate(seeds.begin(), seeds.end());
        
        return seeds;
    }
    
    template<typename T>
    inline void EnterData(const std::string &file, const std::function<void (const std::vector<T> &)> &func)
    {
        std::ifstream in(file.c_str());
        if (!in.is_open())
        {
            std::cout << "Cannot open " << file << std::endl;
            exit(-1);
        }
        
        std::string line;
        while (std::getline(in, line))
        {
            line = line.substr(0, line.find_first_of("#"));
            std::stringstream ss(line);
            std::vector<T> values;
            T val;
            
            while(ss >> val)
            {
                values.push_back(val);
            }
            
            func(values);
        }
    }
    
    inline std::string add_prefix(const std::string &prefix, const std::string &file)
    {
        std::string::size_type i = file.find_last_of("/") + 1;

        return file.substr(0, i) + prefix + file.substr(i);
    }
    
    template <typename T>
    inline double get_map_value(const std::unordered_map<std::string, T> &map, 
                                const std::string &key, const T &def)
    {
        return (map.find(key) != map.end()) ? map.at(key) : def;
    }
    
    template <typename T>
    inline double get_map_value(const std::unordered_map<std::string, T> &map, 
                                const std::string &key)
    {
        return map.at(key);
    }
    
    template <typename T>
    constexpr T SQ(const T &a){return a*a;}

    template<typename... T>
    constexpr decltype(auto) band_data(T &&...s) {return make_tuple(std::forward<T>(s)...);}
    
    namespace details 
    {
        template<typename F>
        constexpr int dummy(F &&) {return 0;}
        template<typename F, typename U, typename... T>
        inline int dummy(F&& f, U &&b, T &&... a){f(b); return dummy(std::forward<F>(f), std::forward<T>(a)...);}
        
        template<typename F>
        constexpr int dummy_for(F &&, std::index_sequence<>) {return 0;}
        template<typename F, typename U, std::size_t i, std::size_t... I, typename... T>
        inline int dummy_for(F&& f, std::index_sequence<i, I...>, U &&b, T &&... a){f(b, i); return dummy_for(std::forward<F>(f), std::index_sequence<I...>{}, std::forward<T>(a)...);}
        
        constexpr int sum(){return 0;}
        template<typename U, typename... T>
        constexpr decltype(auto) sum(U&& b, T&&... a)
        {
            return b + sum(std::forward<T>(a)...);
        }
        
        template <class F, class Tuple, std::size_t... I>
        constexpr decltype(auto) combine_dummy(F &&f, Tuple &&t, std::index_sequence<I...>) 
        {
            return dummy(f, (std::get<I>(std::forward<Tuple>(t)))...);
        }
        
        template <class F, class Tuple, std::size_t... I>
        constexpr decltype(auto) combine_for(F &&f, Tuple &&t, std::index_sequence<I...>) 
        {
            return dummy_for(f, std::index_sequence<I...>{}, (std::get<I>(std::forward<Tuple>(t)))...);
        }
        
        template <class F, class Tuple, std::size_t... I>
        constexpr decltype(auto) combine_sum(F &&f, Tuple &&t, std::index_sequence<I...>) 
        {
            return sum(f(std::get<I>(std::forward<Tuple>(t)))...);
        }
    }
    
    template <class F, class Tuple>
    constexpr decltype(auto) apply_tuple(Tuple &&t, F &&f) 
    {
        return details::combine_dummy(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{});
    }
    
    template <class F, class Tuple>
    constexpr decltype(auto) for_tuple(Tuple &&t, F &&f) 
    {
        return details::combine_for(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{});
    }

    template <class F, class Tuple>
    constexpr decltype(auto) sum_tuple(Tuple &&t, F &&f) 
    {
        return details::combine_sum(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{});
    }
    
    // c++11 version below
    /*
    template <typename T>
    inline T SQ(const T &a){return a*a;}

    template<typename... T>
    inline auto band_data(T &&...s) -> decltype(make_tuple(std::forward<T>(s)...)) {return make_tuple(std::forward<T>(s)...);}
    
    namespace details 
    {
        template<typename F>
        inline int dummy(F &&) {return 0;}
        template<typename F, typename U, typename... T>
        inline int dummy(F&& f, U &&b, T &&... a){f(b); return dummy(std::forward<F>(f), std::forward<T>(a)...);}
        
        template<typename F>
        inline int dummy_for(F &&, std::index_sequence<>) {return 0;}
        template<typename F, typename U, std::size_t i, std::size_t... I, typename... T>
        inline int dummy_for(F&& f, std::index_sequence<i, I...>, U &&b, T &&... a){f(b, i); return dummy_for(std::forward<F>(f), std::index_sequence<I...>{}, std::forward<T>(a)...);}
        
        constexpr int sum(){return 0;}
        template<typename U, typename... T>
        inline auto sum(U&& b, T&&... a) -> decltype(b + sum(std::forward<T>(a)...))
        {
            return b + sum(std::forward<T>(a)...);
        }
        
        template <class F, class Tuple, std::size_t... I>
        inline auto combine_dummy(F &&f, Tuple &&t, std::index_sequence<I...>) 
        -> decltype(dummy(f, (std::get<I>(std::forward<Tuple>(t)))...))
        {
            return dummy(f, (std::get<I>(std::forward<Tuple>(t)))...);
        }
        
        template <class F, class Tuple, std::size_t... I>
        inline auto combine_for(F &&f, Tuple &&t, std::index_sequence<I...>) 
        -> decltype(dummy_for(f, std::index_sequence<I...>{}, (std::get<I>(std::forward<Tuple>(t)))...))
        {
            return dummy_for(f, std::index_sequence<I...>{}, (std::get<I>(std::forward<Tuple>(t)))...);
        }
        
        template <class F, class Tuple, std::size_t... I>
        inline auto combine_sum(F &&f, Tuple &&t, std::index_sequence<I...>) 
        -> decltype(sum(f(std::get<I>(std::forward<Tuple>(t)))...))
        {
            return sum(f(std::get<I>(std::forward<Tuple>(t)))...);
        }
    }
    
    template <class F, class Tuple>
    inline auto apply_tuple(Tuple &&t, F &&f) 
    -> decltype(details::combine_dummy(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{}))
    {
        return details::combine_dummy(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{});
    }
    
    template <class F, class Tuple>
    inline auto for_tuple(Tuple &&t, F &&f) 
    -> decltype(details::combine_for(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{}))
    {
        return details::combine_for(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{});
    }

    template <class F, class Tuple>
    inline auto sum_tuple(Tuple &&t, F &&f) 
    -> decltype(details::combine_sum(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{}))
    {
        return details::combine_sum(
            std::forward<F>(f), std::forward<Tuple>(t),
            std::make_index_sequence<std::tuple_size<std::decay_t<Tuple>>::value>{});
    }
    */
}

namespace prior_utils
{
    template <class T>
    class prior_base
    {
    private:
        int _size;
        std::vector<std::string> _params;
        
        class _int
        {
        private:
            _int *parent;
            int i;
            
        public:
            _int() : parent(0), i(0) {}
            _int(_int *parent) : parent(parent), i(0) {}
            _int begin(){return _int(this);}
            
            _int & operator ++()
            {
                inc();
                return *this;
            }
            
            _int & operator ++(int)
            {
                inc();
                return *this;
            }
            
            void inc()
            {
                i++;
                if (parent != 0)
                    parent->inc();
            }
            
            double operator *() const
            {
                return 0.5;
            }
            
            size_t size() const {return i;}
        };
        
    public:
        prior_base()
        {
            std::unordered_map<std::string, double> map;
            _int vec;
            static_cast<T&>(*this).transform(map, vec);
            
            _size = vec.size();
            
            for(auto &&m : map)
                _params.push_back(m.first);
            
            std::sort(_params.begin(), _params.end());
        }
        
        void operator()(std::unordered_map<std::string, double> &map, const std::vector<double> &vec)
        {
            static_cast<T&>(*this).transform(map, vec);
        }
        // number of free parameters
        int size() const {return _size;}
        std::vector<std::string> params() const {return _params;}
        std::vector<std::string> &params_ref() {return _params;}
    };
}

#endif
