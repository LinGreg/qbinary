#ifndef ABC_UTILS_HPP
#define ABC_UTILS_HPP

#include "file_utils.hpp"
#include "macros.hpp"

namespace ABC
{
    using namespace file_utils;
    using namespace abc_macros;
    
    constexpr double SQR(const double &x){return x*x;}
    
//     template<typename T> void mpi_type(){}
//     template<>() auto mpi_type<short int>(){return ;}
//     MPI_SHORT	short int
// MPI_INT	int
// MPI_LONG	long int
// MPI_LONG_LONG	long long int
// MPI_UNSIGNED_CHAR	unsigned char
// MPI_UNSIGNED_SHORT	unsigned short int
// MPI_UNSIGNED	unsigned int
// MPI_UNSIGNED_LONG	unsigned long int
// MPI_UNSIGNED_LONG_LONG	unsigned long long int
// MPI_FLOAT	float
// MPI_DOUBLE	double
// MPI_LONG_DOUBLE	long double
// MPI_BYTE	char
    
    void CalcCov(const std::vector<double> &weights, const std::vector<std::vector<double>> &points, std::vector<std::vector<double>> &cov, std::vector<double> &avg)
    {
        int N = cov.size();
        for (int i = 0; i < N; i++)
        {
            avg[i] = 0.0;
            for (int j = 0; j < N; j++)
            {
                cov[i][j] = 0.0;
            }
        }
        
        for (int k = 0, end = weights.size(); k < end; k++)
        {
            for (int i = 0; i < N; i++)
            {
                avg[i] += weights[k]*points[k][i];
                for (int j = 0; j < N; j++)
                {
                    cov[i][j] += weights[k]*points[k][i]*points[k][j];
                }
            }
        }
        
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
            {
                cov[i][j] = cov[i][j] - avg[i]*avg[j];
            }
        }
    }
    
    void CalcAvg(const std::vector<double> &weights, const std::vector<std::vector<double>> &points, std::vector<double> &avg)
    {
        int N = avg.size();
        for (int i = 0; i < N; i++)
        {
            avg[i] = 0.0;
        }
        
        for (int k = 0, end = weights.size(); k < end; k++)
        {
            for (int i = 0; i < N; i++)
            {
                avg[i] += weights[k]*points[k][i];
            }
        }
    }

    inline bool checkunit(const std::vector<double> &vec)
    {
        for (auto &&elem : vec)
            if (elem <= 0.0 || elem >= 1.0)
                return false;
        return true;
    }
    
    inline void write_temp_data(int K, int dim, int N, int int_total, std::vector<std::vector<double>> &distances, std::vector<double> &weights, std::vector<std::vector<double>> &points, const std::string &resume_file)
    {
        std::ofstream out(resume_file + ".temp", std::ofstream::out | std::ofstream::binary);
        
        if (!out.is_open())
            std::runtime_error("resume error: no temporary file to resume from.");
        
        int dist_dim = distances.size();
        out.write((char*)&dist_dim, sizeof(int));
        out.write((char*)&K, sizeof(int));
        out.write((char*)&dim, sizeof(int));
        out.write((char*)&N, sizeof(int));
        out.write((char*)&int_total, sizeof(int));
        
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < dist_dim; j++)
                out.write((char *)&distances[j][i], sizeof(double));
            out.write((char *)&weights[i], sizeof(double));
            out.write((char *)&points[i][0], dim*sizeof(double));
        }
        
        out.close();
    }
    
    inline void read_temp_data_old(int &K, int &dim, int &N, int &int_total, std::vector<std::vector<double>> &distances, std::vector<double> &weights, std::vector<std::vector<double>> &points, const std::string &resume_file)
    {
        std::ifstream out(resume_file + ".temp", std::ofstream::in | std::ifstream::binary);
        
        if (!out.is_open())
            std::runtime_error("resume error: no temporary file to resume from.");
        
        out.read((char*)&K, sizeof(int));
        out.read((char*)&dim, sizeof(int));
        out.read((char*)&N, sizeof(int));
        out.read((char*)&int_total, sizeof(int));
        
        weights.resize(N);
        distances.resize(1);
        distances[0].resize(N);
        points.resize(N);
        for (auto &&pt : points)
            pt.resize(dim);
        
        for (int i = 0; i < N; i++)
        {
            out.read((char *)&distances[0][i], sizeof(double));
            out.read((char *)&weights[i], sizeof(double));
            out.read((char *)&points[i][0], dim*sizeof(double));
        }
        
        out.close();
    }
    
    inline void read_temp_data(int &K, int &dim, int &N, int &int_total, std::vector<std::vector<double>> &distances, std::vector<double> &weights, std::vector<std::vector<double>> &points, const std::string &resume_file, bool find_dist_dim=false)
    {
        std::ifstream out(resume_file + ".temp", std::ofstream::in | std::ifstream::binary);
        
        if (!out.is_open())
            std::runtime_error("resume error: no temporary file to resume from.");
        
        int dist_dim;
        out.read((char*)&dist_dim, sizeof(int));
        if (dist_dim != int(distances.size()))
        {
            if (find_dist_dim)
            {
                distances.resize(dist_dim);
            }
            else
            {
                out.close();
                std::cout << "assuming the resume file is in the old format..." << std::endl;
                read_temp_data_old(K, dim, N, int_total, distances, weights, points, resume_file);
                return;
            }
        }
        
        out.read((char*)&K, sizeof(int));
        out.read((char*)&dim, sizeof(int));
        out.read((char*)&N, sizeof(int));
        out.read((char*)&int_total, sizeof(int));
        
        weights.resize(N);
        points.resize(N);
        for (auto &&pt : points)
            pt.resize(dim);
        for (auto &&dist : distances)
            dist.resize(N);
        
        for (int i = 0; i < N; i++)
        {
            
            for (int j = 0; j < dist_dim; j++)
                out.read((char *)&distances[j][i], sizeof(double));
            out.read((char *)&weights[i], sizeof(double));
            out.read((char *)&points[i][0], dim*sizeof(double));
        }
        
        out.close();
    }
    
    class status_bar
    {
    private:
        std::mutex mutex;
        int NN;
        
    public:
        status_bar(const int &NN) : NN(NN)
        {
            std::cout << "[\033[20C]\033[21D" << std::flush;
        }
        
        void operator()(const int &total)
        {
            mutex.lock();
            for (int l = 0, n_eq = int(20.0*(total+1)/double(NN)) - int(20.0*(total)/double(NN)); l < n_eq; l++) 
                std::cout << "=" << std::flush;
            mutex.unlock();
        }
    };
    
    template<typename T>
    std::pair<T, T> get_range(const std::vector<T> &vec)
    {
        std::pair<T, T> ret;
        
        if (vec.size() == 0)
            return {0.0, 0.0};
        
        ret.first = ret.second = vec[0];
        for (int i = 0, end = vec.size(); i < end; i++)
        {
            if (ret.first > vec[i])
                ret.first = vec[i];
            else if (ret.second < vec[i])
                ret.second = vec[i];
        }
        
        return ret;
    }

    class uniform_twalk
    {
        double alim;
        double alimt;
        double ratio;
        int dim;
        std::uniform_real_distribution<double> dist;
        double norm_low;
        double norm_hi;
        double norm_ratio;
        double normt_ratio;
        
    public:
        uniform_twalk(double alim, double alimt, double ratio, int dim) : alim(alim), alimt(alimt), ratio(ratio), dim(dim), dist(0.0,1.0)
        {
            norm_low = 1.0 - std::pow(alim, 0.5 - double(dim));
            norm_hi = std::sqrt(alim) - 1.0;
            norm_ratio = norm_low/(norm_low + (2.0*double(dim) - 1.0)*norm_hi);
            normt_ratio = (alimt-1.0)/(double(dim) + 2.0*alimt - 2.0);
        }
        
        template<typename G>
        void operator()(std::vector<double> &temp, std::vector<double> &x, std::vector<double> &x0, G &gen)
        {
            double Z;
            
            if (dist(gen) < ratio)
            {
                if (dist(gen) < norm_ratio)
                {
                    Z = std::pow(1.0 - norm_low*dist(gen), 2.0/(2.0*double(dim)-1.0));
                }
                else
                {
                    Z = SQR(norm_hi*dist(gen) + 1.0);
                }
            }
            else
            {
                if (dist(gen) < normt_ratio)
                {
                    Z = -std::pow(dist(gen), 1.0/(double(dim)+alimt-1.0));
                }
                else
                {
                    Z = -std::pow(dist(gen), 1.0/(1.0-alimt));
                }
            }
            
            for (int i = 0; i < dim; i++)
            {
                temp[i] = x0[i] + Z*(x[i] - x0[i]);
            }
        }
    };

}

#endif
