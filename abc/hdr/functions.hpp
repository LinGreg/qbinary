#ifndef __FUNCTIONS_HPP__
#define __FUNCTIONS_HPP__

#include <cmath>
#include <random>
#include <boost/math/special_functions/erf.hpp>

namespace Spec
{
    class break_pwr
    {
    private:
        double a0, a1, a2, nu_b, nu_b2;
        double nubexp, nub2exp;

    public:
        break_pwr(const std::unordered_map<std::string, double> &map) : a0(map.at("a0")), a1(map.at("a1")), a2(map.at("a2")), nu_b(map.at("b1")), nu_b2(map.at("b2"))
        {
            nubexp = std::pow(nu_b, a0-a1);
            nub2exp = std::pow(nu_b2, a2-a1);
        }
        
        double operator()(const double &freq_in)
        {
            if (freq_in < nu_b)
                return std::pow(freq_in, -a0)*nubexp;
            else if(freq_in < nu_b2)
                return std::pow(freq_in, -a1);
            else
                return std::pow(freq_in, -a2)*nub2exp;
        }
    };

    class smooth_pwr
    {
    private:
        double delta, a1, a2, nu_b, nu_b2;
        double nub22exp;

    public:
        smooth_pwr(const std::unordered_map<std::string, double> &map) : delta(map.at("delta")), a1(map.at("a1")), a2(map.at("a2")), nu_b(map.at("b1")), nu_b2(map.at("b2"))
        {
            nub22exp = std::pow(1.0 + std::pow(nu_b2/nu_b, a1/delta), -delta);
        }
        
        double operator()(const double &freq_in)
        {
            double ret = 0.0;
            if (freq_in < nu_b2)
                ret = std::pow((1.0 + std::pow(freq_in/nu_b, a1/delta)), -delta);
            else
                ret = std::pow(freq_in/nu_b2, -a2)*nub22exp;
            
            if (ret == 0.0)
            {
                std::runtime_error("Settings for the power spectrum are not set properly.");
            }
            
            return ret;
        }
    };
}

namespace Conv
{
    class pwr_law
    {
    private:
        double fac, pole, offset, index, upper_cut;
        double corr;
        double exp;
        
    public:
        pwr_law(const std::unordered_map<std::string, double> & map) : fac(map.at("fac")), pole(map.at("cutoff")), offset(map.at("offset")), index(map.at("slope")), upper_cut(map.at("upper_cut"))
        {
            corr = 1.0/(std::pow(upper_cut/(-pole),(index-1.0))-1.0);
            exp = 1.0/(1.0-index);
        }
        
        pwr_law(const std::unordered_map<std::string, double> & map, const std::string &prefix) : fac(map.at(prefix+"fac")), pole(map.at(prefix+"cutoff")), offset(map.at(prefix+"offset")), index(map.at(prefix+"slope")), upper_cut(map.at(prefix+"upper_cut"))
        {
            corr = 1.0/(std::pow(upper_cut/(-pole),(index-1.0))-1.0);
            exp = 1.0/(1.0-index);
        }
        
        double operator()(const double &u)
        {
            return fac*(-pole* std::pow((u+corr)/(1.0+corr), exp)) + offset;
        }
    };
    
    class log_norm
    {
    private:
        double mu, sig, offset;
        
    public:
        log_norm(const std::unordered_map<std::string, double> & map) : mu(map.at("mu")), sig(map.at("sig")), offset(map.at("offset"))
        {
        }
        
        log_norm(const std::unordered_map<std::string, double> & map, const std::string &prefix) : mu(map.at(prefix+"mu")), sig(map.at(prefix+"sig")), offset(map.at(prefix+"offset"))
        {
        }
        
        double operator()(const double &u)
        {
            return std::exp(sig * M_SQRT2* boost::math::erf_inv(2.0 * u - 1.0) + mu) + offset;
        }
    };
}

namespace Noise
{
    template <class T>
    class dev : public T 
    {
    public:
        dev(const std::unordered_map<std::string, double> &map) : T(0.0, map.at("noise")){}
        dev(const std::unordered_map<std::string, double> &map, const std::string &prefix) : T(0.0, map.at(prefix + "noise")){}
        
        template<class URNG>
        double operator()(URNG& g, const double &)
        {
            return static_cast<T&>(*this)(g);
        }
    };
    
    typedef dev<std::normal_distribution<double>> normal;
    typedef dev<std::cauchy_distribution<double>> cauchy;
    
    class normal_ls
    {
    private:
        double Amp;
        double phase;
        double freq;
        std::normal_distribution<double> normal;
        
    public:
        normal_ls(const std::unordered_map<std::string, double> &map) : Amp(map.at("Amp")), phase(map.at("phase")), freq(map.at("freq")), normal(0.0, map.at("noise")) {}
        normal_ls(const std::unordered_map<std::string, double> &map, const std::string &prefix) : Amp(map.at(prefix + "Amp")), phase(map.at(prefix + "phase")), freq(map.at(prefix + "freq")), normal(0.0, map.at(prefix + "noise")) {}
        
        template<class URNG>
        double operator()(URNG& g, const double &time)
        {
            return normal(g) + Amp*std::sin(2.0*M_PI*freq*(time - phase));
        }
    };
}

#endif
