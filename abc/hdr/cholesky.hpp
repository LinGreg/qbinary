//  GAMBIT: Global and Modular BSM Inference Tool
//  *********************************************
///  \file
///
///  declaration for scanner module
///
///  *********************************************
///
///  Authors (add name and date if you modify):
//
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date Feb 2014
///
///  *********************************************

#ifndef CHOLESKY_HPP
#define CHOLESKY_HPP

#include <vector>
#include <cmath>
#include <iostream>
#include <cassert>

class Cholesky
{
private:
    std::vector<std::vector<double>> el;
        
public:
    Cholesky(){}
    
    Cholesky(const int num) : el(num, std::vector<double>(num)) {}
    
    Cholesky(std::vector<std::vector<double>> &a) :  el(a.size(), std::vector<double>(a.size()))
    {
        assert(EnterMat(a));
    }
    
    Cholesky(const std::vector<double> &a) :  el(a.size(), std::vector<double>(a.size()))
    {
        assert(EnterMat(a));
    }
                
    bool EnterMat(const std::vector<std::vector<double>> &a)
    {
        assert(a.size() == el.size());
        el = a;
        return CalcMat();
    }
    
    bool EnterMat(const std::vector<double> &a)
    {
        assert(a.size() == el.size());
        
        for (int i = 0, end = a.size(); i < end; i++)
            for (int j = 0, endj = end-i; j < endj; j++)
                el[j][i+j] = el[i+j][j] = a[i];
            
        return CalcMat();
    }
        
    bool CalcMat()
    {
        int num = el.size();
        double sum = 0;
        int i, j, k;
        
        for (i = 0; i < num; i++)
        {
            for (j = i; j < num; j++)
            {
                for(sum = el[i][j], k = i - 1; k >= 0; k--)
                    sum -= el[i][k]*el[j][k];
                if(i == j)
                {
                    if(sum <= 0.0)
                    {
                        return false;
                    }
                    el[i][i] = std::sqrt(sum);
                }
                else
                    el[j][i] = sum/el[i][i];
            }
        }
        
        for (i = 0; i < num; i++)
            for (j = 0; j < i; j++)
                el[j][i] = 0.0;
                
        return true;
    }
        
    void ElMult (const std::vector<double> &y, std::vector<double> &b) const
    {
        int i, j;
        int num = el.size();
        
        for(i = 0; i < num; i++)
        {
            b[i] = 0.0;
            for (j = 0; j <= i; j++)
            {
                b[i] += el[i][j]*y[j];
            }
        }
        
        //y = b;
    }
    
    void ElMult (std::vector<double> &y) const
    {
        int i, j;
        int num = el.size();
        std::vector<double> b(num);
        
        for(i = 0; i < num; i++)
        {
            b[i] = 0.0;
            for (j = 0; j <= i; j++)
            {
                b[i] += el[i][j]*y[j];
            }
        }
        
        y = b;
    }
    
    std::vector<std::vector<double>> Inverse()
    {
        double sum;
        int num = el.size();
        std::vector<std::vector<double>> ainv(num, std::vector<double>(num));
        
        for (int i = 0; i < num; i++)
            for (int j = 0; j <= i; j++)
            {
                sum = (i == j ? 1.0 : 0.0);
                for (int k = i-1; k >= j; k--)
                    sum -= el[i][k]*ainv[j][k];
                ainv[j][i] = sum/el[i][i];
            }

        for (int i = num - 1; i >= 0; i--)
            for (int j = 0; j <= i; j++)
            {
                sum = (i < j ? 0.0 : ainv[j][i]);
                for (int k = i + 1; k < num; k++)
                    sum -= el[k][i]*ainv[j][k];
                ainv[i][j] = ainv[j][i] = sum/el[i][i];
            }
            
        return ainv;
    }
    
    std::vector<double> Inverse(int j)
    {
        double sum;
        int num = el.size();
        std::vector<double> ainv(num);
        
        for (int i = 0; i < num; i++)
        {
            sum = (i == j ? 1.0 : 0.0);
            for (int k = i-1; k >= j; k--)
                sum -= el[i][k]*ainv[k];
            ainv[i] = sum/el[i][i];
        }

        for (int i = num - 1; i >= 0; i--)
        {
            sum = (i < j ? 0.0 : ainv[i]);
            for (int k = i + 1; k < num; k++)
                sum -= el[k][i]*ainv[k];
            ainv[i] = sum/el[i][i];
        }
            
        return ainv;
    }
        
    double Square(const std::vector<double> &y, const std::vector<double> &y0)
    {
        int i, j, num = y.size();
        double sum;
        std::vector<double> x(num);
        
        for (i = 0; i < num; i++)
        {
            for (sum = (y[i]-y0[i]), j=0; j < i; j++)
                sum -= el[i][j]*x[j];
            
            x[i]=sum/el[i][i];
        }
        
        sum = 0.0;
        for (i = 0; i < num; i++)
            sum += x[i]*x[i];
        
        return sum;
    }
        
    double DetSqrt()
    {
        double temp = 1.0;
        int num = el.size();
        for (int i = 0; i < num; i++)
            temp *= el[i][i];
        return temp;
    }
};

#endif
