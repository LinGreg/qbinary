#include <cmath>
#include <boost/math/special_functions/erf.hpp>
#include "integrator.hpp"
#include "multinest.hpp"
#include "sgravar_utils.hpp"
#include "ez_thread.hpp"

namespace likelihood
{
    inline double normal_cdf_inv(const double &u)
    {
        double temp;
        try
        {
            temp = M_SQRT2*boost::math::erf_inv(2.0*u - 1.0);
        }
        catch(std::exception &e)
        {
            //std::cout << u << ": " << e.what() << std::endl;
            if (u < 0.5)
                temp = -1.0e100;
            else
                temp = 1.0e100;
        }
        
        return temp;
    }
    
    inline double normal_cdf(const double &u)
    {
        return (std::erf(u/M_SQRT2) + 1.0)/2.0;
    }
    
    constexpr double SQ(const double &x)
    {
        return x*x;
    }
    
    class normal_pwr_law
    {
    private:
        double fac, pole, offset, index, upper_cut;
        double corr;
        double exp;
        double noise;
        double lognorm;
        double norm;
        double hi, low;
        double hi_u, low_u;
        double cdf_0;
        double cdf_norm;
        
        double normal_cdf_inv_i(const double &u)
        {
            if (u <= low_u)
                return low;
            if (u >= hi_u)
                return hi;
            double temp = (u <= 1.0e-15) ? 0.0 : (((1.0-u) <= 1.0e-15) ? 1.0 : M_SQRT2*boost::math::erf_inv(2.0*u - 1.0));
            if (!std::isfinite(temp))
            {
                std::cout << "normal_cdf_inv_i: " << u << "   " << temp << std::endl;
                getchar();
            }
            return temp;
        }
        
    public:
        normal_pwr_law(const std::unordered_map<std::string, double> & map) : fac(map.at("fac")), pole(map.at("cutoff")), offset(map.at("offset")), index(map.at("slope")), upper_cut(map.at("upper_cut")), noise(map.at("noise"))
        {
            corr = 1.0/(std::pow(upper_cut/(-pole),(index-1.0))-1.0);
            exp = 1.0/(1.0-index);
            
            double up = (1.0-index)*std::log(fac*upper_cut)/2.0;
            double low = (1.0-index)*std::log(-fac*pole)/2.0;
            lognorm = (up + low) + std::log(2.0*std::sinh(up-low)/(1.0-index));
            norm = std::exp(lognorm);
            cdf_norm = norm*(1.0-index);
            hi_u = normal_cdf(fac*upper_cut/noise);
            low_u = normal_cdf(-fac*pole/noise);
            hi = (fac*upper_cut/noise);
            low = (-fac*pole/noise);
            cdf_0 = std::pow(-fac*pole, 1.0-index);
        }
        
        double pwr_inv_cdf(const double &u)
        {
            //return fac*(-pole* std::pow((1.0-u+corr)/(1.0+corr), exp)) + offset;
            return std::pow(cdf_norm*u + cdf_0, exp) + offset;
        }
        
        double pwr_cdf(const double &x)
        {
            if (x <= -fac*pole+offset)
                return 0.0;
            else if(x >= fac*upper_cut+offset)
                return 1.0;
            else
                //return 1.0-((1.0+corr)*std::pow(-(x - offset)/fac/pole, 1.0/exp) - corr);
                return (std::pow(x-offset, 1.0-index) - cdf_0)/cdf_norm;
            //return std::pow(x-offset, 1.0-index)-pow(-fac*pole,1.0-index)
        }
        
        double log_pdf(const double &x)
        {
            static integrator::GaussLegendre inte(100);
            double low, hi, c;
            if (x < -pole*fac + offset)
            {
                c = SQ((-pole*fac + offset)/noise)/2.0;
            }
            else if (x > upper_cut*fac + offset)
            {
                c = SQ((upper_cut*fac + offset)/noise)/2.0;
            }
            else
            {
                c = 0.0;
            }
           
	    low = -3.0*noise+x;
            hi = 3.0*noise+x;
 
            if (-3.0*noise+x < -pole*fac + offset)
            {
                low = -pole*fac + offset;
            }
            
	    if (3.0*noise+x > upper_cut*fac + offset)
            {
                hi = upper_cut*fac + offset;
            }

            if (hi <= low)
                return -1.0e300;
              
            double temp = integrator::RombInt([&](const double &y){return std::exp(-SQ((x-pwr_inv_cdf(y))/noise)/2.0);}, pwr_cdf(low), pwr_cdf(hi), 1.0e-6);
            //double temp = inte([&](const double &y){return std::exp(c-SQ((x-pwr_inv_cdf(y))/noise)/2.0);}, pwr_cdf(low), pwr_cdf(hi));
            //double temp = integrator::RombInt([&](const double &y){return std::pow(x+noise*normal_cdf_inv(y)-offset, -index);}, normal_cdf((low-x)/noise), normal_cdf((hi-x)/noise));
            //double temp = inte([&](const double &y){return std::exp(-SQ((x-y)/noise)/2.0 + (1.0-index)*std::log(y-offset));}, low, hi);
	    //std::cout << temp << std::endl;
	    double temp2;
            if (temp <= 0.0)
                temp2 = -1.0e300;
            else
                temp2 = std::log(temp) - std::log(noise);
                //temp2 = std::log(temp) - lognorm - std::log(noise);

            if (!std::isfinite(temp2))
		{
                        temp = -1.0e100;
			std::cout << "temp = " << temp << " c =  " << c << " lognorm = " << lognorm << " temp2 = " << temp2 << " hi/low = " << hi << "    " << low  << std::endl;
			//getchar();
		}
             return temp2;
        }
        
        double cdf(const double &x)
        {
            static integrator::GaussLegendre inte(100);
            double low, hi;
            if (-4.0*noise+x < -pole*fac + offset)
            {
                low = -pole*fac + offset;
                hi = low + 8.0*noise;
            }
            else if (4.0*noise+x > upper_cut*fac + offset)
            {
                hi = upper_cut*fac + offset;
                low = hi - 8.0*offset;
            }
            else
            {
                low = -4.0*noise+x;
                hi = 4.0*noise+x;
            }
            
            //return integrator::RombInt([&](const double &y){return normal_cdf((x-pwr_inv_cdf(y))/noise);}, pwr_cdf(low), pwr_cdf(hi));
            return inte([&](const double &y){return normal_cdf((x-pwr_inv_cdf(y))/noise);}, pwr_cdf(low), pwr_cdf(hi));
        }
    };
    
    class fft_like
    {
    private:
        fftw_complex *data;
        fftw_complex *fft;
        fftw_plan plan;
        double samp;
        int nnn;
        
    public:
        fft_like(fft_like &&fft_in) : data(fft_in.data), fft(fft_in.fft), samp(fft_in.samp), nnn(fft_in.nnn)
        {
            data = fft = 0;
        }
        
        fft_like(double samp, const int N) : samp(samp)
        {
            nnn = N;//pow(2, int(std::log(double(times.size()))/std::log(2.0)) + 2);
            data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_BACKWARD, FFTW_ESTIMATE);
        }
        
        template<typename pow_type>
        double like(const std::vector<double> &vec, pow_type &&pow_func)
        {
            double spec, sig2 = 0.0, like = 0.0, norm = 0.0, diag = 0.0;
            int N = vec.size();
            for (int i = 0; i < N; i++)
            {
                data[i][0] = vec[i];
                data[i][1] = 0.0;
                diag += vec[i]*vec[i];
            }
            
            for (int i = N; i < nnn; i++)
            {
                data[i][0] = data[i][1] = 0.0;
            }
            
            fftw_execute(plan);
            
            for (int i = 1, n = nnn/2; i < n; i++)
            {
                spec = pow_func(double(i)/double(nnn*samp))/2.0;
                sig2 += 4.0*spec;
                like += (fft[i][0]*fft[i][0] + fft[i][1]*fft[i][1])/spec/nnn/nnn;
                norm += 2.0*std::log(spec);
            }
            
            spec = pow_func(double(nnn/2)/double(nnn*samp));
            sig2 += spec;
            like += fft[nnn/2][0]*fft[nnn/2][0]/spec/nnn/nnn;
            norm += std::log(spec);
            
            return sig2*like - diag + norm - (nnn-1.0)*std::log(sig2);
        }
        
        ~fft_like()
        {
            if (data != 0)
            {
                fftw_destroy_plan(plan);
                fftw_free(data);
                fftw_free(fft);
            }
        }
    };
    
    template<typename U>
    class sgra_like_base
    {
    protected:
        std::vector<std::vector<double>> restimes;
        std::vector<std::vector<double>> fluxes, even_fluxes;
        std::vector<double> samp;
        int NThread;
        

        
    public:
        sgra_like_base(const std::vector<std::string> &files, const int &NThread = 1) : restimes(files.size()), fluxes(files.size()), even_fluxes(files.size()), samp(files.size()), NThread(NThread)
        {
            for (int i = 0, end = files.size(); i < end; i++)
            {
                sgravar::EnterData(files[i], [&](const std::vector<double> &vec)
                {
                    if (vec.size() == 2)
                    {
                        restimes[i].push_back(vec[0]);
                        fluxes[i].push_back(vec[1]);
                        
                    }
                });
                
                if (restimes.size() % 2 == 1)
                {
                    restimes[i].pop_back();
                    fluxes[i].pop_back();
                }
                
                samp[i] = std::abs(restimes[i].back() - restimes[i].front())/double(restimes[i].size()-1);
                
                even_fluxes[i].resize(fluxes[i].size());
                spline::spline spl(restimes[i], fluxes[i]);
                for (int j = 0, endj = restimes[i].size(); j < endj; j++)
                {
                    even_fluxes[i][j] = spl.Splint(restimes[i].front() + samp[i]*j);
                }
            }
        }
         
        template<typename T>
        void multinest(T &&prior, const std::string &file_name = "chains")
        {
            multinest::multinest<U, T> nest(file_name, 0);
            nest(static_cast<U&>(*this), prior);
        }
    };
    
    template<typename PDF, typename pow_type>
    class sgra_like : public sgra_like_base<sgra_like<PDF, pow_type>>
    {
    public:
        
        sgra_like(const std::vector<std::string> &files, const int &NThread = 1) : sgra_like_base<sgra_like<PDF, pow_type>>(files, NThread) {}
        
        double operator() (const std::unordered_map<std::string, double> &map)
        {
            PDF pdf(map);
            double like = 0.0;
            pow_type pow_func(map);
            //std::cout << "here" << std::endl;
            for (int i = 0, end = this->restimes.size(); i < end; i++)
            {
                int N = this->restimes[i].size();
                std::vector<double> transformed_flux(N);
                std::vector<double> pdfs(N);
                
                ez_thread_do(thread_number, this->NThread, j, N)
                {
                    pdfs[j] = -2.0*pdf.log_pdf(this->fluxes[i][j]);
                    transformed_flux[j] = normal_cdf_inv(pdf.cdf(this->even_fluxes[i][j]));
                };
                
                for (auto &&p : pdfs)
                {
                    like += p;
                }
                
                fft_like fft(this->samp[i], N);
                double temp;
                temp = fft.like(transformed_flux, pow_func);
                //std::cout << temp << std::endl;
                like += temp;
            }
            //std::cout << "like = " << like << std::endl;
            return like;
        }
    };
    
    template<typename PDF, typename pow_type>
    class sgra_like_no_corr : public  sgra_like_base<sgra_like_no_corr<PDF, pow_type>>
    {
    public:
        
        sgra_like_no_corr(const std::vector<std::string> &files, const int &NThread = 1) : sgra_like_base<sgra_like_no_corr<PDF, pow_type>>(files, NThread) 
        {}
        
        double operator()(const std::unordered_map<std::string, double> &map)
        {
            PDF pdf(map);
            double like = 0.0;
            
            for (int i = 0, end = this->restimes.size(); i < end; i++)
            {
                int N = this->restimes[i].size();
                std::vector<double> pdfs(N);
                
                ez_thread_do(thread_number, this->NThread, j, N)
                {
                    pdfs[j] = -2.0*pdf.log_pdf(this->fluxes[i][j]);
                };

                for (auto &&p : pdfs)
                {
                    like += p;
                }
            }
            //std::cout << "like = " << like << std::endl;
            return like;
        }
    };
    
    template<typename PDF, typename pow_type>
    class sgra_like_corr : public sgra_like_base<sgra_like_corr<PDF, pow_type>>
    {
    public:
        
        sgra_like_corr(const std::vector<std::string> &files, const int &NThread = 1) : sgra_like_base<sgra_like_corr<PDF, pow_type>>(files, NThread) 
        {}
        
        double operator()(const std::unordered_map<std::string, double> &map)
        {
            PDF pdf(map);
            pow_type pow_func(map);
            double like = 0.0;
            
            for (int i = 0, end = this->restimes.size(); i < end; i++)
            {
                int N = this->restimes[i].size();
                std::vector<double> transformed_flux(N);
                
                ez_thread_do(thread_number, this->NThread, j, N)
                {
                    transformed_flux[j] = normal_cdf_inv(pdf.cdf(this->even_fluxes[i][j]));
                };
                
                fft_like fft(this->samp[i], N);
                double temp;
                temp = fft.like(transformed_flux, pow_func);
                std::cout << temp << std::endl;
                like += temp;
            }
            //std::cout << "like = " << like << std::endl;
            return like;
        }
    };
}
