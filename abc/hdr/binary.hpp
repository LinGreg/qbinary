#ifndef __SGRAVAR_HPP__
#define __SGRAVAR_HPP__

#include <fftw3.h>
#include <algorithm>
#include <cassert>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <fstream>
#include <cmath>
#include <sstream>
#include <map>
#include <ctime>
#include <iomanip>
#include <random>
#include <set>
#include <type_traits>
#include "spline.hpp"
#include "abc.hpp"
#include "sgravar_utils.hpp"
#include "structure_function.hpp"
#include "tk_draw.hpp"

namespace binary
{
    const unsigned char SEP_DIST = 0x01;
    const unsigned char RESUME = 0x02;
    const unsigned char SMC = 0x04;
    
    template <typename star_func>
    class Binary
    {
    private:
        std::vector<double> Masses;
        std::vector<double> names;
        std::vector<std::vector<double>> times;
        std::vector<std::vector<double>> velocities;
        std::vector<std::vector<double>> mock_velocities;
        std::vector<std::vector<double>> mock_moments;
        std::vector<std::vector<double>> moments;
        std::vector<std::vector<double>> errors;
        double data_avg;
        double data_cov
        double data_davg;
        double data_dcov;
        
    public:
        Binary(const std::unordered_map<std::string, double> &options_map, const std::vector<std::string> &files, const std::string &prefix = "")
        {
            double temp;
            
            int j = 0, i = -1, N;
            EnterData<std::string>(files[i], [&](const std::vector<std::string> &vec)
            {
                if (vec.size() == 3)
                {
                    if (std::stringstream(vec[0]) >> temp)
                    {
                        times[i][j] = temp;
                        std::stringstream(vec[1]) >> velocities[i][j];
                        std::stringstream(vec[2]) >> errors[i][j];
                        j++;
                    }
                    else
                    {
                        names.push_back(vec[0]);
                        std::stringstream(vec[1]) >> temp;
                        Masses.push_back(temp);
                        std::stringstream(vec[2]) >> N;
                        times.emplace_back(N);
                        velocites.emplace_back(N);
                        errors.emplace_back(N);
                        i++;
                        j = 0;
                    }
                }
                else
                {
                    std::err << "Input file not correct format!" << std::endl;
                }
            });
        }
        
        double Avg(int j)
        {
            double avg_diff = 0;
            //double std_diff = 0;

            for (auto &&sim_flux : simulated_sorted_fluxes[j])
            {
                avg_diff += sim_flux;
                /*for (auto &&flx : sim_flux[j])
                {
                    avg_diff += flx;
                }*/
            }
            //for (auto &&sim_flux : simulated_sorted_fluxes)
            //{
            //     for (auto &&flx : sim_flux[j])
            //    {
            //        std_diff += std::pow((flx - avg_diff/N_data),2.0);
            //    }    
                
            //}
            
            return std::abs((avg_diff/N_data - data_avg) / data_avg)/15.; // + std::abs((std::sqrt(std_diff/N_data) - 1.41115) / 1.41115)/50.;
        }
        
        double Median(int j)
        {
            double median = find_median(simulated_sorted_fluxes[j]);
            
            return std::abs((median - data_median)/data_median);
        }
        
        double Chi2(int j) const
        {
            double sum1 = 0.0;
            double sum2 = 0.0;
            int ii = 0;
            for (int i = 0, end = D.size(); i < end; i++)
                if (struct_func[i] < 128.0)
                {
                    ii++;
                    sum1 += SQ(std::log(D[i]/simulated_Ds[j][i]));
                }
                else
                	sum2 += SQ(std::log(D[i]/simulated_Ds[j][i]));
            
            return (sum1/(ii+3) + sum2/(ii+3)*3)*10.0;
        }

        template <typename G>
        void CalcMock(int flux_index, const std::unordered_map<std::string, double> &map, G &generator)
        {
            
        }
        
        template <typename G>
        void CalcStructFunc(int flux_index, const std::unordered_map<std::string, double> &map, G &)
        {
            simulated_Ds[flux_index] = struct_func(sim_lc[flux_index], flux_index);
        }
        
        template <typename G>
        void CalcFlux(int flux_index, const std::unordered_map<std::string, double> &map, G &generator)
        {
            CalcOnlyFlux(flux_index, map, generator);
            CalcStructFunc(flux_index, map, generator);
        }
        
        void CalcFlux(int NThreads, const std::unordered_map<std::string, double> &map)
        {
            assert(NThreads <= n_real);

            std::vector<std::thread> threads;
            //std::mutex mutex;
            std::vector<std::mt19937_64> engines;
            make_generators(engines, NThreads);
            
            for (int t = 0; t < NThreads; t++)
            {
                threads.emplace_back([&](int threadNum)
                                    {
                                        for (int i = threadNum; i < n_real; i += NThreads)
                                        {
                                            CalcFlux(i, map, engines[threadNum]);
                                        }
                                    }, t);
            }
            
            for (auto &&thr : threads)
            {
                thr.join();
            }
        }
    
        ~SgrAVar() 
        {
            fftw_cleanup();
        }
    };
    
    template <bool add_dist = true, typename P, typename... S>
    void ABC (const std::tuple<S*...> &sgra, double tol, int N, int M, int NThreads, P &&pr, const std::string &file = "run", const unsigned char &flag = 0x00)
    {
        apply_tuple (sgra, [&](auto &&s)
        {
            assert(NThreads <= s->NReal());
        });
        
        std::mutex mutex;
        std::vector<std::unordered_map<std::string, double>> maps(NThreads);
        std::vector<std::function<double(int i, const std::vector<double> &)>> distances;
        std::vector<std::mt19937_64> engines;
        make_generators(engines, NThreads);
        
        distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
        {
            std::unordered_map<std::string, double> &map = maps[i];
            pr(map, vec);
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcOnlyFlux(i, map, engines[i]);

		//return s->Avg(i);
                return 0.0;
            });
        });
        
//             distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
//             {
//                 return 0.0; //Peak(i);
//             });
        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            //std::unordered_map<std::string, double> &map = maps[i];
            //pr(map, vec);
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcStructFunc(i, maps[i], engines[i]);
                return s->Chi2(i);
            });
        });
        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            std::normal_distribution<double> dist;
            //std::normal_distribution<double> dist2;
            std::unordered_map<std::string, double> &map = maps[i];
            
            double ratio_data = 0.5*dist(engines[i]) + 12.4;
            //double offset_data = 0.08*dist2(engines[i]) - 1.72;
            //double flux_corr = (offset_data-map["offset"])/ratio_data + 0.149;
	    //if (flux_corr <= 0.0 )
	    //    flux_corr = 0.0000001;		
	    double flux_corr = 0.150;
	    double ratio = std::exp((std::log(flux_corr)-map["vlt_mu"]+std::log(9.63829))*map["sig"]/map["vlt_sig"] + map["mu"])/flux_corr;
	    double ans = (ratio-ratio_data)/0.5;
                                   
            return ans*ans/50.;
        });
        
        if(!bool(flag & SMC))
        {
            ABC::ABC(tol, N, M, pr.size(), NThreads, engines[0], mutex, distances,
            [&](int K, const std::vector<std::vector<double>> &distances, const std::vector<double> &weights, const std::vector<std::vector<double>> &points)
            {
                int N = distances[0].size();
                std::unordered_map<std::string, double> map;
                
                std::ofstream fout(file + ".particle");

                //fout << std::setw(16) << "distance" 
                fout << std::setw(16) << "weight";
                for (auto &&elem : pr.params_ref())
                    fout << std::setw(16) << elem;
                fout << std::endl;
                for (int i = 0; i < N; i++)
                {
                    pr(map, points[i]);
                    //fout << std::setw(16) << std::setprecision(10) << *(distances.end()-1)[i];
                    fout << std::setw(16) << std::setprecision(10) << weights[i];
                    for (auto && name : pr.params_ref())
                        fout << std::setw(16) << std::setprecision(10) << map[name];
                    fout << std::endl;
                }
                
                static int iter = 0;
                static std::ofstream out1(file + ".chi2_distances");
                out1 << std::setw(6) << iter;
                for (auto &&elem : distances[0])
                {
                    out1 << std::setw(16) << std::setprecision(10) << elem;
                }
                out1 << std::endl;
                
                static std::ofstream out2(file + ".avg_distances");
                out2 << std::setw(6) << iter;
                for (auto &&elem : distances[1])
                {
                    out2 << std::setw(16) << std::setprecision(10) << elem;
                }
                out2 << std::endl;
                
                static std::ofstream outt(file + ".distances");
                outt << std::setw(6) << iter;
                for (auto &&elem : *(distances.end()-1))
                {
                    outt << std::setw(16) << std::setprecision(10) << elem;
                }
                outt << std::endl;
                
                static std::ofstream outa(file + ".acceptance");
                if (K > 0)
                {
                    outa << std::setw(6) << iter;
                    outa << std::setw(16) << std::setprecision(10) << double(N)/double(K) << std::endl;
                }
                iter++;
            },
            (!bool(flag & SEP_DIST)) ? ABC::ADD_DIST : ABC::SEP_DIST, file, bool(flag & RESUME));
        }
        else
        {
            ABC::SMC_ABC(tol, N, M, pr.size(), NThreads, engines[0], mutex, distances,
            [&](double pacc, const std::vector<std::vector<double>> &distances, const std::vector<std::vector<double>> &points)
            {
                int N = distances[0].size();
                std::unordered_map<std::string, double> map;
                
                std::ofstream fout(file + ".particle");

                //fout << std::setw(16) << "distance" 
                fout << std::setw(16) << "weight";
                for (auto &&elem : pr.params_ref())
                    fout << std::setw(16) << elem;
                fout << std::endl;
                for (int i = 0; i < N; i++)
                {
                    pr(map, points[i]);
                    //fout << std::setw(16) << std::setprecision(10) << *(distances.end()-1)[i];
                    fout << std::setw(16) << std::setprecision(10) << 1.0;
                    for (auto && name : pr.params_ref())
                        fout << std::setw(16) << std::setprecision(10) << map[name];
                    fout << std::endl;
                }
                
                static int iter = 0;
                static std::ofstream out1(file + ".chi2_distances");
                out1 << std::setw(6) << iter;
                for (auto &&elem : distances[0])
                {
                    out1 << std::setw(16) << std::setprecision(10) << elem;
                }
                out1 << std::endl;
                
                static std::ofstream out2(file + ".avg_distances");
                out2 << std::setw(6) << iter;
                for (auto &&elem : distances[1])
                {
                    out2 << std::setw(16) << std::setprecision(10) << elem;
                }
                out2 << std::endl;
                
                static std::ofstream outt(file + ".distances");
                outt << std::setw(6) << iter;
                for (auto &&elem : *(distances.end()-1))
                {
                    outt << std::setw(16) << std::setprecision(10) << elem;
                }
                outt << std::endl;
                
                static std::ofstream outa(file + ".acceptance");
                {
                    outa << std::setw(6) << iter;
                    outa << std::setw(16) << std::setprecision(10) << pacc << std::endl;
                }
                iter++;
            },
            (!bool(flag & SEP_DIST)) ? ABC::ADD_DIST : ABC::SEP_DIST, file, bool(flag & RESUME));
        }
        
        std::ofstream out(file + ".info");
        
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "n_real:" << std::setprecision(10) << n_real << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "samp:" << std::setprecision(10) << samp << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "lag_bin_size:" << std::setprecision(10) << struct_func.LagBinSize() << std::endl;
        
        out << "\nPrior Info:\n\n" << pr.info() << std::endl;
        out.close();
    }
        
#ifdef USE_MPI

    template <bool add_dist = true, typename P, typename... S>
    void ABC_MPI (const std::tuple<S*...> &sgra, double tol, int N, int M, P &&pr, const std::string &file = "run", bool resume = false)
    {
        std::unordered_map<std::string, double> map;
        std::vector<std::function<double(int i, const std::vector<double> &)>> distances;
        std::vector<std::uint32_t> seeds;
        std::uint32_t seed;
        
        int rank, numtasks;
        MPI_Comm_size(MPI_COMM_WORLD,&numtasks); 
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        
        if (rank == 0)
            seeds = make_seeds(numtasks);
        
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Scatter (&seeds[0], 1, MPI_UNSIGNED, &seed, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
        
        std::mt19937_64 engine(seed);
        
        distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
        {
            pr(map, vec);
            
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcOnlyFlux(i, map, engine);
                //return s->Avg(i);
                return 0.0;
            });
        });
        
//             distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
//             {
//                 return 0.0; //Peak(i);
//             });

        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            //std::unordered_map<std::string, double> &map = maps[i];
            //pr(map, vec);
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcStructFunc(i, map, engine);
                return s->Chi2(i);
            });
        });
       
	distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
        std::normal_distribution<double> dist;
        //std::unordered_map<std::string, double> &map = maps[i];

        double ratio_data = 0.5*dist(engine) + 12.4;
	double flux_corr = 0.150;
	double ratio = std::exp((std::log(flux_corr)-map["vlt_mu"]+std::log(9.63829))*map["sig"]/map["vlt_sig"] + map["mu"])/flux_corr;
        double ans = (ratio-ratio_data)/0.5;

        return ans*ans/50.;
        });
 
        ABC::ABC_MPI(tol, N, M, pr.size(), engine, distances,
            [&](int K, const std::vector<std::vector<double>> &distances, const std::vector<double> &weights, const std::vector<std::vector<double>> &points)
            {
                int N = distances[0].size();
                std::unordered_map<std::string, double> map;
                
                std::ofstream fout(file + ".particle");

                //fout << std::setw(16) << "distance" 
                fout << std::setw(16) << "weight";
                for (auto &&elem : pr.params_ref())
                    fout << std::setw(16) << elem;
                fout << std::endl;
                for (int i = 0; i < N; i++)
                {
                    pr(map, points[i]);
                    //fout << std::setw(16) << std::setprecision(10) << *(distances.end()-1)[i];
                    fout << std::setw(16) << std::setprecision(10) << weights[i];
                    for (auto && name : pr.params_ref())
                        fout << std::setw(16) << std::setprecision(10) << map[name];
                    fout << std::endl;
                }
                
                static int iter = 0;
                static std::ofstream out1(file + ".chi2_distances");
                out1 << std::setw(6) << iter;
                for (auto &&elem : distances[0])
                {
                    out1 << std::setw(16) << std::setprecision(10) << elem;
                }
                out1 << std::endl;
                
                static std::ofstream out2(file + ".avg_distances");
                out2 << std::setw(6) << iter;
                for (auto &&elem : distances[1])
                {
                    out2 << std::setw(16) << std::setprecision(10) << elem;
                }
                out2 << std::endl;
                
                static std::ofstream outt(file + ".distances");
                outt << std::setw(6) << iter;
                for (auto &&elem : *(distances.end()-1))
                {
                    outt << std::setw(16) << std::setprecision(10) << elem;
                }
                outt << std::endl;
                
                static std::ofstream outa(file + ".acceptance");
                if (K > 0)
                {
                    outa << std::setw(6) << iter;
                    outa << std::setw(16) << std::setprecision(10) << double(N)/double(K) << std::endl;
                }
                iter++;
            },
            add_dist ? ABC::ADD_DIST : ABC::SEP_DIST, file, resume
        );
        
        std::ofstream out(file + ".info");
        
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "n_real:" << std::setprecision(10) << n_real << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "samp:" << std::setprecision(10) << samp << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "lag_bin_size:" << std::setprecision(10) << struct_func.LagBinSize() << std::endl;
        
        out << "\nPrior Info:\n\n" << pr.info() << std::endl;
        out.close();
    }

#endif
        
    template <bool add_dist = true, typename P, typename... S>
    void ABC_Data (const std::tuple<S*...> &sgra, int N, int NThreads, P &&pr, const std::string &file = "run", bool use_cov=true)
    {
        apply_tuple (sgra, [&](auto &&s)
        {
            assert(NThreads <= s->NReal());
        });
        
        std::mutex mutex;
        std::vector<std::unordered_map<std::string, double>> maps(NThreads);
        std::vector<std::function<double(int i, const std::vector<double> &)>> distances;
        std::vector<std::mt19937_64> engines;
        make_generators(engines, NThreads);
        
        distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
        {
            std::unordered_map<std::string, double> &map = maps[i];
            pr(map, vec);
            
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcOnlyFlux(i, map, engines[i]);
                //return s->Avg(i);
                return 0.0;
            });
        });
        
//             distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
//             {
//                 return 0.0; //Peak(i);
//             });
        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            //std::unordered_map<std::string, double> &map = maps[i];
            //pr(map, vec);

            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcStructFunc(i, maps[i], engines[i]);
                return s->Chi2(i);
            });
        });
        
        std::vector<std::vector<std::vector<std::vector<double>>>> saved_fluxes(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        for_tuple(sgra, [&](auto &s, int i)
        {
            saved_fluxes[i].resize(s->Size());
        });
        std::vector<std::vector<std::vector<double>>> saved_Ds(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        
        ABC::ABC_Data(N, NThreads, engines[0], mutex, distances,
            [&](int threadNum)
            {
                for_tuple(sgra, [&](auto &s, int ii)
                {
                    //for (int i = 0, end = restimes.size(); i < end; i++)
                    //    saved_fluxes[i].emplace_back(sim_lc[threadNum][i]);
                    //saved_Ds.emplace_back(simulated_Ds[threadNum]);
                    for (int i = 0, end = s->Size(); i < end; i++)
                        saved_fluxes[ii][i].emplace_back(s->Simulated_Fluxes(threadNum,i));
                    saved_Ds[ii].emplace_back(s->Simulated_Ds(threadNum));
                });
            },
            add_dist ? ABC::ADD_DIST : ABC::SEP_DIST, file, use_cov
        );
        
        for_tuple(sgra, [&](auto &s, int ii)
        {
            s->PrintFlux(saved_fluxes[ii], file);
            s->PrintStruct(saved_Ds[ii], file);
        });
        
        std::cout << "finished making files." << std::endl;
    }
    
    template <bool add_dist = true, typename P, typename... S>
    void ABC_Ran (const std::tuple<S*...> &sgra, int N, int NThreads, P &&pr, const std::string &file = "run")
    {
        apply_tuple (sgra, [&](auto &&s)
        {
            assert(NThreads <= s->NReal());
        });
        
        std::mutex mutex;
        std::vector<std::unordered_map<std::string, double>> maps(NThreads);
        std::vector<std::mt19937_64> engines;
        make_generators(engines, NThreads);
        
        std::vector<std::vector<std::vector<std::vector<double>>>> saved_fluxes(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        std::vector<std::vector<std::vector<std::vector<double>>>> saved_pwr_spec(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        for_tuple(sgra, [&](auto &s, int i)
        {
            saved_fluxes[i].resize(s->Size());
            saved_pwr_spec[i].resize(s->Size());
        });
        std::vector<std::vector<std::vector<double>>> saved_Ds(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        
        ABC::ABC_Ran(N, NThreads, engines[0], mutex,
            [&](int threadNum, const std::vector<double> &vec)
            {
                for_tuple(sgra, [&](auto &s, int ii)
                {
                    std::unordered_map<std::string, double> &map = maps[threadNum];
                    pr(map, vec);
                    s->CalcOnlyFlux(threadNum, map, engines[threadNum]);
                    s->CalcStructFunc(threadNum, map, engines[threadNum]);
                    
                    mutex.lock();
                    for (int i = 0, end = s->Size(); i < end; i++)
                    {
                        saved_fluxes[ii][i].emplace_back(s->Simulated_Fluxes(threadNum,i));
                        saved_pwr_spec[ii][i].emplace_back(s->GetPwrSpec(threadNum, i, map, engines[threadNum]));
                    }
                    saved_Ds[ii].emplace_back(s->Simulated_Ds(threadNum));
                    mutex.unlock();
                });
                
                return true;
            },
            file
        );
        
        for_tuple(sgra, [&](auto &s, int ii)
        {
            s->PrintFlux(saved_fluxes[ii], file);
            s->PrintStruct(saved_Ds[ii], file);
            s->PrintPwr(saved_pwr_spec[ii], file);
        });
        
        std::cout << "finished making files." << std::endl;
    }

}

#endif
