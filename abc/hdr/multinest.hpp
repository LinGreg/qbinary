#include "multinest.h"

namespace multinest
{
    template<typename like_type, typename prior_type>
    class multinest
    {
        private:
            
        int IS = 1;                      // do Nested Importance Sampling?
        int mmodal = 0;                  // do mode separation?
        int ceff = 0;                    // run in constant efficiency mode?
        int nlive = 1000;                // number of live points
        double efr = 0.8;                // set the required efficiency
        double tol = 0.5;                // tol, defines the stopping criteria
        int updInt = 1000;               // after how many iterations feedback is required & the output files should be updated
                                // note: posterior files are updated & dumper routine is called after every updInt*10 iterations
        
        double Ztol = -1E90;             // all the modes with logZ < Ztol are ignored
        int maxModes = 100;              // expected max no. of modes (used only for memory allocation)
        
        //char root[100] = "chains";     // root for output files
        std::string file_name;
        int seed = -1;                   // random no. generator seed, if < 0 then take the seed from system clock
        int fb = 1;                      // need feedback on standard output?
        int resume = 0;                  // resume from a previous job?
        int outfile = 1;                 // write output files?
        int initMPI = 1;                 // initialize MPI routines?, relevant only if compiling with MPI
                                // set it to F if you want your main program to handle MPI initialization
        double logZero = -1E90;          // points with loglike < logZero will be ignored by MultiNest
        int maxiter = 0;                 // max no. of iterations, a non-positive value means infinity. MultiNest will terminate if either it 
                                // has done max no. of iterations or convergence criterion (defined through tol) has been satisfied
        struct input_struct
        {
            int size;
            std::vector<std::string> params;
            like_type& like;
            prior_type& prior;
            
            input_struct(like_type &like, prior_type & prior) : size(prior.size()), params(prior.params()), like(like), prior(prior)
            {}
        };
        
        static void LogLike(double *Cube, int &ndim, int &npars, double &lnew, void *context)
        {
            static std::unordered_map<std::string, double> map;
            static input_struct &input = *(input_struct *)context;
            input.prior(map, std::vector<double>(Cube, Cube+ndim));
            for (int i = 0; i < npars; i++)
            {
                Cube[i] = map[input.params[i]];
            }
            
            lnew = -input.like(map)/2.0;
        }
        
        static void dumper(int &, int &, int &, double **, double **, double **, double &, double &, double &, double &, void *)
        {}
        
    public:
        multinest(const std::string &file_name = "chains", int resume = 1) : file_name(file_name), resume(resume) {}
        
        void operator()(like_type &like, prior_type & prior)
        {
            const input_struct input(like, prior);
            // set the MultiNest sampling parameters
            
            int ndims = input.size;            // dimensionality (no. of free parameters)
            int nPar = input.params.size();    // total no. of parameters including free & derived parameters
            int nClsPar = input.size;          // no. of parameters to do mode separation on
            int pWrap[ndims];                  // which parameters to have periodic boundary conditions?
            for(int i = 0; i < ndims; i++) pWrap[i] = 0;
            const char *root = file_name.c_str();
            
            void *context = (void *)&input;    // not required by MultiNest, any additional information user wants to pass
            
            // calling MultiNest

            nested::run(IS, mmodal, ceff, nlive, tol, efr, ndims, nPar, nClsPar, maxModes, updInt, Ztol, root, seed, pWrap, fb, resume, outfile, initMPI,
            logZero, maxiter, LogLike, dumper, context);
        }
    };
}
