#ifndef __SGRAVAR_OLD_UTILS__
#define __SGRAVAR_OLD_UTILS__

#include <cmath>
#include <random>
#include <vector>
#include <thread>
#include <cmath>
#include <algorithm>

namespace sgravar
{
    inline std::vector<double> exact_perfect_D(const std::vector<double> &vec, int N)
    {
        std::vector<double> ans(N, 0.0);
        for (int i = 0, end = N; i < end; i++)
        {
            for (int j = 0, endj = N-i; j < endj; j++)
            {
                ans[i] += (vec[j] - vec[i+j])*(vec[j] - vec[i+j]);
            }
        }
        
        return ans;
    }
    
    template <typename T>
    struct crs_matrix
    {
        int rowN, colN;
        std::vector<T> val;
        std::vector<int> row;
        std::vector<int> col;
      
        template <typename U> 
        crs_matrix(const std::vector<std::vector<U>> &mat)
        {
            rowN = mat.size();
            colN = mat[0].size();
            for (int i = 0; i < colN; i++)
            {
                row.push_back(val.size());
                for (int j = 0; j < rowN; j++)
                {
                    if (mat[i][j] != 0)
                    {
                        val.push_back(mat[i][j]);
                        col.push_back(j);
                    }
                }
            }
            row.push_back(val.size());
        }
        
        template <typename U>
        crs_matrix(const std::map<int, std::map<int, U>> &mat, int rowN, int colN) : rowN(rowN), colN(colN)
        {
            for (auto &&row_elem : mat)
            {
                row.push_back(val.size());
                for (auto &&elem : row_elem.second)
                {
                    val.push_back(elem.second);
                    col.push_back(elem.first);
                }
            }
            row.push_back(val.size());
        }
        
        template <typename U>
        crs_matrix(const std::vector<std::map<int, U>> &mat, int colN) : rowN(mat.size()), colN(colN)
        {
            for (auto &&row_elem : mat)
            {
                row.push_back(val.size());
                for (auto &&elem : row_elem)
                {
                    val.push_back(elem.second);
                    col.push_back(elem.first);
                }
            }
            row.push_back(val.size());
        }
    };
    
    namespace Details
    {
        template<typename T>
        inline void find_missing_index(std::vector<T> &ret, const std::vector<T> &vec, int start, int end)
        {
            if (start == end || vec[start]-start == vec[end]-end)
                return;
            
            int kHi = end, kLow = start, k;

            while (kHi-kLow > 1) 
            {
                k=(kHi+kLow) >> 1;

                if (vec[kLow]-kLow < vec[k]-k) 
                    kHi=k;
                else 
                    kLow=k;
            }
            
            for (int i = 1, end = vec[kHi]-vec[kLow]; i < end; i++)
                ret.push_back(vec[kLow]+i);
            find_missing_index(ret, vec, start, kLow);
            find_missing_index(ret, vec, kHi, end);
        }
    }

    template<typename T>
    inline std::vector<T> find_missing_index(const std::vector<T> &vec, int len)
    {
        std::vector<T> ret;
        for (int i = 0, end = vec.front(); i < end; i++)
        {
            ret.push_back(i);
        }
        
        Details::find_missing_index(ret, vec, 0, vec.size()-1);
        
        for (int i = vec.back(), end = len-1; i < end; i++)
        {
            ret.push_back(i);
        }
        
        return ret;
    }
    private:
        fftw_complex *data;
        fftw_complex *fft;
        fftw_plan plan;
        const std::vector<double> &times;
        std::vector<double> fvec;
        std::vector<double> sim_flux;
        double samp;
        int nnn;
        int toff;
        
    public:
        tk_draw(tk_draw &&in) : data(in.data), fft(in.fft), plan(in.plan), times(std::ref(in.times)), fvec(std::move(in.fvec)), sim_flux(std::move(in.sim_flux)), samp(in.samp), nnn(in.nnn), toff(in.toff)
        {
            in.data = 0;
            in.fft = 0;
        }
        
        tk_draw(const std::vector<double> &times, double samp = -1.0) : times(times), sim_flux(times.size()), samp(samp)
        {
            double actual_samp = (times.back() - times.front())/(times.size()-1);
            if (samp <= 0.0)
                samp = actual_samp;
            nnn = pow(2, int(std::log(double(times.size())*actual_samp/samp)/std::log(2.0)) + 2);
            toff = int((double(nnn) - times.size()*actual_samp/samp)/2.0 + 0.5);
            data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_FORWARD, FFTW_ESTIMATE);
            fvec.resize(nnn);
        }
        
        template<typename T, typename U, typename V, typename W>
        void calc(T&& generator, U&& pow_func, V&& conv_func, W&& noise)
        {
            int N = nnn/2;
            //int corr_len = 1;
            std::normal_distribution<double> normal;
            double sqrt2 = std::sqrt(2.0);
            double var = 0.0;
            //std::vector<double> corr(corr_len, 0.0);
            double spec;
            
            data[0][0] = data[0][1] = 0.0;
            for (int i = 1; i < N; i++)
            {
                spec = std::sqrt(pow_func(double(i)/double(nnn*samp)));
                data[i][0] = spec*normal(generator);
                data[i][1] = spec*normal(generator);
                data[nnn-i][0] = data[i][0];
                data[nnn-i][1] = -data[i][1];
                var += 4.0*spec*spec;
                //for (int j = 1; j < corr_len; j++)
                //    corr[j] += 4.0*spec*spec*std::cos(2.0*M_PI*i*j/double(nnn));
            }
            
            spec = std::sqrt(pow_func(double(N)/double(nnn*samp)));
            data[N][0] = spec*normal(generator);
            data[N][1] = 0.0;
            var += spec*spec;
            //for (int j = 1; j < corr_len; j++)
            //    corr[j] += spec*spec*std::pow(-1.0, j);
            
            fftw_execute(plan);
            
//             {
//                 for (auto &&c : corr)
//                 {
//                     c /= var;
//                 }
//                 
//                 corr[0] = 1.0;
//                 
//                 Cholesky chol(corr);
//                 auto fish = chol.Inverse(corr_len-1);
// 
//                 for (int i = 0; i < corr_len; i++)
//                 {
//                     double x = 0.0;
//                     int last = corr_len-1;
//                     for (int j = 0, k = i-corr_len; j < last; j++, k++)
//                         x += fish[j]*fft[(k + nnn)%nnn][0]/fish[last];
//                     
//                     fvec[i] = conv_func(0.5*(1.0 + std::erf((fft[i][0] + x)*std::sqrt(fish[last]/var/2.0))));
//                 }
//                 
//                 for (int i = corr_len; i < nnn; i++)
//                 {
//                     double x = 0.0;
//                     int last = corr_len-1;
//                     for (int j = 0, k = i-corr_len; j < last; j++, k++)
//                         x += fish[j]*fft[k][0]/fish[last];
//                     
//                     fvec[i] = conv_func(0.5*(1.0 + std::erf((fft[i][0] + x)*std::sqrt(fish[last]/var/2.0))));
//                 }
//             }
            
            var = std::sqrt(var);
            for (int i = 0; i < nnn; i++)
            {
                fvec[i] = conv_func(0.5*(1.0 + std::erf(fft[i][0]/var/sqrt2)));
            }
            
            for (int i = 0, end = times.size(); i < end; i++)
            {
                sim_flux[i] = operator()(times[i]);
            }
            
            for (auto &&elem : sim_flux)
                elem += noise(generator);
        }
        
        template<typename T>
        double add_power_real(std::vector<double> &file, T&& pow_func)
        {
            double var = 0.0;
            for (int i = 1; i < nnn; i++)
            {
                file[i] = pow_func(double(i)/double(nnn*samp));
                var += file[i];
            }
            
            return var;
        }
        
        double add_power(std::vector<double> &file)
        {
            //int N = nnn/2;
            fftw_complex* data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_complex* fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_plan plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_BACKWARD, FFTW_ESTIMATE);
            
            for (int i = 0; i < nnn; i++)
            {
                data[i][0] = fvec[i];
                data[i][1] = 0.0;
            }
            
            fftw_execute(plan);
            
            double var = 0.0;
            
            for (int i = 1; i < nnn; i++)
            {
                file[i] += fft[i][0]*fft[i][0] + fft[i][1]*fft[i][1];
                var += file[i];
            }
            
            fftw_destroy_plan(plan);
            fftw_free(data);
            fftw_free(fft);
            
            return var;
        }
        
        double operator()(double time) const
        {
            double x = time/samp + double(toff);
            return spline::LinearInterpGrid<double>(x, int(x), fvec);
        }
        
        double operator[](size_t i) const
        {
            return sim_flux[i];
        }
        
        operator std::vector<double> &()
        {
            return sim_flux;
        }
        
        int size() const {return nnn;}
        
        ~tk_draw()
        {
            if (data != 0)
            {
                fftw_destroy_plan(plan);
                fftw_free(data);
                fftw_free(fft);
            }
        }
    };
}

#endif
