#ifndef __ABC_HPP__
#define __ABC_HPP__

#ifdef USE_MPI
#include "mpi.h"
#endif

#include <random>
#include <functional>
#include <thread>
#include <vector>
#include <thread>
#include <mutex>
#include <algorithm>
#include "cholesky.hpp"
#include "abc_utils.hpp"

namespace ABC
{
// #ifdef USE_MPI  
//     inline void Build_mpi_pair_type(MPI_Datatype* mytype)
//     {
//         int array_of_blocklengths[2]={1,1};
//         MPI_Datatype array_of_types[2]={MPI_DOUBLE, MPI_INT};
//         int* index_i=NULL;
//         int* index_j=NULL;
//         std::pair<double,int> bla;
//         index_i=&(bla.first);
//         index_j=&(bla.second);
//         MPI_Aint i_addr, j_addr;
//         MPI_Aint array_of_displacements[2]={0};
//         MPI_Get_address(index_i,&i_addr);
//         MPI_Get_address(index_j,&j_addr);
//         array_of_displacements[1]=j_addr-i_addr;
//         MPI_Type_create_struct(2,array_of_blocklengths,array_of_displacements,array_of_types,mytype);
//         MPI_Type_commit(mytype);
//     }
// #endif
    const unsigned char ADD_DIST = 0x00;
    const unsigned char SEP_DIST = 0x01;
    
    inline void init_mpi(int argc, char *argv[])
    {
#ifdef USE_MPI
        MPI_Init(&argc,&argv);
#endif
    }
    
    inline void end_mpi()
    {
#ifdef USE_MPI
        MPI_Finalize();
#endif
    }
    
    inline int get_rank()
    {
        int rank = 0;
#ifdef USE_MPI
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
#endif
        return rank;
    }
    
    typedef std::function<void(double, const std::vector<std::vector<double>> &, const std::vector<std::vector<double>> &)> dumper_type;
    typedef std::vector<std::function<double(int i, const std::vector<double> &)>> distance_type;

    template<typename G>
    void SMC_ABC(double delta, int N, int M, int dim, int NThreads, G& generator, std::mutex &mutex, const distance_type &distance, dumper_type dumper, const unsigned char flag, const std::string &resume_file="", bool resume = false)
    {
        std::normal_distribution<double> normal;
        std::uniform_real_distribution<double> uniform;
        uniform_twalk twalk(2.4, 6.0, 0.5, dim);
        
        std::vector<std::vector<double>> S(distance.size() + 1, std::vector<double>(N));
        std::vector<std::vector<double>> points(N, std::vector<double>(dim));
        std::vector<int> dist_indices(N);
        std::vector<double> e(S.size());
        double pacc = 0.45;
        double c = 0.01;
        int total = 0, int_total=0;
        int Njumps = std::ceil(std::log(c)/std::log(1.0-pacc));
        int K = 0;
        int NN = N*0.45;
        int dN = N - NN;
        int sav_NN = NN;
        
#ifdef USE_MPI
        int rank, numtasks;
        MPI_Comm_size(MPI_COMM_WORLD,&numtasks); 
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        int start_M = int(double(M*rank)/double(numtasks));
        int end_M = int(double(M*(rank+1))/double(numtasks));
        int size_M = end_M - start_M;
        int start_N = NN + int(double(dN*rank)/double(numtasks));
        int end_N = NN + int(double(dN*(rank+1))/double(numtasks));
        int size_N = end_N - start_N;
        //MPI_Datatype DOUB_INT_PAIR;
        //Build_mpi_pair_type(&DOUB_INT_PAIR);
#else
        int start_M = 0, end_M = M, size_M = M;
        int start_N = NN, end_N = N, size_N = dN;
#endif
        
        for (int i = 0; i < N; i++)
            dist_indices[i] = i;
        
        if(resume)
        {
            int dim_temp = 0, N_temp = 0;
            read_data_file(resume_file + ".temp", dim_temp, N_temp, int_total, pacc);
            
            if(dim_temp != dim)
                std::runtime_error("resume error: input dimension must be the same as the previous run.");
            dim = dim_temp;
            
            if (N != N_temp)
            {
                std::vector<std::vector<double>> S_temp(distance.size() + 1, std::vector<double>(N_temp));
                std::vector<std::vector<double>> points_temp(N_temp, std::vector<double>(dim));
                std::vector<int> indices(N_temp);
        
                for (int i = 0; i < N_temp; i++)
                    indices[i] = i;
                
                read_data_file(resume_file + ".temp", dim_temp, N_temp, int_total, pacc, S_temp, points_temp);
                
                if (N < N_temp)
                {
                    std::sort(indices.begin(), indices.end(),
                            [&](const int &a, const int &b)
                            {
                                return S.back()[a] < S.back()[b];
                            });
                    
                    for (int i = 0; i < N; i++)
                    {
                        points[i] = points_temp[indices[i]];
                        for (int j = 0, end = S.size(); j < end; j++)
                        {
                            S[j][i] = S_temp[j][indices[i]];
                        }
                    }
                }
                else
                {
                    
                    for (int i = 0; i < N_temp; i++)
                    {
                        points[i] = points_temp[indices[i]];
                        for (int j = 0, end = S.size(); j < end; j++)
                        {
                            S[j][i] = S_temp[j][indices[i]];
                        }
                    }
                    
                    for (int i = N_temp; i < N; i++)
                    {
                        points[i] = points_temp[indices.back()];
                        for (int j = 0, end = S.size(); j < end; j++)
                        {
                            S[j][i] = S_temp[j][indices.back()];
                        }
                    }
                    
                    NN = N_temp - 1;
                    dN = N - NN;
#ifdef USE_MPI
                    start_N = NN + int(double(dN*rank)/double(numtasks));
                    end_N = NN + int(double(dN*(rank+1))/double(numtasks));
                    size_N = end_N - start_N;
#else
                    start_N = NN, end_N = N, size_N = dN;
#endif
                }
            }
            else
            {
                read_data_file(resume_file + ".temp", dim, N, int_total, pacc, S, points);
            }
            
            Njumps = std::ceil(std::log(c)/std::log(1.0-pacc));
        }
        else
        {
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                std::cout << "initalizing particle samples:  [\033[20C]\033[21D" << std::flush;
            }
            
            std::vector<std::pair<double, int>> init(M, std::pair<double, int>(0.0, 0));
            std::vector<std::vector<double>> S_temp(M, std::vector<double>(distance.size()));
            std::vector<std::vector<double>> pt_temp(M, std::vector<double>(dim));
            
            multi_thread(threadNum, NThreads)
            {
                std::vector<double> temp(dim);
                int i=start_M;
                for(;;)
                {
                    mutex.lock();
                    i = total + start_M;
                    if (total < size_M)
                    {
                        for (auto &&elem : temp)
                            elem = uniform(generator);
                        for (int l = 0, n_eq = int(20.0*double(total+1+start_M)/double(M)) - int(20.0*double(total+start_M)/double(M)); l < n_eq; l++) 
                            std::cout << "=" << std::flush;
                        total++;
                    }
                    mutex.unlock();
                    
                    if (i == end_M)
                        break;
                    
                    double dist = 0.0;
                    for (int j = 0, end = distance.size(); j < end; j++)
                    {
                        dist = distance[j](threadNum, temp);
                        S_temp[i][j] = dist;
                        init[i].first += dist;
                    }
                    
                    init[i].second = i;
                    pt_temp[i] = temp;
                }
            };
            
#ifdef USE_MPI
            for (int r = 0; r < numtasks; r++)
            {
                int start = int(double(M*r)/double(numtasks));
                int stop = int(double(M*(r+1))/double(numtasks));
                for (int i = start; i < stop; i++)
                {
                    MPI_Barrier(MPI_COMM_WORLD);
                    MPI_Bcast (&init[i].first, 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
                    MPI_Bcast (&init[i].second, 1, MPI_INT, r, MPI_COMM_WORLD);
                    MPI_Bcast (&S_temp[i][0], distance.size(), MPI_DOUBLE, r, MPI_COMM_WORLD);
                    MPI_Bcast (&pt_temp[i][0], dim, MPI_DOUBLE, r, MPI_COMM_WORLD);
                }
            }
#endif
            
            std::sort(init.begin(), init.end(),
                    [&](const std::pair<double, int> &a, const std::pair<double, int> &b)
                    {
                        return a.first < b.first;
                    });
            
            for (int i = 0; i < N; i++)
            {
                for (int j = 0, end = distance.size(); j < end; j++)
                    S[j][i] = S_temp[init[i].second][j];
                S[distance.size()][i] = init[i].first;
                points[i] = pt_temp[init[i].second];
            }
            
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                save_data_file(resume_file + ".temp", dim, N, int_total, pacc, S, points);
                dumper(pacc, S, points);
            }
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(S[i].size()*0.45)];
            }
            
            std::cout << "\ninitialization completed" << std::endl;
        }
        
        while (pacc > delta)
        {
            std::sort(dist_indices.begin(), dist_indices.end(),
                [&](const int &a, const int &b)
                {
                    return S.back()[a] < S.back()[b];
                });
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                e[i] = S.back()[dist_indices[NN]];
            }
            
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                for (int i = 0, end = distance.size(); i < end; i++)
                {
                    auto range = get_range(S[i]);
                    std::cout << "\033[1C\n    distance range (" << i << "): [" << range.first << ", " << range.second << "]" << std::endl;
                    std::cout << "    45%-th distance (" << i << "): " << e[i] << std::endl;
                }
                auto range = get_range(S.back());
                std::cout << "\033[1C\n    distance range (total): [" << range.first << ", " << range.second << "]" << std::endl;
                std::cout << "    45%-th distance (total): " << e[distance.size()] << std::endl;
                if (K > 0)
                    std::cout << "    tolerance (" << delta << "): " << pacc << std::endl;
                std::cout << "Number of MCMC jumps per point: " << Njumps << std::endl;
                
                std::cout << "\ncalculating particle samples (" << int_total << "):  [\033[20C]\033[21D" << std::flush;
            }
            
            int K_local = 0;
            
            total = 0;
            
            multi_thread(threadNum, NThreads)
            {
                std::vector<double> temp(dim, 0.0), next_pt(dim, 0.0);
                int i = 0;
                int node;
                
                do
                {
                    mutex.lock();
                    next_pt = temp = points[dist_indices[NN*uniform(generator)]];
                    mutex.unlock();
                    
                    std::vector<double> dists(distance.size()+1, 0.0);
                    
                    for (int j = 0; j < Njumps; j++)
                    {
                        mutex.lock();
                        node = NN*uniform(generator);
                        mutex.unlock();
                        
                        twalk(temp, next_pt, points[dist_indices[node]], generator);
                    
                        bool pass = true;
                    
                        if(!checkunit(temp))
                        {
                            pass = false;
                        }
                        else
                        {
                            for (int j = 0, end = distance.size(); j < end; j++)
                            {
                                dists[j] = distance[j](threadNum, temp);
                                dists[end] += dists[j];
                                
                                if (dists[end] > e[end])
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        
                        if (pass)
                        {
                            mutex.lock();
                            K_local++;
                            mutex.unlock();
                            next_pt = temp;
                        }
                    }
                    
                    mutex.lock();
                    i = total + start_N;
                    if (total < size_N)
                    {
                        for (int l = 0, n_eq = int(20.0*(total+1+start_N)/double(dN)) - int(20.0*(total+start_N)/double(dN)); l < n_eq; l++) 
                            std::cout << "=" << std::flush;
                        total++;
                    }
                    mutex.unlock();
                    
                    if (i < end_N)
                    {
                        for (int j = 0, end = S.size(); j < end; j++)
                            S[j][dist_indices[i]] = dists[j];
                        points[dist_indices[i]] = next_pt;
                    }
                }
                while(total < size_N);
            };
            
#ifdef USE_MPI
            for (int r = 0; r < numtasks; r++)
            {
                int start = NN + int(double(dN*r)/double(numtasks));
                int end = NN + int(double(dN*(r+1))/double(numtasks));
                for (int i = start; i < end; i++)
                {
                    MPI_Barrier(MPI_COMM_WORLD);
                    MPI_Bcast (&points[dist_indices[i]][0], dim, MPI_DOUBLE, r, MPI_COMM_WORLD);
                    for (int j = 0, end = S.size(); j < end; j++)
                        MPI_Bcast(&S[j][dist_indices[i]], 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
                }
            }

            MPI_Allreduce (&K_local, &K, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#else
            K = K_local;
#endif
            
            pacc = double(K)/(double(Njumps)*double(dN));
            Njumps = std::ceil(std::log(c)/std::log(1.0-pacc));
            int_total++;
            
            if (NN != sav_NN)
            {
                NN = sav_NN;
                dN = N - NN;
#ifdef USE_MPI
                start_N = NN + int(double(dN*rank)/double(numtasks));
                end_N = NN + int(double(dN*(rank+1))/double(numtasks));
                size_N = end_N - start_N;
#else
                start_N = NN, end_N = N, size_N = dN;
#endif
            }
            
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                save_data_file(resume_file + ".temp", dim, N, int_total, pacc, S, points);
                dumper(pacc, S, points);
            }
            
        }
        
        std::cout << "\nSMC-ABC finished" << std::endl;
    }

    template<typename G>
    void ABC(double delta, int N, int M, int dim, int NThreads, G& generator, std::mutex &mutex, const std::vector<std::function<double(int i, const std::vector<double> &)>> &distance, std::function<void(int, const std::vector<std::vector<double>> &, const std::vector<double> &, const std::vector<std::vector<double>> &)> dumper, const unsigned char flag, const std::string &resume_file="", bool resume = false)
    {
        std::normal_distribution<double> normal;
        std::uniform_real_distribution<double> uniform;
        Cholesky chol(dim);
        
        std::vector<std::vector<double>> S(distance.size() + 1, std::vector<double>(N));
        std::vector<double> weights(N, 1.0/N), weightsNew(N);
        std::vector<std::vector<double>> points(N, std::vector<double>(dim)), pointsNew(N, std::vector<double>(dim));
        std::vector<std::vector<double>> cov(dim, std::vector<double>(dim));
        std::vector<double> avg(dim);
        std::vector<double> e(S.size());
        int total = 0, int_total=0;
        int K = 0;
        
#ifdef USE_MPI
        int rank, numtasks;
        MPI_Comm_size(MPI_COMM_WORLD,&numtasks); 
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        int start_M = int(double(M*rank)/double(numtasks));
        int end_M = int(double(M*(rank+1))/double(numtasks));
        int size_M = end_M - start_M;
        int start_N = int(double(N*rank)/double(numtasks));
        int end_N = int(double(N*(rank+1))/double(numtasks));
        int size_N = end_N - start_N;
        //MPI_Datatype DOUB_INT_PAIR;
        //Build_mpi_pair_type(&DOUB_INT_PAIR);
#else
        int start_M = 0, end_M = M, size_M = M;
        int start_N = 0, end_N = N, size_N = N;
#endif
        
        if(resume)
        {
            int dim_temp = 0, N_temp = 0;
            read_temp_data(K, dim_temp, N_temp, int_total, S, weights, points, resume_file);
            if(dim_temp != dim)
                std::runtime_error("resume error: input dimension must be the same as the previous run.");
            dim = dim_temp;
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(S[i].size()*0.45)];
                if (N != N_temp)
                {
                    S[i].resize(N);
                }
            }
        }
        else
        {
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                std::cout << "initalizing particle samples:  [\033[20C]\033[21D" << std::flush;
            }
            
            std::vector<std::pair<double, int>> init(M, std::pair<double, int>(0.0, 0));
            std::vector<std::vector<double>> S_temp(M, std::vector<double>(distance.size()));
            std::vector<std::vector<double>> pt_temp(M, std::vector<double>(dim));
            std::vector<std::thread> threads;
            
            for (int t = 0; t < NThreads; t++)
            {
                threads.emplace_back([&](int threadNum)
                {
                    std::vector<double> temp(dim);
                    int i=start_M;
                    for(;;)
                    {
                        mutex.lock();
                        i = total + start_M;
                        if (total < size_M)
                        {
                            for (auto &&elem : temp)
                                elem = uniform(generator);
                            for (int l = 0, n_eq = int(20.0*double(total+1+start_M)/double(M)) - int(20.0*double(total+start_M)/double(M)); l < n_eq; l++) 
                                std::cout << "=" << std::flush;
                            total++;
                        }
                        mutex.unlock();
                        
                        if (i == end_M)
                            break;
                        
                        double dist = 0.0;
                        for (int j = 0, end = distance.size(); j < end; j++)
                        {
                            dist = distance[j](threadNum, temp);
                            S_temp[i][j] = dist;
                            init[i].first += dist;
                        }
                        
                        init[i].second = i;
                        pt_temp[i] = temp;
                    }
                }, t);
            }
            
            for (auto &&thr : threads)
                thr.join();
            
#ifdef USE_MPI
            for (int r = 0; r < numtasks; r++)
            {
                int start = int(double(M*r)/double(numtasks));
                int stop = int(double(M*(r+1))/double(numtasks));
                for (int i = start; i < stop; i++)
                {
                    MPI_Barrier(MPI_COMM_WORLD);
                    MPI_Bcast (&init[i].first, 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
                    MPI_Bcast (&init[i].second, 1, MPI_INT, r, MPI_COMM_WORLD);
                    MPI_Bcast (&S_temp[i][0], distance.size(), MPI_DOUBLE, r, MPI_COMM_WORLD);
                    MPI_Bcast (&pt_temp[i][0], dim, MPI_DOUBLE, r, MPI_COMM_WORLD);
                }
            }
#endif
            
            std::sort(init.begin(), init.end(),
                    [&](const std::pair<double, int> &a, const std::pair<double, int> &b)
                    {
                        return a.first < b.first;
                    });
            
            for (int i = 0; i < N; i++)
            {
                for (int j = 0, end = distance.size(); j < end; j++)
                    S[j][i] = S_temp[init[i].second][j];
                S[distance.size()][i] = init[i].first;
                points[i] = pt_temp[init[i].second];
            }
            
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                write_temp_data(K, dim, N, int_total, S, weights, points, resume_file);
                dumper(K, S, weights, points);
            }
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(S[i].size()*0.45)];
            }
            
            std::cout << "\ninitialize completed" << std::endl;
        }
        
        while (N > K*delta)
        {
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                for (int i = 0, end = distance.size(); i < end; i++)
                {
                    std::cout << "\033[1C\n    distance range (" << i << "): [" << S[i][0] << ", " << *(--S[i].end()) << "]" << std::endl;
                    std::cout << "    45%-th distance (" << i << "): " << e[i] << std::endl;
                }
                std::cout << "\033[1C\n    distance range (total): [" << S[distance.size()][0] << ", " << *(--S[distance.size()].end()) << "]" << std::endl;
                std::cout << "    45%-th distance (total): " << e[distance.size()] << std::endl;
                if (K > 0)
                    std::cout << "    tolerance (" << delta << "): " << double(N)/double(K) << std::endl;
                
                std::cout << "\ncalculating particle samples (" << int_total << "):  [\033[20C]\033[21D" << std::flush;
            }
            
            int K_local = 0;
            CalcCov(weights, points, cov, avg);
            if(!chol.EnterMat(cov))
            {
                std::cout << "WARNING: Covariance matrix is not positive definite.  Setting weights to be equal." << std::endl;
                for (auto &&elem : weights)
                {
                    elem = 1.0/weights.size();
                }
                
                CalcCov(weights, points, cov, avg);
                if (!chol.EnterMat(cov))
                {
                    std::cout << "Cannot calcuate positive definite matrix.  Shuting down..." << std::endl;
                    exit(-1);
                }
            }
            
            total = 0;
            std::vector<std::thread> threads;
            
            for (int t = 0; t < NThreads; t++)
            {
                threads.emplace_back([&](int threadNum)
                {
                    std::vector<double> temp(dim, 0.0);
                    double u;
                    int t0;
                    int i = 0;
                    
                    do
                    {
                        mutex.lock();
                        K_local++;
                        mutex.unlock();
                        
                        do
                        {
                            mutex.lock();
                            u = uniform(generator);
                            mutex.unlock();
                            int w_size = weights.size();
                            for (t0 = 0; t0 < w_size; t0++)
                            {
                                u -= weights[t0];
                                if (u < 0.0)
                                {
                                    break;
                                }
                            }
                            
                            mutex.lock();
                            for (auto &&elem : temp)
                            {
                                elem = normal(generator);
                            }
                            mutex.unlock();
                            
                            chol.ElMult(temp);
                            for (int j = 0; j < dim; j++)
                                temp[j] += points[t0][j];
                        }
                        while (!checkunit(temp));
                        
                        bool pass = true;
                        std::vector<double> dists(distance.size()+1, 0.0);
                        
                        if (flag & SEP_DIST)
                        {
                            for (int j = 0, end = distance.size(); j < end; j++)
                            {
                                dists[j] = distance[j](threadNum, temp);
                                dists[end] += dists[j];

                                if (dists[j] > e[j])
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int j = 0, end = distance.size(); j < end; j++)
                            {
                                dists[j] = distance[j](threadNum, temp);
                                dists[end] += dists[j];
                                
                                if (dists[end] > e[end])
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        
                        if (pass)
                        {
                            mutex.lock();
                            i = total + start_N;
                            if (total < size_N)
                            {
                                for (int l = 0, n_eq = int(20.0*(total+1+start_N)/double(N)) - int(20.0*(total+start_N)/double(N)); l < n_eq; l++) 
                                    std::cout << "=" << std::flush;
                                total++;
                            }
                            mutex.unlock();
                            
                            if (i < end_N)
                            {
                                for (int j = 0, end = S.size(); j < end; j++)
                                    S[j][i] = dists[j];
                                pointsNew[i] = temp;
                                double norm = 0.0;
                                for (int k = 0, end = points.size(); k < end; k++)
                                    norm += weights[k]*std::exp(-chol.Square(points[k], temp)/2.0)/chol.DetSqrt();
                                weightsNew[i] = 1.0/norm;
                            }
                        }
                    }
                    while(total < size_N);
                }, t);
            }
            
            for (auto &&thr : threads)
                thr.join();
            
#ifdef USE_MPI
            for (int r = 0; r < numtasks; r++)
            {
                int start = int(double(N*r)/double(numtasks));
                int end = int(double(N*(r+1))/double(numtasks));
                for (int i = start; i < end; i++)
                {
                    MPI_Barrier(MPI_COMM_WORLD);
                    MPI_Bcast (&pointsNew[i][0], dim, MPI_DOUBLE, r, MPI_COMM_WORLD);
                    MPI_Bcast (&weightsNew[i], 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
                    for (int j = 0, end = S.size(); j < end; j++)
                        MPI_Bcast(&S[j][i], 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
                }
            }

            MPI_Allreduce (&K_local, &K, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#else
            K = K_local;
#endif
            points = pointsNew;
            weights = weightsNew;
            
            double wtnorm = 0.0;
            for (auto &&wt : weights)
                wtnorm += wt;
        
            for (auto &&elem : weights)
                elem /= wtnorm;
            
            int_total++;
            
#ifdef USE_MPI
            if (rank == 0)
#endif
            {
                write_temp_data(K, dim, N, int_total, S, weights, points, resume_file);
                dumper(K, S, weights, points);
            }
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(S[i].size()*0.45)];
            }
        }
        
        std::cout << "\nABC finished" << std::endl;
    }
    
    
#ifdef USE_MPI
    template<typename G>
    void ABC_MPI(double delta, int N, int M, int dim, G& generator, const std::vector<std::function<double(int i, const std::vector<double> &)>> &distance, std::function<void(int, const std::vector<std::vector<double>> &, const std::vector<double> &, const std::vector<std::vector<double>> &)> dumper, const unsigned char flag, const std::string &resume_file="", bool resume = false)
    {
        std::normal_distribution<double> normal;
        std::uniform_real_distribution<double> uniform;
        Cholesky chol(dim);
        
        std::vector<std::vector<double>> S(distance.size() + 1, std::vector<double>(N));
        std::vector<double> weights(N, 1.0/N), weightsNew(N);
        std::vector<std::vector<double>> points(N, std::vector<double>(dim)), pointsNew(N, std::vector<double>(dim));
        std::vector<std::vector<double>> cov(dim, std::vector<double>(dim));
        std::vector<double> avg(dim);
        std::vector<double> e(S.size());
        int int_total=0;
        int K = 0;
        
        int rank, numtasks;
        MPI_Comm_size(MPI_COMM_WORLD,&numtasks); 
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        
        if(resume)
        {
            int dim_temp = 0, N_temp = 0;
            read_temp_data(K, dim_temp, N_temp, int_total, S, weights, points, resume_file);
            if(dim_temp != dim)
                std::runtime_error("resume error: input dimension must be the same as the previous run.");
            dim = dim_temp;
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(S[i].size()*0.45)];
                if (N != N_temp)
                {
                    S[i].resize(N);
                }
            }
        }
        else
        {
            std::vector<int> ranks(M); 
            std::vector<std::pair<double, int>> init(M, std::pair<double, int>(0.0, 0));
            std::vector<std::vector<double>> S_temp(M, std::vector<double>(distance.size()));
            std::vector<std::vector<double>> pt_temp(M, std::vector<double>(dim));
                
            if (rank == 0)
            {
                std::cout << "initializing particle samples:  [\033[20C]\033[21D" << std::flush;
            
                int incount = numtasks-1;
                std::vector<int> buf(incount);
                std::vector<MPI_Request> reqs(incount);
                std::vector<int> indices(incount);
                std::vector<MPI_Status> stats(incount);
                int outcount;
                int counter = 0, cont = 1;
                
                for (int r = 1; r < numtasks; r++)
                {
                    MPI_Irecv(&buf[r-1], 1, MPI_INT, r, r, MPI_COMM_WORLD, &reqs[r-1]);
                }
                
                do
                {
                    MPI_Waitsome(incount, &reqs[0], &outcount, &indices[0], &stats[0]);
                    for (int rr = 0; rr < outcount; rr++)
                    {
                        int r = indices[rr]+1;
                        MPI_Send(&counter, 1, MPI_INT, r, r+numtasks, MPI_COMM_WORLD);
                        MPI_Irecv(&buf[r-1], 1, MPI_INT, r, r, MPI_COMM_WORLD, &reqs[r-1]);

                        if (counter != M)
                        {
                            for (int l = 0, n_eq = int(20.0*double(counter+1)/double(M)) - int(20.0*double(counter)/double(M)); l < n_eq; l++) 
                                std::cout << "=" << std::flush;
                            ranks[counter++] = r;
                        }
                        else
                            cont++;
                    }
                }
                while(cont < numtasks);
                
                for (int r = 1; r < numtasks; r++)
                {
                    MPI_Cancel(&reqs[r-1]);
                }
            }
            else
            {
                std::vector<double> temp(dim);
                int i, iii = 0;
                MPI_Status stat;
                
                for(;;)
                {
                    MPI_Request req;
                    MPI_Isend(&iii, 1, MPI_INT, 0, rank, MPI_COMM_WORLD, &req);
                    MPI_Recv(&i, 1, MPI_INT, 0, rank + numtasks, MPI_COMM_WORLD, &stat);
                    
                    if (i == M)
                        break;
                    else
                        for (auto &&elem : temp)
                            elem = uniform(generator);
                    
                    double dist = 0.0;
                    for (int j = 0, end = distance.size(); j < end; j++)
                    {
                        dist = distance[j](0, temp);
                        S_temp[i][j] = dist;
                        init[i].first += dist;
                    }
                    
                    init[i].second = i;
                    pt_temp[i] = temp;
                }
            }
            
            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Bcast (&ranks[0], ranks.size(), MPI_INT, 0, MPI_COMM_WORLD);
            
            for (int i = 0; i < M; i++)
            {
                double r = ranks[i];
                MPI_Barrier(MPI_COMM_WORLD);
                MPI_Bcast (&init[i].first, 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
                MPI_Bcast (&init[i].second, 1, MPI_INT, r, MPI_COMM_WORLD);
                MPI_Bcast (&S_temp[i][0], distance.size(), MPI_DOUBLE, r, MPI_COMM_WORLD);
                MPI_Bcast (&pt_temp[i][0], dim, MPI_DOUBLE, r, MPI_COMM_WORLD);
            }
            
            std::sort(init.begin(), init.end(),
                    [&](const std::pair<double, int> &a, const std::pair<double, int> &b)
                    {
                        return a.first < b.first;
                    });
            
            for (int i = 0; i < N; i++)
            {
                for (int j = 0, end = distance.size(); j < end; j++)
                    S[j][i] = S_temp[init[i].second][j];
                S[distance.size()][i] = init[i].first;
                points[i] = pt_temp[init[i].second];
            }
            
            if (rank == 0)
            {
                write_temp_data(K, dim, N, int_total, S, weights, points, resume_file);
                dumper(K, S, weights, points);
            }
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(S[i].size()*0.45)];
            }
            
            std::cout << "\ninitialize completed" << std::endl;
        }
        
        while (N > K*delta)
        {
            if (rank == 0)
            {
                for (int i = 0, end = distance.size(); i < end; i++)
                {
                    std::cout << "\033[1C\n    distance range (" << i << "): [" << S[i][0] << ", " << *(--S[i].end()) << "]" << std::endl;
                    std::cout << "    45%-th distance (" << i << "): " << e[i] << std::endl;
                }
                std::cout << "\033[1C\n    distance range (total): [" << S[distance.size()][0] << ", " << *(--S[distance.size()].end()) << "]" << std::endl;
                std::cout << "    45%-th distance (total): " << e[distance.size()] << std::endl;
                if (K > 0)
                    std::cout << "    tolerance (" << delta << "): " << double(N)/double(K) << std::endl;
                
                std::cout << "\ncalculating particle samples (" << int_total << "):  [\033[20C]\033[21D" << std::flush;
            }
            
            int K_local = 0;
            CalcCov(weights, points, cov, avg);
            if(!chol.EnterMat(cov))
            {
                std::cout << "WARNING: Covariance matrix is not positive definite.  Setting weights to be equal." << std::endl;
                for (auto &&elem : weights)
                {
                    elem = 1.0/weights.size();
                }
                
                CalcCov(weights, points, cov, avg);
                if (!chol.EnterMat(cov))
                {
                    std::cout << "Cannot calcuate positive definite matrix.  Shuting down..." << std::endl;
                    exit(-1);
                }
            }
            
            std::vector<int> ranks(N);
            
            if(rank == 0)
            {
                int incount = numtasks-1;
                std::vector<int> buf(incount);
                std::vector<MPI_Request> reqs(incount);
                std::vector<int> indices(incount);
                std::vector<MPI_Status> stats(incount);
                int outcount;
                int counter = 0, cont = 1;
                
                for (int r = 1; r < numtasks; r++)
                {
                    MPI_Irecv(&buf[r-1], 1, MPI_INT, r, r, MPI_COMM_WORLD, &reqs[r-1]);
                }
                
                do
                {
                    MPI_Waitsome(incount, &reqs[0], &outcount, &indices[0], &stats[0]);
                    for (int rr = 0; rr < outcount; rr++)
                    {
                        int r = indices[rr]+1;
                        MPI_Send(&counter, 1, MPI_INT, r, r+numtasks, MPI_COMM_WORLD);
                        
                        if (buf[r-1])
                        {
                            if (counter != N)
                            {
                                for (int l = 0, n_eq = int(20.0*(counter+1)/double(N)) - int(20.0*(counter)/double(N)); l < n_eq; l++) 
                                    std::cout << "=" << std::flush;
                                ranks[counter++] = r;
                            }
                            else
                                cont++;
                        }
                        else
                        {
                            
                            if (counter == N)
                            {
                                cont++;
                            }
                        }
                        
                        MPI_Irecv(&buf[r-1], 1, MPI_INT, r, r, MPI_COMM_WORLD, &reqs[r-1]);
                    }
                }
                while(cont < numtasks);
                
                for (int r = 1; r < numtasks; r++)
                {
                    MPI_Cancel(&reqs[r-1]);
                }
            }
            else
            {
                std::vector<double> temp(dim, 0.0);
                double u;
                int t0;
                int i = 0;
                
                do
                {
                    K_local++;
                    
                    do
                    {
                        u = uniform(generator);
                        int w_size = weights.size();
                        for (t0 = 0; t0 < w_size; t0++)
                        {
                            u -= weights[t0];
                            if (u < 0.0)
                            {
                                break;
                            }
                        }
                        
                        for (auto &&elem : temp)
                        {
                            elem = normal(generator);
                        }
                        
                        chol.ElMult(temp);
                        for (int j = 0; j < dim; j++)
                            temp[j] += points[t0][j];
                    }
                    while (!checkunit(temp));
                    
                    bool pass = true;
                    std::vector<double> dists(distance.size()+1, 0.0);
                    
                    if (flag & SEP_DIST)
                    {
                        for (int j = 0, end = distance.size(); j < end; j++)
                        {
                            dists[j] = distance[j](0, temp);
                            dists[end] += dists[j];

                            if (dists[j] > e[j])
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        for (int j = 0, end = distance.size(); j < end; j++)
                        {
                            dists[j] = distance[j](0, temp);
                            dists[end] += dists[j];
                            
                            if (dists[end] > e[end])
                            {
                                pass = false;
                                break;
                            }
                        }
                    }
                    
                    int iii = pass ? 1 : 0;
                    MPI_Status stat;
                    MPI_Request req;
                    MPI_Isend(&iii, 1, MPI_INT, 0, rank, MPI_COMM_WORLD, &req);
                    MPI_Recv(&i, 1, MPI_INT, 0, rank + numtasks, MPI_COMM_WORLD, &stat);
                    
                    if (pass)
                    {
                        if (i < N)
                        {
                            for (int j = 0, end = S.size(); j < end; j++)
                                S[j][i] = dists[j];
                            pointsNew[i] = temp;
                            double norm = 0.0;
                            for (int k = 0, end = points.size(); k < end; k++)
                                norm += weights[k]*std::exp(-chol.Square(points[k], temp)/2.0)/chol.DetSqrt();
                            weightsNew[i] = 1.0/norm;
                        }
                    }
                }
                while(i < N);
            }
            
            MPI_Barrier(MPI_COMM_WORLD);
            MPI_Bcast (&ranks[0], ranks.size(), MPI_INT, 0, MPI_COMM_WORLD);
            
            for (int i = 0; i < N; i++)
            {
                double r = ranks[i];
                MPI_Barrier(MPI_COMM_WORLD);
                MPI_Bcast (&pointsNew[i][0], dim, MPI_DOUBLE, r, MPI_COMM_WORLD);
                MPI_Bcast (&weightsNew[i], 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
                for (int j = 0, end = S.size(); j < end; j++)
                    MPI_Bcast(&S[j][i], 1, MPI_DOUBLE, r, MPI_COMM_WORLD);
            }

            MPI_Allreduce (&K_local, &K, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
            
            points = pointsNew;
            weights = weightsNew;
            
            double wtnorm = 0.0;
            for (auto &&wt : weights)
                wtnorm += wt;
        
            for (auto &&elem : weights)
                elem /= wtnorm;
            
            int_total++;
            
            if (rank == 0)
            {
                write_temp_data(K, dim, N, int_total, S, weights, points, resume_file);
                dumper(K, S, weights, points);
            }
            
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(S[i].size()*0.45)];
            }
        }
        
        std::cout << "\nABC finished" << std::endl;
    }
#endif
    
    template<typename G>
    void ABC_Data(int NN, int NThreads, G& generator, std::mutex &mutex, const std::vector<std::function<double(int i, const std::vector<double> &)>> &distance, std::function<void(int)> dumper, const unsigned char flag, const std::string &resume_file="", bool use_cov = true)
    {
        int N, dim;
        std::normal_distribution<double> normal;
        std::uniform_real_distribution<double> uniform;
        std::vector<std::vector<double>> S(distance.size() + 1);
        std::vector<double> weights;
        std::vector<std::vector<double>> points;
        
        std::vector<double> e(S.size());
        int total = 0, int_total=0;
        int K = 0;
        
        read_temp_data(K, dim, N, int_total, S, weights, points, resume_file);
        
        std::vector<double> avg(dim);
        std::vector<std::vector<double>> cov(dim, std::vector<double>(dim));
        Cholesky chol(dim);
        
        if (use_cov)
        {
            CalcCov(weights, points, cov, avg);

            if(!chol.EnterMat(cov))
            {
                std::cout << "WARNING: Covariance matrix is not positive definite.  Setting weights to be equal." << std::endl;
                for (auto &&elem : weights)
                {
                    elem = 1.0/N;
                }
                
                CalcCov(weights, points, cov, avg);
                if (!chol.EnterMat(cov))
                {
                    std::cout << "Cannot calcuate positive definite matrix.  Shuting down..." << std::endl;
                    exit(-1);
                }
            }
        }
        else
        {
            CalcAvg(weights, points, avg);
        }
        
        {
            for (int i = 0, end = S.size(); i < end; i++)
            {
                std::sort(S[i].begin(), S[i].end());
                e[i] = S[i][int(N*0.45)];
            }
            
            for (int i = 0, end = distance.size(); i < end; i++)
            {
                std::cout << "\033[1C\n    distance range (" << i << "): [" << S[i][0] << ", " << S[i][N-1] << "]" << std::endl;
                std::cout << "    45%-th distance (" << i << "): " << e[i] << std::endl;
            }
            std::cout << "\033[1C\n    distance range (total): [" << S[distance.size()][0] << ", " << S[distance.size()][N-1] << "]" << std::endl;
            std::cout << "    45%-th distance (total): " << e[distance.size()] << std::endl;
            std::cout << "\ncalculating particle samples (" << int_total << "):  [\033[20C]\033[21D" << std::flush;
            
            total = 0;
            std::vector<std::thread> threads;
            
            for (int t = 0; t < NThreads; t++)
            {
                threads.emplace_back([&](int threadNum)
                {
                    double u;
                    double t0;
                    std::vector<double> temp = avg;
                    
                    do
                    {
                        if (use_cov)
                        {
                            do
                            {
                                mutex.lock();
                                K++;
                                u = uniform(generator);
                                mutex.unlock();
                                
                                for (t0 = 0; t0 < N; t0++)
                                {
                                    u -= weights[t0];
                                    if (u < 0.0)
                                    {
                                        break;
                                    }
                                }
                                
                                mutex.lock();
                                for (auto &&elem : temp)
                                {
                                    elem = normal(generator);
                                }
                                mutex.unlock();
                                chol.ElMult(temp);
                                for (int j = 0; j < dim; j++)
                                    temp[j] += points[t0][j];
                            }
                            while (!checkunit(temp));
                        }
                        
                        bool pass = true;
                        std::vector<double> dists(distance.size()+1, 0.0);
                        
                        if (flag & SEP_DIST)
                        {
                            for (int j = 0, end = distance.size(); j < end; j++)
                            {
                                dists[j] = distance[j](threadNum, temp);
                                dists[end] += dists[j];

                                if (dists[j] > e[j])
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            for (int j = 0, end = distance.size(); j < end; j++)
                            {
                                dists[j] = distance[j](threadNum, temp);
                                dists[end] += dists[j];
                                
                                if (dists[end] > e[end])
                                {
                                    pass = false;
                                    break;
                                }
                            }
                        }
                        
                        if (pass)
                        {
                            mutex.lock();
                            
                            if (total < NN)
                            {
                                dumper(threadNum);
                                for (int l = 0, n_eq = int(20.0*(total+1)/double(NN)) - int(20.0*(total)/double(NN)); l < n_eq; l++) 
                                    std::cout << "=" << std::flush;
                                total++;
                            }
                            mutex.unlock();
                        }
                    }
                    while(total < NN);
                }, t);
            }
            
            for (auto &&thr : threads)
                thr.join();
        }
        
        std::cout << "\nfinished" << std::endl;
    }
    
    template<typename G>
    void ABC_Ran(int NN, int NThreads, G& generator, std::mutex &mutex, const std::function<bool(int i, const std::vector<double> &)> &dumper, const std::string &resume_file="")
    {
        int N, dim;
        std::uniform_real_distribution<double> uniform;
        std::vector<std::vector<double>> S;
        std::vector<double> weights;
        std::vector<std::vector<double>> points;
        int total = 0, int_total=0;
        int K = 0;
        
        read_temp_data(K, dim, N, int_total, S, weights, points, resume_file, true);

        std::cout << "\ncalculating particle samples (" << int_total << "):  [\033[20C]\033[21D" << std::flush;
        
        total = 0;
        std::vector<std::thread> threads;
        
        for (int t = 0; t < NThreads; t++)
        {
            threads.emplace_back([&](int threadNum)
            {
                double u;
                double t0;
                int next;
                
                do
                {
                    mutex.lock();
                    u = uniform(generator);
                    next = ++total;
                    mutex.unlock();
                    
                    for (t0 = 0; t0 < N; t0++)
                    {
                        u -= weights[t0];
                        if (u < 0.0)
                        {
                            break;
                        }
                    }
                    
                    if (next <= NN)
                    {
                        if (dumper(threadNum, points[t0]))
                        {
                            mutex.lock();
                            for (int l = 0, n_eq = int(20.0*(next)/double(NN)) - int(20.0*(next-1)/double(NN)); l < n_eq; l++) 
                                std::cout << "=" << std::flush;
                            mutex.unlock();
                        }
                        else
                        {
                            mutex.lock();
                            total--;
                            mutex.unlock();
                        }
                    }
                    
                }
                while(total < NN);
            }, t);
        }
        
        for (auto &&thr : threads)
            thr.join();
        
        std::cout << "\nfinished" << std::endl;
    }
}

#endif
