#ifndef __STRUCTURE_FUNCTION_HPP__
#define __STRUCTURE_FUNCTION_HPP__

#include <cmath>
#include <vector>
#include <thread>
#include <algorithm>
#include <fftw3.h>
#include "sgravar_utils.hpp"

namespace sgravar
{
    struct D_info
    {
        std::vector<std::pair<int, int>> add;
        std::vector<std::pair<int, int>> sub;
        std::vector<int> ind;
    };
    
    class fft_perfect_D
    {
    private:
        fftw_complex *data;
        fftw_complex *fft;
        fftw_plan plan;
        fftw_plan plan2;
        std::vector<double> ans;
        int nnn;
        
    public:
        fft_perfect_D(int n) : ans(n) //,nnn(2*n)
        {
            nnn = pow(2, int(std::log(double(2*n-1))/std::log(2.0)) + 1);
            data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_FORWARD, FFTW_ESTIMATE);
            plan2 = fftw_plan_dft_1d(nnn, fft, data, FFTW_BACKWARD, FFTW_ESTIMATE);
        }
        
        fft_perfect_D(fft_perfect_D &&in) : data(in.data), fft(in.fft), plan(in.plan), plan2(in.plan2), ans(std::move(in.ans)), nnn(in.nnn)
        {
            in.data = 0;
            in.fft = 0;
        }
        
        size_t size() const {return ans.size();}
        
        void calc(const std::vector<double> &vec)
        {
            for (int i = 0, end = ans.size(); i < end; i++)
            {
                data[i][0] = vec[i];
                data[i][1] = 0.0;
            }
            
            for (int i = ans.size(); i < nnn; i++)
            {
                data[i][0] = 0.0;
                data[i][1] = 0.0;
            }

            fftw_execute(plan);
            
            for (int i = 0; i < nnn; i++)
            {
                fft[i][0] = fft[i][0]*fft[i][0] + fft[i][1]*fft[i][1];
                fft[i][1] = 0.0;
            }
            
            fftw_execute(plan2);
            
            double tot = 0.0;
            for (int i = ans.size()-1; i >= 0; i--)
            {
                ans[i] = tot + SQ(vec[ans.size() - i - 1]) + SQ(vec[i]);
                tot = ans[i];
            }
            
            for (int i = 0, end = ans.size(); i < end; i++)
            {
                ans[i] += -2.0*data[i][0]/nnn;
            }
        }
        
        double operator[](const int &i) const {return ans[i];}
            
        ~fft_perfect_D()
        {
            if (data != 0)
            {
                fftw_destroy_plan(plan);
                fftw_destroy_plan(plan2);
                fftw_free(data);
                fftw_free(fft);
            }
        }
    };

    class structure_function
    {
    private:
        int n2;
        double lag_bin_size;
        std::vector<std::vector<fft_perfect_D>> perfect_Ds;
        std::vector<int> sum;
        std::vector<std::vector<int>> sums;
        std::vector<double> bin_lags;
        std::vector<std::vector<D_info>> D_data;
        std::vector<std::vector<std::pair<std::pair<int, int>, std::vector<std::pair<int, int>>>>> addsubs;
#ifdef CHECK
        std::vector<std::vector<std::vector<std::pair<int, int>>>> hashes;
#endif
        
    public:
        structure_function(){}
        
        //structure_function(const std::vector<std::vector<double>> &restimes, double lag_max, double log_shift, double lag_bin_size, double lag_max_cutoff = -1, int threads = 1) : lag_bin_size(lag_bin_size), perfect_Ds(threads), sums(restimes.size()), D_data(restimes.size()), addsubs(restimes.size())
        
        
        //(restimes, lag_max, log_shift, lag_bin_size, lag_max_cutoff, n_real);
        
        structure_function(const std::vector<std::vector<double>> &restimes, const std::unordered_map<std::string, double> &options_map) : lag_bin_size(get_map_value(options_map, "lag_bin_size")), perfect_Ds(get_map_value(options_map, "n_real", 1.0)), sums(restimes.size()), D_data(restimes.size()), addsubs(restimes.size())
#ifdef CHECK
        , hashes(files.size())
#endif
        {
            double lag_min = get_map_value(options_map, "lag_min", 1.0e100);
            double lag_max = get_map_value(options_map, "lag_max");
            double log_shift = get_map_value(options_map, "log_shift");
            double lag_bin_size = get_map_value(options_map, "lag_bin_size");
            double lag_max_cutoff = get_map_value(options_map, "lag_max_cutoff", -1.0);
            
            //double log_shift = 0.35;
            int step = 1;
            bool find_lag_min = (lag_min == 1.0e100) ? true : false;
            
            for (auto &&r : restimes)
            {
                for (auto &&d : perfect_Ds)
                {
                    d.emplace_back(r.size());
                }
                
                if (find_lag_min)
                {
                    for (int j = 1, end = r.size(); j < end; j++)
                    {
                        if (lag_min > r[j] - r[j-1])
                        {
                            lag_min = r[j] - r[j-1];
                        }
                    }
                }
            }
            
            int n2m1 = int(std::log10((lag_max + log_shift)/(lag_min + log_shift))/lag_bin_size);
            n2 = (lag_max_cutoff > 0.0) ? n2m1 + 1 : n2m1;
            
            sum = std::vector<int>(n2, 0);
            bin_lags = std::vector<double>(n2, 0.0);
            for (auto &&d : D_data)
                d.resize(n2);
#ifdef CHECK
            for (auto &&h : hashes)
                h.resize(n2);
#endif
            for (int ii = 0, endii = restimes.size(); ii < endii; ii++)
            {
                sums[ii] = std::vector<int>(n2, 0);
                int N = restimes[ii].size();
                auto &restime = restimes[ii];
                
                std::vector<std::map<int, std::vector<int>>> hash_count(N);
                
                for (int i=0, end = N-1; i < end; i+=step)
                {
                    for (int k=i+step; k < N; k+=step)
                    {
                        double ddd = std::log10(restime[k]-restime[i] + log_shift) - std::log10(lag_min + log_shift);
                        int ind = int(ddd/lag_bin_size);

                        if (ind >= 0)
                        {
                            if(ind < n2m1)
                            {
                                sum[ind]++;
                                sums[ii][ind]++;
#ifdef CHECK
                                hashes[ii][ind].emplace_back(i, k);
#endif
                                bin_lags[ind] += restime[k]-restime[i];
                                hash_count[k-i][ind].push_back(i);
                            }
                            else if (lag_max_cutoff > 0.0 && std::abs(restime[k]-restime[i]) < lag_max_cutoff)
                            {
                                sum[n2m1]++;
                                sums[ii][n2m1]++;
#ifdef CHECK
                                hashes[ii][n2m1].emplace_back(i, k);
#endif
                                bin_lags[n2m1] += restime[k]-restime[i];
                                hash_count[k-i][n2m1].push_back(i);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                    
                int csize = N-1;
                std::map<int, std::map<int , std::vector<std::pair<int, int>>>> addsubtemp;
                for (int i = 1, end = hash_count.size(); i < end; i++)
                {
                    if (hash_count[i].size() > 0)
                    {
                        int tot = 0.0;
                        int bb = hash_count[i].begin()->first;
                        int bbv = hash_count[i].begin()->second.size();
                        for (auto &&h : hash_count[i])
                        {
                            if (bbv < int(h.second.size()))
                            {
                                bbv = h.second.size();
                                bb = h.first;
                            }

                            tot += h.second.size();
                        }
                        
                        tot = csize-tot;
                        if (bbv > tot)
                        {
                            auto &&b_vec = hash_count[i][bb];
                            for (auto &&h : hash_count[i])
                            {
                                if (h.first != bb)
                                {
                                    if (tot > 0)
                                        b_vec.insert(b_vec.end(), h.second.begin(), h.second.end());
                                    
                                    for (auto &&ind : h.second)
                                    {
                                        //D_data[ii][h.first].add.emplace_back(ind, i+ind);
                                        //D_data[ii][bb].sub.emplace_back(ind, i+ind);
                                        addsubtemp[bb][h.first].emplace_back(ind,i+ind);
                                    }
                                }
                            }
                            
                            D_data[ii][bb].ind.push_back(i);
                        }
                        else
                        {
                            for (auto &&h : hash_count[i])
                            {
                                for (auto &&ind : h.second)
                                {
                                    D_data[ii][h.first].add.emplace_back(ind, i+ind);
                                }
                            }
                        }
                    }
                    
                    csize--;
                }
                
                for (auto &&ast : addsubtemp)
                    for (auto &&a : ast.second)
                        addsubs[ii].emplace_back(std::make_pair(ast.first, a.first), a.second);
            }
            
            for (int i = 0; i < n2; i++)
                bin_lags[i] /= sum[i];
            
            for (int i = 0; i < n2; i++)
            {
                if (sum[i]<1)
                {
                    std::cout << "structure function: logarithmic bin size too small" << std::endl;
                    exit(-1);
                }
                assert(sum[i]);
            }
        }
        
        template<typename T>
        std::vector<double> operator()(std::vector<T> &sim_lc, int thread = 0)
        {
            //for (auto &&s : simulated_Ds[flux_index])
            //    s = 0.0;
            std::vector<double> simulated_Ds(n2, 0.0);
            
            for (int ii = 0, endii = addsubs.size(); ii < endii; ii++)
            {
                std::vector<double> &sim_flux = sim_lc[ii];
#ifdef CHECK
                for (auto &&s : simulated_Ds)
                    s = 0.0;
                
                for (int k=0, end = hashes[ii].size(); k < end; k++)
                {
                    for (auto &&ind : hashes[ii][k])
                    {
                        simulated_Ds[flux_index][k] += SQ(sim_flux[ind.second]-sim_flux[ind.first]);
                    }
                    simulated_Ds[k] /= sums[ii][k];
                }
                auto stemp = simulated_Ds;
                for (auto &&s : simulated_Ds)
                    s = 0.0;
                std::vector<std::string> flags(stemp.size());
#endif
                perfect_Ds[thread][ii].calc(sim_flux);
                //auto perfect_DS = exact_perfect_D(sim_flux, perfect_Ds[flux_index][ii].size());
                
                for (auto &&as : addsubs[ii])
                {
                    double temp = 0.0;
                    for (auto &&ind : as.second)
                        temp += SQ(sim_flux[ind.second] - sim_flux[ind.first]);
                    simulated_Ds[as.first.second] += temp;
                    simulated_Ds[as.first.first] -= temp;
#ifdef CHECK
                    if (temp > 0.0)
                    {
                       flags[as.first.second] += "addsub-add ";
                       flags[as.first.first] += "addsub-sub ";
                    }
#endif
                }
                
                for (int k = 0, endk = D_data[ii].size(); k < endk; k++)
                {
#ifdef CHECK
                    if (D_data[ii][k].ind.size() > 0)
                       flags[k] += "int ";
                    if (D_data[ii][k].add.size() > 0)
                       flags[k] += "add ";
                    if (D_data[ii][k].sub.size() > 0)
                       flags[k] += "sub ";
#endif
                    for (auto &&ind : D_data[ii][k].ind)
                    {
                        simulated_Ds[k] += perfect_Ds[thread][ii][ind];
                    }
                    
                    for (auto &&ind : D_data[ii][k].add)
                    {
                        simulated_Ds[k] += SQ(sim_flux[ind.second] - sim_flux[ind.first]);
                    }
                    
                    for (auto &&ind : D_data[ii][k].sub)
                    {
                        simulated_Ds[k] -= SQ(sim_flux[ind.second] - sim_flux[ind.first]);
                    }
#ifdef CHECK
                    simulated_Ds[k] /= sums[ii][k];
#endif
                }
#ifdef CHECK
                for (int l = 0, endll = stemp.size(); l < endll; l++)
                {
                    std::cout << ii << ": "<< flags[l] << std::endl;
                    std::cout << l << ":  " << stemp[l] << "   " << simulated_Ds[l] << std::endl;
                }
                getchar();
#endif
            }
            
            for (int k = 0; k < n2; k++)
            {
                simulated_Ds[k] /= sum[k];
            }
            
            return simulated_Ds;
        }
        
        double operator[](int i) const {return bin_lags[i];}
        double LagBinSize() const {return lag_bin_size;}
        size_t size() const {return n2;}
    };
}

#endif
