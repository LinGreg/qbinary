#ifndef __TK_DRAW_UTILS__
#define __TK_DRAW_UTILS__

#include <cmath>
#include <random>
#include <vector>
#include <thread>
#include <algorithm>
#include <fftw3.h>
#include "sgravar_utils.hpp"

namespace sgravar
{
    class tk_draw
    {
    private:
        fftw_complex *data;
        fftw_complex *fft;
        fftw_plan plan;
        const std::vector<double> &times;
        std::vector<double> fvec;
        std::vector<double> sim_flux;
        double samp;
        int nnn;
        int toff;
        
    public:
        tk_draw(tk_draw &&in) : data(in.data), fft(in.fft), plan(in.plan), times(std::ref(in.times)), fvec(std::move(in.fvec)), sim_flux(std::move(in.sim_flux)), samp(in.samp), nnn(in.nnn), toff(in.toff)
        {
            in.data = 0;
            in.fft = 0;
        }
        
        tk_draw(const std::vector<double> &times, double samp = -1.0) : times(times), sim_flux(times.size()), samp(samp)
        {
            double actual_samp = (times.back() - times.front())/(times.size()-1);
            if (samp <= 0.0)
                samp = actual_samp;
            nnn = pow(2, int(std::log(double(times.size())*actual_samp/samp)/std::log(2.0)) + 2);
            toff = int((double(nnn) - times.size()*actual_samp/samp)/2.0 + 0.5);
            data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_FORWARD, FFTW_ESTIMATE);
            fvec.resize(nnn);
        }
        
        template<typename T, typename U, typename V, typename W>
        void calc(T&& generator, U&& pow_func, V&& conv_func, W&& noise)
        {
            int N = nnn/2;
            //int corr_len = 1;
            std::normal_distribution<double> normal;
            double sqrt2 = std::sqrt(2.0);
            double var = 0.0;
            //std::vector<double> corr(corr_len, 0.0);
            double spec;
            
            data[0][0] = data[0][1] = 0.0;
            for (int i = 1; i < N; i++)
            {
                spec = std::sqrt(pow_func(double(i)/double(nnn*samp)));
                data[i][0] = spec*normal(generator);
                data[i][1] = spec*normal(generator);
                data[nnn-i][0] = data[i][0];
                data[nnn-i][1] = -data[i][1];
                var += 4.0*spec*spec;
                //for (int j = 1; j < corr_len; j++)
                //    corr[j] += 4.0*spec*spec*std::cos(2.0*M_PI*i*j/double(nnn));
            }
            
            spec = std::sqrt(pow_func(double(N)/double(nnn*samp)));
            data[N][0] = spec*normal(generator);
            data[N][1] = 0.0;
            var += spec*spec;
            //for (int j = 1; j < corr_len; j++)
            //    corr[j] += spec*spec*std::pow(-1.0, j);
            
            fftw_execute(plan);
            
//             {
//                 for (auto &&c : corr)
//                 {
//                     c /= var;
//                 }
//                 
//                 corr[0] = 1.0;
//                 
//                 Cholesky chol(corr);
//                 auto fish = chol.Inverse(corr_len-1);
// 
//                 for (int i = 0; i < corr_len; i++)
//                 {
//                     double x = 0.0;
//                     int last = corr_len-1;
//                     for (int j = 0, k = i-corr_len; j < last; j++, k++)
//                         x += fish[j]*fft[(k + nnn)%nnn][0]/fish[last];
//                     
//                     fvec[i] = conv_func(0.5*(1.0 + std::erf((fft[i][0] + x)*std::sqrt(fish[last]/var/2.0))));
//                 }
//                 
//                 for (int i = corr_len; i < nnn; i++)
//                 {
//                     double x = 0.0;
//                     int last = corr_len-1;
//                     for (int j = 0, k = i-corr_len; j < last; j++, k++)
//                         x += fish[j]*fft[k][0]/fish[last];
//                     
//                     fvec[i] = conv_func(0.5*(1.0 + std::erf((fft[i][0] + x)*std::sqrt(fish[last]/var/2.0))));
//                 }
//             }
            
            var = std::sqrt(var);
            for (int i = 0; i < nnn; i++)
            {
                fvec[i] = conv_func(0.5*(1.0 + std::erf(fft[i][0]/var/sqrt2)));
                if (!std::isfinite(fvec[i]))
                {
                    std::runtime_error("tk_draw: The light curve is again NaN.");
                }
            }
            
            for (int i = 0, end = times.size(); i < end; i++)
            {
                sim_flux[i] = noise(generator, times[i]) + operator()(times[i]);
            }
            
            //for (auto &&elem : sim_flux)
            //    elem += noise(generator);
        }
        
        template<typename T, typename U, typename V1, typename V2, typename W1, typename W2>
        void calc_diff(T&& generator, U&& pow_func, V1&& conv_func1, V2&& conv_func2, W1&& noise1, W2&& noise2)
        {
            int N = nnn/2;
            std::normal_distribution<double> normal;
            double sqrt2 = std::sqrt(2.0);
            double var = 0.0;
            double spec;
            
            data[0][0] = data[0][1] = 0.0;
            for (int i = 1; i < N; i++)
            {
                spec = std::sqrt(pow_func(double(i)/double(nnn*samp)));
                data[i][0] = spec*normal(generator);
                data[i][1] = spec*normal(generator);
                data[nnn-i][0] = data[i][0];
                data[nnn-i][1] = -data[i][1];
                var += 4.0*spec*spec;
            }
            
            spec = std::sqrt(pow_func(double(N)/double(nnn*samp)));
            data[N][0] = spec*normal(generator);
            data[N][1] = 0.0;
            var += spec*spec;
            
            fftw_execute(plan);
            
            var = std::sqrt(var);
            for (int i = 0; i < nnn; i++)
            {
                fvec[i] = conv_func1(0.5*(1.0 + std::erf(fft[i][0]/var/sqrt2)));
                if (!std::isfinite(fvec[i]))
                {
                    std::runtime_error("tk_draw: The light curve is again NaN.");
                }
            }
            
            for (int i = 0, end = times.size(); i < end; i++)
            {
                sim_flux[i] = noise1(generator, times[i]) + operator()(times[i]);
            }
            
            for (int i = 0; i < nnn; i++)
            {
                fvec[i] = conv_func2(0.5*(1.0 + std::erf(fft[i][0]/var/sqrt2)));
                if (!std::isfinite(fvec[i]))
                {
                    std::runtime_error("tk_draw: The light curve is again NaN.");
                }
            }
            
            for (int i = 0, end = times.size(); i < end; i++)
            {
                sim_flux[i] -= noise2(generator, times[i]) + operator()(times[i]);
            }
        }
        
        template<typename T>
        double add_power_real(std::vector<double> &file, T&& pow_func)
        {
            double var = 0.0;
            for (int i = 1; i < nnn; i++)
            {
                file[i] = pow_func(double(i)/double(nnn*samp));
                var += file[i];
            }
            
            return var;
        }
        
        double add_power(std::vector<double> &file)
        {
            //int N = nnn/2;
            fftw_complex* data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_complex* fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_plan plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_BACKWARD, FFTW_ESTIMATE);
            
            for (int i = 0; i < nnn; i++)
            {
                data[i][0] = fvec[i];
                data[i][1] = 0.0;
            }
            
            fftw_execute(plan);
            
            double var = 0.0;
            
            for (int i = 1; i < nnn; i++)
            {
                file[i] += fft[i][0]*fft[i][0] + fft[i][1]*fft[i][1];
                var += file[i];
            }
            
            fftw_destroy_plan(plan);
            fftw_free(data);
            fftw_free(fft);
            
            return var;
        }
        
        double get_power_spec(std::vector<double> &file)
        {
            //int N = nnn/2;
            fftw_complex* data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_complex* fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_plan plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_BACKWARD, FFTW_ESTIMATE);
            
            for (int i = 0; i < nnn; i++)
            {
                data[i][0] = fvec[i];
                data[i][1] = 0.0;
            }
            
            fftw_execute(plan);
            
            double var = 0.0;
            
            for (int i = 1; i < nnn; i++)
            {
                file[i] += fft[i][0]*fft[i][0] + fft[i][1]*fft[i][1];
                var += file[i];
            }
            
            fftw_destroy_plan(plan);
            fftw_free(data);
            fftw_free(fft);
            
            return var;
        }
        
        template <typename N, typename G>
        std::vector<double> get_power_spec(N &&noise, G&& generator)
        {
            //int N = nnn/2;
            std::vector<double> file(nnn);
            fftw_complex* data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_complex* fft = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * nnn);
            fftw_plan plan = fftw_plan_dft_1d(nnn, data, fft, FFTW_BACKWARD, FFTW_ESTIMATE);
            
            for (int i = 0; i < nnn; i++)
            {
                data[i][0] = noise(generator, double(i - toff)*samp) + fvec[i];
                data[i][1] = 0.0;
            }
            
            fftw_execute(plan);
            
            for (int i = 1; i < nnn; i++)
            {
                file[i] = fft[i][0]*fft[i][0] + fft[i][1]*fft[i][1];
            }
            
            fftw_destroy_plan(plan);
            fftw_free(data);
            fftw_free(fft);
            
            return file;
        }
        
        double operator()(double time) const
        {
            double x = time/samp + double(toff);
            return spline::LinearInterpGrid<double>(x, int(x), fvec);
        }
        
        double operator[](size_t i) const
        {
            return sim_flux[i];
        }
        
        operator std::vector<double> &()
        {
            return sim_flux;
        }
        
        int size() const {return nnn;}
        
        ~tk_draw()
        {
            if (data != 0)
            {
                fftw_destroy_plan(plan);
                fftw_free(data);
                fftw_free(fft);
            }
        }
    };
}

#endif
