#ifndef __SPLINE_HPP__
#define __SPLINE_HPP__

#include <vector>
#include <sstream>
#include <fstream>
#include <cassert>
#include <stdexcept>

namespace spline
{
    inline double LinearInterp(double xin, const std::vector<double> &x, const std::vector<double> &y)
    {
        assert(x.size() == y.size());
        
        int kLow = 0, kHi = x.size() - 1, k;

        if (xin >= x[kHi])
            return y[kHi];
        else if (xin <= x[kLow])
            return y[kLow];
        
        while (kHi-kLow > 1) 
        {
            k=(kHi+kLow) >> 1;

            if (x[k] > xin) 
                kHi=k;
            else 
                kLow=k;
        }
        
        return y[kLow] + (xin - x[kLow])*(y[kHi] - y[kLow])/(x[kHi] - x[kLow]);
    }
    
    template<typename T>
    inline double LinearInterpGrid(const T &xin, int i, const std::vector<T> &y)
    {
        return y[i] + (xin - double(i))*(y[i+1] - y[i]);
    }
    
    inline double *SolveTriDiagMatrix(double *a, double *b, double *c, double *u, int N)
    {
        double *ans = new double[N];
        double *temp = new double[N];
        double det;
        int i;

        ans[0] = u[0]/(det = b[0]);
        for (i = 1; i < N; i++)
        {
            temp[i] = c[i - 1]/det;
            det = b[i] - a[i]*temp[i];
            ans[i] = (u[i] - a[i]*ans[i-1])/det;
        }
        for(i = N-2; i>=0; i--)
            ans[i] -= temp[i+1]*ans[i+1];

        delete[] temp;

        return ans;
    }

    inline double *FindSplineMs(double *x, double *y, int N)
    {
        double *a = new double[N-2];
        double *b = new double[N-2];
        double *c = new double[N-2];
        double *u = new double[N-2];
        double *m = new double[N];
        double *temp;
        m[0] = m[N-1] = 0;

        b[0] = 2.0 * (x [2] - x [0]);
        c[0] = x[2] - x[1];
        u[0] = 6.0*((y[2] - y[1])/(x[2] - x[1])-(y[1]-y[0])/(x[1]-x[0]));
        for (int i = 1; i < N - 3; i++)
        {
                    a[i] = x[i+1] - x[i];
                    b[i] = 2.0*(x[i+2] - x[i]);
                    c[i] = x[i+2] - x[i+1];
                    u[i] = 6.0*((y[i+2] - y[i+1])/(x[i+2] - x[i+1])-(y[i+1]-y[i])/(x[i+1]-x[i]));
        }
        a[N-3] = x[N-2] - x[N-3];
        b[N-3] = 2.0*(x[N-1] - x[N-3]);
        u[N-3] = 6.0*((y[N-1] - y[N-2])/(x[N-1] - x[N-2])-(y[N-2]-y[N-3])/(x[N-2]-x[N-3]));

        temp = SolveTriDiagMatrix(a, b, c, u, N - 2);
        for (int i = 1; i < N-1; i++)
            m[i] = temp [i-1];

        delete[] temp;
        delete[] a;
        delete[] b;
        delete[] c;
        delete[] u;

        return m;
    }
    
    class spline
    {
    private:
        double *m;
        double *x;
        double *y;
        int N;
            
    public:
        spline() : m(NULL), x(NULL), y(NULL), N(0) {}
        
        spline(double *xin, double *yin, int number)
        {
            x = xin;
            y = yin;
            N = number;
            
            m = FindSplineMs(x, y, N);
        }
        
        spline(std::vector<double> &xin, std::vector<double> &yin)
        {
            if (xin.size() != yin.size())
                std::runtime_error("spline: input x and y not the same size");
            x = &xin[0];
            y = &yin[0];
            N = xin.size();
            
            m = FindSplineMs(x, y, N);
        }
        
        spline(char *name)
        {
            std::ifstream in(name);
            double dummy;
            int i;
            
            for (i = 0; (in >> dummy); i++);
            in.close();
            N = i/2;
            x = new double[N];
            y = new double[N];
            in.open(name);
            
            for (i = 0; (in >> x[i]); i++)
            {
                in >> y[i];
            }
            in.close();
            m = FindSplineMs(x, y, N);
        }
        
        void Input(char *name)
        {
            std::ifstream in(name);
            double dummy;
            int i;
            
            for (i = 0; (in >> dummy); i++);
            in.close();
            N = i/2;
            if(N != 0)
            {
                delete[] x;
                delete[] y;
                delete[] m;
            }
            x = new double[N];
            y = new double[N];
            in.open(name);
            
            for (i = 0; (in >> x[i]); i++)
                in >> y[i];
            in.close();
            m = FindSplineMs(x, y, N);
        }
        
        void Input(double *xin, double *yin, int number)
        {
            if(N != 0)
            {
                delete[] x;
                delete[] y;
                delete[] m;
            }
            x = xin;
            y = yin;
            N = number;
            
            m = FindSplineMs(x, y, N);
            
            return;
        }
        
        double Splint(double xin)
        {
            int kLow = 0, kHi = N - 1, k;

            if (xin >= x[kHi])
                return y[kHi];
            else if (xin <= x[kLow])
                return y[kLow];
            
            while (kHi-kLow > 1) 
            {
                k=(kHi+kLow) >> 1;

                if (x[k] > xin) 
                    kHi=k;
                else 
                    kLow=k;
            }
            
            double s0 = y[kLow],
            s1 = (y[kHi] - y[kLow])/(x[kHi] - x[kLow]) - (x[kHi] - x[kLow])*(2.0*m[kLow] + m[kHi])/6.0,
            s2 = m[kLow]/2.0,
            s3 = (m[kHi] - m[kLow])/6.0/(x[kHi] - x[kLow]);

            double w = xin - x[kLow];
//             std::cout << x[kLow] << "   " << xin << "   " << x[kHi] << std::endl;
//             std::cout << y[kLow] << "   " << ((s3*w + s2)*w + s1)*w + s0 << "   " << y[kHi] << std::endl;
//             std::cout << s1 << "   " << s2 << "   " << s3 << std::endl;
            return ((s3*w + s2)*w + s1)*w + s0;
        }
        
        double DSplint(double xin)
        {
            int kLow = 0,
            kHi = N - 1,
            k;

            while (kHi-kLow > 1) 
            {
                k=(kHi+kLow) >> 1;

                if (x[k] > xin) 
                    kHi=k;
                else 
                    kLow=k;
            }
            
            //double s0 = y[kLow],
            double s1 = (y[kHi] - y[kLow])/(x[kHi] - x[kLow]) - (x[kHi] - x[kLow])*(2.0*m[kLow] + m[kHi])/6.0,
            s2 = m[kLow]/2.0,
            s3 = (m[kHi] - m[kLow])/6.0/(x[kHi] - x[kLow]);

            double w = xin - x[kLow];
            
            return (3.0*s3*w + 2.0*s2)*w + s1;
        }

        double Integrate()
        {
            double w, s1, result = 0.0;
            
            for (int i = 0; i < N; i++)
            {
                w = (x[i + 1] - x[i]);
                s1 = (y[i + 1] - y[i])/w - w*(2.0*m[i] + m[i + 1])/6.0;
                    
                result += (((m[i + 1] + 3.0*m[i])/24.0*w + s1/2.0)*w + y[i])*w;
            }
            
            return result;
        }

        ~spline()
        {
            if(N != 0)
            {
                //delete[] x;
                //delete[] y;
                delete[] m;
            }
            N = 0;
        }
    };
}

#endif
