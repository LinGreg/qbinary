#include <vector>
#include <cmath>
#include <iostream>

namespace integrator
{
    class GaussLegendre
    {
    private:
        std::vector<double> weights, points;
        
    public:
        GaussLegendre(int N, const double EPS = 1.0e-14) : weights(N), points(N)
        {
            int max = (N + 1)/2;
            double z1, z, pp, p3, p2, p1;
            
            for (int i = 0; i < max; i++)
            {
                z = cos(M_PI*(i + 0.75)/(N + 0.5));
                
                do
                {
                    p1 = 1.0;
                    p2 = 0.0;
                    for (int j = 0; j < N; j++)
                    {
                            p3 = p2;
                            p2 = p1;
                            p1 = ((2.0*j + 1.0)*z*p2-j*p3)/(j + 1.0);
                    }
                    pp = N*(z*p1 - p2)/(z*z - 1.0);
                    z1 = z;
                    z = z1 - p1/pp;
                }
                while(fabs(z-z1) > EPS);
                
                points[i] = -z;
                points[N - 1 - i] = z;
                weights[i] = 2.0/((1.0 - z*z)*pp*pp);
                weights[N - 1 - i] = weights[i];
            }
        }
        
        template<typename T>
        double operator()(T &&f, double a, double b)
        {
            double result = 0;

            for (int i = 0, end = weights.size(); i < end; i++)
                result += weights[i]*f(((a+b) + (b-a)*points[i])/2.0);

            return (b-a)*result/2.0;
        }
    };
    
    class Polint
    {
    private:
        double *ya;
        double *xa;
        double dy;
        int n;
        
    public:
        Polint(){}
        Polint(double *xin, double *yin, int nin){xa = xin; ya = yin; n = nin;}
        void Input(double *xin, double *yin, int nin){xa = xin; ya = yin; n = nin;}
        double Error(){return dy;}

        double Output(double x)
        {
            int i,m,ns=0;
            double den,dif,dift,ho,hp,w;
            std::vector<double> c(n), d(n);
            double y;

            dif=fabs(x-xa[0]);
            
            for (i=0;i<n;i++) 
            {
                if ( (dift=fabs(x-xa[i])) < dif) 
                {
                    ns=i;
                    dif=dift;
                }
                c[i]=ya[i];
                d[i]=ya[i];
            }
            y=ya[ns--];
            for (m=1;m<n;m++) 
            {
                for (i=0;i<n-m;i++) 
                {
                    ho=xa[i]-x;
                    hp=xa[i+m]-x;
                    w=c[i+1]-d[i];
                    den = ho - hp;
                    den=w/den;
                    d[i]=hp*den;
                    c[i]=ho*den;
                }
                y += (dy=(2*(ns+1) < (n-m) ? c[ns+1] : d[ns--]));
            }
            
            return y;
        }
    };
    
    template<typename T>
    class Trapzd
    {
    private:
        T &f;
        double a, b;
        double s;
        unsigned int n;
            
    public:
        Trapzd(T& f, double a, double b) : f(f), a(a), b(b), s(0.0), n(0) {}

        double next()
        {
            double x,tnm,sum,del;
            unsigned long long it, j;
            
            n++;
            if (n == 1) 
            {
                return (s=0.5*(b-a)*(f(a)+f(b)));
            } 
            else 
            {
                for (it=1,j=1;j<n-1;j++) it <<= 1;
                //it = 1;
                //it <<= (n-2);
                tnm=it;
                del=(b-a)/tnm;
                x=a+0.5*del;
                for (sum=0.0,j=0;j<it;j++,x+=del) sum += f(x);
                s=0.5*(s+(b-a)*sum/tnm);
                
                return s;
            }
        }
    };

    template<typename T>
    inline double QTrap(T &&f, double a, double b, const double eps = 1.0e-10)
    {
        int j;
        double ss,olds=0.0;
        static const int JMAX = 65;

        Trapzd<T> trap(f, a, b);
        for (j=0;j<JMAX;j++) {
            ss=trap.next();
            if (j > 5)
            {
                //std::cout << j << ":  " << fabs(ss-olds)/fabs(olds) << std::endl;
                if (fabs(ss-olds) < eps*fabs(olds) || (ss == 0.0 && olds == 0.0)) 
                    return ss;
            }
            olds=ss;
        }
        std::cout << "Too many steps in routine qtrap" << std::endl;
        return ss;
    }

    template<typename T>
    inline double RombInt(T&& f, const double &a, const double &b, const double eps = 1.0e-10)
    {
        static const int JMAX = 40;
        static const int JMAXP=JMAX + 1, K = 5;
        double temp=0.0;
        //double s[JMAXP], h[JMAXP];
        std::vector<double> s(JMAXP), h(JMAXP);
        Polint polint;
        int j;
        
        Trapzd<T> trap(f, a, b);
        h[0]=1.0;
        for (j=1;j<=JMAX;j++) 
        {
            s[j-1]=trap.next();
            if (j >= K) 
            {
                polint.Input(&h[j-K],&s[j-K], K);
                temp = polint.Output(0.0);
                //std::cout << j << ":  " << fabs(polint.Error())/fabs(temp) << "   " << fabs(polint.Error()) << "   " << fabs(temp) << std::endl;
                if (fabs(polint.Error()) <= eps*fabs(temp)) 
                {
                    return temp;
                }
            }
            h[j]=0.25*h[j-1];
        }
        std::cout << "Too many steps in routine rombint" << std::endl;
        
        return temp;
    }

}
