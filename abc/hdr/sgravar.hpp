#ifndef __SGRAVAR_HPP__
#define __SGRAVAR_HPP__

#include <fftw3.h>
#include <algorithm>
#include <cassert>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <fstream>
#include <cmath>
#include <sstream>
#include <map>
#include <ctime>
#include <iomanip>
#include <random>
#include <set>
#include <type_traits>
#include "spline.hpp"
#include "abc.hpp"
#include "sgravar_utils.hpp"
#include "structure_function.hpp"
#include "tk_draw.hpp"

namespace sgravar
{
    const unsigned char SEP_DIST = 0x01;
    const unsigned char RESUME = 0x02;
    const unsigned char SMC = 0x04;
    
    template <typename pwr_spec_func, typename convert_func, typename noise_func>
    class SgrAVar
    {
    private:
        int n_real;
        int N_data;
        double samp;
        double data_avg;
        double data_median;
        std::string prefix;
        
        std::vector<std::vector<double>> restimes;
        std::vector<std::vector<double>> fluxes;
        std::vector<double> sorted_fluxes;
        //std::vector<std::vector<std::vector<double>>> simulated_fluxes;
        std::vector<std::vector<double>> simulated_sorted_fluxes;
        std::vector<std::vector<double>> simulated_Ds;
        std::vector<std::vector<tk_draw>> sim_lc;
        std::vector<double> D;
        std::vector<double> cdfs;
        structure_function struct_func;
        
    public:
        SgrAVar(int nreal, double samp, double lag_bin_size, double log_shift, double lag_max, const std::string &file, double lag_max_cutoff = -1, const std::string &prefix = "", int step = 1);// : SgrAVar(nreal, samp, lag_bin_size, lag_max, std::vector<std::string>(1, file), lag_max_cutoff, step) {}
        
        //SgrAVar(int nreal, double samp, double lag_bin_size, double log_shift, double lag_max, const std::vector<std::string> &files, double lag_max_cutoff = -1, const std::string &prefix = "", int step = 1) : n_real(nreal), samp(samp), prefix(prefix), restimes(files.size()), fluxes(files.size()), sorted_fluxes(files.size()), /*simulated_fluxes(files.size()),*/ simulated_sorted_fluxes(files.size()), sim_lc(nreal), cdfs(files.size())
        
        SgrAVar(const std::unordered_map<std::string, double> &options_map, const std::vector<std::string> &files, const std::string &prefix = "") : n_real(get_map_value(options_map, "n_real")), samp(get_map_value(options_map, "samp")), prefix(prefix), restimes(files.size()), fluxes(files.size()), /*sorted_fluxes(files.size()), simulated_fluxes(files.size()), simulated_sorted_fluxes(files.size()),*/ sim_lc(get_map_value(options_map, "n_real")), cdfs(files.size())
        {
            simulated_sorted_fluxes.resize(n_real);
            data_avg = 0.0;
            N_data = 0;
            
            for (int i = 0, end = files.size(); i < end; i++)
            {
                EnterData(files[i], [&](const std::vector<double> &vec)
                {
                    if (vec.size() == 2)
                    {
                        restimes[i].push_back(vec[0]);
                        fluxes[i].push_back(vec[1]);
                    }
                });
                
                double val = restimes[i][0];
                for (auto &&res : restimes[i])
                    res -= val;
                
                for (auto &&lc : sim_lc)
                {
                    lc.emplace_back(restimes[i], samp);
                }
                
                //simulated_fluxes[i] = std::vector<std::vector<double>> (nreal, std::vector<double>(restimes[i].size()));
                
                //sorted_fluxes[i] = fluxes[i];
                sorted_fluxes.insert(sorted_fluxes.end(), fluxes[i].begin(), fluxes[i].end());
                
                
                for (int j = 0, endj = fluxes[i].size(); j < endj; j++)
                    data_avg += fluxes[i][j];
                
                N_data += fluxes[i].size();
            }
            
            data_avg /= N_data;
            
            simulated_sorted_fluxes = std::vector<std::vector<double>> (n_real, std::vector<double>(sorted_fluxes.size()));
            cdfs = lc_cdf(sorted_fluxes); // sorted_fluxes is sorted by lc_cdf!
            data_median = find_median(sorted_fluxes);
            
            //struct_func = structure_function(restimes, lag_max, log_shift, lag_bin_size, lag_max_cutoff, n_real);
            struct_func = structure_function(restimes, options_map);
            
            simulated_Ds = std::vector<std::vector<double>> (n_real, std::vector<double>(struct_func.size()));
            
            D = struct_func(fluxes);
        }
        
        int NReal() const {return n_real;}
        int Size() const {return restimes.size();}
        std::vector<double> Simulated_Ds(int i) const {return simulated_Ds[i];}
        std::vector<double>& Simulated_Fluxes(int thnum, int i) {return sim_lc[thnum][i];}

        void PrintFlux(const std::string &file, const bool &option = true)
        {
            for (int ii = 0, endii = restimes.size(); ii < endii; ii++)
            {
                std::stringstream ss;
                ss << ii;
                std::ofstream out(std::string(file) + "." + ss.str());
                for (int i = 0, end = restimes[ii].size(); i < end; i++)
                {
                    out << restimes[ii][i];
                    if (option)
                        out << "\t" << fluxes[ii][i];
                    for (int j = 0; j < n_real; j++)
                        out << "\t" << sim_lc[j][ii][i];
                    out << std::endl;
                }
                out.close();
            }
        }
        
        void PrintFlux(const std::vector<std::vector<std::vector<double>>> &saved_fluxes, const std::string &file)
        {
            for (int ii = 0, endii = restimes.size(); ii < endii; ii++)
            {
                std::stringstream ss;
                ss << ii;
                std::ofstream outf(add_prefix(prefix, file + "." + ss.str() + ".fluxes").c_str());
                for (int i = 0, end = restimes[ii].size(); i < end; i++)
                {
                    outf << restimes[ii][i];
                    //for (auto &&elem : saved_fluxes[ii])
                    //    outf << "\t" << elem[i];
                    for (int j = 0, endj = saved_fluxes[ii].size(); j < endj; j++)
                        outf << "\t" << saved_fluxes[ii][j][i];
                    outf << std::endl;
                }
                outf.close();
            }
        }
        
        void PrintStruct(const std::string &file) const
        {
            std::ofstream out(file);
            for (int i = 0, end = D.size(); i < end; i++)
            {
                out << struct_func[i] << "\t" << D[i];
                for (auto &&elem : simulated_Ds)
                {
                    out << "\t" << elem[i];
                }
                out << std::endl;
            }
            out.close();
        }
        
        void PrintStruct(const std::vector<std::vector<double>> &saved_Ds, const std::string &file)
        {
            std::ofstream outd(add_prefix(prefix, file + ".structs").c_str());
            for (int i = 0, end = D.size(); i < end; i++)
            {
                outd << struct_func[i];
                for (auto &&elem : saved_Ds)
                {
                    outd << "\t" << elem[i];
                }
                outd << std::endl;
            }
            outd.close();
        }
        
        void PrintCDF(const std::string &file) const
        {
            std::ofstream out(file);
            
            for (int i = 0, end = cdfs.size(); i < end; i++)
            {
                out << sorted_fluxes[i] << "\t";
                for (auto &&elem : simulated_sorted_fluxes)
                {
                    out << elem[i] << "\t";
                }
                out << cdfs[i] << std::endl;
            }
        }
        
        template<typename G>
        std::vector<double> GetPwrSpec(int flux_index, int i, const std::unordered_map<std::string, double> &map, G &&generator)
        {
            return sim_lc[flux_index][i].get_power_spec(noise_func(map, prefix), generator);
        }
        
        void PrintPwr(int num, const std::string &file, const std::unordered_map<std::string, double> &map, const bool with_real = true)
        {
            std::ofstream out(file.c_str());
            double var = 0.0, var_real = 0.0;
            int N = sim_lc[0][num].size()/2;
            std::vector<double> vec(2*N, 0.0), vec_real(2*N, 0.0);
            
            var_real = sim_lc[0][num].add_power_real(vec_real, pwr_spec_func(map));
            
            for (auto &&simv : sim_lc)
                var += simv[num].add_power(vec);
            var /= n_real;
            
            for (int i = 1; i < N; i++)
            {
                out << i << "   ";
                
                if (with_real)
                    out << vec_real[i]/var_real;
                
                out << "   " << vec[i]/var << std::endl;
            }
        }
        
        void PrintPwr(const std::vector<std::vector<std::vector<double>>> &saved_pwr_spec, const std::string &file)
        {
            for (int ii = 0, endii = restimes.size(); ii < endii; ii++)
            {
                std::stringstream ss;
                ss << ii;
                std::ofstream outf(add_prefix(prefix, file + "." + ss.str() + ".pwr_spec").c_str());
                for (int i = 1, end = sim_lc[0][ii].size()/2; i < end; i++)
                {
                    outf << i;
                    
                    for (int j = 0, endj = saved_pwr_spec[ii].size(); j < endj; j++)
                        outf << "\t" << saved_pwr_spec[ii][j][i];
                    outf << std::endl;
                }
                outf.close();
            }
        }
        
//         double KS(int j, int k)
//         {
//             //spline::spline spl(simulated_sorted_fluxes[j], cdf);
//             double ks = 0;
//             for (int i = 0, end = sorted_fluxes[k].size(); i < end; i++)
//             {
//                 //double s = std::abs(spl.Splint(sorted_flux[i]) - cdf[i]);
//                 double s = std::abs(spline::LinearInterp(sorted_fluxes[k][i], simulated_sorted_fluxes[k][j], cdfs[k]) - cdfs[k][i]);
//                 if (s > ks)
//                     ks = s;
//             }
//             
//             //ks += std::pow(std::abs((smooth_max - pre_max_flux[j])/(*(sorted_flux.end()-1))), 1.2);
//             //ks += std::abs((*(sorted_flux.end()-1) - pre_max_flux[j])/(*(sorted_flux.end()-1)));
//             //ks += (std::abs(*(sorted_flux.end()-1) - *(simulated_sorted_fluxes[j].end()-1)))/std::abs(*(sorted_flux.end()-1));
//             //10^6*(4*(0.7)/sqrt(20)/9.55)^6
//             
//             return ks;
//         }
        
        double Avg(int j)
        {
            double avg_diff = 0;
            //double std_diff = 0;

            for (auto &&sim_flux : simulated_sorted_fluxes[j])
            {
                avg_diff += sim_flux;
                /*for (auto &&flx : sim_flux[j])
                {
                    avg_diff += flx;
                }*/
            }
            //for (auto &&sim_flux : simulated_sorted_fluxes)
            //{
            //     for (auto &&flx : sim_flux[j])
            //    {
            //        std_diff += std::pow((flx - avg_diff/N_data),2.0);
            //    }    
                
            //}
            
            return std::abs((avg_diff/N_data - data_avg) / data_avg)/15.; // + std::abs((std::sqrt(std_diff/N_data) - 1.41115) / 1.41115)/50.;
        }
        
        double Median(int j)
        {
            double median = find_median(simulated_sorted_fluxes[j]);
            
            return std::abs((median - data_median)/data_median);
        }
        
        double Peak(int j)
        {
            double p = 0;

            for (auto &&sim_flux : simulated_sorted_fluxes[j])
            {
                if (sim_flux > p)
                {
                    p = sim_flux;
                }
                
                /*for (auto &&flx : sim_flux[j])
                {
                    if (flx > p){
                    	p = flx;
                    }
                }*/
            }
            
            if (std::abs(p - 9.5) > 2.5) 
            {
                return p/9.5;
            } 
            else 
            {
                return 0.0;
            }
        }      
        
        double Chi2(int j) const
        {
            double sum1 = 0.0;
            double sum2 = 0.0;
            int ii = 0;
            for (int i = 0, end = D.size(); i < end; i++)
                if (struct_func[i] < 128.0)
                {
                    ii++;
                    sum1 += SQ(std::log(D[i]/simulated_Ds[j][i]));
                }
                else
                	sum2 += SQ(std::log(D[i]/simulated_Ds[j][i]));
            
            return (sum1/(ii+3) + sum2/(ii+3)*3)*10.0;
        }

        template <typename G>
        void CalcOnlyFlux(int flux_index, const std::unordered_map<std::string, double> &map, G &generator)
        {
            for (int ii = 0, cnt = 0, endii = restimes.size(); ii < endii; ii++)
            {
                sim_lc[flux_index][ii].calc(generator, pwr_spec_func(map), convert_func(map, prefix), noise_func(map, prefix));
                
                std::vector<double> &sim_flux = sim_lc[flux_index][ii];
                
                for (auto &&f : sim_flux)
                {
                    simulated_sorted_fluxes[flux_index][cnt++] = f;
                }
            }
            
            std::sort(simulated_sorted_fluxes[flux_index].begin(), simulated_sorted_fluxes[flux_index].end());
        }
        
        template <typename G>
        void CalcStructFunc(int flux_index, const std::unordered_map<std::string, double> &map, G &)
        {
            simulated_Ds[flux_index] = struct_func(sim_lc[flux_index], flux_index);
        }
        
        template <typename G>
        void CalcFlux(int flux_index, const std::unordered_map<std::string, double> &map, G &generator)
        {
            CalcOnlyFlux(flux_index, map, generator);
            CalcStructFunc(flux_index, map, generator);
        }
        
        void CalcFlux(int NThreads, const std::unordered_map<std::string, double> &map)
        {
            assert(NThreads <= n_real);

            std::vector<std::thread> threads;
            //std::mutex mutex;
            std::vector<std::mt19937_64> engines;
            make_generators(engines, NThreads);
            
            for (int t = 0; t < NThreads; t++)
            {
                threads.emplace_back([&](int threadNum)
                                    {
                                        for (int i = threadNum; i < n_real; i += NThreads)
                                        {
                                            CalcFlux(i, map, engines[threadNum]);
                                        }
                                    }, t);
            }
            
            for (auto &&thr : threads)
            {
                thr.join();
            }
        }
    
        ~SgrAVar() 
        {
            fftw_cleanup();
        }
    };
        
    template <typename pwr_spec_func, typename convert_func1, typename convert_func2, typename noise_func1, typename noise_func2>
    class SyncData
    {
    private:
        double data_avg;
        double data_var;
        double sim_avg;
        double sim_var;
        int N_data;
        int n_real;
        double samp;
        std::string prefix1; 
        std::string prefix2;
        std::vector<std::vector<double>> restimes;
        std::vector<std::vector<double>> flux_diffs;
        std::vector<std::vector<tk_draw>> sim_lc;
        
    public:
        SyncData(const std::unordered_map<std::string, double> &options_map, const std::vector<std::string> &files, const std::string &prefix1 = "", const std::string prefix2 = "") : n_real(get_map_value(options_map, "n_real")), samp(get_map_value(options_map, "samp")), prefix1(prefix1), prefix2(prefix2), restimes(files.size()), flux_diffs(files.size()), sim_lc(get_map_value(options_map, "n_real"))
        {
            N_data = 0;
            
            for (int i = 0, end = files.size(); i < end; i++)
            {
                EnterData(files[i], [&](const std::vector<double> &vec)
                {
                    if (vec.size() == 4)
                    {
                        restimes[i].push_back(vec[0]);
                        double flux1 = vec[1];
                        double flux2 = vec[3];
                        flux_diffs[i].push_back(flux1 - flux2);
                    }
                    else
                    {
                        std::runtime_error("sync_data:  input file in unknown format.");
                    }
                });
                
                double val = restimes[i][0];
                for (auto &&res : restimes[i])
                    res -= val;
                
                for (auto &&lc : sim_lc)
                {
                    lc.emplace_back(restimes[i], samp);
                }
                
                N_data += restimes[i].size();
            }
            
            find_var(flux_diffs, data_var, data_avg, N_data);
        }
        
        template <typename G>
        void CalcOnlyFlux(int flux_index, const std::unordered_map<std::string, double> &map, G &generator)
        {
            for (auto &&lc : sim_lc[flux_index])
            {
                lc.calc_diff(generator, pwr_spec_func(map), convert_func1(map, prefix1), convert_func2(map, prefix2), noise_func1(map, prefix1), noise_func2(map, prefix2));
            }
            
            find_var(sim_lc[flux_index], sim_var, sim_avg, N_data);
        }
        
        template <typename G>
        void CalcStructFunc(int, const std::unordered_map<std::string, double> &, G &) {}
        
        template <typename G>
        void CalcFlux(int flux_index, const std::unordered_map<std::string, double> &map, G &generator)
        {
            CalcOnlyFlux(flux_index, map, generator);
            CalcStructFunc(flux_index, map, generator);
        }
        
        double Avg(int flux_index)
        {
            return std::abs((sim_avg-data_avg)/data_avg);
        }
        
        double Chi2(int flux_index)
        {
            return std::abs((sim_var-data_var)/data_var);
        }
        
        int NReal() const {return n_real;}
        int Size() const {return restimes.size();}
    };
    
    template <bool add_dist = true, typename P, typename... S>
    void ABC (const std::tuple<S*...> &sgra, double tol, int N, int M, int NThreads, P &&pr, const std::string &file = "run", const unsigned char &flag = 0x00)
    {
        apply_tuple (sgra, [&](auto &&s)
        {
            assert(NThreads <= s->NReal());
        });
        
        std::mutex mutex;
        std::vector<std::unordered_map<std::string, double>> maps(NThreads);
        std::vector<std::function<double(int i, const std::vector<double> &)>> distances;
        std::vector<std::mt19937_64> engines;
        make_generators(engines, NThreads);
        
        distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
        {
            std::unordered_map<std::string, double> &map = maps[i];
            pr(map, vec);
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcOnlyFlux(i, map, engines[i]);

		//return s->Avg(i);
                return 0.0;
            });
        });
        
//             distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
//             {
//                 return 0.0; //Peak(i);
//             });
        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            //std::unordered_map<std::string, double> &map = maps[i];
            //pr(map, vec);
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcStructFunc(i, maps[i], engines[i]);
                return s->Chi2(i);
            });
        });
        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            std::normal_distribution<double> dist;
            //std::normal_distribution<double> dist2;
            std::unordered_map<std::string, double> &map = maps[i];
            
            double ratio_data = 0.5*dist(engines[i]) + 12.4;
            //double offset_data = 0.08*dist2(engines[i]) - 1.72;
            //double flux_corr = (offset_data-map["offset"])/ratio_data + 0.149;
	    //if (flux_corr <= 0.0 )
	    //    flux_corr = 0.0000001;		
	    double flux_corr = 0.150;
	    double ratio = std::exp((std::log(flux_corr)-map["vlt_mu"]+std::log(9.63829))*map["sig"]/map["vlt_sig"] + map["mu"])/flux_corr;
	    double ans = (ratio-ratio_data)/0.5;
                                   
            return ans*ans/50.;
        });
        
        if(!bool(flag & SMC))
        {
            ABC::ABC(tol, N, M, pr.size(), NThreads, engines[0], mutex, distances,
            [&](int K, const std::vector<std::vector<double>> &distances, const std::vector<double> &weights, const std::vector<std::vector<double>> &points)
            {
                int N = distances[0].size();
                std::unordered_map<std::string, double> map;
                
                std::ofstream fout(file + ".particle");

                //fout << std::setw(16) << "distance" 
                fout << std::setw(16) << "weight";
                for (auto &&elem : pr.params_ref())
                    fout << std::setw(16) << elem;
                fout << std::endl;
                for (int i = 0; i < N; i++)
                {
                    pr(map, points[i]);
                    //fout << std::setw(16) << std::setprecision(10) << *(distances.end()-1)[i];
                    fout << std::setw(16) << std::setprecision(10) << weights[i];
                    for (auto && name : pr.params_ref())
                        fout << std::setw(16) << std::setprecision(10) << map[name];
                    fout << std::endl;
                }
                
                static int iter = 0;
                static std::ofstream out1(file + ".chi2_distances");
                out1 << std::setw(6) << iter;
                for (auto &&elem : distances[0])
                {
                    out1 << std::setw(16) << std::setprecision(10) << elem;
                }
                out1 << std::endl;
                
                static std::ofstream out2(file + ".avg_distances");
                out2 << std::setw(6) << iter;
                for (auto &&elem : distances[1])
                {
                    out2 << std::setw(16) << std::setprecision(10) << elem;
                }
                out2 << std::endl;
                
                static std::ofstream outt(file + ".distances");
                outt << std::setw(6) << iter;
                for (auto &&elem : *(distances.end()-1))
                {
                    outt << std::setw(16) << std::setprecision(10) << elem;
                }
                outt << std::endl;
                
                static std::ofstream outa(file + ".acceptance");
                if (K > 0)
                {
                    outa << std::setw(6) << iter;
                    outa << std::setw(16) << std::setprecision(10) << double(N)/double(K) << std::endl;
                }
                iter++;
            },
            (!bool(flag & SEP_DIST)) ? ABC::ADD_DIST : ABC::SEP_DIST, file, bool(flag & RESUME));
        }
        else
        {
            ABC::SMC_ABC(tol, N, M, pr.size(), NThreads, engines[0], mutex, distances,
            [&](double pacc, const std::vector<std::vector<double>> &distances, const std::vector<std::vector<double>> &points)
            {
                int N = distances[0].size();
                std::unordered_map<std::string, double> map;
                
                std::ofstream fout(file + ".particle");

                //fout << std::setw(16) << "distance" 
                fout << std::setw(16) << "weight";
                for (auto &&elem : pr.params_ref())
                    fout << std::setw(16) << elem;
                fout << std::endl;
                for (int i = 0; i < N; i++)
                {
                    pr(map, points[i]);
                    //fout << std::setw(16) << std::setprecision(10) << *(distances.end()-1)[i];
                    fout << std::setw(16) << std::setprecision(10) << 1.0;
                    for (auto && name : pr.params_ref())
                        fout << std::setw(16) << std::setprecision(10) << map[name];
                    fout << std::endl;
                }
                
                static int iter = 0;
                static std::ofstream out1(file + ".chi2_distances");
                out1 << std::setw(6) << iter;
                for (auto &&elem : distances[0])
                {
                    out1 << std::setw(16) << std::setprecision(10) << elem;
                }
                out1 << std::endl;
                
                static std::ofstream out2(file + ".avg_distances");
                out2 << std::setw(6) << iter;
                for (auto &&elem : distances[1])
                {
                    out2 << std::setw(16) << std::setprecision(10) << elem;
                }
                out2 << std::endl;
                
                static std::ofstream outt(file + ".distances");
                outt << std::setw(6) << iter;
                for (auto &&elem : *(distances.end()-1))
                {
                    outt << std::setw(16) << std::setprecision(10) << elem;
                }
                outt << std::endl;
                
                static std::ofstream outa(file + ".acceptance");
                {
                    outa << std::setw(6) << iter;
                    outa << std::setw(16) << std::setprecision(10) << pacc << std::endl;
                }
                iter++;
            },
            (!bool(flag & SEP_DIST)) ? ABC::ADD_DIST : ABC::SEP_DIST, file, bool(flag & RESUME));
        }
        
        std::ofstream out(file + ".info");
        
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "n_real:" << std::setprecision(10) << n_real << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "samp:" << std::setprecision(10) << samp << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "lag_bin_size:" << std::setprecision(10) << struct_func.LagBinSize() << std::endl;
        
        out << "\nPrior Info:\n\n" << pr.info() << std::endl;
        out.close();
    }
        
#ifdef USE_MPI

    template <bool add_dist = true, typename P, typename... S>
    void ABC_MPI (const std::tuple<S*...> &sgra, double tol, int N, int M, P &&pr, const std::string &file = "run", bool resume = false)
    {
        std::unordered_map<std::string, double> map;
        std::vector<std::function<double(int i, const std::vector<double> &)>> distances;
        std::vector<std::uint32_t> seeds;
        std::uint32_t seed;
        
        int rank, numtasks;
        MPI_Comm_size(MPI_COMM_WORLD,&numtasks); 
        MPI_Comm_rank(MPI_COMM_WORLD,&rank);
        
        if (rank == 0)
            seeds = make_seeds(numtasks);
        
        MPI_Barrier(MPI_COMM_WORLD);
        MPI_Scatter (&seeds[0], 1, MPI_UNSIGNED, &seed, 1, MPI_UNSIGNED, 0, MPI_COMM_WORLD);
        
        std::mt19937_64 engine(seed);
        
        distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
        {
            pr(map, vec);
            
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcOnlyFlux(i, map, engine);
                //return s->Avg(i);
                return 0.0;
            });
        });
        
//             distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
//             {
//                 return 0.0; //Peak(i);
//             });

        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            //std::unordered_map<std::string, double> &map = maps[i];
            //pr(map, vec);
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcStructFunc(i, map, engine);
                return s->Chi2(i);
            });
        });
       
	distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
        std::normal_distribution<double> dist;
        //std::unordered_map<std::string, double> &map = maps[i];

        double ratio_data = 0.5*dist(engine) + 12.4;
	double flux_corr = 0.150;
	double ratio = std::exp((std::log(flux_corr)-map["vlt_mu"]+std::log(9.63829))*map["sig"]/map["vlt_sig"] + map["mu"])/flux_corr;
        double ans = (ratio-ratio_data)/0.5;

        return ans*ans/50.;
        });
 
        ABC::ABC_MPI(tol, N, M, pr.size(), engine, distances,
            [&](int K, const std::vector<std::vector<double>> &distances, const std::vector<double> &weights, const std::vector<std::vector<double>> &points)
            {
                int N = distances[0].size();
                std::unordered_map<std::string, double> map;
                
                std::ofstream fout(file + ".particle");

                //fout << std::setw(16) << "distance" 
                fout << std::setw(16) << "weight";
                for (auto &&elem : pr.params_ref())
                    fout << std::setw(16) << elem;
                fout << std::endl;
                for (int i = 0; i < N; i++)
                {
                    pr(map, points[i]);
                    //fout << std::setw(16) << std::setprecision(10) << *(distances.end()-1)[i];
                    fout << std::setw(16) << std::setprecision(10) << weights[i];
                    for (auto && name : pr.params_ref())
                        fout << std::setw(16) << std::setprecision(10) << map[name];
                    fout << std::endl;
                }
                
                static int iter = 0;
                static std::ofstream out1(file + ".chi2_distances");
                out1 << std::setw(6) << iter;
                for (auto &&elem : distances[0])
                {
                    out1 << std::setw(16) << std::setprecision(10) << elem;
                }
                out1 << std::endl;
                
                static std::ofstream out2(file + ".avg_distances");
                out2 << std::setw(6) << iter;
                for (auto &&elem : distances[1])
                {
                    out2 << std::setw(16) << std::setprecision(10) << elem;
                }
                out2 << std::endl;
                
                static std::ofstream outt(file + ".distances");
                outt << std::setw(6) << iter;
                for (auto &&elem : *(distances.end()-1))
                {
                    outt << std::setw(16) << std::setprecision(10) << elem;
                }
                outt << std::endl;
                
                static std::ofstream outa(file + ".acceptance");
                if (K > 0)
                {
                    outa << std::setw(6) << iter;
                    outa << std::setw(16) << std::setprecision(10) << double(N)/double(K) << std::endl;
                }
                iter++;
            },
            add_dist ? ABC::ADD_DIST : ABC::SEP_DIST, file, resume
        );
        
        std::ofstream out(file + ".info");
        
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "n_real:" << std::setprecision(10) << n_real << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "samp:" << std::setprecision(10) << samp << std::endl;
//         out << std::setw(16) << std::setiosflags (std::ios::left) << "lag_bin_size:" << std::setprecision(10) << struct_func.LagBinSize() << std::endl;
        
        out << "\nPrior Info:\n\n" << pr.info() << std::endl;
        out.close();
    }

#endif
        
    template <bool add_dist = true, typename P, typename... S>
    void ABC_Data (const std::tuple<S*...> &sgra, int N, int NThreads, P &&pr, const std::string &file = "run", bool use_cov=true)
    {
        apply_tuple (sgra, [&](auto &&s)
        {
            assert(NThreads <= s->NReal());
        });
        
        std::mutex mutex;
        std::vector<std::unordered_map<std::string, double>> maps(NThreads);
        std::vector<std::function<double(int i, const std::vector<double> &)>> distances;
        std::vector<std::mt19937_64> engines;
        make_generators(engines, NThreads);
        
        distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
        {
            std::unordered_map<std::string, double> &map = maps[i];
            pr(map, vec);
            
            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcOnlyFlux(i, map, engines[i]);
                //return s->Avg(i);
                return 0.0;
            });
        });
        
//             distances.emplace_back([&](int i, const std::vector<double> &vec) -> double
//             {
//                 return 0.0; //Peak(i);
//             });
        
        distances.emplace_back([&](int i, const std::vector<double> &) -> double
        {
            //std::unordered_map<std::string, double> &map = maps[i];
            //pr(map, vec);

            return sum_tuple(sgra, [&](auto &s)
            {
                s->CalcStructFunc(i, maps[i], engines[i]);
                return s->Chi2(i);
            });
        });
        
        std::vector<std::vector<std::vector<std::vector<double>>>> saved_fluxes(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        for_tuple(sgra, [&](auto &s, int i)
        {
            saved_fluxes[i].resize(s->Size());
        });
        std::vector<std::vector<std::vector<double>>> saved_Ds(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        
        ABC::ABC_Data(N, NThreads, engines[0], mutex, distances,
            [&](int threadNum)
            {
                for_tuple(sgra, [&](auto &s, int ii)
                {
                    //for (int i = 0, end = restimes.size(); i < end; i++)
                    //    saved_fluxes[i].emplace_back(sim_lc[threadNum][i]);
                    //saved_Ds.emplace_back(simulated_Ds[threadNum]);
                    for (int i = 0, end = s->Size(); i < end; i++)
                        saved_fluxes[ii][i].emplace_back(s->Simulated_Fluxes(threadNum,i));
                    saved_Ds[ii].emplace_back(s->Simulated_Ds(threadNum));
                });
            },
            add_dist ? ABC::ADD_DIST : ABC::SEP_DIST, file, use_cov
        );
        
        for_tuple(sgra, [&](auto &s, int ii)
        {
            s->PrintFlux(saved_fluxes[ii], file);
            s->PrintStruct(saved_Ds[ii], file);
        });
        
        std::cout << "finished making files." << std::endl;
    }
    
    template <bool add_dist = true, typename P, typename... S>
    void ABC_Ran (const std::tuple<S*...> &sgra, int N, int NThreads, P &&pr, const std::string &file = "run")
    {
        apply_tuple (sgra, [&](auto &&s)
        {
            assert(NThreads <= s->NReal());
        });
        
        std::mutex mutex;
        std::vector<std::unordered_map<std::string, double>> maps(NThreads);
        std::vector<std::mt19937_64> engines;
        make_generators(engines, NThreads);
        
        std::vector<std::vector<std::vector<std::vector<double>>>> saved_fluxes(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        std::vector<std::vector<std::vector<std::vector<double>>>> saved_pwr_spec(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        for_tuple(sgra, [&](auto &s, int i)
        {
            saved_fluxes[i].resize(s->Size());
            saved_pwr_spec[i].resize(s->Size());
        });
        std::vector<std::vector<std::vector<double>>> saved_Ds(std::tuple_size<typename std::decay<decltype(sgra)>::type>::value);
        
        ABC::ABC_Ran(N, NThreads, engines[0], mutex,
            [&](int threadNum, const std::vector<double> &vec)
            {
                for_tuple(sgra, [&](auto &s, int ii)
                {
                    std::unordered_map<std::string, double> &map = maps[threadNum];
                    pr(map, vec);
                    s->CalcOnlyFlux(threadNum, map, engines[threadNum]);
                    s->CalcStructFunc(threadNum, map, engines[threadNum]);
                    
                    mutex.lock();
                    for (int i = 0, end = s->Size(); i < end; i++)
                    {
                        saved_fluxes[ii][i].emplace_back(s->Simulated_Fluxes(threadNum,i));
                        saved_pwr_spec[ii][i].emplace_back(s->GetPwrSpec(threadNum, i, map, engines[threadNum]));
                    }
                    saved_Ds[ii].emplace_back(s->Simulated_Ds(threadNum));
                    mutex.unlock();
                });
                
                return true;
            },
            file
        );
        
        for_tuple(sgra, [&](auto &s, int ii)
        {
            s->PrintFlux(saved_fluxes[ii], file);
            s->PrintStruct(saved_Ds[ii], file);
            s->PrintPwr(saved_pwr_spec[ii], file);
        });
        
        std::cout << "finished making files." << std::endl;
    }

}

#endif
