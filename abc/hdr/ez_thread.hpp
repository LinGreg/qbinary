#ifndef __EZ_THREAD_HPP__
#define __EZ_THREAD_HPP__

#include <thread>
#include <mutex>
#include <vector>

#define ez_thread(threadnum, ...) \
__ez_thread_func__(__VA_ARGS__) = [&](size_t threadnum) \

#define ez_thread_for(threadnum, NThreads, iter, ...) \
__ez_thread_for_func__(NThreads, __VA_ARGS__) = [&](size_t threadnum, size_t iter) \

#define ez_thread_do(threadnum, NThreads, iter, ...) \
__ez_thread_do_func__(NThreads, __VA_ARGS__) = [&](size_t threadnum, size_t iter) \

struct __ez_thread__
{
    size_t NThreads;
    
    template<typename T>
    inline void operator = (T&& func)
    {
        std::vector<std::thread> threads;
                
        for (size_t t = 0; t < NThreads; t++)
        {
            threads.emplace_back(func, t);
        }
        
        for (auto &&thr : threads)
            thr.join();
    }
};

struct __ez_thread_for__
{
    size_t NThreads;
    size_t cont_num;
    
    template<typename T>
    inline void operator = (T&& func)
    {
        std::vector<std::thread> threads;
                
        for (size_t t = 0; t < NThreads; t++)
        {
            threads.emplace_back([&](size_t thread_num)
            {
                for (size_t i = thread_num; i < cont_num; i+=NThreads)
                {
                    func(thread_num, i);
                }
            }, t);
        }
        
        for (auto &&thr : threads)
            thr.join();
    }
};

struct __ez_thread_do__
{
    size_t NThreads;
    size_t cont_num;
    
    template<typename T>
    inline void operator = (T&& func)
    {
        std::vector<std::thread> threads;
        std::mutex __mutex__;
        size_t k = NThreads;
        
        for (size_t t = 0; t < NThreads; t++)
        {
            threads.emplace_back([&](size_t thread_num)
            {
                size_t j = thread_num;
                
                do
                {
                    func(thread_num, j);
                    
                    __mutex__.lock();
                    j = k++;
                    __mutex__.unlock();
                }
                while(j < cont_num);
            }, t);
        }
        
        for (auto &&thr : threads)
            thr.join();
    }
};

constexpr __ez_thread__ __ez_thread_func__(size_t NThreads)
{
    return {NThreads};
}

constexpr __ez_thread_for__ __ez_thread_for_func__(size_t NThreads, size_t N)
{
    return {NThreads, N};
}

constexpr __ez_thread_do__ __ez_thread_do_func__(size_t NThreads, size_t N)
{
    return {NThreads, N};
}

#endif
