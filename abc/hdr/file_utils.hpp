#ifndef FILE_UTILS
#define FIEL_UTILS

#include <vector>
#include <utility>
#include <type_traits>
#include <fstream>

namespace file_utils
{
    namespace is_vector_details
    {
        template<typename T>
        struct is_pair : std::false_type {};
        template<typename T1, typename T2>
        struct is_pair<std::pair<T1, T2>> : std::true_type {};
        
        template<typename T>
        struct is_vector : std::false_type {};
        template<typename T>
        struct is_vector<std::vector<T>> : std::true_type {};
    }
    
    template<typename T>
    struct is_vector : public is_vector_details::is_vector<typename std::decay<T>::type> {};
    
    template<typename T>
    struct is_pair : public is_vector_details::is_pair<typename std::decay<T>::type> {};
    
    template<typename T>
    constexpr std::ostream & operator << (std::ostream &out, std::vector<T> &vec)
    {
        if (vec.size() == 0)
            return out << "[]";
        
        out << "[" << vec[0];
        for (int i = 1, end = vec.size(); i < end; i++)
        {
            out << ", " << vec[i];
        }
        
        return out << "]"; 
    }
    
    namespace data_details
    {
        inline void save_data(std::ofstream &){}
        
        template<typename T>
        inline typename std::enable_if<!is_vector<T>::value>::type
        save_datum(std::ofstream &out, T &datum)
        {
            out.write((char *)&datum, sizeof(typename std::decay<T>::type));
        }
        
        template<typename T>
        inline typename std::enable_if<is_vector<T>::value>::type
        save_datum(std::ofstream &out, T &datum)
        {
            for (auto &a : datum)
                save_datum(out, a);
        }
        
        template<typename T,typename... U>
        inline void save_data(std::ofstream &out, T& datum, U&... data)
        {
            save_datum(out, datum);
            save_data(out, data...);
        }
        
        void read_data(std::ifstream &){}
        
        template<typename T>
        inline typename std::enable_if<!is_vector<T>::value>::type
        read_datum(std::ifstream &in, T &datum)
        {
            in.read((char *)&datum, sizeof(typename std::decay<T>::type));
        }
        
        template<typename T>
        inline typename std::enable_if<is_vector<T>::value>::type
        read_datum(std::ifstream &in, T &datum)
        {
            for (auto &a : datum)
                read_datum(in, a);
        }
        
        template<typename T, typename... U>
        inline void read_data(std::ifstream &in, T& datum, U&... data)
        {
            read_datum(in, datum);
            read_data(in, data...);
        }
    }
    
    template<typename... T>
    inline void save_data_file(const std::string &file, T&... data)
    {
        std::ofstream out(file, std::ofstream::out | std::ofstream::binary);
        data_details::save_data(out, data...);
    }
    
    template<typename... T>
    inline void read_data_file(const std::string &file, T&... data)
    {
        std::ifstream in(file, std::ifstream::in | std::ifstream::binary);
        data_details::read_data(in, data...);
    }
}

#endif
