CC= `which g++` 
MCC= `which mpic++`
DEBUG= -Wall -std=c++14
CFLAGS= -O3 -Wall -std=c++14
INC= -I/opt/local/include -Ihdr
CLINC = -DUSE_VEX_CL -I/opt/local/include -I/usr/local/include -Ihdr
OBJS = src/main.o
EXOBJS = src/example.o
MN_ROOT = /home/gmartine/MultiNest_v3.10_CMake/multinest
LIBS = -lpthread -lfftw3 -L/opt/local/lib
CLLIBS= -lpthread -lfftw3 -lboost_system -lboost_timer -L/opt/local/lib -lOpenCL -lboost_system -lboost_timer -L/opt/local/lib 
PNAME = red_n

.PHONY: default
default: $(PNAME)

src/%.o: src/%.cpp
	$(CC) $(CFLAGS) $(INC) -c $< -o $@

precompile : $(OBJS)

example: $(EXOBJS)
	$(CC) -o example $(EXOBJS) $(LIBS) $(CFLAGS)

$(PNAME): $(OBJS) 
	$(CC) -o $(PNAME) $(OBJS) $(LIBS) $(CFLAGS) 

cl:
	$(CC) $(CFLAGS) $(CLINC) -c src/main.cpp -o src/main.o
	$(CC) -o $(PNAME) $(OBJS) $(CLLIBS)
	
cl_all:
	$(CC) $(CFLAGS) $(CLINC) -DUSE_VEX_CL2 -c src/main.cpp -o src/main.o
	$(CC) -o $(PNAME) $(OBJS) $(CLLIBS)
	
mpi:
	$(MCC) $(CFLAGS) $(INC) -DUSE_MPI -c src/main.cpp -o src/main.o
	$(MCC) -o $(PNAME) $(OBJS) $(LIBS)
	
like:
	$(CC) $(CFLAGS) $(INC) -I$(MN_ROOT)/include -c src/main.cpp -o src/main.o
	$(CC) -o $(PNAME) $(OBJS) $(LIBS) -lmultinest -L$(MN_ROOT)/lib
	
debug:
	$(CC) $(DEBUG) $(INC) -c src/main.cpp -o src/main.o
	$(CC) -o $(PNAME) $(OBJS) $(LIBS) $(DEBUG) 

clean:
	rm -f *.o *.mod *.d *.pc *.obj src/*.o $(PNAME) example

