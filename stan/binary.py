
import os
import re
import math
import pystan
import numpy as np
import sys
from matplotlib import interactive
from matplotlib import rcParams
import corner
import constants

os.environ["CC"] = "g++ --std=c++14 -g -DNDEBUG"
interactive(True)

def parseStanCode2(code):
    hdr_str = """
#include <stan/math.hpp>
#include "{0}/code/efit_utils.hpp"
#include "{0}/code/nstar_function.hpp"
#include "{0}/code/eccentric_anomaly.hpp"
#include "{0}/code/benchmark.hpp"

using namespace efit;
using namespace efit::functions;
""".format(str(os.path.realpath(os.getcwd())))

    func_str="""
BoundMeanAnomaly(const T0__& e, const T1__& dt, std::ostream* pstream__) 
{
    typedef typename boost::math::tools::promote_args<T0__, T1__>::type fun_scalar_t__;
    typedef fun_scalar_t__ fun_return_scalar_t__;
    
    //auto temp = BoundEccentricAnomaly<fun_scalar_t__>(e, dt);
    fun_scalar_t__ temp = bound_eccentric_anomaly_autodiff(e, dt);

    return temp;//stan::math::promote_scalar<fun_return_scalar_t__>(temp);
}

"""
    
    multi_str="for_multi_thread(thread_i, thread_start, (thread_stop+1), thread_N)"
    par_str="};{"
    
    idx = code.index("<stan/model/model_header.hpp>") + 29
    
    first = code.index("BoundMeanAnomaly(")
    second = code.index("struct BoundMeanAnomaly_functor__")
    third = code.index("for (int thread_i = thread_start; thread_i <= thread_stop; ++thread_i)")
    fourth = code.index("stan::math::assign(thread_N, thread_N);")
    
    return code[0:idx] + hdr_str + code[idx:first] + func_str + code[second:third] + multi_str + code[third+70:fourth] + par_str + code[fourth+39:-1]

def parseStanCode(code):
    hdr_str = """
#include <stan/math.hpp>
#include "{0}/code/efit_utils.hpp"
#include "{0}/code/nstar_function.hpp"
#include "{0}/code/eccentric_anomaly.hpp"
#include "{0}/code/benchmark.hpp"
#include "{0}/code/binary.hpp"
""".format(str(os.path.realpath(os.getcwd())))

    func_str="""
likelihood(const std::vector<Eigen::Matrix<T0__, Eigen::Dynamic,1> >& u,
               const Eigen::Matrix<T1__, Eigen::Dynamic,1>& hyper_u, std::ostream* pstream__) 
{
    typedef typename boost::math::tools::promote_args<T0__, T1__>::type fun_scalar_t__;
    typedef fun_scalar_t__ fun_return_scalar_t__;
    const static bool propto__ = true;
    (void) propto__;
        fun_scalar_t__ DUMMY_VAR__(std::numeric_limits<double>::quiet_NaN());
        (void) DUMMY_VAR__;  // suppress unused var warning

    int current_statement_begin__ = -1;
    
    static binary::Binary<fun_scalar_t__> binary("simcluster.dat");
    
    std::unordered_map<std::string, fun_scalar_t__> map;
    
    map["<logP>_hi"] = 10.0;
    map["<logP>_low"] = -10.0;
    map["<q>_hi"] = 1.0;
    map["<q>_low"] = 0.0;
    map["<e>_hi"] = 1.0;
    map["<e>_low"] = 0.0;
    map["Disp(logP)_hi"] = 2.0;
    map["Disp(logP)_low"] = -2.0;
    map["Disp(q)_hi"] = 2.0;
    map["Disp(q)_low"] = -2.0;
    map["Disp(e)_hi"] =  2.0;
    map["Disp(e)_low"] = -2.0;
    
    return binary(map, u, hyper_u);
}

"""

    dump_str="""
dumper(const std::vector<Eigen::Matrix<T0__, Eigen::Dynamic,1> >& u,
               const Eigen::Matrix<T1__, Eigen::Dynamic,1>& hyper_u, std::ostream* pstream__) 
{
    typedef typename boost::math::tools::promote_args<T0__, T1__>::type fun_scalar_t__;
    typedef int fun_return_scalar_t__;
    const static bool propto__ = true;
    (void) propto__;
        fun_scalar_t__ DUMMY_VAR__(std::numeric_limits<double>::quiet_NaN());
        (void) DUMMY_VAR__;  // suppress unused var warning

    int current_statement_begin__ = -1;
    
    std::unordered_map<std::string, fun_scalar_t__> map;
    
    map["<logP>_hi"] = 10.0;
    map["<logP>_low"] = -10.0;
    map["<q>_hi"] = 1.0;
    map["<q>_low"] = 0.0;
    map["<e>_hi"] = 1.0;
    map["<e>_low"] = 0.0;
    map["Disp(logP)_hi"] = 2.0;
    map["Disp(logP)_low"] = -2.0;
    map["Disp(q)_hi"] = 2.0;
    map["Disp(q)_low"] = -2.0;
    map["Disp(e)_hi"] =  2.0;
    map["Disp(e)_low"] = -2.0;
    
    binary::Dumper<fun_scalar_t__>(map, u, hyper_u);
    
    return 0;
}

"""
    
    multi_str="for_multi_thread(thread_i, thread_start, (thread_stop+1), thread_N)"
    par_str="};{"
    
    idx = code.index("<stan/model/model_header.hpp>") + 29
    
    first = code.index("likelihood(")
    second = code.index("struct likelihood_functor__")
    third = code.index("dumper(")
    fourth = code.index("struct dumper_functor__")
    
    return code[0:idx] + hdr_str + code[idx:first] + func_str + code[second:-1];# + dump_str + code[fourth:-1]


def get_array(a, i):
    return [a[j][i] for j in range(len(a))]

def remove_script_comments(text):
    '''remove comments'''
    def replacer(match):
        s = match.group(0)
        if s.startswith('#'):
            return ""
        else:
            return s
        
    pattern = re.compile(
        r'#.*?\n|\'(?:#.|[^#\'])*\'|"(?:#.|[^#"])*"',
        re.DOTALL | re.MULTILINE 
    )
    
    return re.sub(pattern, replacer, text[:])

def neatsplit(regex,string):
    return [x for x in re.split(regex,string) if x != '']

def enterData(file_name, names):
    """Enter data for a list of stars in "filenames".  The files must be named 
       file_name.points and file_name.rv"""

    starN = 0
    epochN = []
    star_big = 0
    times = []
    velocities = []
    errors = []
    m_ps = []
    
    time = []
    err = []
    vel = []
    with open(file_name, "r") as txt:
        for line in neatsplit('\n|\r', remove_script_comments(txt.read())):
            values = [val for val in neatsplit(" |\t|\r|\t|\n", line)]

            if len(values) == 3:
                try:
                    float(values[0])
                except ValueError:
                    names.append(values[0])
                    m_ps.append(float(values[1]))
                    
                    if len(time) > 0:
                        times.append(time)
                        errors.append(err)
                        velocities.append(vel)
                        epochN.append(len(time))
                        if len(time) > star_big:
                            star_big = len(time)
                        time = []
                        err = []
                        vel = []
                else:
                    time.append(float(values[0]))
                    vel.append(float(values[1]))
                    err.append(float(values[2]))
            else:
                print "nooooooooooooooooooooooooooooooooooooooooooo"
                exit(-1);
                
        if len(time) > 0:
            times.append(time)
            errors.append(err)
            velocities.append(vel)
            epochN.append(len(time))
            
        star_big = max(epochN)
        
        timestot = []
        velocitiestot = []
        errorstot = []
        for i in range(len(times)):
            times[i] += list(np.zeros(star_big - len(times[i])))
            velocities[i] += list(np.zeros(star_big - len(velocities[i])))
            errors[i] += list(np.zeros(star_big - len(errors[i])))
            timestot += times[i]
            velocitiestot += velocities[i]
            errorstot += errors[i]
                
            
            #times += [0]*(star_big - len(times))
            #errors += [0]*(star_big - len(errors))
            #velocities += [0]*(star_big - len(velocities))
    
    max_stars = len(epochN)

    efit_dat = {'starN' : max_stars}#,#len(times),
                #'epochN': epochN,
                #'star_big': star_big,
                #'times': times,
                #'velocities' : velocities,
                #'errors' : errors,
                #'m_ps' : m_ps}
    
    return efit_dat

def makeTriPlot(data, name, labels, title):
    '''Make a triangle plot with the labels "lables", title "title" and using
       data in the numpy array "data" outputed to "name".'''
    rcParams["font.size"] = 12#16
    rcParams["font.family"] = "sans-serif"
    rcParams["font.sans-serif"] = ["Computer Modern Sans"]
    rcParams["text.usetex"] = True
    #rcParams["text.latex.preamble"] = r"\usepackage{cmbright}"

    # Plot it.
    figure = corner.corner(data, labels=labels,
                        quantiles=[0.16, 0.5, 0.84],
                        show_titles=True, title_kwargs={"fontsize": 10})#12

    figure.gca().annotate(title,
                        xy=(1.0, 1.0), xycoords="figure fraction",
                        xytext=(-20, -10), textcoords="offset points",
                        ha="right", va="top")
    figure.savefig(name, dpi=300)
    
def binary_stan(data_file, pts_num, chains, output_file = ""):
    names =[]
    dat = enterData("simcluster.dat", names)
    
    sm_cpp = pystan.stanc(file='binary.stan')
    sm_cpp['cppcode'] = unicode(parseStanCode(sm_cpp['cppcode']));
    fc = open("temp.hpp", "w")
    fc.write(sm_cpp['cppcode'])
    fc.close()
        
    sm = pystan.StanModel(stanc_ret=sm_cpp, verbose=True)#(file='temp.stan')
    fit = sm.sampling(data=dat, iter=pts_num, chains=chains) #, verbose=True
    
    f1 = open("stan.summary", "w")
    f1.write(str(fit))
    f1.close()

    chains = fit.extract(permuted=True)
    
    #data = [chains["logqs_mu"], chains["logqs_sig"], chains["logPs_mu"], chains["logPs_sig"], chains["atanes_mu"], chains["atanes_sig"], chains["v_cms_mu"], chains["v_cms_sig"], chains["B"]]
    data = [chains["logqs_mu"], chains["logqs_sig"], chains["logPs_mu"], chains["logPs_sig"], chains["atanes_mu"], chains["atanes_sig"], chains["B"]]
    
    for i in xrange(len(names)):
        data += [[chains["Ps"][:, i]], chains["es"][:, i], chains["qs"][:, i], chains["cos_thetas"][:, i], chains["v_cms"][:, i], chains["phis"][:, i]]
        
    np.savetxt("stan.chains", np.transpose(data), delimiter='\t')
    
    makeTriPlot(np.transpose(data[0:9]), name = output_file + "_hyper_plot.png", title = "", labels = ["<atan(q)>", "2log(var(atan(q)))", "<log(P)>", "2log(var(log(P)))", "<atan(e)>", "2log(var(atan(e)))", "<v_{cm}>", "2log(var(v_{cm}))", "B"])
    
    for i in xrange(len(names)):
        makeTriPlot(np.transpose(data[9:15+i*6]), name = output_file + "_" + names[i] + "_hyper_plot.png", title = "", labels = ["Period", "e", "q", "cos(\\theta)", "v_{cm}", "\phi"])
        
    return fit
    
def main(argv):
    binary_stan("simcluster.dat", 40000, 1)

    #data = np.transpose([chains["k"], chains["mu"], chains["sig"], chains["noise"], chains["A"][:, 0], chains["phase"][:, 0], chains["period"][:,0], chains["shift"][:,0], chains["A"][:,1], chains["phase"][:,1], chains["period"][:,1], chains["shift"][:,1]])
    "simcluster.dat"
    #chains = fit.extract(permuted=True)
    
    #data = np.transpose([chains["k"], chains["mu"], chains["sig"], chains["noise"], chains["A"][:, 0], chains["phase"][:, 0], chains["period"][:,0], chains["shift"][:,0], chains["A"][:,1], chains["phase"][:,1], chains["period"][:,1], chains["shift"][:,1]])
    
    #np.savetxt("stan.chains", data, delimiter='\t')
    
    #makeTriPlot(data, name = "plot.png", title = "", labels = ["k", "mu", "sig", "noise", "A[0]", "phase[0]", "period[0]", "shift[0]", "A[1]", "phase[1]", "period[1]", "shift[1]"])
    
if __name__ == "__main__":
    main(sys.argv[1:])
