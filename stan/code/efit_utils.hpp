//  EFIT 6
//  *********************************************
///  \file efit_utils.hpp
///
///  \brief Global constants used throughout the code
///
///   In this header are utilizities that make life easier 
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez (gregory.david.martinez@gmail.com)
///          A. Hees (ahees@astro.ucla.edu)
///
///  \date 2016 May
///
///  *********************************************


#ifndef __CONSTANTS_HPP__
#define __CONSTANTS_HPP__

#include <tuple>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <type_traits>
#include <cassert>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <vector>
#include <functional>
#include <list>
#include <forward_list>
#include <deque>
#include <array>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/range.hpp>
//#include "utils/benchmark.hpp"

#define __EXPAND_TYPE__(...) __VA_ARGS__

namespace efit
{
    ///  \fn  inline double convertUTCToJYr(double time, int t0)
    ///  \brief transform UTC time (expressed in year) to Julian Year
         ///
         ///  \param[in] time input UTC time in year
         ///  \param[in] t0 input UTC reference time in year
    ///  \return the difference time - t0 in julian year
    ///
    ///  \warning NEED TO BE TESTED !
         ///

    inline double convertUTCToJYr(double time, int t0)
    {
        struct tm y1, y2, y3;
        double yr;
        double frac = std::modf(time, &yr);
        int n = int(yr) - t0;

        y1.tm_hour = y1.tm_min = y1.tm_mon = y1.tm_sec = y1.tm_yday = y1.tm_wday = 0;
        y1.tm_mday = 1;
        y1.tm_year = t0 - 1900;

        y2.tm_hour = y2.tm_min = y2.tm_mon = y2.tm_sec = y2.tm_yday = y2.tm_wday = 0;
        y2.tm_mday = 1;
        y2.tm_year = yr - 1900;
        
        y3.tm_hour = y3.tm_min = y3.tm_mon = y3.tm_sec = y3.tm_yday = y3.tm_wday = 0;
        y3.tm_mday = 1;
        y3.tm_year = yr - 1899;
        
        return (difftime(mktime(&y2), mktime(&y1)) + frac*difftime(mktime(&y2), mktime(&y3)))/3600.0/24.0/365.25;
    }
    
    ///  \fn  inline double convertUTCToMJD(double time, int t0)
    ///  \brief transform UTC time (expressed in year) to Modified Julian Day
         ///
         ///  \param[in] time input UTC time in year
    ///  \return the time converted into Modified Julian Days
    ///
    ///  \warning NEED TO BE TESTED !
    ///  \warning leapseconds neglected.
         ///
    
    inline double convertUTCToMJD(double time)
    {
        struct tm y1, y2, y3;
        double yr;
        double frac = std::modf(time, &yr);

        y1.tm_hour = y1.tm_min = y1.tm_sec = y1.tm_yday = y1.tm_wday = 0;
        y1.tm_mday = 17;
        y1.tm_mon = 10;
        y1.tm_year = -42;

        y2.tm_hour = y2.tm_min = y2.tm_mon = y2.tm_sec = y2.tm_yday = y2.tm_wday = 0;
        y2.tm_mday = 1;
        y2.tm_year = yr - 1900;
        
        y3.tm_hour = y2.tm_min = y2.tm_mon = y2.tm_sec = y2.tm_yday = y2.tm_wday = 0;
        y3.tm_mday = 1;
        y3.tm_year = yr - 1899;
        
        return (difftime(mktime(&y2), mktime(&y1)) + frac*difftime(mktime(&y2), mktime(&y3)))/3600.0/24.0;
    }
    
    ///  \fn  constexpr double SQ(double a)
    ///  \brief return the square of a double
         ///
         ///  \param[in] a double
    ///  \return the square of a
    ///

    template <typename T>
    constexpr auto SQ(T &&a) -> decltype(a*a) {return a*a;}

    ///  \fn  template <typename... T> inline auto zip(const T&... containers)
    ///  \brief to be added
         ///
         ///   \return to be added!
         ///
    template <typename... T>
    constexpr auto zip(T&&... containers) -> boost::iterator_range<boost::zip_iterator<decltype(boost::make_tuple(std::begin(containers)...))>>
    {
        return {
            boost::make_zip_iterator(boost::make_tuple(std::begin(containers)...)),
            boost::make_zip_iterator(boost::make_tuple(std::end(containers)...))};
    }
    
    ///  \fn  void EnterEFitData(const std::string &file, const std::function<void (const std::vector<double> &)> &func)
    ///  \brief enters data from a file
         ///
         ///  \param[file] the file containing the data
         ///  \param[func] a function that is applied to the inputed data (data inputed into vector).
    ///  \return void
    ///
    inline void EnterEFitData(const std::string &file, const std::function<void (const std::vector<double> &)> &func)
    {
        std::ifstream in(file.c_str());
        if (!in.is_open())
        {
            std::cout << "Cannot open " << file << std::endl;
            exit(-1);
        }
        
        std::string line;
        while (std::getline(in, line))
        {
            line = line.substr(0, line.find_first_of("#"));
            std::stringstream ss(line);
            std::vector<double> values;
            double val;
            
            while(ss >> val)
            {
                values.push_back(val);
            }
            
            func(values);
        }
    }
    
    template <typename T>
    struct find_result_of_func;
    
    template <typename ret, typename... args>
    struct find_result_of_func <ret (args...)>
    {
        typedef ret type;
    };
    
    template <typename ret, typename... args>
    struct find_result_of_func <ret (*)(args...)>
    {
        typedef ret type;
    };
    
    template <typename ret, typename... args>
    struct find_result_of_func <ret (&)(args...)>
    {
        typedef ret type;
    };
    
    template <typename ret, typename T, typename... args>
    struct find_result_of_func <ret (T::*)(args...)>
    {
        typedef ret type;
    };
    
//     template <typename ret, typename T, typename... args>
//     struct find_result_of_func <ret (T::)(args...)>
//     {
//         typedef ret type;
//     };
//     
//     template <typename ret, typename T, typename... args>
//     struct find_result_of_func <ret (T::&)(args...)>
//     {
//         typedef ret type;
//     };
    
    ///  \fn  template <typename... T> struct int_sequence
    ///  \brief create sequence of ints
    template<int...> struct int_sequence {};

    template<int N, int... Is> struct make_int_sequence
        : make_int_sequence<N-1, N-1, Is...> {};
    template<int... Is> struct make_int_sequence<0, Is...>
        : int_sequence<Is...> {};
       
    ///  \fn  template<class C, class Ret, class... Args>
    ///  \brief binds member function or object
    /// @{
    template<int>
    struct placeholder_template {};
    
    template<class C, class Ret, class... Args, int... Is>
    auto _bind_member_(C& c, Ret (C::*p)(Args...), int_sequence<Is...>) -> decltype(std::bind(p, c, placeholder_template<Is>{}...))
    {
        return std::bind(p, c, placeholder_template<Is>{}...);
    }

    template<class C, class Ret, class... Args>
    auto bind_member(C& c, Ret (C::*p)(Args...)) -> decltype(_bind_member_(c, p, make_int_sequence< sizeof...(Args) >{}))
    {
        return _bind_member_(c, p, make_int_sequence< sizeof...(Args) >{});
    }
    /// @}
    
    ///  \fn  template <typename... T> struct remove_all
    ///  \brief removes all qualifiers from type.
    template <typename T>
    struct remove_all
    {
        typedef typename std::remove_cv
        <
            typename std::remove_volatile
            <
                typename std::remove_const
                <
                    typename std::remove_reference
                    <
                        T
                    >::type
                >::type
            >::type
        >::type type;
    };
    
    ///  \fn  template <typename T> is_container
    ///  \brief value = true is T is container
    /// @{
    template <typename T>
    struct __is_container__
    {
            static const bool value = false;
            typedef T type;
    };

    template <typename T>
    struct __is_container__<std::vector<T>>
    {
            static const bool value = true;
            typedef T type;
    };

    template <typename T>
    struct __is_container__<std::set<T>>
    {
            static const bool value = true;
            typedef T type;
    };
    
    template <typename T>
    struct __is_container__<std::multiset<T>>
    {
            static const bool value = true;
            typedef T type;
    };

    template <typename T1, typename T2>
    struct __is_container__<std::map<T1, T2>>
    {
            static const bool value = true;
            typedef std::pair<T1, T2> type;
    };

    template <typename T1, typename T2>
    struct __is_container__<std::multimap<T1, T2>>
    {
            static const bool value = true;
            typedef std::pair<T1, T2> type;
    };
    
    template <typename T1, typename T2>
    struct __is_container__<std::unordered_map<T1, T2>>
    {
            static const bool value = true;
            typedef std::pair<T1, T2> type;
    };
    
    template <typename T1, typename T2>
    struct __is_container__<std::unordered_multimap<T1, T2>>
    {
            static const bool value = true;
            typedef std::pair<T1, T2> type;
    };

    template <typename T>
    struct __is_container__<std::unordered_set<T>>
    {
            static const bool value = true;
            typedef T type;
    };
    
    template <typename T>
    struct __is_container__<std::unordered_multiset<T>>
    {
            static const bool value = true;
            typedef T type;
    };

    template <typename T>
    struct __is_container__<std::deque<T>>
    {
            static const bool value = true;
            typedef T type;
    };
    
    template <typename T, size_t N>
    struct __is_container__<std::array<T, N>>
    {
            static const bool value = true;
            typedef T type;
    };
    
    template <typename T>
    struct __is_container__<std::list<T>>
    {
            static const bool value = true;
            typedef T type;
    };

    template <typename T>
    struct __is_container__<std::forward_list<T>>
    {
            static const bool value = true;
            typedef T type;
    };

    template <typename T>
    struct is_container
    {
            const static bool value = __is_container__<typename remove_all<T>::type>::value;
            typedef typename __is_container__<typename remove_all<T>::type>::type type;
    };
    ///@}
    
///  \fn  template <typename T> is_vector
    ///  \brief value = true is T is vector
    /// @{
    template <typename T>
    struct __is_vector__
    {
        static const bool value = false;
        typedef T type;
    };
    
    template <typename T>
    struct __is_vector__<std::vector<T>>
    {
        static const bool value = true;
        typedef T type;
    };
    
    template <typename T>
    struct is_vector
    {
        const static bool value = __is_vector__<typename remove_all<T>::type>::value;
        typedef typename __is_vector__<typename remove_all<T>::type>::type type;
    };
    /// @}
    
///  \fn  template <typename T> is_pair
    ///  \brief value = true is T is pair
    /// @{
    template <typename T>
    struct __is_pair__
    {
        const static bool value = false;
        typedef T first_type;
        typedef T second_type;
    };

    template <typename T1, typename T2>
    struct __is_pair__ <std::pair<T1, T2>>
    {
        const static bool value = true;
        typedef T1 first_type;
        typedef T2 second_type;
    };
                    
    template <typename T>
    struct is_pair
    {
        const static bool value = __is_pair__<typename remove_all<T>::type>::value;
        typedef typename __is_pair__<typename remove_all<T>::type>::first_type first_type;
        typedef typename __is_pair__<typename remove_all<T>::type>::second_type second_type;
    };
    /// @}
    
    ///  \fn  template <typename T> is_pair
    ///  \brief value = true is T is pair
    /// @{
    template <typename T>
    struct __is_init_list__
    {
        const static bool value = false;
        typedef T type;
    };

    template <typename T>
    struct __is_init_list__ <std::initializer_list<T>>
    {
        const static bool value = true;
        typedef T type;
    };
                    
    template <typename T>
    struct is_init_list
    {
        const static bool value = __is_pair__<typename remove_all<T>::type>::value;
        typedef typename __is_init_list__<typename remove_all<T>::type>::type type;
    };
    /// @}
    
///  \fn  operator << ()
    ///  \brief container operator overloads
    /// @{
    template <typename T>
    inline typename std::enable_if <is_container<T>::value, std::ostream &>::type
    operator << (std::ostream &out, const T &in)
    {
        if (in.size() == 0)
            return out << "[]";
        
        
        auto it = in.begin();
        auto end = in.end();
        out << "[" << *it;
        for (++it; it != end; ++it)
        {
            out << ", " << *it;
        }
        
        return out << "]";
    }
    
    template <typename T>
    inline typename std::enable_if <is_pair<T>::value, std::ostream &>::type
    operator << (T &&a, T &&b)
    {
        return T(a.first + b.first, a.second + b.second);
    }
    /// @}
    
    ///  \fn  template <typename T> apply
    ///  \brief unrolls a parameter pack and applies function to it.
    /// @{
    namespace apply_details
    {
        
        template<int n>
        struct apply_struct
        {
            template<typename F, typename T, typename... U, int... Is>
            constexpr static decltype(auto) apply_tuple(F &&func, T &&tup, int_sequence<Is...>, U&&... params)
            {
                return apply_struct<n-1>::apply(std::forward<F>(func), std::forward<U>(params)..., std::get<Is>(tup)...);
            }
        
            template<typename F, typename... T, typename... U>
            constexpr static decltype(auto) apply(F &&func, std::tuple<T...> &tup, U&&... params)
            {
                return apply_tuple(std::forward<F>(func), tup, make_int_sequence<sizeof...(T)>{}, std::forward<U>(params)...);
            }
            
            template<typename F, typename... T, typename... U>
            constexpr static decltype(auto) apply(F &&func, const std::tuple<T...> &tup, U&&... params)
            {
                return apply_tuple(std::forward<F>(func), tup, make_int_sequence<sizeof...(T)>{}, std::forward<U>(params)...);
            }
            
            template<typename F, typename... T, typename... U>
            constexpr static decltype(auto) apply(F &&func, std::tuple<T...> &&tup, U&&... params)
            {
                return apply_tuple(std::forward<F>(func), tup, make_int_sequence<sizeof...(T)>{}, std::forward<U>(params)...);
            }
            
            template<typename F, typename T, typename... U>
            constexpr static decltype(auto) apply(F &&func, T &&param, U&&... params)
            {
                return apply_struct<n-1>::apply(std::forward<F>(func), std::forward<U>(params)..., std::forward<T>(param));
            }
        };
        
        template<>
        struct apply_struct<0>
        {
            template<typename F, typename... T>
            constexpr static decltype(auto) apply(F&& func, T&&... params)
            {
                return func(std::forward<T>(params)...);
            }
        };
        
    }
    
    template<typename F, typename... T>
    constexpr decltype(auto) apply(F&&func, T&&... params)
    {
        return apply_details::apply_struct<sizeof...(T)>::apply(std::forward<F>(func), std::forward<T>(params)...);
    }
    /// @}
    
//     template<typename F>
//     template<size_t N>
//     struct __apply__ 
//     {
//         template<typename F, typename T, typename... A>
//         constexpr static auto apply(F && f, T && t, A &&... a)
//                 -> decltype(__apply__<N-1>::apply(
//                 ::std::forward<F>(f), ::std::forward<T>(t),
//                 ::std::get<N-1>(::std::forward<T>(t)), ::std::forward<A>(a)...))
//         {
//             return __apply__<N-1>::apply(::std::forward<F>(f), ::std::forward<T>(t),
//                 ::std::get<N-1>(::std::forward<T>(t)), ::std::forward<A>(a)...);
//         }
//     };
// 
//     template<>
//     struct __apply__<0> 
//     {
//         template<typename F, typename T, typename... A>
//         constexpr static auto apply(F && f, T &&, A &&... a)
//             -> decltype(::std::forward<F>(f)(::std::forward<A>(a)...))
//         {
//             return ::std::forward<F>(f)(::std::forward<A>(a)...);
//         }
//     };
// 
//     template<typename F, typename T>
//     constexpr auto apply(F && f, T && t)
//         -> decltype(__apply__< ::std::tuple_size<typename ::std::decay<T>::type
//         >::value>::apply(::std::forward<F>(f), ::std::forward<T>(t)))
//     {
//         return __apply__< ::std::tuple_size<
//             typename ::std::decay<T>::type
//         >::value>::apply(::std::forward<F>(f), ::std::forward<T>(t));
//     }
}

namespace std
{
    template<int N>
    struct is_placeholder< efit::placeholder_template<N> >
        : integral_constant<int, N+1> {};
}

#endif
