
/*!
 * @file
 * @brief Defines the benchmark macro.
 */

#ifndef __EFIT_FUNCTIONS_BENCHMARK_HPP__
#define __EFIT_FUNCTIONS_BENCHMARK_HPP__

#include <ctime>
#include <chrono>
#include <string>
#include <thread>
#include <mutex>
#include <vector>
    
/*!
 * \brief Benckmarks a section of code.
 * 
 * Benchmarks a section of code that is defined in
 *\code
 * benchmark("code label")
 * {
 *      \\code that will be time here
 * };
 *\endcode
 * 
 * the execution time in seconds is printed to the std output.  For
 * the above code, possible output may look like:
 * 
 * > code_label: 0.0232435
 * 
 * @param[in] msg:  The label of the code that is going to be benchmarked.
 */
#define benchmark(...)                                      \
efit::benchmark_details::benchmark_tmp(__VA_ARGS__) = [&]() \

#define multi_thread(threadnum, ...)                                                \
efit::multi_thread_details::multi_thread_tmp(__VA_ARGS__) = [&](size_t threadnum)   \

#define for_multi_thread(threadnum, loop_start, loop_length, ...)                                                \
efit::multi_thread_details::for_multi_thread_tmp(loop_start, loop_length, __VA_ARGS__) = [&](size_t threadnum)   \

namespace efit
{
    
    namespace benchmark_details
    {
        
        class benchmark_tmp
        {
        private:
            std::string msg;
            
        public:
            benchmark_tmp(const std::string &msg) : msg(msg) {}
            
            template<typename func_t>
            void operator=(func_t &&func) const
            {
                std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
                func();
                std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();
                
                std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);
                std::cout << msg << ": " << time_span.count() << std::endl;
            }
        };
        
    }
    
    namespace multi_thread_details
    {
        
        struct multi_thread_tmp
        {
            size_t NThreads;
            
            constexpr multi_thread_tmp(size_t NThreads) : NThreads(NThreads) {}
            
            template<typename T>
            inline void operator = (T&& func)
            {
                std::vector<std::thread> threads;
                        
                for (size_t t = 0; t < NThreads; t++)
                {
                    threads.emplace_back(func, t);
                }
                
                for (auto &&thr : threads)
                    thr.join();
            }
        };
        
        struct for_multi_thread_tmp
        {
            size_t N_start;
            size_t N;
            size_t NThreads;
            
            constexpr for_multi_thread_tmp(size_t N_start, size_t N, size_t NThreads) : N_start(N_start), N(N), NThreads(NThreads) {}
            
            template<typename T>
            inline void operator = (T&& func)
            {
                std::vector<std::thread> threads;
                std::mutex mutex;
                size_t i = N_start;
                
                for (size_t t = 0; t < NThreads; t++)
                {
                    threads.emplace_back([&](size_t thread_num)
                    {
                        size_t j;
                        
                        for(;;)
                        {
                            mutex.lock();
                            j = i++;
                            mutex.unlock();
                            
                            if (j >= N)
                                break;
                            
                            func(j);
                        }
                    }, t);
                }
                
                for (auto &&thr : threads)
                    thr.join();
            }
        };
    }
}

#endif
