//  EFIT 6
//  *********************************************
///  \file
///
///  Eccentric Anomaly implemation
///
///  *********************************************
///
///  Authors (add name and date if you modify):
///
///  \author Gregory Martinez
///          (gregory.david.martinez@gmail.com)
///  \date 2016 May
///
///  *********************************************

#ifndef __FUNCTIONS_ECCENTRIC_ANOMALY_HPP__
#define __FUNCTIONS_ECCENTRIC_ANOMALY_HPP__

//#include "constants.hpp"

namespace efit
{
    namespace functions
    {
        ///Solves 'E - e*sin(E) = dt' for E with e < 1.
        template <typename param, int N = 20>
        inline param BoundEccentricAnomaly(param e, param dt)
        {
            using stan::math::abs;
            using stan::math::sin;
            using stan::math::cos;
            //using std::abs;
            //using std::sin;
            //using std::cos;
            
            if (e == 0)
                return dt;
            
            if (dt == 0.0)
                return 0.0;
            
            //int n = (dt > 0.0) ? int(dt/constants::_2pi + 0.5) : int(dt/constants::_2pi - 0.5);
            
            //param s = std::sqrt(8.0*(1.0-e)/e);
            //param E = s*std::sinh(std::asinh(3.0*(dt-n*constants::_2pi)/(1.0-e)/s)/3.0) + n*constants::_2pi;
            param E = dt + e * sin(dt) + e*e*sin(2.0*dt)/2.0;
            
            param dE, func, dfunc, ddfunc, dddfunc;
            
            for (int i = 0; i < N; i++)
            {
                dddfunc = e*cos(E);
                ddfunc = e*sin(E);
                dfunc = 1.0 - dddfunc;
                func = E - ddfunc - dt;
                
                dE = func*(6.0*dfunc*dfunc - 3.0*func*ddfunc)/(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc);
                //dE = 4.0*func*(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc)/(dfunc*dfunc*(24.0*dfunc*dfunc - 36.0*func*ddfunc) + func*func*(6.0*ddfunc*ddfunc + 8.0*dfunc*dddfunc + func*ddfunc));
                E -= dE;
                
                if (abs(dE/E) < 1.0e-9)
                    return E;
            }
            
            //std::cout << "Max interations exceeded in BoundEccentricAnomaly." << std::endl;
            
            return E;
        }
        
        ///Solves 'e*sinh(E) - E = dt' for E with e > 1.
        template<typename param, int N = 20>
        inline param UnBoundEccentricAnomaly(param e, param dt)
        {
            using stan::math::abs;
            using stan::math::asinh;
            using stan::math::sinh;
            using stan::math::cosh;
            
            if (dt == 0.0)
                return 0.0;
            
            param dE, func, dfunc, ddfunc, dddfunc;
            
            param s = sqrt(8.0*(e-1.0)/e);
            param E = s*sinh(asinh(3.0*dt/(e-1.0)/s)/3.0);
            
            for (int i = 0; i < N; i++)
            {
                dddfunc = e*cosh(E);
                ddfunc = e*sinh(E);
                dfunc = dddfunc - 1.0;
                func = ddfunc - E - dt;
                
                dE = func*(6.0*dfunc*dfunc - 3.0*func*ddfunc)/(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc);
                //dE = 4.0*func*(6.0*dfunc*(dfunc*dfunc - func*ddfunc) + func*func*dddfunc)/(dfunc*dfunc*(24.0*dfunc*dfunc - 36.0*func*ddfunc) + func*func*(6.0*ddfunc*ddfunc + 8.0*dfunc*dddfunc - func*ddfunc));
                E -= dE;
                
                if (abs(dE/E) < 1.0e-9)
                    return E;
            }
            
            //std::cout << "Max interations exceeded in BoundEccentricAnomaly." << std::endl;
            
            return E;
        }
        
        ///Solves (E^3)/6 + E/2 = dt for E with e = 1.
        template<typename param>
        inline param EqBoundEccentricAnomaly(param dt)
        {
            using stan::math::asinh;
            using stan::math::sinh;
            
            return 2.0*sinh(stan::math::asinh(3.0*dt)/3.0);
        }
        
        namespace eccentric_anomaly_details
        {
            double DeBoundEccentricAnomaly(double E, double e, double dt)
            {
                return std::sin(E)/(1.0 - e*std::cos(E));
            }
            
            double DdtBoundEccentricAnomaly(double E, double e, double dt)
            {
                return 1.0/(1.0 - e*std::cos(E));
            }
            
            double DeUnBoundEccentricAnomaly(double E, double e, double dt)
            {
                return std::sinh(E)/(1.0 - e*std::cosh(E));
            }
            
            double DdtUnBoundEccentricAnomaly(double E, double e, double dt)
            {
                return 1.0/(e*std::cosh(E) - 1.0);
            }
            
            double DdtEqBoundEccentricAnomaly(double E, double dt)
            {
                return 2.0/(E*E + 1.0);
            }
        }
        
        auto bound_eccentric_anomaly_autodiff = make_nstar_function(BoundEccentricAnomaly<double>, eccentric_anomaly_details::DeBoundEccentricAnomaly, eccentric_anomaly_details::DdtBoundEccentricAnomaly);
        
        auto unbound_eccentric_anomaly_autodiff = make_nstar_function(UnBoundEccentricAnomaly<double>, eccentric_anomaly_details::DeUnBoundEccentricAnomaly, eccentric_anomaly_details::DdtUnBoundEccentricAnomaly);
        
        auto eqbound_eccentric_anomaly_autodiff = make_nstar_function(EqBoundEccentricAnomaly<double>, eccentric_anomaly_details::DdtEqBoundEccentricAnomaly);
        
        //auto bound_eccentric_anomaly = vectorize(bound_eccentric_anomaly_autodiff);
        //auto unbound_eccentric_anomaly = vectorize(unbound_eccentric_anomaly_autodiff);
        //auto eqbound_eccentric_anomaly = vectorize(eqbound_eccentric_anomaly_autodiff);
    }
}

#endif
