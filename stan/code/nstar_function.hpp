#ifndef __FUNCTIONS_NSTAR_FUNCTION_HPP__
#define __FUNCTIONS_NSTAR_FUNCTION_HPP__

namespace efit
{
    
    namespace functions
    {
        
        namespace nstar_func_details
        {
            
            template<typename T>
            struct get_vari_type
            {
                typedef T type;
            };
            
            template<>
            struct get_vari_type<stan::math::var>
            {
                typedef stan::math::vari * type;
            };
            
            template<typename T>
            constexpr T get_val(T val){return val;}
            
            constexpr double get_val(stan::math::vari *val){return val->val_;}
            
            template<typename T>
            constexpr T get_ptr(const T &val){return val;}
            
            constexpr stan::math::vari* get_ptr(const stan::math::var &val){return val.vi_;}
            
            namespace is_var_details
            {
                template<typename T>
                struct is_var : std::false_type {};
                template<>
                struct is_var<stan::math::var> : std::true_type {};
            }
            
            template<typename T>
            struct is_var : is_var_details::is_var<typename std::decay<T>::type> {};
            
            template<typename... T>
            struct check_for_var;
            
            template<typename U, typename... T>
            struct check_for_var<U, T...> : std::integral_constant<bool, is_var<U>::value || check_for_var<T...>::value> {};
            
            template<>
            struct check_for_var<> : std::false_type {};
            
            template<typename F>
            struct input
            {
                template<typename T>
                struct repeat 
                {
                    typedef F type;
                };
            };
            
            template<typename F, typename T>
            struct repeat 
            {
                typedef F type;
            };
            
        }
            
        //template<typename T>
        //class nstar_function;
        
        //template <typename ret, typename... args>
        template <typename F_t, typename... funcs_t>
        class nstar_function//<ret(args...)>
        {
        private:
            
            //typedef ret (*F_t) (args...);
            //typedef ret (*F) (double, args...);
            //
            //template<typename T>
            //struct repeat 
            //{
            //    typedef F type;
            //};
            //
            //typedef std::tuple<typename repeat<args>::type...> tup_type;
            
            typedef std::tuple<funcs_t...> tup_type;
            
            template<typename F>
            struct wrapper
            {
                F &func;
                
                template<typename... Args>
                constexpr decltype(auto) operator()(Args&&... params)
                {
                    return func(nstar_func_details::get_val(params)...);
                }
            };
            
        public:
            F_t func;
            tup_type funcs;
            
            template<int i, typename U, typename P, typename... T>
            inline
            typename std::enable_if<i < sizeof...(T) && std::is_same<typename std::tuple_element<i, std::tuple<T..., void> >::type, stan::math::vari *>::value>::type
            chain(U& adj_, P& val_, std::tuple<T...> &vis_)
            {
                std::get<i>(vis_)->adj_ += adj_ * efit::apply(wrapper<typename std::tuple_element<i, tup_type>::type>{std::get<i>(funcs)}, val_, vis_);
                chain<i+1>(adj_, val_, vis_);
            }
            
            template<int i, typename U, typename P, typename... T>
            constexpr
            typename std::enable_if<i < sizeof...(T) && !std::is_same<typename std::tuple_element<i, std::tuple<T..., void> >::type, stan::math::vari *>::value>::type
            chain(U& adj_, P& val_, std::tuple<T...> &vis_)
            {
                chain<i+1>(adj_, val_, vis_);
            }
            
            template<int i, typename U, typename P, typename... T>
            constexpr
            typename std::enable_if<i == sizeof...(T)>::type
            chain(U&, P &,std::tuple<T...> &) {}
            
            template<typename... T>
            struct multi_var : public stan::math::vari
            {
                nstar_function *parent;
                std::tuple<T...> vis_;
                
                multi_var(nstar_function *parent, T... params) : 
                        stan::math::vari(parent->func(nstar_func_details::get_val(params)...)), 
                        parent(parent), vis_(params...) {}
                
                void chain()
                {
                    parent->chain<0>(adj_, val_, vis_);
                }
            };
            
            template<typename Fn, typename... Fns>
            nstar_function(Fn&& fn, Fns&&... fns) : func(fn), funcs(fns...) {}
            
            template <typename... Args>
            constexpr typename std::enable_if<nstar_func_details::check_for_var<Args...>::value, stan::math::var>::type 
            operator()(Args&&... params)
            {
                return stan::math::var(new multi_var<typename nstar_func_details::get_vari_type<typename std::decay<Args>::type>::type...>(this, nstar_func_details::get_ptr(params)...));
            }
            
            template <typename... Args>
            constexpr typename std::enable_if<!nstar_func_details::check_for_var<Args...>::value, decltype(func(std::declval<Args>()...))>::type 
            operator()(Args&&... params)
            {
                return func(std::forward<Args>(params)...);
            }
        
        };
        
        template <typename ret, typename... args>
        class nstar_function<ret(args...)> : public nstar_function<ret(*)(args...), typename nstar_func_details::repeat<ret(*)(ret, args...), args>::type...> 
        {
        public:
            using nstar_function<ret(*)(args...), typename nstar_func_details::repeat<ret(*)(ret, args...), args>::type...>::nstar_function;
        };
        
        template<typename... Fns>
        constexpr decltype(auto) make_nstar_function(Fns&&... funcs)
        {
            return nstar_function<typename std::decay<Fns>::type...>(funcs...);
        }
        
    }
    
}

#endif
