functions
{
    real likelihood(vector[] u, vector hyper_u)
    {
        return 0.0;
    }
    
    int dumper(vector[] u, vector hyper_u)
    {
        return 0;
    }
}

data
{
    int starN;
}

transformed data
{
}

parameters
{    
    vector<lower=0.0, upper=1.0>[6] u[starN];
    vector<lower=0.0, upper=1.0>[7] hyper_u;
}

transformed parameters
{
}

model
{
    target += likelihood(u, hyper_u);
}

generated quantities
{
    real B;
    
    B = hyper_u[7];//dumper(u, hyper_u);
}
