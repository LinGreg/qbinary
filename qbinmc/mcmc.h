#ifndef MCMC_H
#define MCMC_H

#include "vector.h"
#include "mathexpr.h"
#include "spline.h"
#include <vector>

enum Mode
{
	Nothing,
	Test,
	Dispersion_Posterior_No_Binaries,
	Binaries_No_MW,
	Binaries_No_MW_V_Sysparam,
	//Binaries_No_MW_V_Fixed_Sysparam2,
	Binaries_No_MW_V_Sysparam2,
	Binaries_No_MW_V_Sysparam3,
	Binaries_No_MW_V_Fixed_Sysparam3,
	No_Binaries_No_MW_V_Sysparam,
	No_Binaries_No_MW_V_Sysparam2,
	No_Binaries_No_MW_V_Sysparam3,
	DV_No_MW,
	DV_No_MW_No_Period_Params,
	W_Only,
	W_Only_Fixed_Metallicity_Params,
	DV_Fixed_Metallicity_Params,
	DV_Fixed_Metallicity_Params_No_Period_Params,
	DVW_No_Period_Params,
	DV_Fixed_Binary_Fraction,
	DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params,
	DV_Error_Model,
	DV_Error_Model_with_Binaries,
	DV_Error_Model_with_Binaries_Fixed_W_Params,
	DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel,
	DV_Error_Model_Extended,
	DV_Error_Model_Metallicity,
	DV_logdvlike,
	DV_Fixed_Metallicity_Params_logdvlike,
	DVW_logdvlike,
	Binaries_MW_No_Metallicities,
	Binaries_VR,
	Binaries_VR_Fixed_Metallicity_Params,
	Binaries_VR_Fixed_W_Rs,
	MW_Velocities_No_Binaries,
	MW_Velocities_Metallicities_No_Binaries,
	Binaries_VW,
	Binaries_VW_No_Period_Params,
	Binaries_V_Fixed_W_Params_No_Period_Params,
	Binaries_V_Fixed_W_Params,
	Binaries_V_Fixed_W_Params_Test_RTables,
	Binaries_V_No_MW,
	Full_Monty_No_Period_Params,
	Full_Monty,
	Gauss_Hermite
};

void posteriors_No_Binaries_No_MW(char *datafile, Mode mode);
void posteriors_Binaries_No_MW(char *datafile, Mode mode);
void posteriors_No_Binaries_No_MW_V_Sysparam(char *datafile, Mode mode);
void posteriors_No_Binaries_No_MW_V_Sysparam2(char *datafile, Mode mode);
void posteriors_No_Binaries_No_MW_V_Sysparam3(char *datafile, Mode mode);
void posteriors_Binaries_No_MW_V_Sysparam(char *datafile, Mode mode);
void posteriors_Binaries_No_MW_V_Sysparam2(char *datafile, Mode mode);
void posteriors_Binaries_No_MW_V_Sysparam3(char *datafile, Mode mode);
void posteriors_Binaries_No_MW_V_Fixed_Sysparam3(char *datafile, Mode mode);
//void posteriors_Binaries_No_MW_V_Fixed_Sysparam2(char *datafile, Mode mode);
void posteriors_DV_No_MW(char *datafile, Mode mode);
void posteriors_DV_No_MW_No_Period_Params(char *datafile, Mode mode);
void posteriors_DVW_No_Period_Params(char *datafile, Mode mode);
void posteriors_DV_Fixed_Metallicity_Params_logdvlike(char *datafile, Mode mode);
void posteriors_DVW_logdvlike(char *datafile, Mode mode);
void posteriors_W_Only_Fixed_Metallicity_Params(char *datafile, Mode mode);
void posteriors_W_Only(char *datafile, Mode mode);
void posteriors_DV_Fixed_Metallicity_Params_No_Period_Params(char *datafile, Mode mode);
void posteriors_DV_Fixed_Metallicity_Params(char *datafile, Mode mode);
void posteriors_DV_Fixed_Binary_Fraction(char *datafile, Mode mode);
void posteriors_DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params(char *datafile, Mode mode);
void posteriors_DV_Error_Model(char *datafile, Mode mode);
void posteriors_DV_Error_Model_with_Binaries(char *datafile, Mode mode);
void posteriors_DV_Error_Model_with_Binaries_Fixed_W_Params(char *datafile, Mode mode);
void posteriors_DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel(char *datafile, Mode mode);
void posteriors_DV_logdvlike(char *datafile, Mode mode);
void posteriors_DV_Error_Model_Extended(char *datafile, Mode mode);
void posteriors_DV_Error_Model_Metallicity(char *datafile, Mode mode);
void posteriors_MW_Velocities_No_Binaries(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_MW_Velocities_Metallicities_No_Binaries(char *datafile,char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_VW(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_VW_No_Period_Params(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_V_Fixed_W_Params_No_Period_Params(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_V_Fixed_W_Params(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_V_Fixed_W_Params_Test_RTables(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_V_No_MW(char *datafile, Mode mode);
void posteriors_Binaries_MW_No_Metallicities(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_VR(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_VR_Fixed_Metallicity_Params(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Binaries_VR_Fixed_W_Rs(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Full_Monty_No_Period_Params(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_Full_Monty(char *datafile, char *mw_likelihood_file, Mode mode);
void posteriors_test(char *datafile, char *mw_likelihood_file, Mode mode);
void gauss_hermite(char *datafile, Mode mode);
void find_velocity_params_from_sigma_clip(char *datafile, double &mu, double &sig, bool use_walker_cut);
void append_nstars_to_output_file(string &output_file, int nstars);
char *advance(char *p);
void mkdir(const string& dir);
void output_param_files(int nparams, string* paramnames, string *latex_paramnames, double* lowerlimit, double* upperlimit, string paramnames_filename, string latex_paramnames_filename, string ranges_filename);

struct Star
{
	string id;
	double magnitude, R;
	double abs_magnitude;
	int n_epochs;
	dvector times, vlos_data, v_errors, w_data, w_errors;
	imatrix sysparam_i;
	vector<int> sysparam_order;
	dvector dv_data;
	dvector r_value;
	ivector channel_color;
	dvector v_error_model;
	double v_avg, sigv; // average velocity and measurement error
	double w_avg, sigw; // average velocity and measurement error
	double vlike_mw;
	bool no_metallicities;
	bool clipped_outliers;

	Spline rfactor_spline;
	dvector rfactor_vals, vcm_vals;
	double rfactor_vcm_min, rfactor_vcm_max;
	int rfactor_vcm_nn;
	double rnorm, nfactor;
	double dvlike, jfactor_dv;
	double zeroval;

	dmatrix *rtable;
	dmatrix ***rctable; // coefficients c_ij for bicubic interpolation
	dmatrix jtable;
	dmatrix **jctable; // coefficients c_ij for bicubic interpolation
	double mu_logP_nn; // needed for deleting the above arrays at the end

	double n_sysparams;
	dvector *rltable1;
	dmatrix *rltable2;
	dmatrix **rltable3;
	double sysparam1_nn;
	Spline *rltable_spline;

	bool long_time_interval;
	int k_longtime, mag_number, year_number;

	Star() { rtable = NULL; rctable = NULL; jctable = NULL; rltable1 = NULL; rltable2 = NULL; rltable_spline = NULL; }
	~Star() {
		if (rtable != NULL) delete[] rtable;
		if (rltable1 != NULL) delete[] rltable1;
		if (rltable2 != NULL) delete[] rltable2;
		if (rltable_spline != NULL) delete[] rltable_spline;
		if (rctable != NULL) {
			for (int j=0; j < rfactor_vcm_nn; j++) {
				for (int k=0; k < mu_logP_nn; k++)
					delete[] rctable[j][k];
				delete[] rctable[j];
			}
			delete[] rctable;
		}
		if (jctable != NULL) {
			for (int k=0; k < mu_logP_nn; k++)
				delete[] jctable[k];
			delete[] jctable;
		}
	}
	double rfactor_interpolate(const double mu_logP, const double sig_logP, const int j);
	void find_velocity_averages(void)
	{
		double sigvsq_inv=0;
		v_avg=0;
		for (int j=0; j < n_epochs; j++) {
			sigvsq_inv += 1.0/SQR(v_errors[j]);
			v_avg += vlos_data[j]/SQR(v_errors[j]);
		}
		sigv = sqrt(1.0/sigvsq_inv);
		v_avg /= sigvsq_inv;
	}

	void find_width_averages(void)
	{
		double sigwsq_inv=0;
		w_avg=0;
		no_metallicities = true;
		for (int j=0; j < n_epochs; j++) {
			if ((w_data[j] != 1e30) and (w_errors[j] != 1e30)) no_metallicities = false;
			// we need at least one epoch where metallicities are recorded, otherwise we don't use the metallicity likelihood for this star
		}

		if (no_metallicities) return;
		else
		{
			for (int j=0; j < n_epochs; j++) {
				if (w_data[j]==1e30) // this is the case where the metallicity wasn't recorded, in which case it's marked as 1e30. The following code makes sure it doesn't affect w_avg
				{
					w_data[j] = 0;
					if (w_errors[j] != 1e30) w_errors[j] = 1e30;
				}
				sigwsq_inv += 1.0/SQR(w_errors[j]);
				w_avg += w_data[j]/SQR(w_errors[j]);
			}
			sigw = sqrt(1.0/sigwsq_inv);
			w_avg /= sigwsq_inv;
		}
	}
};

#endif // MCMC_H
