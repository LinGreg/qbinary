#define M_2PI 6.28318530718
#include "vector.h"
#include "rand.h"

/*
// you should make this sometime and have the R_Factor class inherit it. Everything would use the Binary class (it would be a complete restructuring of binmc.cpp, but would be much nicer).
class Binary
{
	protected:
	bool fix_q, use_experimental_qdist;

	double logP, q, e, phi, a;
	double period, time, newtime;
	double theta, psi;	// Euler angles
	double roche_lobe_radius;
	lumr_mass_Spline_age lumr_mass_spline_age;

	public:
	Binary();
	void generate_binary_velocities(const double &primary_mass, const int &epochs, dvector &vbin, const dvector &time_interval_years, dvector &times);
	double stellar_radius(double mass);
	double gen_logP(void);
	double gen_q(double logP);
	double gen_q_experimental(double logP);
	double gen_e(double logP);
	double a_orbit = orbital_separation_semimajor_axis(double logP, double primary_mass, double q);
	roche_lobe_radius = roche_lobe_radius_approximation(a_orbit,q,e);
};
*/

struct R_Factors
{
	ofstream logfile;
	double galaxy_distance;

	double systematic_err;
	int vcm_window_divisions, vcm_window_size;
	double initial_fractional_accuracy, fractional_accuracy;
	double initial_vcm_window_divisions;
	double vcm, vcm_min, vcm_max, vcm_step;
	double vcm_mean_approx, vcm_dispersion_approx;
	int vcm_nn, n_tail;
	double size_min, size_max, size, size_step;
	double binsize_min, binsize_max;
	dvector binsizes, tail_binsizes;
	dvector sq_bin_radii, dv_bin_area, sq_bin_radii_tail;
	double vcm_bin_area[2], vcm_bin_area_tail[2];
	long int n_simulated_stars, initial_n_simulated_stars;
	long int total_n_simulated_stars;
	dvector dv_like;
	dvector vcm_like[2];
	int *vcm_bincount[2], *vcm_bincount_part[2], *vcm_bincount_sum[2];
	vector<double> *jvals;
	vector<double> *dv_like_vals;
	dvector vcm_vals;
	int *dv_bincount, *dv_bincount_part, *dv_bincount_sum;
	double nfactor, like_nb;
	int nlike_max_nfactor; // for including offsets
	double *nfactor_sysparam; //for including offsets
	double *like_nb_sysparam; //for including offsets
	int test_iterations, binsizes_test_iterations, iterations_converged;
	int iteration_threshold;
	double star_v_avg;
	double nonmember_threshold;
	int vcm_nn_min;
	int ktail_left, ktail_right;
	bool use_large_tail_bins;
	int n_times_increased_binsize, n_times_decreased_binsize;
	int star_i;
	double c_n;
	int ndim;
	bool keep_star;
	int nstars_keep;
	bool zero_jfactor, new_binsize;

	double sig_logP_min, sig_logP_max, mu_logP_min, mu_logP_max, mu_logP_step, sig_logP_step;
	int mu_logP_nn, sig_logP_nn;

	double logP, q, e, phi, a;
	double period, time, newtime;
	double theta, psi;	// Euler angles
	double roche_lobe_radius;

	R_Factors(const double distance);
	R_Factors(const double vcm_mean_guess, const double vcm_dispersion_guess, const double distance);
	void set_musig_table_params(const double mumin, const double mumax, const int mu_nn, const double sigmin, const double sigmax, const int sig_nn);
	void generate_rfactors(const string datafile_str, const string rdir, const int n_threads);
	void generate_rfactors_sysparams(const string datafile_str, const string rdir, const int n_threads);
	void generate_rtables(const string datafile_str, const string rdir, const int n_threads);
	void calculate_verr_no_systematic(const dvector& vlos_data, const dvector& v_errors, dvector& v_errors_no_systematic);
	void calculate_nfactor(const dvector& vlos_data, const dvector& v_errors, double &nfactor_val, double &like_nb_val, double &sigm_no_systematic);

	void generate_jfactors(const string datafile_str, const string rdir, const int n_threads);
	void sort_out_nfactors(const string datafile_str, const string rdir);
	void generate_jtables(const string datafile_str, const string rdir, const int n_threads);
	void determine_accuracy_and_n_simulated_stars(const double &like_nb, const double &nfac, const double &sigm);
	void determine_accuracy_and_n_simulated_stars_sysparam(int n_sysparam_star, const double &sigm);
	void determine_dvlike_accuracy_and_n_simulated_stars(const double &nfac, const double &sigm);
	void output_rfactor_files(const string rfilename, bool nonzero_rfactor);
	void output_like_nb_file(const string like_nb_filename, const double v_avg, const double sigm);
	void output_nstars(const string &nstarfilename, const int &ii);
	void test_convergence(int iterations, bool &converged, bool &nonmember, const bool print_musigvals);
	void test_convergence_sysparam(int iterations, bool &converged, bool &nonmember, const bool print_musigvals, int n_offset_star);
	void test_dv_convergence(int iterations, bool &converged, const bool print_musigvals);
	void adapt_stepsizes(int &iterations, int &iterations_since_accuracy_reset, const double &max_dvlike, int &n_times_zero_pts_in_bin, bool &converged);
	void calculate_normalized_likelihood();
	void calculate_normalized_likelihood_sysparam(int n_sysparam_star);
};

struct Thread_Data
{
	int n_simulated_stars_thread;
	Random_Sequence rand;
	double magnitude, primary_mass, stellar_radius;
	int epochs, ndim;
	dvector data, v_errors, time_interval_years;
	dvector sq_bin_radii;
	imatrix offset_i;
	int n_sys_params, sys_param_steps, sys_param_steps2, sys_param_steps3;

	bool bin_in_vcm;
	ivector dv_bincount; // only used if binning in dv
	vector<double> postpts[5];
	double postpt[5];

	// the following are only used if binning in v over a table of v_cm values
	dvector vcm_like[2];
	ivector vcm_bincount[2];
	int vcm_nn;
	double vcm_min, vcm_step;
	int ktail_left, ktail_right;
	dvector sq_bin_radii_tail;
	dvector* vcm_like1[2];
	ivector* vcm_bincount1[2];
	dvector** vcm_like2[2];
	ivector** vcm_bincount2[2];
	dvector*** vcm_like3[2];
	ivector*** vcm_bincount3[2];

	void input(const int &nsim, const Random_Sequence &input_rand, const dvector &data_in, const dvector &verrs, const dvector &timeints, const dvector &sqbinradii, const double &mag, const double &mp, const double &stellar_rad, const bool bin_in_vcm_in = false, const int vcm_nn_in = 0, const double vcm_min_in = 0, const double vcm_step_in = 0, const int ktail_left_in = 0, const int ktail_right_in = 0, const int n_offset_in = 0, const imatrix &offset_i_in = NULL, const int offset_steps_in = 0, const int offset_steps2_in = -1, const int offset_steps3_in = -1)
	{
		n_simulated_stars_thread = nsim;
		n_sys_params = n_offset_in;
		sys_param_steps = offset_steps_in;
		if (offset_steps2_in==-1) sys_param_steps2 = offset_steps_in;
		else sys_param_steps2 = offset_steps2_in;
		if (offset_steps3_in==-1) sys_param_steps3 = offset_steps_in;
		else sys_param_steps3 = offset_steps3_in;
		rand.input(input_rand);
		data.input(data_in);
		ndim = data.size();
		v_errors.input(verrs);
		epochs = v_errors.size();
		if (n_sys_params != 0) offset_i.input(offset_i_in);
		time_interval_years.input(timeints);
		sq_bin_radii.input(sqbinradii);
		bin_in_vcm = bin_in_vcm_in;
		if (!bin_in_vcm) {
			dv_bincount.input(2);
			for (int i=0; i < 2; i++) dv_bincount[i] = 0;
		} else {
			sq_bin_radii_tail.input(sqbinradii);
			vcm_nn = vcm_nn_in;
			vcm_min = vcm_min_in;
			vcm_step = vcm_step_in;
			ktail_left = ktail_left_in;
			ktail_right = ktail_right_in;
			if (n_sys_params==0) {
				for (int k=0; k < 2; k++) {
					vcm_like[k].input(vcm_nn);
					vcm_like[k]=0;
					vcm_bincount[k].input(vcm_nn);
					vcm_bincount[k]=0;
				}
			} else if (n_sys_params==1) {
				int k,l;
				for (k=0; k < 2; k++) {
					vcm_like1[k] = new dvector[sys_param_steps];
					vcm_bincount1[k] = new ivector[sys_param_steps];
					for (l=0; l < sys_param_steps; l++) {
						vcm_like1[k][l].input(vcm_nn);
						vcm_like1[k][l]=0;
						vcm_bincount1[k][l].input(vcm_nn);
						vcm_bincount1[k][l]=0;
					}
				}
			} else if (n_sys_params==2) {
				int k,l,m;
				for (k=0; k < 2; k++) {
					vcm_like2[k] = new dvector*[sys_param_steps];
					vcm_bincount2[k] = new ivector*[sys_param_steps];
					for (l=0; l < sys_param_steps; l++) {
						vcm_like2[k][l] = new dvector[sys_param_steps2];
						vcm_bincount2[k][l] = new ivector[sys_param_steps2];
						for (m=0; m < sys_param_steps2; m++) {
							vcm_like2[k][l][m].input(vcm_nn);
							vcm_like2[k][l][m]=0;
							vcm_bincount2[k][l][m].input(vcm_nn);
							vcm_bincount2[k][l][m]=0;
						}
					}
				}
			} else if (n_sys_params==3) {
				int k,l,m,p;
				for (k=0; k < 2; k++) {
					vcm_like3[k] = new dvector**[sys_param_steps];
					vcm_bincount3[k] = new ivector**[sys_param_steps];
					for (l=0; l < sys_param_steps; l++) {
						vcm_like3[k][l] = new dvector*[sys_param_steps2];
						vcm_bincount3[k][l] = new ivector*[sys_param_steps2];
						for (m=0; m < sys_param_steps2; m++) {
							vcm_like3[k][l][m] = new dvector[sys_param_steps3];
							vcm_bincount3[k][l][m] = new ivector[sys_param_steps3];
							for (p=0; p < sys_param_steps3; p++) {
								vcm_like3[k][l][m][p].input(vcm_nn);
								vcm_like3[k][l][m][p]=0;
								vcm_bincount3[k][l][m][p].input(vcm_nn);
								vcm_bincount3[k][l][m][p]=0;
							}
						}
					}
				}
			}
		}

		primary_mass = mp;
		stellar_radius = stellar_rad;
		magnitude = mag;
	}
	void update(const int &nsim, const dvector &sqbinradii) {
		n_simulated_stars_thread = nsim;
		sq_bin_radii.input(sqbinradii);
		if (bin_in_vcm)
		{
			if (n_sys_params==0) {
				for (int k=0; k < 2; k++) {
					vcm_like[k].input(vcm_nn);
					vcm_like[k]=0;
					vcm_bincount[k].input(vcm_nn);
					vcm_bincount[k]=0;
				}
			} else if (n_sys_params==1) {
				int k,l;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						vcm_like1[k][l].input(vcm_nn);
						vcm_like1[k][l]=0;
						vcm_bincount1[k][l].input(vcm_nn);
						vcm_bincount1[k][l]=0;
					}
				}
			} else if (n_sys_params==2) {
				int k,l,m;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						for (m=0; m < sys_param_steps2; m++) {
							vcm_like2[k][l][m].input(vcm_nn);
							vcm_like2[k][l][m]=0;
							vcm_bincount2[k][l][m].input(vcm_nn);
							vcm_bincount2[k][l][m]=0;
						}
					}
				}
			} else if (n_sys_params==3) {
				int k,l,m,p;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						for (m=0; m < sys_param_steps2; m++) {
							for (p=0; p < sys_param_steps3; p++) {
								vcm_like3[k][l][m][p].input(vcm_nn);
								vcm_like3[k][l][m][p]=0;
								vcm_bincount3[k][l][m][p].input(vcm_nn);
								vcm_bincount3[k][l][m][p]=0;
							}
						}
					}
				}
			}
		}
		else {
			for (int i=0; i < 2; i++) dv_bincount[i] = 0;
		}
	}
	void update(const int &nsim, const dvector &sqbinradii, const dvector &sqbinradii_tail) {
		n_simulated_stars_thread = nsim;
		sq_bin_radii.input(sqbinradii);
		sq_bin_radii_tail.input(sqbinradii_tail);
		if (bin_in_vcm)
		{
			if (n_sys_params==0) {
				for (int k=0; k < 2; k++) {
					vcm_like[k].input(vcm_nn);
					vcm_like[k]=0;
					vcm_bincount[k].input(vcm_nn);
					vcm_bincount[k]=0;
				}
			} else if (n_sys_params==1) {
				int k,l;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						vcm_like1[k][l].input(vcm_nn);
						vcm_like1[k][l]=0;
						vcm_bincount1[k][l].input(vcm_nn);
						vcm_bincount1[k][l]=0;
					}
				}
			} else if (n_sys_params==2) {
				int k,l,m;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						for (m=0; m < sys_param_steps2; m++) {
							vcm_like2[k][l][m].input(vcm_nn);
							vcm_like2[k][l][m]=0;
							vcm_bincount2[k][l][m].input(vcm_nn);
							vcm_bincount2[k][l][m]=0;
						}
					}
				}
			} else if (n_sys_params==3) {
				int k,l,m,p;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						for (m=0; m < sys_param_steps2; m++) {
							for (p=0; p < sys_param_steps3; p++) {
								vcm_like3[k][l][m][p].input(vcm_nn);
								vcm_like3[k][l][m][p]=0;
								vcm_bincount3[k][l][m][p].input(vcm_nn);
								vcm_bincount3[k][l][m][p]=0;
							}
						}
					}
				}
			}
		}
		else {
			for (int i=0; i < 2; i++) dv_bincount[i] = 0;
		}
	}

	void output_random_sequence(Random_Sequence& input_rand) {
		input_rand.input(rand);
	}
	void free_vcm_arrays() {
		delete[] vcm_like;
		delete[] vcm_bincount;
	}
	~Thread_Data() {
		if (bin_in_vcm) {
			if (n_sys_params==0) {
				// nothing to delete
			} else if (n_sys_params==1) {
				int k;
				for (k=0; k < 2; k++) {
					delete[] vcm_like1[k];
					delete[] vcm_bincount1[k];
				}
			} else if (n_sys_params==2) {
				int k,l;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						delete[] vcm_like2[k][l];
						delete[] vcm_bincount2[k][l];
					}
				}
			} else if (n_sys_params==3) {
				int k,l,m;
				for (k=0; k < 2; k++) {
					for (l=0; l < sys_param_steps; l++) {
						for (m=0; m < sys_param_steps2; m++) {
							delete[] vcm_like3[k][l][m];
							delete[] vcm_bincount3[k][l][m];
						}
					}
				}
			}
		}
	}
};

void *bin_dv_multi_thread(void *dataptr);
void *bin_vcm_multi_thread(void *dataptr);
void *bin_dv_full_likelihood(void *dataptr);
void *bin_logdv_2epoch_likelihood(void *dataptr);

double ndev(double min, double max, Random_Sequence&); // Generates normal deviates
double normal_deviate(Random_Sequence&);

double gen_m_kroupa ();
inline double find_stellar_radius(double mass);
double gen_logP(void);
double gen_q(double logP);
double gen_q_white_dwarf (void);
double gen_q_experimental(double logP);
double gen_q_experimental(double logP, Random_Sequence &rand);

double qroot_equation(const double q);
double find_qroot(const double w);
double qdist(const double q);
double xdist_integrand(const double theta);
double xdist_integral(const double x);

double gen_phi(double e, double &time);
double gen_e(double logP);
double gen_e_experimental(double logP);
double gen_e_experimental(double logP, Random_Sequence &rand);
double generate_nonmember_velocity(void);
void generate_euler_angles(double &theta, double &psi);
double evolve_phi(double e, double time);
void generate_binary_velocities_multi_thread(Thread_Data &thread_data, dvector &vbin,  lvector &times);

inline double gen_logP(Random_Sequence &rand);
inline double gen_q(double logP, Random_Sequence &rand);
inline double gen_e(double logP, Random_Sequence &rand);
inline double gen_phi_multi_thread(const double e, double &time, Random_Sequence &rand);
inline void generate_euler_angles(double &theta, double &psi, Random_Sequence &rand);
inline double evolve_phi_multi_thread(const double e, const double time);

double anomaly_time_equation(double anomaly);
double anomaly_time_equation_multi_thread(const double anomaly);
double find_eta_root(const double eval, const double timeval, const double x1, const double x2, const double tol);
double eta2phi (double phi, double e);

double primary_semimajor_axis(double logP, double M, double q);
double roche_lobe_radius_approximation(double a_orbit, double q);
long double vbin_los(double a, double period, double e, double theta, double psi, double phi);
double vbin_tot(double a, double period, double e, double phi);
double find_absolute_magnitude(double distance, double magnitude);

double generate_velocity_error(double &sigma_error);
double generate_velocity_error_from_rvalue(double &sigma_error, double &rvalue, int &channel);
double orbital_separation_semimajor_axis(double logP, double M, double q);

double find_stellar_mass(double absolute_mag);
void generate_velocities(double binfrac, const string vbinfilename, const string logvbinfilename, const string vfilename, const string logdvfilename, const string dvfilename, const string vdatafilename, const int add_to_seed);

void generate_magnitude_distribution(double distance);
double magnitude_from_mass(double mass);
double mass_mag_root(double mass);

double generate_nonmember_position(void);
double generate_position_from_plummer_profile(void);
void plot_sigma_posterior(const string datafile);
void find_approximate_velocity_params(char *datafile, double &vsys, double &disp);

void plot_binary_period_loglikelihood(void);
double binary_period_loglikelihood(double sigma_mu);
double binary_period_likelihood_integrand(double sigma);
void create_duquennoy_mayor_dataset(int nn_logP_datapoints);
double like_integrand(double vcm);
void output_fake_galaxy_data_real_errors(double binfrac, char *datafile);
void plot_new_qdist(void);
double gen_q_myfit (double logP);
double find_compact_binary_ratio(const double observed_mass);

void remove_comments(string& instring);
void remove_equal_sign(int& nwords, vector<string>& words, stringstream *ws);


