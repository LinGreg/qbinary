#include <iostream>
#include <cstring>
#include <fstream>
#include <sstream>
#include "stdlib.h"
#include "mcmchdr.h"
#include "mcmc.h"
#include "mathexpr.h"
#include "vector.h"
#include "matrix.h"
#include "errors.h"
#include "spline.h"
#include <sys/stat.h>

#ifdef USE_OPENMP
#include <omp.h>
#endif

using namespace std;

bool bicubic_interpolation;
bool nonconservative_prior;
bool log_dispersion_prior;
ofstream logfile;
double fsys = 0.75;

class Galaxy : public UCMC
{
	private:
	int nstars;
	bool include_binaries;
	bool include_metallicities;
	bool include_error_params;
	bool include_MW_likelihood;
	Star *star;

	bool start;
	dvector likebvec, likenbvec;

	double binfrac;
	int mu_logP_nn, sig_logP_nn;
	dvector mu_logP_vals, sig_logP_vals;
	double mu_logP, sig_logP;
	int mu_logP_k, sig_logP_l; // used for interpolating to find rfactor for given mu_logP, sig_logP
	double uu, tt, ww; // used for interpolating to find rfactor for given mu_logP, sig_logP

	double wavg_gal, wsig_gal;
	double wavg_mw, wsig_mw;
	double plummer_scale_radius;

	double dispersion, systemic_velocity;
	double member_fraction;
	double MW_mean_velocity, MW_dispersion; // if MW velocity distribution is modeled as Gaussian
	Spline MW_Likelihood;

	int magvals_nn, n_logdv_vals;
	double logdv_vals_max, logdv_vals_min;
	int k_longtime, mag_number, year_number;
	double nfactor_normalizing_factor;
	dvector mag_vals, logdv_vals, dv_vals;
	static const int n_logdvlike_years = 4;
	dvector **logdv_likelihoods;

	double mwlike_vcm_min, mwlike_vcm_max;
	int mwlike_vcm_nn;
	dvector mwlike_vcm_vals;
	dvector mwlike_vcm;

	int star_i;

	int **wt;

	public:

	bool include_sysparams;
	int n_sysparams;
	int sysparam_nn[3];
	int sysparam_type[3];
	dvector sysparam_vals[3];
	double sysparam1, sysparam2, sysparam3;
	int sysparam1_k, sysparam2_l, sysparam3_m; // used for interpolating to find rfactor for given sysparam1, sysparam2

	Galaxy() : UCMC() { star = NULL; wt = NULL; logdv_likelihoods = NULL; include_sysparams = false; }
	Galaxy(Mode loglike_mode);
	~Galaxy();
	void Count_Stars(void);
	double LogLike_No_Binaries_No_MW(double *a);
	double LogLike_Binaries_No_MW(double *a);
	double LogLike_No_Binaries_No_MW_V_Sysparam(double *a);
	double LogLike_No_Binaries_No_MW_V_Sysparam2(double *a);
	double LogLike_No_Binaries_No_MW_V_Sysparam3(double *a);
	double LogLike_Binaries_No_MW_V_Sysparam(double *a);
	double LogLike_Binaries_No_MW_V_Sysparam2(double *a);
	double LogLike_Binaries_No_MW_V_Sysparam3(double *a);
	double LogLike_Binaries_No_MW_V_Fixed_Sysparam3(double *a);
	double LogLike_DV_No_MW(double *a);
	double LogLike_DV_Fixed_Binary_Fraction(double *a);
	double LogLike_DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params(double *a);
	double LogLike_DVW_No_Period_Params(double *a);
	double LogLike_W_Only(double *a);
	double LogLike_W_Only_Fixed_Metallicity_Params(double *a);
	double LogLike_DV_Fixed_Metallicity_Params_logdvlike(double *a);
	double LogLike_DVW_logdvlike(double *a);
	double LogLike_DV_Fixed_Metallicity_Params_No_Period_Params(double *a);
	double LogLike_DV_Fixed_Metallicity_Params(double *a);
	double LogLike_DV_No_MW_No_Period_Params(double *a);
	double LogLike_DV_Error_Model(double *a);
	double LogLike_DV_Error_Model_with_Binaries(double *a);
	double LogLike_DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel(double *a);
	double LogLike_DV_Error_Model_with_Binaries_Fixed_W_Params(double *a);
	double LogLike_DV_Error_Model_Extended(double *a);
	double LogLike_DV_Error_Model_Metallicity(double *a);
	double LogLike_DV_logdvlike(double *a);
	double LogLike_MW_Velocities_No_Binaries(double *a);
	double LogLike_Binaries_MW_No_Metallicities(double *a);
	double LogLike_Binaries_VR(double *a);
	double LogLike_Binaries_VR_Fixed_Metallicity_Params(double *a);
	double LogLike_Binaries_VR_Fixed_W_Rs(double *a);
	double LogLike_MW_Velocities_Metallicities_No_Binaries(double *a);
	double LogLike_Binaries_VW_No_Period_Params(double *a);
	double LogLike_Binaries_VW(double *a);
	double LogLike_Binaries_V_Fixed_W_Params_No_Period_Params(double *a);
	double LogLike_Binaries_V_Fixed_W_Params(double *a);
	double LogLike_Binaries_V_Fixed_W_Params_Test_RTables(double *a);
	double LogLike_Binaries_V_No_MW(double *a);
	double LogLike_Full_Monty(double *a);
	double LogLike_Full_Monty_No_Period_Params(double *a);
	double LogLike_Gauss_Hermite(double *a);
	double LogLike_Test(double *a);
	void input_datafile(char *datafile_str, bool record_position);
	void input_datafile_jfactors(char* datafile_str, bool record_position);
	void input_datafile_rtables(char* datafile_str, bool record_position);
	void input_datafile_sysparams(char* datafile_str, bool record_position);
	void input_datafile_jtables(char* datafile_str, bool record_position);
	void input_datafile_binary_logdv_likelihoods(char* datafile_str, bool record_position);
	void load_bicubic_interpolation_coefficients(void);
	void load_data(ifstream &datafile, bool record_position);
	void calculate_MW_likelihoods(void);
	void input_datafile_velocities_only(char *datafile_str);
	void interpolate_period_params(void);
	void interpolate_sysparams(int sysparams);

	void set_period_params(const double mu, const double sig) { mu_logP=mu; sig_logP=sig; }
	void set_binfrac(const double b) { binfrac=b; }
	double jfactor_integrand_interpolate(const int &j);
	double jfactor_integrand_interpolate_sysparam(const int &j);
	void input_MW_Likelihood(char *datafile);
	double mw_likelihood_integrand(const int j);
	void test_likelihood(void);
	void test_rfactor_interpolate(void);
	void test_jtable_interpolate(void);
	void test_2epoch_likelihood(void);
	void load_metallicity_params(void)
	{
		ifstream wparams_file("metallicity_params.dat");
		wparams_file >> wavg_gal >> wsig_gal >> wavg_mw >> wsig_mw;
	}
	void load_metallicity_params(string galaxy_name)
	{
		string wparams_filename = "metallicity_params." + galaxy_name;
		ifstream wparams_file(wparams_filename.c_str());
		wparams_file >> wavg_gal >> wsig_gal >> wavg_mw >> wsig_mw;
	}
	void set_scale_radius(double rs) { plummer_scale_radius = rs; }
	void set_no_metallicities() { include_metallicities = false; }
	void set_no_error_params() { include_error_params = false; }
	void get_musig_bounds(double &mulogP_min, double &mulogP_max, double &siglogP_min, double &siglogP_max) {
		mulogP_min = mu_logP_vals[0]; mulogP_max = mu_logP_vals[mu_logP_nn-1];
		if (nonconservative_prior==true) {
			siglogP_min = 1; siglogP_max = 3; // non-conservative prior
		} else {
			siglogP_min = sig_logP_vals[0]; siglogP_max = sig_logP_vals[sig_logP_nn-1];
		}
	}
	void get_sysparam_bounds(double& lmin, double& lmax) {
		lmin = sysparam_vals[0][0];
		lmax = sysparam_vals[0][sysparam_nn[0]-1];
	}
	void get_sysparam2_bounds(double& l1min, double& l1max, double& l2min, double& l2max) {
		l1min = sysparam_vals[0][0];
		l1max = sysparam_vals[0][sysparam_nn[0]-1];
		l2min = sysparam_vals[1][0];
		l2max = sysparam_vals[1][sysparam_nn[1]-1];
	}
	void get_sysparam3_bounds(double& l1min, double& l1max, double& l2min, double& l2max, double& l3min, double& l3max) {
		l1min = sysparam_vals[0][0];
		l1max = sysparam_vals[0][sysparam_nn[0]-1];
		l2min = sysparam_vals[1][0];
		l2max = sysparam_vals[1][sysparam_nn[1]-1];
		l3min = sysparam_vals[2][0];
		l3max = sysparam_vals[2][sysparam_nn[2]-1];
	}
	double rfactor_interpolate(const int &j)
	{
		double rfactor;
		if (bicubic_interpolation==false) {
			// bilinear interpolation in (mu_logP, sig_logP) using the four surrounding grid points in the rtable
			rfactor = (1-tt)*(1-uu)*star[star_i].rtable[j][mu_logP_k][sig_logP_l] + tt*(1-uu)*star[star_i].rtable[j][mu_logP_k+1][sig_logP_l]
								+ (1-tt)*uu*star[star_i].rtable[j][mu_logP_k][sig_logP_l+1] + tt*uu*star[star_i].rtable[j][mu_logP_k+1][sig_logP_l+1];
		} else {
			// bicubic interpolation in (mu_logP, sig_logP) using the four surrounding grid points in the rtable
			rfactor = star[star_i].rctable[j][mu_logP_k][sig_logP_l].bicubic_interpolate(tt,uu);
		}
		return rfactor;
	}
	double rlfactor_interpolate(const int &j)
	{
		if (star[star_i].n_sysparams==0) {
			return star[star_i].rltable1[j][0];
		} else if (star[star_i].n_sysparams==1) {
			double sysp;
			if (star[star_i].sysparam_order[0]==0) sysp = sysparam1;
			else if (star[star_i].sysparam_order[0]==1) sysp = sysparam2;
			else if (star[star_i].sysparam_order[0]==2) sysp = sysparam3;
			//cout << star[star_i].rltable_spline[j].xmin() << " " << star[star_i].rltable_spline[j].xmax() << endl;
			return star[star_i].rltable_spline[j].splint(sysp);
		} else if (star[star_i].n_sysparams==2) {
			int sys3[2];
			double vv[2];
			for (int i=0; i < 2; i++) {
				if (star[star_i].sysparam_order[i]==0) { sys3[i] = sysparam1_k; vv[i] = tt; }
				else if (star[star_i].sysparam_order[i]==1) { sys3[i] = sysparam2_l; vv[i] = uu; }
				else if (star[star_i].sysparam_order[i]==2) { sys3[i] = sysparam3_m; vv[i] = ww; }
				else die("sysparam not recognized");
			}

			double rlfactor;
			// bilinear interpolation in (sysparam1, sysparam2) using the four surrounding grid points in the rtable
			//if (j==50) cout << "I " << star[star_i].rltable2[50][sysparam1_k][sysparam2_l] << endl;
			rlfactor = (1-vv[0])*(1-vv[1])*star[star_i].rltable2[j][sys3[0]][sys3[1]] + vv[0]*(1-vv[1])*star[star_i].rltable2[j][sys3[0]+1][sys3[1]]
								+ (1-vv[0])*vv[1]*star[star_i].rltable2[j][sys3[0]][sys3[1]+1] + vv[0]*vv[1]*star[star_i].rltable2[j][sys3[0]+1][sys3[1]+1];
			return rlfactor;
		} else if (star[star_i].n_sysparams==3) {
			double rlfactor;
			int sys3[3];
			double vv[3];
			for (int i=0; i < 3; i++) {
				if (star[star_i].sysparam_order[i]==0) { sys3[i] = sysparam1_k; vv[i] = tt; }
				else if (star[star_i].sysparam_order[i]==1) { sys3[i] = sysparam2_l; vv[i] = uu; }
				else if (star[star_i].sysparam_order[i]==2) { sys3[i] = sysparam3_m; vv[i] = ww; }
				else die("sysparam not recognized");
			}

			// trilinear interpolation in (sysparam1, sysparam2, sysparam2) using the four surrounding grid points in the rtable
			//if (j==50) cout << "I " << star[star_i].rltable3[50][sysparam1_k][sysparam2_l][sysparam3_m] << endl;
			rlfactor = (1-vv[0])*(1-vv[1])*(1-vv[2])*star[star_i].rltable3[j][sys3[0]][sys3[1]][sys3[2]]
						+ vv[0]*(1-vv[1])*(1-vv[2])*star[star_i].rltable3[j][sys3[0]+1][sys3[1]][sys3[2]]
						+ (1-vv[0])*vv[1]*(1-vv[2])*star[star_i].rltable3[j][sys3[0]][sys3[1]+1][sys3[2]]
						+ vv[0]*vv[1]*(1-vv[2])*star[star_i].rltable3[j][sys3[0]+1][sys3[1]+1][sys3[2]]
						+ (1-vv[0])*(1-vv[1])*vv[2]*star[star_i].rltable3[j][sys3[0]][sys3[1]][sys3[2]+1] 
						+ vv[0]*(1-vv[1])*vv[2]*star[star_i].rltable3[j][sys3[0]+1][sys3[1]][sys3[2]+1]
						+ (1-vv[0])*vv[1]*vv[2]*star[star_i].rltable3[j][sys3[0]][sys3[1]+1][sys3[2]+1]
						+ vv[0]*vv[1]*vv[2]*star[star_i].rltable3[j][sys3[0]+1][sys3[1]+1][sys3[2]+1];

			return rlfactor;
		}
	}
	double jfactor_interpolate(void)
	{
		double jfactor;
		if (bicubic_interpolation==false) {
		// bilinear interpolation in (mu_logP, sig_logP) using the four surrounding grid points in the rtable
		jfactor = (1-tt)*(1-uu)*star[star_i].jtable[mu_logP_k][sig_logP_l] + tt*(1-uu)*star[star_i].jtable[mu_logP_k+1][sig_logP_l]
							+ (1-tt)*uu*star[star_i].jtable[mu_logP_k][sig_logP_l+1] + tt*uu*star[star_i].jtable[mu_logP_k+1][sig_logP_l+1];
		} else {
			// bicubic interpolation in (mu_logP, sig_logP) using the four surrounding grid points in the rtable
			jfactor = star[star_i].jctable[mu_logP_k][sig_logP_l].bicubic_interpolate(tt,uu);
		}
		return jfactor;
	}

	double jfactor_integrand(const int j);
	double logdv_likelihood_integrand(const int m);
	double logdv_likelihood_integrand_no_errmodel(const int m);
	double calculate_nfactor(dvector& vlos_data, dvector& v_errors, double &v_avg, double &sigm);
	double test_logdv_likelihood_integrand(const int m, const dvector &dv, const dvector &sig_errors);
	void reset_nstars(int nn) { nstars = nn; }
	double loglikefunc(double *a);
};

bool reset_number_of_stars;
bool zero_systemic_velocity;
bool use_gaussian_milky_way;
bool specified_velocity_params;
bool output_to_logfile;
int nstars_subset;
double systemic_velocity_approx;
double dispersion_approx;
double scale_radius;
bool record_positions;
bool no_metallicities;
bool no_error_params;
bool use_finished_stars;
double distance_kpc;
double rvalue_clip_threshold;
string galaxy_name;
bool galaxy_name_specified;
bool load_dvlikes_not_jfactors;
int live_pts;
bool fix_member_fraction;
double fixed_member_fraction;
double metallicity_limit;
double metallicity_lowlimit;
int mpi_np, mpi_id;
bool include_v_sysparams;
int n_sys_params;

int main(int argc, char *argv[])
{
	// defaults if MPI is not used
	mpi_np=1;
	mpi_id=0;

#ifdef USE_MPI
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_np);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_id);
#endif

	Mode mode;
	char datafile[80] = "";
	char mw_likelihood_file[40] = "";
	reset_number_of_stars = false; // setting this to true (by 'N##' command) allows user to take a smaller subset of stars from the data, for faster computation times
	zero_systemic_velocity = false;
	use_gaussian_milky_way = false;
	specified_velocity_params = false;
	record_positions = false;
	no_metallicities = true;
	bicubic_interpolation = false;
	use_finished_stars = false;
	no_error_params = true;
	distance_kpc=1e30; // default; if left alone, the program will expect absolute magnitude as input, rather than apparent magnitude
	galaxy_name_specified = false;
	char galaxy_char[30] = "";
	rvalue_clip_threshold = 0;
	load_dvlikes_not_jfactors = false;
	live_pts = 2000;
	fix_member_fraction = false;
	metallicity_limit = 1000; // jury-rigging--take this out later!
	metallicity_lowlimit = -1000; // jury-rigging--take this out later!
	nonconservative_prior = false;
	output_to_logfile = false;
	bool check_wtime = true;
	include_v_sysparams = false;
	n_sys_params = 0;
	log_dispersion_prior = true;

	for (int i = 1; i < argc; i++)    // Process command-line arguments 
	{
		if ((*argv[i] == '-') and (isalpha(*(argv[i]+1))))
		{
			while (int c = *++argv[i])
			{
				switch (c) {
				case 'c': bicubic_interpolation = true; break;
				case 'D': log_dispersion_prior = false; break;
				case 'v':
					mode = Dispersion_Posterior_No_Binaries;
					if (sscanf(argv[i], "D:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'z': zero_systemic_velocity = true; break;
				//case 'G': use_gaussian_milky_way = true; break;
				case 'N': use_finished_stars = true; break;
				case 'n':
					if (sscanf(argv[i], "n%i", &nstars_subset)==0) die("invalid number of stars");
					if (nstars_subset <= 0) die("negative number of stars not allowed");
					reset_number_of_stars = true;
					argv[i] = advance(argv[i]);
					break;
				case 'l':
					if (sscanf(argv[i], "l%i", &live_pts)==0) die("invalid number of live points");
					argv[i] = advance(argv[i]);
					break;
				case 'b':
					mode = Binaries_No_MW;
					if (sscanf(argv[i], "b:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				//case 'C':
					//if (sscanf(argv[i], "C%lf", &rvalue_clip_threshold)==0) die("invalid rvalue clip threshold");
					//argv[i] = advance(argv[i]);
					//break;
				case 's':
					if (sscanf(argv[i], "s%lf", &scale_radius)==0) die("invalid scale radius");
					argv[i] = advance(argv[i]);
					break;
				case 'V':
					if (sscanf(argv[i], "V%lf", &systemic_velocity_approx)==0) die("invalid systemic velocity");
					argv[i] = advance(argv[i]);
					specified_velocity_params = true;
					break;
				case 'd':
					if (sscanf(argv[i], "d%lf", &dispersion_approx)==0) die("invalid dispersion");
					argv[i] = advance(argv[i]);
					specified_velocity_params = true;
					break;
				case 'X': record_positions = true; break;
				case 'Y': no_metallicities = false; break;
				case 'Z': no_error_params = false; break;
				case 'H': nonconservative_prior = true; break;
				//case 'D': load_dvlikes_not_jfactors = true; break;
				case 'k':
					if (sscanf(argv[i], "k%lf", &distance_kpc)==0) die("invalid distance");
					argv[i] = advance(argv[i]);
					break;
				//case 'v':
					//mode = Binaries_MW_No_Metallicities;
					//if (sscanf(argv[i], "v:%s", datafile)==1) {
						//argv[i] += (1 + strlen(datafile));
					//}
					//break;
				case 'R':
					mode = Binaries_VR;
					if (sscanf(argv[i], "R:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'S':
					mode = Binaries_VR_Fixed_W_Rs;
					if (sscanf(argv[i], "S:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'r':
					mode = Binaries_VR_Fixed_Metallicity_Params;
					if (sscanf(argv[i], "r:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'M':
					mode = Full_Monty_No_Period_Params;
					if (sscanf(argv[i], "M:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'P':
					mode = Full_Monty;
					if (sscanf(argv[i], "P:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'p':
					mode = Binaries_V_No_MW;
					if (sscanf(argv[i], "p:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'T':
					mode = Binaries_V_Fixed_W_Params_Test_RTables;
					if (sscanf(argv[i], "T:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 't':
					mode = Test;
					cout << "Testing mode...\n";
					if (sscanf(argv[i], "t:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'w':
					mode = Binaries_VW_No_Period_Params;
					if (sscanf(argv[i], "w:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'W':
					mode = Binaries_VW;
					if (sscanf(argv[i], "W:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'x':
					mode = Binaries_V_Fixed_W_Params_No_Period_Params;
					if (sscanf(argv[i], "x:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'j':
					mode = DV_No_MW_No_Period_Params;
					if (sscanf(argv[i], "j:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'J':
					mode = DV_No_MW;
					if (sscanf(argv[i], "J:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'K':
					mode = DV_Fixed_Metallicity_Params_No_Period_Params;
					if (sscanf(argv[i], "K:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'y':
					mode = DVW_No_Period_Params;
					if (sscanf(argv[i], "y:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'L': output_to_logfile = true; break;
				case 'e':
					mode = DV_Error_Model;
					if (sscanf(argv[i], "e:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'E':
					mode = DV_Error_Model_with_Binaries;
					if (sscanf(argv[i], "E:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'F':
					mode = DV_Error_Model_with_Binaries_Fixed_W_Params;
					if (sscanf(argv[i], "F:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'C':
					mode = DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel;
					if (sscanf(argv[i], "C:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'q':
					mode = DV_logdvlike;
					if (sscanf(argv[i], "q:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'Q':
					mode = DVW_logdvlike;
					if (sscanf(argv[i], "Q:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'u':
					mode = DV_Fixed_Metallicity_Params_logdvlike;
					if (sscanf(argv[i], "u:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'o':
					if (*(argv[i]+1)=='3') {
						mode = No_Binaries_No_MW_V_Sysparam3;
						include_v_sysparams = true;
						n_sys_params = 3;
						if (sscanf(argv[i], "o3:%s", datafile)==1) {
							argv[i] += (2 + strlen(datafile));
						}
					} else if (*(argv[i]+1)=='2') {
						mode = No_Binaries_No_MW_V_Sysparam2;
						include_v_sysparams = true;
						n_sys_params = 2;
						if (sscanf(argv[i], "o2:%s", datafile)==1) {
							argv[i] += (2 + strlen(datafile));
						}
					} else {
						mode = No_Binaries_No_MW_V_Sysparam;
						include_v_sysparams = true;
						n_sys_params = 1;
						if (sscanf(argv[i], "o:%s", datafile)==1) {
							argv[i] += (1 + strlen(datafile));
						}
					}
					break;
					//mode = W_Only_Fixed_Metallicity_Params;
					//if (sscanf(argv[i], "o:%s", datafile)==1) {
						//argv[i] += (1 + strlen(datafile));
					//}
					//break;
				case 'O':
					if (*(argv[i]+1)=='4') {
						mode = Binaries_No_MW_V_Fixed_Sysparam3;
						include_v_sysparams = true;
						if (sscanf(argv[i], "O4:%s", datafile)==1) {
							argv[i] += (2 + strlen(datafile));
						}
					} else if (*(argv[i]+1)=='3') {
						mode = Binaries_No_MW_V_Sysparam3;
						include_v_sysparams = true;
						if (sscanf(argv[i], "O3:%s", datafile)==1) {
							argv[i] += (2 + strlen(datafile));
						}
					} else if (*(argv[i]+1)=='2') {
						mode = Binaries_No_MW_V_Sysparam2;
						include_v_sysparams = true;
						n_sys_params = 2;
						if (sscanf(argv[i], "O2:%s", datafile)==1) {
							argv[i] += (2 + strlen(datafile));
						}
					} else {
						mode = Binaries_No_MW_V_Sysparam;
						include_v_sysparams = true;
						n_sys_params = 1;
						if (sscanf(argv[i], "O:%s", datafile)==1) {
							argv[i] += (1 + strlen(datafile));
						}
					}
					break;
				//case 'E':
					//mode = DV_Error_Model_Extended;
					//if (sscanf(argv[i], "E:%s", datafile)==1) {
						//argv[i] += (1 + strlen(datafile));
					//}
					//break;
				//case 'E':
					//mode = DV_Error_Model_Metallicity;
					//if (sscanf(argv[i], "E:%s", datafile)==1) {
						//argv[i] += (1 + strlen(datafile));
					//}
					//break;
				//case 'b':
					//mode = DV_Fixed_Binary_Fraction;
					//if (sscanf(argv[i], "b:%s", datafile)==1) {
						//argv[i] += (1 + strlen(datafile));
					//}
					//break;
				case 'm':
					mode = MW_Velocities_No_Binaries;
					if (sscanf(argv[i], "m:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				//case 'f':
					//if (sscanf(argv[i], "f:%s", mw_likelihood_file)==1) {
						//argv[i] += (1 + strlen(mw_likelihood_file));
					//}
					//break;
				case 'f':
					if (sscanf(argv[i], "f%lf", &fixed_member_fraction)==0) die("invalid member fraction");
					fix_member_fraction = true;
					argv[i] = advance(argv[i]);
					break;
				case 'U':
					if (sscanf(argv[i], "U%lf", &metallicity_lowlimit)==0) die("invalid metallicity limit");
					argv[i] = advance(argv[i]);
					break;
				//case 'O':
					//if (sscanf(argv[i], "O%lf", &metallicity_limit)==0) die("invalid metallicity limit");
					//argv[i] = advance(argv[i]);
					//break;
				case 'g':
					galaxy_name_specified = true;
					if (sscanf(argv[i], "g:%s", galaxy_char)==1) {
						argv[i] += (1 + strlen(galaxy_char));
					}
					galaxy_name.assign(galaxy_char);
					break;
				case 'G':
					mode = Gauss_Hermite;
					if (sscanf(argv[i], "G:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				}
			}
		}
	}

	if (strcmp(mw_likelihood_file,"")==0) {
		string galaxy_name(datafile);
		string mwfilename = "mwlike." + galaxy_name;
		strcpy(mw_likelihood_file,mwfilename.c_str());
	}

	if (galaxy_name_specified) {
		if (galaxy_name=="sculptor") {
			distance_kpc = 86;
		} else if (galaxy_name=="fornax") {
			distance_kpc = 140;
		} else if (galaxy_name=="carina") {
			distance_kpc = 103;
		} else if (galaxy_name=="sextans") {
			distance_kpc = 87;
		} else die("unknown galaxy specified");
	}

	string rdir(datafile);
	string logfilename = "logfile." + rdir;
	if ((mpi_id==0) and (output_to_logfile)) logfile.open(logfilename.c_str());

	double clocktime0, wtime;
#ifdef USE_OPENMP
	if (check_wtime) clocktime0 = omp_get_wtime();
#endif

	if (mode==Dispersion_Posterior_No_Binaries)
	{
		posteriors_No_Binaries_No_MW(datafile,mode);
	}
	else if (mode==Binaries_No_MW)
	{
		posteriors_Binaries_No_MW(datafile,mode);
	}
	else if (mode==No_Binaries_No_MW_V_Sysparam3)
	{
		posteriors_No_Binaries_No_MW_V_Sysparam3(datafile,mode);
	}
	else if (mode==No_Binaries_No_MW_V_Sysparam2)
	{
		posteriors_No_Binaries_No_MW_V_Sysparam2(datafile,mode);
	}
	else if (mode==No_Binaries_No_MW_V_Sysparam)
	{
		posteriors_No_Binaries_No_MW_V_Sysparam(datafile,mode);
	}
	else if (mode==Binaries_No_MW_V_Sysparam)
	{
		posteriors_Binaries_No_MW_V_Sysparam(datafile,mode);
	}
	else if (mode==Binaries_No_MW_V_Fixed_Sysparam3)
	{
		posteriors_Binaries_No_MW_V_Fixed_Sysparam3(datafile,mode);
	}
	else if (mode==Binaries_No_MW_V_Sysparam3)
	{
		posteriors_Binaries_No_MW_V_Sysparam3(datafile,mode);
	}
	else if (mode==Binaries_No_MW_V_Sysparam2)
	{
		posteriors_Binaries_No_MW_V_Sysparam2(datafile,mode);
	}
	else if (mode==MW_Velocities_No_Binaries)
	{
		posteriors_MW_Velocities_No_Binaries(datafile,mw_likelihood_file,mode);
	}
	else if (mode==MW_Velocities_Metallicities_No_Binaries)
	{
		posteriors_MW_Velocities_Metallicities_No_Binaries(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_VW_No_Period_Params)
	{
		posteriors_Binaries_VW_No_Period_Params(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_VW)
	{
		posteriors_Binaries_VW(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_V_Fixed_W_Params_No_Period_Params)
	{
		posteriors_Binaries_V_Fixed_W_Params_No_Period_Params(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_V_Fixed_W_Params)
	{
		posteriors_Binaries_V_Fixed_W_Params(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_V_Fixed_W_Params_Test_RTables)
	{
		posteriors_Binaries_V_Fixed_W_Params_Test_RTables(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_V_No_MW)
	{
		posteriors_Binaries_V_No_MW(datafile,mode);
	}
	else if (mode==Binaries_MW_No_Metallicities)
	{
		posteriors_Binaries_MW_No_Metallicities(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_VR)
	{
		posteriors_Binaries_VR(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_VR_Fixed_Metallicity_Params)
	{
		posteriors_Binaries_VR_Fixed_Metallicity_Params(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Binaries_VR_Fixed_W_Rs)
	{
		posteriors_Binaries_VR_Fixed_W_Rs(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Full_Monty_No_Period_Params)
	{
		posteriors_Full_Monty_No_Period_Params(datafile,mw_likelihood_file,mode);
	}
	else if (mode==Full_Monty)
	{
		posteriors_Full_Monty(datafile,mw_likelihood_file,mode);
	}
	else if (mode==DV_No_MW)
	{
		posteriors_DV_No_MW(datafile,mode);
	}
	else if (mode==DV_No_MW_No_Period_Params)
	{
		posteriors_DV_No_MW_No_Period_Params(datafile,mode);
	}
	else if (mode==DV_Fixed_Binary_Fraction)
	{
		posteriors_DV_Fixed_Binary_Fraction(datafile,mode);
	}
	else if (mode==DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params)
	{
		posteriors_DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params(datafile,mode);
	}
	else if (mode==DV_Fixed_Metallicity_Params)
	{
		posteriors_DV_Fixed_Metallicity_Params(datafile,mode);
	}
	else if (mode==W_Only_Fixed_Metallicity_Params)
	{
		posteriors_W_Only_Fixed_Metallicity_Params(datafile,mode);
	}
	else if (mode==W_Only)
	{
		posteriors_W_Only(datafile,mode);
	}
	else if (mode==DVW_No_Period_Params)
	{
		posteriors_DVW_No_Period_Params(datafile,mode);
	}
	else if (mode==DVW_logdvlike)
	{
		posteriors_DVW_logdvlike(datafile,mode);
	}
	else if (mode==DV_Fixed_Metallicity_Params_logdvlike)
	{
		posteriors_DV_Fixed_Metallicity_Params_logdvlike(datafile,mode);
	}
	else if (mode==DV_Fixed_Metallicity_Params_No_Period_Params)
	{
		posteriors_DV_Fixed_Metallicity_Params_No_Period_Params(datafile,mode);
	}
	else if (mode==DV_Error_Model)
	{
		posteriors_DV_Error_Model(datafile,mode);
	}
	else if (mode==DV_Error_Model_with_Binaries)
	{
		posteriors_DV_Error_Model_with_Binaries(datafile,mode);
	}
	else if (mode==DV_Error_Model_with_Binaries_Fixed_W_Params)
	{
		posteriors_DV_Error_Model_with_Binaries_Fixed_W_Params(datafile,mode);
	}
	else if (mode==DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel)
	{
		posteriors_DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel(datafile,mode);
	}
	else if (mode==DV_Error_Model_Extended)
	{
		posteriors_DV_Error_Model_Extended(datafile,mode);
	}
	else if (mode==DV_Error_Model_Metallicity)
	{
		posteriors_DV_Error_Model_Metallicity(datafile,mode);
	}
	else if (mode==DV_logdvlike)
	{
		posteriors_DV_logdvlike(datafile,mode);
	}
	else if (mode==Gauss_Hermite)
	{
		gauss_hermite(datafile,mode);
	}
	else if (mode==Test)
	{
		posteriors_test(datafile,mw_likelihood_file,mode);
	}

#ifdef USE_OPENMP
	if ((mpi_id==0) and (check_wtime)) {
		wtime = ((double) (omp_get_wtime() - clocktime0));
		if ((mpi_id==0) and (output_to_logfile)) logfile << "total wall time: " << wtime << endl;
		else cout << "total wall time: " << wtime << endl;
	}
#endif

#ifdef USE_MPI
	MPI_Finalize();
#endif
}

void mkdir(const string& dir)
{
	struct stat sb;
	stat(dir.c_str(),&sb);
	if (S_ISDIR(sb.st_mode)==false)
		mkdir(dir.c_str(),S_IRWXU | S_IRWXG);
}

void output_param_files(int nparams, string* paramnames, string *latex_paramnames, double* lowerlimit, double* upperlimit, string paramnames_filename, string latex_paramnames_filename, string ranges_filename)
{
	ofstream paramnames_file(paramnames_filename.c_str());
	for (int i=0; i < nparams; i++) paramnames_file << paramnames[i] << endl;
	paramnames_file.close();
	ofstream latex_paramnames_file(latex_paramnames_filename.c_str());
	for (int i=0; i < nparams; i++) {
		latex_paramnames_file << paramnames[i] << "\t" << latex_paramnames[i] << endl;
	}
	latex_paramnames_file.close();

	ofstream ranges_file(ranges_filename.c_str());
	for (int i=0; i < nparams; i++) ranges_file << lowerlimit[i] << " " << upperlimit[i] << endl;
	ranges_file.close();
}

void posteriors_No_Binaries_No_MW(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	if (no_metallicities) galaxy.set_no_metallicities();
	galaxy.input_datafile(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;
	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 6*disp_3clip;
		mumax=mu_3clip + 6*disp_3clip;
		sigmin = 0.03;
		sigmax = 2.5*disp_3clip;
	}
	//sigmin=0.1; sigmax = 5;
	//mumin = -108; mumax = -94;
	//double temp=mumax; mumax = -mumin; mumin = -temp;
	//if ((mumin < 0) and (mumax < 0)) { double temp=mumax; mumax = mumin; mumin = temp; }
	if (mpi_id==0) {
		cout << "mumin=" << mumin << ", mumax=" << mumax << endl;
		cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;
	}

	const int dim = 2;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin};
	double upperlimit[dim] = {mumax,sigmax};
	string paramnames[dim] = {"vsys","sigma"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	//double mu, sig, mustep, sigstep;
	//int mu_nn=20, sig_nn=20;
	//mustep = (mumax-mumin)/(mu_nn-1);
	//sigstep = (sigmax-sigmin)/(sig_nn-1);
	//int i, j;
	//double chisq;
	//for (i=0, mu=mumin; i < mu_nn; i++, mu += mustep) cout << mu << endl;
	//for (j=0, sig=sigmin; j < sig_nn; j++, sig += sigstep) cout << sig << endl;
	//die();
	//for (i=0, mu=mumin; i < mu_nn; i++, mu += mustep) {
		//for (j=0, sig=sigmin; j < sig_nn; j++, sig += sigstep) {
			//pt[0]=mu;
			//pt[1]=sig;
			//chisq = galaxy.loglikefunc(pt);
			//cout << mu << " " << sig << " " << chisq << endl;
		//}
		//cout << endl;
	//}
	//die();

	int number_of_live_pts = live_pts;
	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "nobins." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_No_MW(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double bmin=0, bmax=1;

	const int dim = 3;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,bmin};
	double upperlimit[dim] = {mumax,sigmax,bmax};
	string paramnames[dim] = {"vsys","sigma","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "vlike." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_No_Binaries_No_MW_V_Sysparam(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double lmin, lmax;
	galaxy.get_sysparam_bounds(lmin,lmax);

	const int dim = 3;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,lmin};
	double upperlimit[dim] = {mumax,sigmax,lmax};
	string sysparam_name, sysparam_latex_name;
	if (galaxy.sysparam_type[0]==0) {
		sysparam_name = "lambda";
		sysparam_latex_name = "\\lambda";
	} else {
		sysparam_name = "sigma_sys";
		sysparam_latex_name = "\\sigma_{sys}";
	}
	string paramnames[dim] = {"vsys","sigma",sysparam_name};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma",sysparam_latex_name};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_nobins_sysparam." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_No_Binaries_No_MW_V_Sysparam2(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double l1min, l1max, l2min, l2max;
	galaxy.get_sysparam2_bounds(l1min,l1max,l2min,l2max);

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,l1min,l2min};
	double upperlimit[dim] = {mumax,sigmax,l1max,l2max};
	string sysparam1_name, sysparam1_latex_name;
	string sysparam2_name, sysparam2_latex_name;
	if (galaxy.sysparam_type[0]==0) {
		sysparam1_name = "lambda";
		sysparam1_latex_name = "\\lambda";
	} else {
		sysparam1_name = "sigma_sys";
		sysparam1_latex_name = "\\sigma_{sys";
	}
	if (galaxy.sysparam_type[1]==0) {
		sysparam2_name = "lambda";
		sysparam2_latex_name = "\\lambda";
	} else {
		sysparam2_name = "sigma_sys";
		sysparam2_latex_name = "\\sigma_{sys";
	}
	if (sysparam1_name==sysparam2_name) {
		if (sysparam1_name=="lambda") {
			sysparam1_latex_name += "_";
			sysparam2_latex_name += "_";
		}
		sysparam1_name += "1";
		sysparam1_latex_name += "1";
		sysparam2_name += "2";
		sysparam2_latex_name += "2";
	}
	if (galaxy.sysparam_type[0]==1) sysparam1_latex_name += "}";
	if (galaxy.sysparam_type[1]==1) sysparam2_latex_name += "}";

	string paramnames[dim] = {"vsys","sigma",sysparam1_name,sysparam2_name};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma",sysparam1_latex_name,sysparam2_latex_name};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_nobins_sysparam2." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_No_Binaries_No_MW_V_Sysparam3(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double l1min, l1max, l2min, l2max, l3min, l3max;
	galaxy.get_sysparam3_bounds(l1min,l1max,l2min,l2max,l3min,l3max);

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,l1min,l2min,l3min};
	double upperlimit[dim] = {mumax,sigmax,l1max,l2max,l3max};
	string sysparam1_name, sysparam1_latex_name;
	string sysparam2_name, sysparam2_latex_name;
	string sysparam3_name, sysparam3_latex_name;
	if (galaxy.sysparam_type[0]==0) {
		sysparam1_name = "lambda";
		sysparam1_latex_name = "\\lambda";
	} else {
		sysparam1_name = "sigma_sys";
		sysparam1_latex_name = "\\sigma_{sys";
	}
	if (galaxy.sysparam_type[1]==0) {
		sysparam2_name = "lambda";
		sysparam2_latex_name = "\\lambda";
	} else {
		sysparam2_name = "sigma_sys";
		sysparam2_latex_name = "\\sigma_{sys";
	}
	if (galaxy.sysparam_type[2]==0) {
		sysparam3_name = "lambda";
		sysparam3_latex_name = "\\lambda";
	} else {
		sysparam3_name = "sigma_sys";
		sysparam3_latex_name = "\\sigma_{sys";
	}

	if (sysparam1_name==sysparam2_name) {
		if (sysparam1_name=="lambda") {
			sysparam1_latex_name += "_";
			sysparam2_latex_name += "_";
		}
		sysparam1_name += "1";
		sysparam1_latex_name += "1";
		sysparam2_name += "2";
		sysparam2_latex_name += "2";
	} else if (sysparam1_name==sysparam3_name) {
		if (sysparam1_name=="lambda") {
			sysparam1_latex_name += "_";
			sysparam3_latex_name += "_";
		}
		sysparam1_name += "1";
		sysparam1_latex_name += "1";
		sysparam3_name += "2";
		sysparam3_latex_name += "2";
	} else if (sysparam2_name==sysparam3_name) {
		if (sysparam2_name=="lambda") {
			sysparam2_latex_name += "_";
			sysparam3_latex_name += "_";
		}
		sysparam2_name += "1";
		sysparam2_latex_name += "1";
		sysparam3_name += "2";
		sysparam3_latex_name += "2";
	}
	if (galaxy.sysparam_type[0]==1) sysparam1_latex_name += "}";
	if (galaxy.sysparam_type[1]==1) sysparam2_latex_name += "}";
	if (galaxy.sysparam_type[2]==1) sysparam3_latex_name += "}";

	string paramnames[dim] = {"vsys","sigma",sysparam1_name,sysparam2_name,sysparam3_name};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma",sysparam1_latex_name,sysparam2_latex_name,sysparam3_latex_name};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_nobins_sysparam3." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_No_MW_V_Sysparam(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double bmin=0, bmax=1;
	double lmin, lmax;
	galaxy.get_sysparam_bounds(lmin,lmax);

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,bmin,0};
	double upperlimit[dim] = {mumax,sigmax,bmax,lmax};

	string sysparam_name, sysparam_latex_name;
	if (galaxy.sysparam_type[0]==0) {
		sysparam_name = "lambda";
		sysparam_latex_name = "\\lambda";
	} else {
		sysparam_name = "sigma_sys";
		sysparam_latex_name = "\\sigma_{sys}";
	}
	string paramnames[dim] = {"vsys","sigma","b",sysparam_name};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","b",sysparam_latex_name};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_sysparam." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames,lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_No_MW_V_Sysparam2(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double bmin=0, bmax=1;
	double l1min, l1max;
	double l2min, l2max;
	galaxy.get_sysparam2_bounds(l1min,l1max,l2min,l2max);

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,bmin,l1min,l2min};
	double upperlimit[dim] = {mumax,sigmax,bmax,l1max,l2max};

	string sysparam1_name, sysparam1_latex_name;
	string sysparam2_name, sysparam2_latex_name;
	if (galaxy.sysparam_type[0]==0) {
		sysparam1_name = "lambda";
		sysparam1_latex_name = "\\lambda";
	} else {
		sysparam1_name = "sigma_sys";
		sysparam1_latex_name = "\\sigma_{sys";
	}
	if (galaxy.sysparam_type[1]==0) {
		sysparam2_name = "lambda";
		sysparam2_latex_name = "\\lambda";
	} else {
		sysparam2_name = "sigma_sys";
		sysparam2_latex_name = "\\sigma_{sys";
	}
	if (sysparam1_name==sysparam2_name) {
		if (sysparam1_name=="lambda") {
			sysparam1_latex_name += "_";
			sysparam2_latex_name += "_";
		}
		sysparam1_name += "1";
		sysparam1_latex_name += "1";
		sysparam2_name += "2";
		sysparam2_latex_name += "2";
	}
	if (galaxy.sysparam_type[0]==1) sysparam1_latex_name += "}";
	if (galaxy.sysparam_type[1]==1) sysparam2_latex_name += "}";


	string paramnames[dim] = {"vsys","sigma","b",sysparam1_name,sysparam2_name};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","b",sysparam1_latex_name,sysparam2_latex_name};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_sysparam2." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_No_MW_V_Sysparam3(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double bmin=0, bmax=1;
	double l1min, l1max, l2min, l2max, l3min, l3max;
	galaxy.get_sysparam3_bounds(l1min,l1max,l2min,l2max,l3min,l3max);

	const int dim = 6;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,bmin,l1min,l2min,l3min};
	double upperlimit[dim] = {mumax,sigmax,bmax,l1max,l2max,l3max};

	string sysparam1_name, sysparam1_latex_name;
	string sysparam2_name, sysparam2_latex_name;
	string sysparam3_name, sysparam3_latex_name;
	if (galaxy.sysparam_type[0]==0) {
		sysparam1_name = "lambda";
		sysparam1_latex_name = "\\lambda";
	} else {
		sysparam1_name = "sigma_sys";
		sysparam1_latex_name = "\\sigma_{sys";
	}
	if (galaxy.sysparam_type[1]==0) {
		sysparam2_name = "lambda";
		sysparam2_latex_name = "\\lambda";
	} else {
		sysparam2_name = "sigma_sys";
		sysparam2_latex_name = "\\sigma_{sys";
	}
	if (galaxy.sysparam_type[2]==0) {
		sysparam3_name = "lambda";
		sysparam3_latex_name = "\\lambda";
	} else {
		sysparam3_name = "sigma_sys";
		sysparam3_latex_name = "\\sigma_{sys";
	}

	if (sysparam1_name==sysparam2_name) {
		if (sysparam1_name=="lambda") {
			sysparam1_latex_name += "_";
			sysparam2_latex_name += "_";
		}
		sysparam1_name += "1";
		sysparam1_latex_name += "1";
		sysparam2_name += "2";
		sysparam2_latex_name += "2";
	} else if (sysparam1_name==sysparam3_name) {
		if (sysparam1_name=="lambda") {
			sysparam1_latex_name += "_";
			sysparam3_latex_name += "_";
		}
		sysparam1_name += "1";
		sysparam1_latex_name += "1";
		sysparam3_name += "2";
		sysparam3_latex_name += "2";
	} else if (sysparam2_name==sysparam3_name) {
		if (sysparam2_name=="lambda") {
			sysparam2_latex_name += "_";
			sysparam3_latex_name += "_";
		}
		sysparam2_name += "1";
		sysparam2_latex_name += "1";
		sysparam3_name += "2";
		sysparam3_latex_name += "2";
	}
	if (galaxy.sysparam_type[0]==1) sysparam1_latex_name += "}";
	if (galaxy.sysparam_type[1]==1) sysparam2_latex_name += "}";
	if (galaxy.sysparam_type[2]==1) sysparam3_latex_name += "}";

	string paramnames[dim] = {"vsys","sigma","b",sysparam1_name,sysparam2_name,sysparam3_name};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","b",sysparam1_latex_name,sysparam2_latex_name,sysparam3_latex_name};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_sysparam3." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_No_MW_V_Fixed_Sysparam3(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);
	galaxy.sysparam3 = 0; // fix to some value

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double bmin=0, bmax=1;
	double l1min, l1max;
	double l2min, l2max;
	galaxy.get_sysparam2_bounds(l1min,l1max,l2min,l2max);

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,bmin,l1min,l2min};
	double upperlimit[dim] = {mumax,sigmax,bmax,l1max,l2max};

	string sysparam1_name, sysparam1_latex_name;
	string sysparam2_name, sysparam2_latex_name;
	if (galaxy.sysparam_type[0]==0) {
		sysparam1_name = "lambda";
		sysparam1_latex_name = "\\lambda";
	} else {
		sysparam1_name = "sigma_sys";
		sysparam1_latex_name = "\\sigma_{sys";
	}
	if (galaxy.sysparam_type[1]==0) {
		sysparam2_name = "lambda";
		sysparam2_latex_name = "\\lambda";
	} else {
		sysparam2_name = "sigma_sys";
		sysparam2_latex_name = "\\sigma_{sys";
	}
	if (sysparam1_name==sysparam2_name) {
		if (sysparam1_name=="lambda") {
			sysparam1_latex_name += "_";
			sysparam2_latex_name += "_";
		}
		sysparam1_name += "1";
		sysparam1_latex_name += "1";
		sysparam2_name += "2";
		sysparam2_latex_name += "2";
	}
	if (galaxy.sysparam_type[0]==1) sysparam1_latex_name += "}";
	if (galaxy.sysparam_type[1]==1) sysparam2_latex_name += "}";


	string paramnames[dim] = {"vsys","sigma","b",sysparam1_name,sysparam2_name};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","b",sysparam1_latex_name,sysparam2_latex_name};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_sysparam2." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}



/*
void posteriors_Binaries_No_MW_V_Fixed_Sysparam2(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_sysparams(datafile,record_positions);

	double mumin, mumax, mu_3clip, disp_3clip, sigmin, sigmax;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.2*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,false);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.1;
		sigmax = 2*disp_3clip;
	}
	if (mpi_id==0) cout << "sigmin=" << sigmin << ", sigmax=" << sigmax << endl;

	double bmin=0, bmax=1;

	const int dim = 3;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,bmin};
	double upperlimit[dim] = {mumax,sigmax,bmax};
	string paramnames[dim] = {"vsys","sigma","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "no_mw_fixed_sysparam2." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}
*/

void gauss_hermite(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	galaxy.input_datafile_velocities_only(datafile);

	double sigmin=3, sigmax=12;
	double mumin=-5, mumax=5;
	double hmax=0.15, hmin=-0.05;
	double h2min=0, h2max=0.05;
	double h3min=hmin, h3max=hmax;
	double h4min=hmin, h4max=hmax;

	const int dim = 3;
	//const int dim = 2;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,h4min};
	double upperlimit[dim] = {mumax,sigmax,h4max};
	string paramnames[dim] = {"vsys","sigma","h4"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","h_4"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "hermite." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_No_MW_No_Period_Params(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_jfactors(datafile,record_positions);
	if ((metallicity_limit != 1000) or (metallicity_lowlimit != -1000)) galaxy.Count_Stars();

	double bmin=0, bmax=1;

	const int dim = 1;

	double *pt = new double[1];
	pt[0]=0;

	double lowerlimit[dim] = {bmin};
	double upperlimit[dim] = {bmax};
	string paramnames[dim] = {"b"};
	string latex_paramnames[dim] = {"b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string label = "dvlike.";
	if (metallicity_limit != 1000) {
		stringstream wstr;
		string wstring;
		wstr << metallicity_limit;
		wstr >> wstring;
		label += "w" + wstring + ".";
	}
	if (metallicity_lowlimit != -1000) {
		stringstream wstr;
		string wstring;
		wstr << metallicity_lowlimit;
		wstr >> wstring;
		label += "W" + wstring + ".";
	}
	label += datafile_str;

	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_W_Only_Fixed_Metallicity_Params(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile(datafile,record_positions);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();

	double fmin=0, fmax=1;

	const int dim = 1;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {fmin};
	double upperlimit[dim] = {fmax};
	string paramnames[dim] = {"f"};
	string latex_paramnames[dim] = {"f"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string label = "w_fixedw." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_W_Only(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile(datafile,record_positions);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();

	double fmin=0, fmax=1;
	double wavg_min=0.2, wavg_max=0.6;
	double wdisp_min=0.02, wdisp_max=0.2;
	double MW_wavg_min=0.6, MW_wavg_max=1;
	double MW_wdisp_min=0.02, MW_wdisp_max=0.2;

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {fmin,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min};
	double upperlimit[dim] = {fmax,wavg_max,wdisp_max,MW_wavg_max,MW_wdisp_max};
	string paramnames[dim] = {"f","wavg","wdisp","mw_wavg","mw_wdisp"};
	string latex_paramnames[dim] = {"f","\\bar w","\\sigma_{w}","\\bar w_{MW}","\\sigma_{w,MW}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string label = "w_only." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Fixed_Metallicity_Params_No_Period_Params(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_jfactors(datafile,record_positions);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();

	double bmin=0, bmax=1;
	double fmin=0, fmax=1;

	if (fix_member_fraction)
	{
		const int dim = 1;

		double *pt = new double[dim];
		for (int i=0; i < dim; i++) pt[i]=0;

		double lowerlimit[dim] = {bmin};
		double upperlimit[dim] = {bmax};
		string paramnames[dim] = {"b"};
		string latex_paramnames[dim] = {"b"};
		galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

		string datafile_str(datafile);
		string label = "dv_fixedwf.no_pp." + datafile_str;
		string output_dir = "chains_" + label;
		mkdir(output_dir);
		string output_file = output_dir + "/" + label;
		if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
		output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

		int number_of_live_pts = live_pts;
		galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
		delete[] pt;
	} else {
		const int dim = 2;

		double *pt = new double[dim];
		for (int i=0; i < dim; i++) pt[i]=0;

		double lowerlimit[dim] = {bmin,fmin};
		double upperlimit[dim] = {bmax,fmax};
		string paramnames[dim] = {"b","f"};
		string latex_paramnames[dim] = {"b","f"};
		galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

		string datafile_str(datafile);
		string label = "dv_fixedw.no_pp." + datafile_str;
		string output_dir = "chains_" + label;
		mkdir(output_dir);
		string output_file = output_dir + "/" + label;
		if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
		output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

		int number_of_live_pts = live_pts;
		galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
		delete[] pt;
	}

	return;
}

void posteriors_DV_Fixed_Metallicity_Params(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_jtables(datafile,record_positions);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();

	double bmin=0, bmax=1;
	double fmin=0, fmax=1;
	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {fmin,bmin,mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {fmax,bmax,mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"f","b","mu_logP","sig_logP"};
	string latex_paramnames[dim] = {"b","f","\\mu_{logP}","\\sigma_{logP}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string label;
	if (nonconservative_prior)
		label = "dv_fixedw.ncp." + datafile_str;
	else
		label = "dv_fixedw." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DVW_No_Period_Params(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_jfactors(datafile,record_positions);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();

	double bmin=0, bmax=1;
	double fmin=0, fmax=1;
	double wavg_min=0, wavg_max=2;
	double wdisp_min=0.02, wdisp_max=0.7;
	double MW_wavg_min=0.1, MW_wavg_max=2;
	double MW_wdisp_min=0.02, MW_wdisp_max=0.7;

	const int dim = 6;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {fmin,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmin};
	double upperlimit[dim] = {fmax,wavg_max,wdisp_max,MW_wavg_max,MW_wdisp_max,bmax};
	string paramnames[dim] = {"f","wavg","wdisp","mw_wavg","mw_wdisp","b"};
	string latex_paramnames[dim] = {"f","\\bar w","\\sigma_{w}","\\bar w_{MW}","\\sigma_{w,MW}","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string label = "dvw.no_pp." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_No_MW(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_jtables(datafile,record_positions);

	double bmin=0, bmax=1;
	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);

	const int dim = 3;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {bmin,mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {bmax,mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"b","mu_logP","sig_logP"};
	string latex_paramnames[dim] = {"b","\\mu_{logP}","\\sigma_{logP}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string label;
	if (nonconservative_prior)
		label = "dv.ncp." + datafile_str;
	else
		label = "dv_pp." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Fixed_Binary_Fraction(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_jtables(datafile,record_positions);
	galaxy.set_binfrac(0.5);

	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);

	const int dim = 2;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"mu_logP","sig_logP"};
	string latex_paramnames[dim] = {"\\mu_{logP}","\\sigma_{logP}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string label;
	if (nonconservative_prior)
		label = "dv_fixedb.ncp." + datafile_str;
	else
		label = "dv_fixedb." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_jtables(datafile,record_positions);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.set_binfrac(0.5);

	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);
	double fmin=0,fmax=1;

	const int dim = 3;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {fmin,mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {fmax,mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"f","mu_logP","sig_logP"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file;
	if (nonconservative_prior)
		output_file = "nest.dv_fixedbw.ncp." + datafile_str;
	else
		output_file = "nest.dv_fixedbw." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Error_Model(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	galaxy.input_datafile(datafile,record_positions);

	double alpha_min_red = 20, alpha_max_red = 200;
	double xmin_red = 1.0, xmax_red = 2.0;
	double alpha_min_blue = 20, alpha_max_blue = 200;
	double xmin_blue = 1.0, xmax_blue = 2.0;
	double beta_min = 0, beta_max = 1;

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {alpha_min_red,xmin_red,alpha_min_blue,xmin_blue};
	double upperlimit[dim] = {alpha_max_red,xmax_red,alpha_max_blue,xmax_blue};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dv_errmodel." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_logdvlike(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_binary_logdv_likelihoods(datafile,record_positions);

	double bmin = 0, bmax = 1;

	const int dim = 1;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {bmin};
	double upperlimit[dim] = {bmax};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dv_quick." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DVW_logdvlike(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_binary_logdv_likelihoods(datafile,record_positions);

	double bmin = 0, bmax = 1;
	double fmin=0, fmax=1;
	double wavg_min=0, wavg_max=2;
	double wdisp_min=0.02, wdisp_max=0.7;
	double MW_wavg_min=0.1, MW_wavg_max=2;
	double MW_wdisp_min=0.02, MW_wdisp_max=0.7;

	const int dim = 6;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {fmin,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmin};
	double upperlimit[dim] = {fmax,wavg_max,wdisp_max,MW_wavg_max,MW_wdisp_max,bmax};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dvw_quick." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Fixed_Metallicity_Params_logdvlike(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_binary_logdv_likelihoods(datafile,record_positions);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();

	double bmin = 0, bmax = 1;
	double fmin=0, fmax=1;

	if (fix_member_fraction)
	{
		const int dim = 1;

		double *pt = new double[dim];
		for (int i=0; i < dim; i++) pt[i]=0;

		double lowerlimit[dim] = {bmin};
		double upperlimit[dim] = {bmax};
		galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

		string datafile_str(datafile);
		string output_file = "nest.dv_fixedwf_quick." + datafile_str;
		if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
		int number_of_live_pts = live_pts;
		galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
		delete[] pt;
	} else {
		const int dim = 2;

		double *pt = new double[dim];
		for (int i=0; i < dim; i++) pt[i]=0;

		double lowerlimit[dim] = {bmin,fmin};
		double upperlimit[dim] = {bmax,fmax};
		galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

		string datafile_str(datafile);
		string output_file = "nest.dv_fixedw_quick." + datafile_str;
		if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
		int number_of_live_pts = live_pts;
		galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
		delete[] pt;
	}

	return;
}

void posteriors_DV_Error_Model_with_Binaries(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	galaxy.input_datafile_binary_logdv_likelihoods(datafile,record_positions);

	double alpha_min_red = 20, alpha_max_red = 200;
	double xmin_red = 1.0, xmax_red = 2.0;
	double alpha_min_blue = 20, alpha_max_blue = 200;
	double xmin_blue = 1.0, xmax_blue = 2.0;
	double beta_min = 0, beta_max = 1;
	double bmin = 0, bmax = 1;

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {alpha_min_red,xmin_red,alpha_min_blue,xmin_blue,bmin};
	double upperlimit[dim] = {alpha_max_red,xmax_red,alpha_max_blue,xmax_blue,bmax};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dv_errmodel_bin." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Error_Model_with_Binaries_Fixed_W_Params(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.input_datafile_binary_logdv_likelihoods(datafile,record_positions);

	double alpha_min_red = 20, alpha_max_red = 200;
	double xmin_red = 1.0, xmax_red = 2.0;
	double alpha_min_blue = 20, alpha_max_blue = 200;
	double xmin_blue = 1.0, xmax_blue = 2.0;
	double beta_min = 0, beta_max = 1;
	double bmin = 0, bmax = 1;
	double fmin = 0, fmax = 1;

	const int dim = 6;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {alpha_min_red,xmin_red,alpha_min_blue,xmin_blue,fmin,bmin};
	double upperlimit[dim] = {alpha_max_red,xmax_red,alpha_max_blue,xmax_blue,fmax,bmax};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dv_errmodel_fbin." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.input_datafile_binary_logdv_likelihoods(datafile,record_positions);

	double alpha_min = 20, alpha_max = 200;
	double xmin = 1.0, xmax = 2.0;
	double bmin = 0, bmax = 1;
	double fmin = 0, fmax = 1;

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {alpha_min,xmin,fmin,bmin};
	double upperlimit[dim] = {alpha_max,xmax,fmax,bmax};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dv_errmodel_1channel_fbin." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Error_Model_Extended(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile(datafile,record_positions);

	double alpha_min_red = 20, alpha_max_red = 200;
	double xmin_red = 1.0, xmax_red = 2.0;
	double alpha_min_blue = 20, alpha_max_blue = 200;
	double xmin_blue = 1.0, xmax_blue = 2.0;
	double beta_min = 0.1, beta_max = 1.5;

	const int dim = 6;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {alpha_min_red,xmin_red,alpha_min_blue,xmin_blue,beta_min,beta_min};
	double upperlimit[dim] = {alpha_max_red,xmax_red,alpha_max_blue,xmax_blue,beta_max,beta_max};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dv_errmodel2." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_DV_Error_Model_Metallicity(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile(datafile,record_positions);

	double alpha0_min_red = 20, alpha0_max_red = 200;
	double alpha1_min_red = -6, alpha1_max_red = 6;
	double alpha2_min_red = -6, alpha2_max_red = 6;
	double x0min_red = 1.0, x0max_red = 2.0;
	double x1min_red = -6, x1max_red = 6;
	double gamma0_min_red = 0, gamma0_max_red = 3.0;
	double gamma1_min_red = 0, gamma1_max_red = 3.0;

	double alpha0_min_blue = 20, alpha0_max_blue = 200;
	double alpha1_min_blue = -6, alpha1_max_blue = 6;
	double alpha2_min_blue = -6, alpha2_max_blue = 6;
	double alpha_slope_min_blue = -6, alpha_slope_max_blue = 6;
	double x0min_blue = 1.0, x0max_blue = 2.0;
	double x1min_blue = -6, x1max_blue = 6;
	double gamma0_min_blue = 0, gamma0_max_blue = 3.0;
	double gamma1_min_blue = 0, gamma1_max_blue = 3.0;


	const int dim = 10;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {alpha0_min_red,alpha1_min_red,alpha2_min_red,x0min_red,x1min_red,alpha0_min_blue,alpha1_min_blue,alpha2_min_blue,x0min_blue,x1min_blue};
	double upperlimit[dim] = {alpha0_max_red,alpha1_max_red,alpha2_max_red,x0max_red,x1max_red,alpha0_max_blue,alpha1_max_blue,alpha2_max_blue,x0max_blue,x1max_blue};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	string output_file = "nest.dv_errmodel_w." + datafile_str;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_MW_Velocities_No_Binaries(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile(datafile,record_positions);

	double mumin=220, mumax=270;
	if (zero_systemic_velocity) { mumin=-10; mumax=10; }
	double sigmin=0.1, sigmax=10;
	double fmin=0, fmax=1;

	const int dim = 3;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin};
	double upperlimit[dim] = {mumax,sigmax,fmax};
	string paramnames[dim] = {"vsys","sigma","f"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "mw_nobins." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_MW_Velocities_Metallicities_No_Binaries(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile(datafile,record_positions);

	double mumin=220, mumax=270;
	if (zero_systemic_velocity) { mumin=-10; mumax=10; }
	double sigmin=0.1, sigmax=10;
	double fmin=0, fmax=1;
	double wavg_min=0, wavg_max=2;
	double wdisp_min=0.05, wdisp_max=0.5;
	double MW_wavg_min=0, MW_wavg_max=2;
	double MW_wdisp_min=0.05, MW_wdisp_max=0.5;

	const int dim = 7;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min};
	double upperlimit[dim] = {mumax,sigmax,fmax,wavg_max,wdisp_max,MW_wavg_max,MW_wdisp_max};
	string paramnames[dim] = {"vsys","sigma","f","wavg","wdisp","mw_wavg","mw_wdisp"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f","\\bar w","\\sigma_{w}","\\bar w_{MW}","\\sigma_{w,MW}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "mw_vw_nobins." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts);
	delete[] pt;

	return;
}

void posteriors_Binaries_VW_No_Period_Params(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile_rtables(datafile,record_positions);
	galaxy.set_period_params(2.23,2.3);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double fmin=0, fmax=1;
	double wavg_min=0, wavg_max=2;
	double wdisp_min=0.02, wdisp_max=0.7;
	double MW_wavg_min=0.1, MW_wavg_max=2;
	double MW_wdisp_min=0.02, MW_wdisp_max=0.7;
	double bmin=0, bmax=1;

	const int dim = 8;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmin};
	double upperlimit[dim] = {mumax,sigmax,fmax,wavg_max,wdisp_max,MW_wavg_max,MW_wdisp_max,bmax};
	string paramnames[dim] = {"vsys","sigma","f","wavg","wdisp","mw_wavg","mw_wdisp","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f","\\bar w","\\sigma_{w}","\\bar w_{MW}","\\sigma_{w,MW}","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "vw_no_pp." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_VW(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile_rtables(datafile,record_positions);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double fmin=0, fmax=1;
	double wavg_min=0, wavg_max=2;
	double wdisp_min=0.02, wdisp_max=0.7;
	double MW_wavg_min=0.1, MW_wavg_max=2;
	double MW_wdisp_min=0.02, MW_wdisp_max=0.7;

	double bmin=0, bmax=1;
	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);

	const int dim = 10;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmin,mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {mumax,sigmax,fmax,wavg_max,wdisp_max,MW_wavg_max,MW_wdisp_max,bmax,mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"vsys","sigma","f","wavg","wdisp","mw_wavg","mw_wdisp","b","mu_logP","sig_logP"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f","\\bar w","\\sigma_{w}","\\bar w_{MW}","\\sigma_{w,MW}","b","\\mu_{logP}","\\sigma_{logP}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "vw." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_V_Fixed_W_Params_No_Period_Params(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.input_datafile(datafile,record_positions);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double fmin=0, fmax=1;
	double bmin=0, bmax=1;

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin,bmin};
	double upperlimit[dim] = {mumax,sigmax,fmax,bmax};
	string paramnames[dim] = {"vsys","sigma","f","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "v_fixedw_no_pp." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_V_Fixed_W_Params(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.input_datafile_rtables(datafile,record_positions);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double fmin=0, fmax=1;
	double bmin=0, bmax=1;
	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);

	const int dim = 6;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin,bmin,mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {mumax,sigmax,fmax,bmax,mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"vsys","sigma","f","b","mu_logP","sig_logP"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f","b","\\mu_{logP}","\\sigma_{logP}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "v_fixedw." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_V_Fixed_W_Params_Test_RTables(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.input_datafile_rtables(datafile,record_positions);
	galaxy.test_rfactor_interpolate();
	galaxy.set_period_params(2.23,2.3);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double fmin=0, fmax=1;
	double bmin=0, bmax=1;

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin,bmin};
	double upperlimit[dim] = {mumax,sigmax,fmax,bmax};
	string paramnames[dim] = {"vsys","sigma","f","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "v_fixedw_test." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_V_No_MW(char *datafile, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_datafile_rtables(datafile,record_positions);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double bmin=0, bmax=1;
	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,bmin,mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {mumax,sigmax,bmax,mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"vsys","sigma","b","mu_logP","sig_logP"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","b","\\mu_logP","\\sigma_{logP}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "v_no_mw." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_MW_No_Metallicities(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_metallicities) galaxy.set_no_metallicities();
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile(datafile,record_positions);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}

	double fmin=0, fmax=1;
	double bmin=0, bmax=1;

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,fmin,bmin};
	double upperlimit[dim] = {mumax,sigmax,fmax,bmax};
	string paramnames[dim] = {"vsys","sigma","f","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","f","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "nometals." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_VR(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile(datafile,true); // the "true" indicates that positions of stars *must* be recorded for this mode

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}

	double Nmin=10, Nmax=200;
	double bmin=0, bmax=1;
	double rs_min=0.05, rs_max=0.9; // in kpc

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,Nmin,rs_min,bmin};
	double upperlimit[dim] = {mumax,sigmax,Nmax,rs_max,bmax};
	string paramnames[dim] = {"vsys","sigma","N","rs","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","N","r_s","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "vr_nometals." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_VR_Fixed_Metallicity_Params(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.input_datafile(datafile,true); // the "true" indicates that positions of stars *must* be recorded for this mode

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}

	double Nmin=10, Nmax=200;
	double bmin=0, bmax=1;
	double rs_min=0.05, rs_max=0.9; // in kpc

	const int dim = 5;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,Nmin,rs_min,bmin};
	double upperlimit[dim] = {mumax,sigmax,Nmax,rs_max,bmax};
	string paramnames[dim] = {"vsys","sigma","N","rs","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","N","r_s","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "vr_fixedw." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Binaries_VR_Fixed_W_Rs(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	if (galaxy_name_specified==true) galaxy.load_metallicity_params(galaxy_name);
	else galaxy.load_metallicity_params();
	galaxy.input_datafile(datafile,true); // the "true" indicates that positions of stars *must* be recorded for this mode
	galaxy.set_scale_radius(scale_radius);

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}

	double Nmin=10, Nmax=200;
	double bmin=0, bmax=1;

	const int dim = 4;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,Nmin,bmin};
	double upperlimit[dim] = {mumax,sigmax,Nmax,bmax};
	string paramnames[dim] = {"vsys","sigma","N","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","N","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "vr_fixedwrs." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Full_Monty_No_Period_Params(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile(datafile,true); // the "true" indicates that positions of stars *must* be recorded for this mode

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double wavg_min=0.1, wavg_max=0.8;
	double wdisp_min=0.02, wdisp_max=0.2;
	double MW_wavg_min=0.4, MW_wavg_max=1.5;
	double MW_wdisp_min=0.05, MW_wdisp_max=0.2;

	double Nmin=15, Nmax=200;
	double rs_min=0.1, rs_max=0.7; // in kpc
	double bmin=0, bmax=1;

	const int dim = 9;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,Nmin,rs_min,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmin};
	double upperlimit[dim] = {mumax,sigmax,Nmax,rs_max,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmax};
	string paramnames[dim] = {"vsys","sigma","N","rs","wavg","wdisp","mw_wavg","mw_wdisp","b"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","N","r_s","\\bar w","\\sigma_{w}","\\bar w_{MW}","\\sigma_{w,MW}","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "full_monty_npp." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_Full_Monty(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile_rtables(datafile,true); // the "true" indicates that positions of stars *must* be recorded for this mode

	double mumin, mumax, sigmin, sigmax, mu_3clip, disp_3clip;

	if (zero_systemic_velocity)
	{
		mumin=-10; mumax=10;
		sigmin=1, sigmax=20;
	}
	else if (specified_velocity_params)
	{
		mumin=systemic_velocity_approx - 2*dispersion_approx;
		mumax=systemic_velocity_approx + 2*dispersion_approx;
		sigmin = 0.03*dispersion_approx;
		sigmax = 2*dispersion_approx;
	}
	else
	{
		find_velocity_params_from_sigma_clip(datafile,mu_3clip,disp_3clip,true);
		mumin=mu_3clip - 2*disp_3clip;
		mumax=mu_3clip + 2*disp_3clip;
		sigmin = 0.03*disp_3clip;
		sigmax = 2*disp_3clip;
	}
	double wavg_min=0.1, wavg_max=0.8;
	double wdisp_min=0.02, wdisp_max=0.2;
	double MW_wavg_min=0.4, MW_wavg_max=1.5;
	double MW_wdisp_min=0.05, MW_wdisp_max=0.2;

	double Nmin=15, Nmax=200;
	double rs_min=0.1, rs_max=0.7; // in kpc
	double bmin=0, bmax=1;
	double mu_logP_min, mu_logP_max;
	double sig_logP_min, sig_logP_max;
	galaxy.get_musig_bounds(mu_logP_min,mu_logP_max,sig_logP_min,sig_logP_max);

	const int dim = 11;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {mumin,sigmin,Nmin,rs_min,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmin,mu_logP_min,sig_logP_min};
	double upperlimit[dim] = {mumax,sigmax,Nmax,rs_max,wavg_min,wdisp_min,MW_wavg_min,MW_wdisp_min,bmax,mu_logP_max,sig_logP_max};
	string paramnames[dim] = {"vsys","sigma","N","rs","wavg","wdisp","mw_wavg","mw_wdisp","b","mu_logP","sig_logP"};
	string latex_paramnames[dim] = {"v_{sys}","\\sigma","N","r_s","\\bar w","\\sigma_{w}","\\bar w_{MW}","\\sigma_{w,MW}","b","\\mu_{logP}","\\sigma_{logP}"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "full_monty." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void posteriors_test(char *datafile, char *mw_likelihood_file, Mode mode)
{
	Galaxy galaxy(mode);
	galaxy.Set_MCMC_MPI(mpi_np,mpi_id);
	if (no_error_params) galaxy.set_no_error_params();
	galaxy.input_MW_Likelihood(mw_likelihood_file);
	galaxy.input_datafile(datafile,record_positions);

	double sigmin=0.1, sigmax=20;
	double bmin=0, bmax=1;

	const int dim = 2;

	double *pt = new double[dim];
	for (int i=0; i < dim; i++) pt[i]=0;

	double lowerlimit[dim] = {sigmin,bmin};
	double upperlimit[dim] = {sigmax,bmax};
	string paramnames[dim] = {"sigma","b"};
	string latex_paramnames[dim] = {"\\sigma","b"};
	galaxy.InputPoint(pt, upperlimit, lowerlimit, dim);

	string datafile_str(datafile);
	if (!log_dispersion_prior) datafile_str += "_flatd";
	string label = "posttest." + datafile_str;
	string output_dir = "chains_" + label;
	mkdir(output_dir);
	string output_file = output_dir + "/" + label;
	if (reset_number_of_stars) append_nstars_to_output_file(output_file,nstars_subset);
	output_param_files(dim,paramnames,latex_paramnames, lowerlimit,upperlimit,output_file + ".paramnames",output_file + ".latex_paramnames",output_file + ".ranges");

	int number_of_live_pts = live_pts;
	galaxy.MonoSample(output_file.c_str(), number_of_live_pts, output_to_logfile);
	delete[] pt;

	return;
}

void Galaxy::test_likelihood(void)
{
	double sig=4,b=0.5;
	double *pt = new double[2];
	pt[0]=sig; pt[1] = b;

	double likeval = LogLike_Test(pt);
	delete[] pt;
}

Galaxy::Galaxy(Mode loglike_mode) : UCMC()
{
	star = NULL;
	wt = NULL;
	logdv_likelihoods = NULL;
	include_MW_likelihood = false; // this will be changed if the MW likelihood is uploaded using the input_MW_Likelihood function
	include_error_params = true;
	include_sysparams = false;
	if (loglike_mode == Dispersion_Posterior_No_Binaries) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_No_Binaries_No_MW);
	} else if (loglike_mode == Binaries_No_MW) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_No_MW);
	} else if (loglike_mode == No_Binaries_No_MW_V_Sysparam) {
		include_binaries = false;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_No_Binaries_No_MW_V_Sysparam);
	} else if (loglike_mode == No_Binaries_No_MW_V_Sysparam2) {
		include_binaries = false;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_No_Binaries_No_MW_V_Sysparam2);
	} else if (loglike_mode == No_Binaries_No_MW_V_Sysparam3) {
		include_binaries = false;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_No_Binaries_No_MW_V_Sysparam3);
	} else if (loglike_mode == Binaries_No_MW_V_Sysparam) {
		include_binaries = true;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_No_MW_V_Sysparam);
	//} else if (loglike_mode == Binaries_No_MW_V_Fixed_Sysparam2) {
		//include_binaries = true;
		//include_metallicities = true;
		//include_sysparams = true;
		//LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_No_MW_V_Fixed_Sysparam2);
	} else if (loglike_mode == Binaries_No_MW_V_Sysparam2) {
		include_binaries = true;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_No_MW_V_Sysparam2);
	} else if (loglike_mode == Binaries_No_MW_V_Sysparam3) {
		include_binaries = true;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_No_MW_V_Sysparam3);
	} else if (loglike_mode == Binaries_No_MW_V_Fixed_Sysparam3) {
		include_binaries = true;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_No_MW_V_Fixed_Sysparam3);
	} else if (loglike_mode == DV_No_MW) {
		include_binaries = true;
		include_metallicities = true;
		include_sysparams = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_No_MW);
	} else if (loglike_mode == DV_No_MW_No_Period_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_No_MW_No_Period_Params);
	} else if (loglike_mode == DV_Error_Model) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Error_Model);
	} else if (loglike_mode == DV_logdvlike) {
		include_binaries = false; // we're including binaries in a different way (using the logdv_likelihoods) compared to using J-factors or R-factors
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_logdvlike);
	} else if (loglike_mode == DV_Error_Model_with_Binaries) {
		include_binaries = false; // we're including binaries in a different way (using the logdv_likelihoods) compared to using J-factors or R-factors
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Error_Model_with_Binaries);
	} else if (loglike_mode == DV_Error_Model_with_Binaries_Fixed_W_Params) {
		include_binaries = false; // we're including binaries in a different way (using the logdv_likelihoods) compared to using J-factors or R-factors
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Error_Model_with_Binaries_Fixed_W_Params);
	} else if (loglike_mode == DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel) {
		include_binaries = false; // we're including binaries in a different way (using the logdv_likelihoods) compared to using J-factors or R-factors
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel);
	} else if (loglike_mode == DV_Error_Model_Extended) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Error_Model_Extended);
	} else if (loglike_mode == DV_Error_Model_Metallicity) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Error_Model_Metallicity);
	} else if (loglike_mode == DV_Fixed_Metallicity_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Fixed_Metallicity_Params);
	} else if (loglike_mode == DVW_No_Period_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DVW_No_Period_Params);
	} else if (loglike_mode == DV_Fixed_Metallicity_Params_logdvlike) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Fixed_Metallicity_Params_logdvlike);
	} else if (loglike_mode == W_Only_Fixed_Metallicity_Params) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_W_Only_Fixed_Metallicity_Params);
	} else if (loglike_mode == W_Only) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_W_Only);
	} else if (loglike_mode == DVW_logdvlike) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DVW_logdvlike);
	} else if (loglike_mode == DV_Fixed_Metallicity_Params_No_Period_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Fixed_Metallicity_Params_No_Period_Params);
	} else if (loglike_mode == DV_Fixed_Binary_Fraction) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params);
	} else if (loglike_mode == DV_Fixed_Binary_Fraction) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params);
	} else if (loglike_mode == MW_Velocities_No_Binaries) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_MW_Velocities_No_Binaries);
	} else if (loglike_mode == MW_Velocities_Metallicities_No_Binaries) {
		include_binaries = false;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_MW_Velocities_Metallicities_No_Binaries);
	} else if (loglike_mode == Binaries_VW_No_Period_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_VW_No_Period_Params);
	} else if (loglike_mode == Binaries_VW) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_VW);
	} else if (loglike_mode == Binaries_V_Fixed_W_Params_No_Period_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_V_Fixed_W_Params_No_Period_Params);
	} else if (loglike_mode == Binaries_V_Fixed_W_Params_Test_RTables) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_V_Fixed_W_Params_Test_RTables);
	} else if (loglike_mode == Binaries_V_Fixed_W_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_V_Fixed_W_Params);
	} else if (loglike_mode == Binaries_V_No_MW) {
		include_binaries = true;
		include_metallicities = false;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_V_No_MW);
	} else if (loglike_mode == Binaries_MW_No_Metallicities) {
		include_binaries = true;
		include_metallicities = true; // probably should just include metallicities no matter what (so there's no confusion over data file)
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_MW_No_Metallicities);
	} else if (loglike_mode == Binaries_VR) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_VR);
	} else if (loglike_mode == Binaries_VR_Fixed_Metallicity_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_VR_Fixed_Metallicity_Params);
	} else if (loglike_mode == Binaries_VR_Fixed_W_Rs) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Binaries_VR_Fixed_W_Rs);
	} else if (loglike_mode == Full_Monty_No_Period_Params) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Full_Monty_No_Period_Params);
	} else if (loglike_mode == Full_Monty) {
		include_binaries = true;
		include_metallicities = true;
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Full_Monty);
	} else if (loglike_mode == Gauss_Hermite) {
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Gauss_Hermite);
	} else if (loglike_mode == Test) {
		include_binaries = true;
		include_metallicities = true; // probably should just include metallicities no matter what (so there's no confusion over data file)
		LogLikePtr = static_cast<double (UCMC::*)(double *)> (&Galaxy::LogLike_Test);
		systemic_velocity = 250;
		if (zero_systemic_velocity) systemic_velocity = 0;
		member_fraction = 0.8;
	}
}

double Galaxy::loglikefunc(double *a)
{
	return (this->*LogLikePtr)(a);
}

void Galaxy::input_datafile_velocities_only(char* datafile_str)
{
	ifstream datafile(datafile_str);
	datafile >> nstars;
	star = new Star[nstars];
	for (int i=0; i < nstars; i++)
	{
		datafile >> star[i].v_avg;
	}
}

void Galaxy::load_data(ifstream &datafile, bool record_position)
{
	star = new Star[nstars];

	int i, j, epochs;
	for (i=0; i < nstars; i++)
	{
		datafile >> star[i].id >> star[i].magnitude;
		if (record_position) datafile >> star[i].R;
		datafile >> epochs;
		star[i].n_epochs = epochs;
		star[i].times.input(epochs);
		star[i].vlos_data.input(epochs);
		star[i].dv_data.input(epochs-1);
		star[i].v_errors.input(epochs);

		if (include_metallicities)
		{
			star[i].w_data.input(epochs);
			star[i].w_errors.input(epochs);
		}
		if (include_error_params) {
			star[i].r_value.input(epochs);
			star[i].channel_color.input(epochs);
			star[i].v_error_model.input(epochs);
		}
		if (include_sysparams) {
			star[i].sysparam_i.input(epochs,2);
			star[i].n_sysparams = 0;
		}
		for (j=0; j < epochs; j++)
		{
			datafile >> star[i].times[j];
			datafile >> star[i].vlos_data[j] >> star[i].v_errors[j];
			if (include_metallicities) {
				datafile >> star[i].w_data[j] >> star[i].w_errors[j];
				if (abs(star[i].w_data[j]) < 1e-25) star[i].w_data[j] = 1e30; // zero values imply the measurement was not made
				if (abs(star[i].w_errors[j]) < 1e-25) star[i].w_errors[j] = 1e30; // zero values imply the measurement was not made
			}
			if (include_error_params) {
				datafile >> star[i].r_value[j];
				datafile >> star[i].channel_color[j];
			}
			if (include_sysparams) {
				for (int kk=0; kk < 2; kk++) {
					datafile >> star[i].sysparam_i[j][kk];
					//cout << "Star " << i << ", epoch " << j << ": " << star[i].sysparam_i[j][kk] << endl;
					//if (star[i].sysparam_i[j][kk] == 2) {
						//star[i].v_errors[j] = sqrt(SQR(star[i].v_errors[j]) + fsys*fsys);
						//star[i].sysparam_i[j][kk] = -1;
					//}
					if (star[i].sysparam_i[j][kk] != -1) {
						star[i].n_sysparams++;
						star[i].sysparam_order.push_back(star[i].sysparam_i[j][kk]);
					}
				}
			}
		}
		//cout << "Star " << i << ": " << star[i].n_sysparams << endl;
		for (j=0; j < epochs-1; j++)
			star[i].dv_data[j] = star[i].vlos_data[j+1] - star[i].vlos_data[j];

		star[i].find_velocity_averages();
		if (include_metallicities) star[i].find_width_averages();

		if (distance_kpc==1e30) star[i].abs_magnitude = star[i].magnitude;
		else star[i].abs_magnitude = star[i].magnitude - 10 - 5*log(distance_kpc)/log(10);
	}
}

void Galaxy::calculate_MW_likelihoods(void)
{
	int i, j;
	for (i=0; i < nstars; i++)
	{
		star_i = i;
		if (use_gaussian_milky_way) star[i].vlike_mw = exp(-0.5*SQR(star[i].v_avg-MW_mean_velocity)/(SQR(MW_dispersion)+SQR(star[i].sigv)))/SQRT_2PI/sqrt(SQR(MW_dispersion+SQR(star[i].sigv)));
		else {
			// trapezoid rule to find MW likelihood
			if ((star[i].v_avg < mwlike_vcm_min) or (star[i].v_avg > mwlike_vcm_max)) { cerr << "star " << star[i].id << " has velocity out of range of MW likelihood (v=" << star[i].v_avg << "); recommend discarding it\n"; }
			double sum = 0.5*(mw_likelihood_integrand(0)+mw_likelihood_integrand(mwlike_vcm_nn-1));
			for (j=1; j < mwlike_vcm_nn-1; j++) sum += mw_likelihood_integrand(j);
			star[i].vlike_mw = (mwlike_vcm_max-mwlike_vcm_min)*sum/(mwlike_vcm_nn-1);
		}
	}
}

void Galaxy::input_datafile(char* datafile_str, bool record_position)
{
	ifstream datafile(datafile_str);
	datafile >> nstars;

	ifstream rnorms_file;
	string star_id_check;
	string rdir(datafile_str);
	if (include_binaries) {
		string nstars_filename = "rfactors." + rdir + "/nstars";
		//string logfilename = "logfile." + rdir;
		//logfile.open(logfilename.c_str());
		ifstream nstars_file(nstars_filename.c_str());
		nstars_file >> nstars;	// this keeps track of how many stars we have calculated R-factors for so far, since dataset might not be finished yet
		nstars_file.close();

		string rnorms_filename = "rfactors." + rdir + "/rnorms";
		rnorms_file.open(rnorms_filename.c_str());
	}
	if (reset_number_of_stars) reset_nstars(nstars_subset);
	load_data(datafile,record_position);

	if (include_MW_likelihood) calculate_MW_likelihoods();

	int i, j;
	if (include_binaries)
	{
		for (i=0; i < nstars; i++)
		{
			rnorms_file >> star_id_check >> star[i].nfactor >> star[i].rnorm;
			if (star[i].id != star_id_check) die("star ID (%s) does not match ID (%s) in rnorms file",star[i].id.c_str(),star_id_check.c_str());
			string rfilename = "rfactors." + rdir + "/rfactor." + star[i].id;
			star[i].rfactor_spline.input(rfilename.c_str());

			// now we'll interpolate the rfactors over an array of vcm values so we don't have to interpolate during the nested sampling
			star[i].rfactor_vcm_min = star[i].rfactor_spline.xmin();
			star[i].rfactor_vcm_max = star[i].rfactor_spline.xmax();
			star[i].rfactor_vcm_nn = star[i].rfactor_spline.length();

			star[i].rfactor_vals.input(star[i].rfactor_vcm_nn);
			star[i].vcm_vals.input(star[i].rfactor_vcm_nn);

			double vcm, vcm_step;
			vcm_step = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)/(star[i].rfactor_vcm_nn-1);
			for (j=0, vcm=star[i].rfactor_vcm_min; j < star[i].rfactor_vcm_nn; j++, vcm += vcm_step)
			{
				star[i].vcm_vals[j]=vcm;
				star[i].rfactor_vals[j]=star[i].rfactor_spline.yval(j);
			}
		}
	}

	return;
}

void Galaxy::input_datafile_jfactors(char* datafile_str, bool record_position)
{
	ifstream datafile(datafile_str);
	datafile >> nstars;

	ifstream jfactor_file;
	string star_id_check;
	string rdir(datafile_str);

	if (reset_number_of_stars) reset_nstars(nstars_subset);
	load_data(datafile,record_position);

	string jfactor_filename = "jfactors." + rdir;
	jfactor_file.open(jfactor_filename.c_str());

	for (int i=0; i < nstars; i++)
	{
		if (star[i].n_epochs==1) continue;
		//if (star[i].id != star_id_check) die("star ID (%s) does not match ID (%s) in jfactor file",star[i].id.c_str(),star_id_check.c_str());
		jfactor_file >> star_id_check >> star[i].jfactor_dv >> star[i].nfactor >> star[i].dvlike;
		if (star[i].id != star_id_check) { warn("star %i ID (%s) does not match ID (%s) in jfactor file; skipping to next in jfactor file",i,star[i].id.c_str(),star_id_check.c_str()); i--; continue; }
	}

	return;
}

void Galaxy::input_datafile_binary_logdv_likelihoods(char* datafile_str, bool record_position)
{
	ifstream datafile(datafile_str);
	datafile >> nstars;

	ifstream jfactor_file;
	string star_id_check;
	string rdir(datafile_str);

	if (reset_number_of_stars) reset_nstars(nstars_subset);
	load_data(datafile,record_position);

	int i;
	string magvals_filename = "logdvlikes.1year/magvals";
	ifstream magvals_file(magvals_filename.c_str());
	magvals_file >> magvals_nn;
	mag_vals.input(magvals_nn);
	for (i=0; i < magvals_nn; i++) magvals_file >> mag_vals[i];
	magvals_file.close();

	logdv_likelihoods = new dvector*[n_logdvlike_years];
	for (i=0; i < n_logdvlike_years; i++)
		logdv_likelihoods[i] = new dvector[magvals_nn];

	double logdv, logdvlike;
	int j,k;

	// now find out how many logdv values there are
	ifstream logdvlike_file0("logdvlikes.1year/logdvlike.0");
	n_logdv_vals = 0;
	while (!logdvlike_file0.eof()) { logdvlike_file0 >> logdv >> logdvlike; n_logdv_vals++; }
	n_logdv_vals--;
	logdvlike_file0.close();
	logdv_vals.input(n_logdv_vals);
	dv_vals.input(n_logdv_vals);

	for (k=0; k < n_logdvlike_years; k++)
	{
		for (i=0; i < magvals_nn; i++)
		{
			stringstream istr, kstr;
			string istring, kstring;
			istr << i;
			istr >> istring;
			kstr << k+1;
			kstr >> kstring;
			string logdvlike_filename = "logdvlikes." + kstring + "year/logdvlike." + istring;
			ifstream logdvlike_file(logdvlike_filename.c_str());
			logdv_likelihoods[k][i].input(n_logdv_vals);
			for (j=0; j < n_logdv_vals; j++) {
				logdvlike_file >> logdv_vals[j] >> logdv_likelihoods[k][i][j];
				dv_vals[j] = pow(10,logdv_vals[j]);
			}
			logdvlike_file.close();
		}
	}
	logdv_vals_min = logdv_vals[0];
	logdv_vals_max = logdv_vals[n_logdv_vals-1];

	int kl, ku, km;
	for (i=0; i < nstars; i++)
	{
		kl=-1;
		ku=magvals_nn;
		while (ku-kl > 1)
		{
			km=(ku+kl) >> 1;
			if (star[i].abs_magnitude >= mag_vals[km]) kl=km;
			else ku=km;
		}
		if (kl==-1) kl=0;
		else if (kl==magvals_nn) kl = magvals_nn-1;  // off the grid
		star[i].mag_number = kl;
		star[i].long_time_interval = false;
		double dt;
		double year, year_min, year_max;
		for (j=0; j < star[i].n_epochs-1; j++) {
			dt = star[i].times[j+1] - star[i].times[j];
			if (dt < 0.03) ;
			else {
				if (star[i].long_time_interval==true) die("more than one long time interval not allowed");
				else star[i].long_time_interval = true;
				star[i].k_longtime = j;
				star[i].year_number = -1;
				for (k=0; k < n_logdvlike_years; k++) {
					year = k+1;
					year_min = year - 0.22;
					year_max = year + 0.22;
					if ((dt > year_min) and (dt < year_max)) star[i].year_number = k;
				}
				if (star[i].year_number==-1) die("long interval %g not recognized for star %i",dt,i);
			}
		}
		star[i].clipped_outliers = false;
		if ((star[i].long_time_interval) and (no_error_params==false)) {
			if ((star[i].r_value[star[i].k_longtime] < rvalue_clip_threshold) or (star[i].r_value[star[i].k_longtime+1] < rvalue_clip_threshold)) star[i].clipped_outliers = true;
		}
	}

	// only used with DV_logdvlike
	start = true;
	likebvec.input(nstars);
	likenbvec.input(nstars);

	return;
}

void Galaxy::input_datafile_rtables(char* datafile_str, bool record_position)
{
	load_bicubic_interpolation_coefficients();

	ifstream datafile(datafile_str);
	datafile >> nstars;

	string rdir(datafile_str);
	string nstars_filename = "rtables." + rdir + "/nstars";
	//string logfilename = "logfile." + rdir;
	//logfile.open(logfilename.c_str());
	ifstream nstars_file(nstars_filename.c_str());
	nstars_file >> nstars;	// this keeps track of how many stars we have calculated R-tables for so far, since dataset might not be finished yet
	nstars_file.close();
	if (reset_number_of_stars) reset_nstars(nstars_subset);
	load_data(datafile,record_position);

	if (include_MW_likelihood) calculate_MW_likelihoods();

	int i;
	string musig_filename = "rtables." + rdir + "/musig_table";
	ifstream musig_file(musig_filename.c_str());
	musig_file >> mu_logP_nn;
	mu_logP_vals.input(mu_logP_nn);
	for (i=0; i < mu_logP_nn; i++) musig_file >> mu_logP_vals[i];
	musig_file >> sig_logP_nn;
	sig_logP_vals.input(sig_logP_nn);
	for (i=0; i < sig_logP_nn; i++) musig_file >> sig_logP_vals[i];
	musig_file.close();

	int vcm_nn;
	int j,k,l;
	for (i=0; i < nstars; i++)
	{
		string zeroval_filename = "rtables." + rdir + "/zerofile." + star[i].id;
		ifstream zerofile(zeroval_filename.c_str());
		zerofile >> star[i].zeroval;
		zerofile.close();
		if (star[i].zeroval==0) continue;

		string vcm_table_filename = "rtables." + rdir + "/rtable." + star[i].id + "/vcm_table";
		ifstream vcm_table(vcm_table_filename.c_str());
		vcm_table >> vcm_nn;
		star[i].rfactor_vcm_nn = vcm_nn;
		star[i].vcm_vals.input(vcm_nn);
		for (j=0; j < vcm_nn; j++) vcm_table >> star[i].vcm_vals[j];
		vcm_table.close();
		star[i].rfactor_vcm_min = star[i].vcm_vals[0];
		star[i].rfactor_vcm_max = star[i].vcm_vals[vcm_nn-1];

		star[i].rtable = new dmatrix[vcm_nn];
		star[i].rctable = new dmatrix**[vcm_nn];
		star[i].mu_logP_nn = mu_logP_nn; // needed for deleting the above arrays later

		string jstring;
		dmatrix y1a(mu_logP_nn,sig_logP_nn); // derivatives needed to calculate c_ij's
		dmatrix y2a(mu_logP_nn,sig_logP_nn); // derivatives needed to calculate c_ij's
		dmatrix y12a(mu_logP_nn,sig_logP_nn); // derivatives needed to calculate c_ij's
		for (j=0; j < vcm_nn; j++)
		{
			star[i].rtable[j].input(mu_logP_nn,sig_logP_nn);
			star[i].rctable[j] = new dmatrix*[mu_logP_nn];
			stringstream jstr;
			jstr << j;
			jstr >> jstring;
			string rtable_filename = "rtables." + rdir + "/rtable." + star[i].id + "/rtable." + jstring;
			ifstream rtable_file(rtable_filename.c_str());
			for (k=0; k < mu_logP_nn; k++) {
				star[i].rctable[j][k] = new dmatrix[sig_logP_nn];
				for (l=0; l < sig_logP_nn; l++) {
					rtable_file >> star[i].rtable[j][k][l];
					star[i].rctable[j][k][l].input(4,4);
				}
			}
			for (k=0; k < mu_logP_nn; k++) {
				for (l=0; l < sig_logP_nn; l++) {
					// set derivatives using centered differencing (except not at the endpoints; dealing with the endpoints makes the code messy)
					if (k==0)
						y1a[k][l] = (star[i].rtable[j][1][l] - star[i].rtable[j][0][l]) / (mu_logP_vals[1] - mu_logP_vals[0]);
					else if (k==mu_logP_nn-1)
						y1a[k][l] = (star[i].rtable[j][k][l] - star[i].rtable[j][k-1][l]) / (mu_logP_vals[k] - mu_logP_vals[k-1]);
					else
						y1a[k][l] = (star[i].rtable[j][k+1][l] - star[i].rtable[j][k-1][l]) / (mu_logP_vals[k+1] - mu_logP_vals[k-1]);

					if (l==0)
						y2a[k][l] = (star[i].rtable[j][k][1] - star[i].rtable[j][k][0]) / (sig_logP_vals[1] - sig_logP_vals[0]);
					else if (l==sig_logP_nn-1)
						y2a[k][l] = (star[i].rtable[j][k][l] - star[i].rtable[j][k][l-1]) / (sig_logP_vals[l] - sig_logP_vals[l-1]);
					else
						y2a[k][l] = (star[i].rtable[j][k][l+1] - star[i].rtable[j][k][l-1]) / (sig_logP_vals[l+1] - sig_logP_vals[l-1]);

					if ((k==0) and (l==0))
						y12a[k][l] = (star[i].rtable[j][k+1][l+1] - star[i].rtable[j][k+1][l] - star[i].rtable[j][k][l+1] + star[i].rtable[j][k][l])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k]) * (sig_logP_vals[l+1] - sig_logP_vals[l]));
					else if ((k==mu_logP_nn-1) and (l==sig_logP_nn-1))
						y12a[k][l] = (star[i].rtable[j][k][l] - star[i].rtable[j][k][l-1] - star[i].rtable[j][k-1][l] + star[i].rtable[j][k-1][l-1])
							/ ((mu_logP_vals[k] - mu_logP_vals[k-1]) * (sig_logP_vals[l] - sig_logP_vals[l-1]));
					else if ((k==0) and (l==sig_logP_nn-1))
						y12a[k][l] = (star[i].rtable[j][k+1][l] - star[i].rtable[j][k+1][l-1] - star[i].rtable[j][k][l] + star[i].rtable[j][k][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k]) * (sig_logP_vals[l] - sig_logP_vals[l-1]));
					else if ((k==mu_logP_nn-1) and (l==0))
						y12a[k][l] = (star[i].rtable[j][k][l+1] - star[i].rtable[j][k][l] - star[i].rtable[j][k-1][l+1] + star[i].rtable[j][k-1][l])
							/ ((mu_logP_vals[k] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l]));
					else if (k==0)
						y12a[k][l] = (star[i].rtable[j][k+1][l+1] - star[i].rtable[j][k+1][l-1] - star[i].rtable[j][k][l+1] + star[i].rtable[j][k][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k]) * (sig_logP_vals[l+1] - sig_logP_vals[l-1]));
					else if (l==0)
						y12a[k][l] = (star[i].rtable[j][k+1][l+1] - star[i].rtable[j][k+1][l] - star[i].rtable[j][k-1][l+1] + star[i].rtable[j][k-1][l])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l]));
					else if (k==mu_logP_nn-1)
						y12a[k][l] = (star[i].rtable[j][k][l+1] - star[i].rtable[j][k][l-1] - star[i].rtable[j][k-1][l+1] + star[i].rtable[j][k-1][l-1])
							/ ((mu_logP_vals[k] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l-1]));
					else if (l==sig_logP_nn-1)
						y12a[k][l] = (star[i].rtable[j][k+1][l] - star[i].rtable[j][k+1][l-1] - star[i].rtable[j][k-1][l] + star[i].rtable[j][k-1][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k-1]) * (sig_logP_vals[l] - sig_logP_vals[l-1]));
					else
						y12a[k][l] = (star[i].rtable[j][k+1][l+1] - star[i].rtable[j][k+1][l-1] - star[i].rtable[j][k-1][l+1] + star[i].rtable[j][k-1][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l-1]));
				}
			}
			double d1, d2, d1d2, xx;
			int m,n,p;
			dvector cl(16), x(16);
			for (k=0; k < mu_logP_nn-1; k++) {
				for (l=0; l < sig_logP_nn-1; l++) {
					d1 = mu_logP_vals[k+1] - mu_logP_vals[k];
					d2 = sig_logP_vals[l+1] - sig_logP_vals[l];
					d1d2 = d1*d2;
					// adapted from Numerical Recipes C++ routine bcucof
					x[0] = star[i].rtable[j][k][l];
					x[1] = star[i].rtable[j][k+1][l];
					x[2] = star[i].rtable[j][k+1][l+1];
					x[3] = star[i].rtable[j][k][l+1];
					x[4] = y1a[k][l] * d1;
					x[5] = y1a[k+1][l] * d1;
					x[6] = y1a[k+1][l+1] * d1;
					x[7] = y1a[k][l+1] * d1;
					x[8] = y2a[k][l] * d2;
					x[9] = y2a[k+1][l] * d2;
					x[10] = y2a[k+1][l+1] * d2;
					x[11] = y2a[k][l+1] * d2;
					x[12] = y12a[k][l] * d1d2;
					x[13] = y12a[k+1][l] * d1d2;
					x[14] = y12a[k+1][l+1] * d1d2;
					x[15] = y12a[k][l+1] * d1d2;

					for (m=0; m < 16; m++) {
						xx = 0;
						for (n=0; n < 16; n++) xx += wt[m][n]*x[n];
						cl[m] = xx;
					}
					p=0;
					for (m=0; m < 4; m++)
						for (n=0; n < 4; n++) star[i].rctable[j][k][l][m][n] = cl[p++];
					//cout << " " << star[i].rtable[j][k][l] << " " << star[i].rctable[j][0][0] << endl;
				}
			}
		}

		if (mpi_id==0) {
			if (output_to_logfile) logfile << "rtable for star " << star[i].id << " loaded" << endl;
			else cout << "rtable for star " << star[i].id << " loaded" << endl;
		}
	}

	return;
}

void Galaxy::input_datafile_sysparams(char* datafile_str, bool record_position)
{
	load_bicubic_interpolation_coefficients();

	ifstream datafile(datafile_str);
	datafile >> nstars;
	string rdir(datafile_str);

	int i;
	string sysparam_filename = rdir + ".sysparam_table";
	ifstream sysparam_file(sysparam_filename.c_str());
	sysparam_file >> n_sysparams;
	if (n_sysparams > 3) die("cannot currently handle more than three sysparam parameters");
	if (n_sysparams == 0) {
		sysparam_nn[0] = 1;
		sysparam_vals[0].input(sysparam_nn[0]);
		sysparam_vals[0][0] = 0; // no sysparam
	} else if (n_sysparams >= 1) {
		for (i=0; i < n_sysparams; i++) sysparam_file >> sysparam_type[i];
		sysparam_file >> sysparam_nn[0];
		sysparam_vals[0].input(sysparam_nn[0]);
		for (i=0; i < sysparam_nn[0]; i++) sysparam_file >> sysparam_vals[0][i];
	}
	if (n_sysparams >= 2) {
		sysparam_file >> sysparam_nn[1];
		sysparam_vals[1].input(sysparam_nn[1]);
		for (i=0; i < sysparam_nn[1]; i++) sysparam_file >> sysparam_vals[1][i];
	}
	if (n_sysparams >= 3) {
		sysparam_file >> sysparam_nn[2];
		sysparam_vals[2].input(sysparam_nn[2]);
		for (i=0; i < sysparam_nn[2]; i++) sysparam_file >> sysparam_vals[2][i];
	}

	sysparam_file.close();

	if (reset_number_of_stars) reset_nstars(nstars_subset);
	load_data(datafile,record_position);

	if (include_MW_likelihood) calculate_MW_likelihoods();
	if (include_binaries) {
		int vcm_nn;
		int j,k,l,m;
		for (i=0; i < nstars; i++)
		{
			//string zeroval_filename = "rltables." + rdir + "/zerofile." + star[i].id;
			//ifstream zerofile(zeroval_filename.c_str());
			//zerofile >> star[i].zeroval;
			//zerofile.close();
			//if (star[i].zeroval==0) continue;

			string vcm_table_filename = "rltables." + rdir + "/rltable." + star[i].id + "/vcm_table";
			ifstream vcm_table(vcm_table_filename.c_str());
			vcm_table >> vcm_nn;
			star[i].rfactor_vcm_nn = vcm_nn;
			star[i].vcm_vals.input(vcm_nn);
			for (j=0; j < vcm_nn; j++) vcm_table >> star[i].vcm_vals[j];
			vcm_table.close();
			star[i].rfactor_vcm_min = star[i].vcm_vals[0];
			star[i].rfactor_vcm_max = star[i].vcm_vals[vcm_nn-1];
			//cout << "star " << i << ": vcm=" << vcm_nn << ", nsys=" << star[i].n_sysparams << endl;

			if (star[i].n_sysparams <= 1) {
				star[i].rltable1 = new dvector[vcm_nn];
				if (star[i].n_sysparams==1) star[i].rltable_spline = new Spline[vcm_nn];
			} else if (star[i].n_sysparams == 2) {
				star[i].rltable2 = new dmatrix[vcm_nn];
			} else if (star[i].n_sysparams == 3) {
				star[i].rltable3 = new dmatrix*[vcm_nn];
			}
			star[i].sysparam1_nn = sysparam_nn[0]; // needed for deleting the above arrays later

			string jstring;
			if (star[i].n_sysparams <= 1) {
				for (j=0; j < vcm_nn; j++) {
					star[i].rltable1[j].input(sysparam_nn[star[i].sysparam_order[0]]);
					stringstream jstr;
					jstr << j;
					jstr >> jstring;
					string rltable_filename = "rltables." + rdir + "/rltable." + star[i].id + "/rltable." + jstring;
					ifstream rltable_file(rltable_filename.c_str());
					for (k=0; k < sysparam_nn[star[i].sysparam_order[0]]; k++) {
						rltable_file >> star[i].rltable1[j][k];
					}
					if (star[i].n_sysparams==1) {
						star[i].rltable_spline[j].input(sysparam_vals[star[i].sysparam_order[0]],star[i].rltable1[j]);
					}
				}
			}
			else if (star[i].n_sysparams==2) {
				for (j=0; j < vcm_nn; j++) {
					star[i].rltable2[j].input(sysparam_nn[star[i].sysparam_order[0]],sysparam_nn[star[i].sysparam_order[1]]);
					stringstream jstr;
					jstr << j;
					jstr >> jstring;
					string rltable_filename = "rltables." + rdir + "/rltable." + star[i].id + "/rltable." + jstring;
					ifstream rltable_file(rltable_filename.c_str());
					for (k=0; k < sysparam_nn[star[i].sysparam_order[0]]; k++) {
						for (l=0; l < sysparam_nn[star[i].sysparam_order[1]]; l++) {
							//if (i==0) cout << "Inputting element " << k << ", " << l << endl;
							rltable_file >> star[i].rltable2[j][k][l];
						}
					}
				}
				if (mpi_id==0) {
					if (output_to_logfile) logfile << "rltable2 for star " << star[i].id << " loaded" << endl;
					else cout << "rltable2 for star " << star[i].id << " loaded" << endl;
				}
				//if (i==2) {
				//ofstream testfile("testrlt.dat");
				//for (j=0; j < vcm_nn; j++) {
					//cout << j << " " << star[i].vcm_vals[j] << " " << star[i].rltable2[j][3][3] << endl;
					//testfile << star[i].vcm_vals[j] << " " << star[i].rltable2[j][3][3] << endl;
				//}
				//testfile.close();
				//}
			}
			else if (star[i].n_sysparams==3) {
				for (j=0; j < vcm_nn; j++) {
					stringstream jstr;
					jstr << j;
					jstr >> jstring;
					string rltable_filename = "rltables." + rdir + "/rltable." + star[i].id + "/rltable." + jstring;
					ifstream rltable_file(rltable_filename.c_str());
					star[i].rltable3[j] = new dmatrix[sysparam_nn[star[i].sysparam_order[0]]];
					for (k=0; k < sysparam_nn[star[i].sysparam_order[0]]; k++) {
						star[i].rltable3[j][k].input(sysparam_nn[star[i].sysparam_order[1]],sysparam_nn[star[i].sysparam_order[2]]);
						for (l=0; l < sysparam_nn[star[i].sysparam_order[1]]; l++) {
							for (m=0; m < sysparam_nn[star[i].sysparam_order[2]]; m++) {
								//if (i==0) cout << "Inputting element " << k << ", " << l << ", " << m << endl;
								rltable_file >> star[i].rltable3[j][k][l][m];
							}
						}
					}
				}
				if (mpi_id==0) {
					if (output_to_logfile) logfile << "rltable3 for star " << star[i].id << " loaded" << endl;
					else cout << "rltable3 for star " << star[i].id << " loaded" << endl;
				}
			}
		}
	}

	return;
}

void Galaxy::load_bicubic_interpolation_coefficients(void)
{
	ifstream bcucof("bcucof.dat");
	wt = new int*[16];
	for (int i=0; i < 16; i++)
	{
		wt[i] = new int[16];
		for (int j=0; j < 16; j++)
			bcucof >> wt[i][j];
	}
}

void Galaxy::input_datafile_jtables(char* datafile_str, bool record_position)
{
	load_bicubic_interpolation_coefficients();

	ifstream datafile(datafile_str);
	datafile >> nstars;

	string rdir(datafile_str);
	if (use_finished_stars) {
		string nstars_filename = "jtables." + rdir + "/nstars";
		ifstream nstars_file(nstars_filename.c_str());
		nstars_file >> nstars;	// this keeps track of how many stars we have calculated R-tables for so far, since dataset might not be finished yet
		nstars_file.close();
	}
	if (reset_number_of_stars) reset_nstars(nstars_subset);
	load_data(datafile,record_position);

	//if (include_MW_likelihood) calculate_MW_likelihoods();

	int i;
	string musig_filename = "jtables." + rdir + "/musig_table";
	ifstream musig_file(musig_filename.c_str());
	musig_file >> mu_logP_nn;
	mu_logP_vals.input(mu_logP_nn);
	for (i=0; i < mu_logP_nn; i++) musig_file >> mu_logP_vals[i];
	musig_file >> sig_logP_nn;
	sig_logP_vals.input(sig_logP_nn);
	for (i=0; i < sig_logP_nn; i++) musig_file >> sig_logP_vals[i];
	musig_file.close();

	int k,l;
	double dvlike, nfac;
	for (i=0; i < nstars; i++)
	{
		star[i].jtable.input(mu_logP_nn,sig_logP_nn);
		star[i].mu_logP_nn = mu_logP_nn; // needed for deleting the above array later

		string jtable_filename = "jtables." + rdir + "/jtable." + star[i].id;
		ifstream jtable_file(jtable_filename.c_str());
		for (k=0; k < mu_logP_nn; k++) {
			for (l=0; l < sig_logP_nn; l++) {
				if (load_dvlikes_not_jfactors) {
					jtable_file >> dvlike >> nfac;
					star[i].jtable[k][l] = dvlike/nfac;
				}
				else jtable_file >> star[i].jtable[k][l];
			}
		}

		if (bicubic_interpolation==true)
		{
			star[i].jctable = new dmatrix*[mu_logP_nn];
			for (k=0; k < mu_logP_nn; k++) {
				star[i].jctable[k] = new dmatrix[sig_logP_nn];
				for (l=0; l < sig_logP_nn; l++) {
					star[i].jctable[k][l].input(4,4);
				}
			}

			string jstring;
			dmatrix y1a(mu_logP_nn,sig_logP_nn); // derivatives needed to calculate c_ij's
			dmatrix y2a(mu_logP_nn,sig_logP_nn); // derivatives needed to calculate c_ij's
			dmatrix y12a(mu_logP_nn,sig_logP_nn); // derivatives needed to calculate c_ij's
			for (k=0; k < mu_logP_nn; k++) {
				for (l=0; l < sig_logP_nn; l++) {
					// set derivatives using centered differencing (except not at the endpoints; dealing with the endpoints makes the code messy)
					if (k==0)
						y1a[k][l] = (star[i].jtable[1][l] - star[i].jtable[0][l]) / (mu_logP_vals[1] - mu_logP_vals[0]);
					else if (k==mu_logP_nn-1)
						y1a[k][l] = (star[i].jtable[k][l] - star[i].jtable[k-1][l]) / (mu_logP_vals[k] - mu_logP_vals[k-1]);
					else
						y1a[k][l] = (star[i].jtable[k+1][l] - star[i].jtable[k-1][l]) / (mu_logP_vals[k+1] - mu_logP_vals[k-1]);

					if (l==0)
						y2a[k][l] = (star[i].jtable[k][1] - star[i].jtable[k][0]) / (sig_logP_vals[1] - sig_logP_vals[0]);
					else if (l==sig_logP_nn-1)
						y2a[k][l] = (star[i].jtable[k][l] - star[i].jtable[k][l-1]) / (sig_logP_vals[l] - sig_logP_vals[l-1]);
					else
						y2a[k][l] = (star[i].jtable[k][l+1] - star[i].jtable[k][l-1]) / (sig_logP_vals[l+1] - sig_logP_vals[l-1]);

					if ((k==0) and (l==0))
						y12a[k][l] = (star[i].jtable[k+1][l+1] - star[i].jtable[k+1][l] - star[i].jtable[k][l+1] + star[i].jtable[k][l])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k]) * (sig_logP_vals[l+1] - sig_logP_vals[l]));
					else if ((k==mu_logP_nn-1) and (l==sig_logP_nn-1))
						y12a[k][l] = (star[i].jtable[k][l] - star[i].jtable[k][l-1] - star[i].jtable[k-1][l] + star[i].jtable[k-1][l-1])
							/ ((mu_logP_vals[k] - mu_logP_vals[k-1]) * (sig_logP_vals[l] - sig_logP_vals[l-1]));
					else if ((k==0) and (l==sig_logP_nn-1))
						y12a[k][l] = (star[i].jtable[k+1][l] - star[i].jtable[k+1][l-1] - star[i].jtable[k][l] + star[i].jtable[k][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k]) * (sig_logP_vals[l] - sig_logP_vals[l-1]));
					else if ((k==mu_logP_nn-1) and (l==0))
						y12a[k][l] = (star[i].jtable[k][l+1] - star[i].jtable[k][l] - star[i].jtable[k-1][l+1] + star[i].jtable[k-1][l])
							/ ((mu_logP_vals[k] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l]));
					else if (k==0)
						y12a[k][l] = (star[i].jtable[k+1][l+1] - star[i].jtable[k+1][l-1] - star[i].jtable[k][l+1] + star[i].jtable[k][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k]) * (sig_logP_vals[l+1] - sig_logP_vals[l-1]));
					else if (l==0)
						y12a[k][l] = (star[i].jtable[k+1][l+1] - star[i].jtable[k+1][l] - star[i].jtable[k-1][l+1] + star[i].jtable[k-1][l])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l]));
					else if (k==mu_logP_nn-1)
						y12a[k][l] = (star[i].jtable[k][l+1] - star[i].jtable[k][l-1] - star[i].jtable[k-1][l+1] + star[i].jtable[k-1][l-1])
							/ ((mu_logP_vals[k] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l-1]));
					else if (l==sig_logP_nn-1)
						y12a[k][l] = (star[i].jtable[k+1][l] - star[i].jtable[k+1][l-1] - star[i].jtable[k-1][l] + star[i].jtable[k-1][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k-1]) * (sig_logP_vals[l] - sig_logP_vals[l-1]));
					else
						y12a[k][l] = (star[i].jtable[k+1][l+1] - star[i].jtable[k+1][l-1] - star[i].jtable[k-1][l+1] + star[i].jtable[k-1][l-1])
							/ ((mu_logP_vals[k+1] - mu_logP_vals[k-1]) * (sig_logP_vals[l+1] - sig_logP_vals[l-1]));
				}
			}
			double d1, d2, d1d2, xx;
			int m,n,p;
			dvector cl(16), x(16);
			for (k=0; k < mu_logP_nn-1; k++) {
				for (l=0; l < sig_logP_nn-1; l++) {
					d1 = mu_logP_vals[k+1] - mu_logP_vals[k];
					d2 = sig_logP_vals[l+1] - sig_logP_vals[l];
					d1d2 = d1*d2;
					// adapted from Numerical Recipes C++ routine bcucof
					x[0] = star[i].jtable[k][l];
					x[1] = star[i].jtable[k+1][l];
					x[2] = star[i].jtable[k+1][l+1];
					x[3] = star[i].jtable[k][l+1];
					x[4] = y1a[k][l] * d1;
					x[5] = y1a[k+1][l] * d1;
					x[6] = y1a[k+1][l+1] * d1;
					x[7] = y1a[k][l+1] * d1;
					x[8] = y2a[k][l] * d2;
					x[9] = y2a[k+1][l] * d2;
					x[10] = y2a[k+1][l+1] * d2;
					x[11] = y2a[k][l+1] * d2;
					x[12] = y12a[k][l] * d1d2;
					x[13] = y12a[k+1][l] * d1d2;
					x[14] = y12a[k+1][l+1] * d1d2;
					x[15] = y12a[k][l+1] * d1d2;

					for (m=0; m < 16; m++) {
						xx = 0;
						for (n=0; n < 16; n++) xx += wt[m][n]*x[n];
						cl[m] = xx;
					}
					p=0;
					for (m=0; m < 4; m++)
						for (n=0; n < 4; n++) star[i].jctable[k][l][m][n] = cl[p++];
				}
			}
		}

		if (mpi_id==0) {
			if (output_to_logfile) logfile << "jtable for star " << star[i].id << " loaded" << endl;
			else cout << "jtable for star " << star[i].id << " loaded" << endl;
		}
	}

	return;
}

double Galaxy::LogLike_MW_Velocities_No_Binaries(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction

	double loglike=0;
	double like_gal, like_mw;

	for (int i=0; i < nstars; i++)
	{
		like_gal = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		loglike += -log((1-a[2])*star[i].vlike_mw + a[2]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_Gauss_Hermite(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction

	double loglike=0;
	double x, xerr, sig, sqx, h3, h4, gaussian, like;

	for (int i=0; i < nstars; i++)
	{
		sig = a[1];
		xerr = (star[i].v_avg-a[0])/sig;
		x = xerr;
		sqx = x*x;
		h3 = 8*x*sqx - 12*x; // need to orthogonalize this one still
		h4 = (4*sqx*sqx - 12*sqx + 3)*0.204124145232; // last factor is 1/sqrt(24)
		gaussian = exp(-0.5*xerr*xerr)/SQRT_2PI/sig;
		like = gaussian*(1 + a[2]*h4)/(1+a[2]*0.612372435696); // numerical factor is 0.5*sqrt(3/2)
		if (like <= 0) like=1e-30;
		loglike += -log(like);
	}
	return loglike;
}

double Galaxy::LogLike_MW_Velocities_Metallicities_No_Binaries(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = mean metal equivalent width
	// a[4] = equivalent width dispersion
	// a[5] = MW mean metal equivalent width
	// a[6] = MW equivalent width dispersion

	double loglike=0;
	double vlike_gal;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_gal = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) wlike_gal = 1; else
		wlike_gal = exp(-0.5*SQR(star[i].w_avg-a[3])/(a[4]*a[4]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[4]*a[4]+SQR(star[i].sigw));
		like_gal = vlike_gal*wlike_gal;

		if (star[i].no_metallicities==true) wlike_mw = 1; else
		wlike_mw = exp(-0.5*SQR(star[i].w_avg-a[5])/(a[6]*a[6]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[6]*a[6]+SQR(star[i].sigw));
		like_mw = star[i].vlike_mw*wlike_mw;

		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_Binaries_VW(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = mean metal equivalent width
	// a[4] = equivalent width dispersion
	// a[5] = MW mean metal equivalent width
	// a[6] = MW equivalent width dispersion
	// a[7] = binary fraction
	// a[8] = mu_logP
	// a[9] = sig_logP

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	mu_logP = a[8];
	sig_logP = a[9];

	interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)

	double loglike=0;
	double vlike_nb;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-a[3])/(a[4]*a[4]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[4]*a[4]+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-a[5])/(a[6]*a[6]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[6]*a[6]+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].zeroval==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand_interpolate(0)+jfactor_integrand_interpolate(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 0.25)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 0.2) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[7])*vlike_nb + a[7]*jfactor);
		}

		like_mw = star[i].vlike_mw*wlike_mw;

		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_VW_No_Period_Params(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = mean metal equivalent width
	// a[4] = equivalent width dispersion
	// a[5] = MW mean metal equivalent width
	// a[6] = MW equivalent width dispersion
	// a[7] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-a[3])/(a[4]*a[4]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[4]*a[4]+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-a[5])/(a[6]*a[6]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[6]*a[6]+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].zeroval==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand_interpolate(0)+jfactor_integrand_interpolate(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[7])*vlike_nb + a[7]*jfactor);
		}

		like_mw = star[i].vlike_mw*wlike_mw;

		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_V_Fixed_W_Params_No_Period_Params(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].rnorm==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[3])*vlike_nb + a[3]*jfactor);
		}

		like_mw = star[i].vlike_mw*wlike_mw;

		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_V_No_MW(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = binary fraction
	// a[4] = mu_logP
	// a[5] = sig_logP

	systemic_velocity = a[0];	// necessary for calculating J-factors
	dispersion = a[1];			// necessary for calculating J-factors
	mu_logP = a[3];				// necessary for calculating J-factors
	sig_logP = a[4];				// necessary for calculating J-factors

	interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)

	double loglike=0;
	double vlike_nb;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));

		if (star[i].zeroval==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand_interpolate(0)+jfactor_integrand_interpolate(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 0.25)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 0.2) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = (1-a[2])*vlike_nb + a[2]*jfactor;
		}

		loglike += -log(like_gal);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_V_Fixed_W_Params(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = binary fraction
	// a[4] = mu_logP
	// a[5] = sig_logP

	systemic_velocity = a[0];	// necessary for calculating J-factors
	dispersion = a[1];			// necessary for calculating J-factors
	mu_logP = a[4];				// necessary for calculating J-factors
	sig_logP = a[5];				// necessary for calculating J-factors

	interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)

	double loglike=0;
	double vlike_nb;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].zeroval==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand_interpolate(0)+jfactor_integrand_interpolate(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 0.25)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 0.2) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[3])*vlike_nb + a[3]*jfactor);
		}

		like_mw = star[i].vlike_mw*wlike_mw;

		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_Binaries_V_Fixed_W_Params_Test_RTables(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = binary fraction

	systemic_velocity = a[0];	// necessary for calculating J-factors
	dispersion = a[1];			// necessary for calculating J-factors

	interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)

	double loglike=0;
	double vlike_nb;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].zeroval==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand_interpolate(0)+jfactor_integrand_interpolate(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[3])*vlike_nb + a[3]*jfactor);
		}

		like_mw = star[i].vlike_mw*wlike_mw;

		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_MW_No_Metallicities(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = member fraction
	// a[3] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb, vlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));

		if (star[i].rnorm==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = (1-a[3])*vlike_nb + a[3]*jfactor;
		}

		like_mw = star[i].vlike_mw;

		if (jfactor < 0) {
			die("jfactor = %g",jfactor);
		}
		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);
		if (fabs(loglike) < 1e30) ;
		else die("infinite loglikelihood");
	}
	return loglike;
}

double Galaxy::LogLike_Binaries_VR(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = N_gal (ratio of galaxy number density over MW number density at R=0)
	// a[3] = Plummer scale radius
	// a[4] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;
	double surface_brightness_profile, local_member_fraction;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));

		if (star[i].rnorm==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = (1-a[4])*vlike_nb + a[4]*jfactor;
		}

		like_mw = star[i].vlike_mw;

		surface_brightness_profile = pow(1+SQR(star[i].R/a[3]),-2); // Projected Plummer profile, where a[3] is the scale radius
		local_member_fraction = a[2]*surface_brightness_profile/(a[2]*surface_brightness_profile + 1);

		if (jfactor < 0) {
			die("jfactor = %g",jfactor);
		}
		loglike += -log((1-local_member_fraction)*like_mw + local_member_fraction*like_gal); // this is the conditional likelihood L(V,W|R)
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_VR_Fixed_Metallicity_Params(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = N_gal (ratio of galaxy number density over MW number density at R=0)
	// a[3] = Plummer scale radius
	// a[4] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb;
	double like_gal, like_mw;
	double wlike_gal, wlike_mw;

	double jfactor, sum;
	int i,j;
	double surface_brightness_profile, local_member_fraction;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].rnorm==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[4])*vlike_nb + a[4]*jfactor);
		}

		like_mw = wlike_mw*star[i].vlike_mw;

		surface_brightness_profile = pow(1+SQR(star[i].R/a[3]),-2); // Projected Plummer profile, where a[3] is the scale radius
		local_member_fraction = a[2]*surface_brightness_profile/(a[2]*surface_brightness_profile + 1);

		if (jfactor < 0) {
			die("jfactor = %g",jfactor);
		}
		loglike += -log((1-local_member_fraction)*like_mw + local_member_fraction*like_gal); // this is the conditional likelihood L(V,W|R)
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_VR_Fixed_W_Rs(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = N_gal (ratio of galaxy number density over MW number density at R=0)
	// a[3] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb;
	double like_gal, like_mw;
	double wlike_gal, wlike_mw;

	double jfactor, sum;
	int i,j;
	double surface_brightness_profile, local_member_fraction;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].rnorm==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[3])*vlike_nb + a[3]*jfactor);
		}

		like_mw = wlike_mw*star[i].vlike_mw;

		surface_brightness_profile = pow(1+SQR(star[i].R/plummer_scale_radius),-2); // Projected Plummer profile, where a[3] is the scale radius
		local_member_fraction = a[2]*surface_brightness_profile/(a[2]*surface_brightness_profile + 1);

		if (jfactor < 0) {
			die("jfactor = %g",jfactor);
		}
		loglike += -log((1-local_member_fraction)*like_mw + local_member_fraction*like_gal); // this is the conditional likelihood L(V,W|R)
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Full_Monty_No_Period_Params(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = N_gal (ratio of galaxy number density over MW number density at R=0)
	// a[3] = Plummer scale radius
	// a[4] = mean metal equivalent width
	// a[5] = equivalent width dispersion
	// a[6] = MW mean metal equivalent width
	// a[7] = MW equivalent width dispersion
	// a[8] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb, vlike_mw;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;
	double surface_brightness_profile, local_member_fraction;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-a[4])/(a[5]*a[5]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[5]*a[5]+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-a[6])/(a[7]*a[7]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[7]*a[7]+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].rnorm==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[8])*vlike_nb + a[8]*jfactor);
		}

		like_mw = star[i].vlike_mw*wlike_mw;

		surface_brightness_profile = pow(1+SQR(star[i].R/a[3]),-2); // Projected Plummer profile, where a[3] is the scale radius
		local_member_fraction = a[2]*surface_brightness_profile/(a[2]*surface_brightness_profile + 1);

		if (jfactor < 0) {
			die("jfactor = %g",jfactor);
		}
		loglike += -log((1-local_member_fraction)*like_mw + local_member_fraction*like_gal); // this is the conditional likelihood L(V,W|R)
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Full_Monty(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = N_gal (ratio of galaxy number density over MW number density at R=0)
	// a[3] = Plummer scale radius
	// a[4] = mean metal equivalent width
	// a[5] = equivalent width dispersion
	// a[6] = MW mean metal equivalent width
	// a[7] = MW equivalent width dispersion
	// a[8] = binary fraction
	// a[9] = mu_logP
	// a[10] = sig_logP

	systemic_velocity = a[0];	// necessary for calculating J-factors
	dispersion = a[1];			// necessary for calculating J-factors
	mu_logP = a[9];				// necessary for calculating J-factors
	sig_logP = a[10];				// necessary for calculating J-factors

	interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)

	double loglike=0;
	double vlike_nb, vlike_mw;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;
	double surface_brightness_profile, local_member_fraction;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-a[4])/(a[5]*a[5]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[5]*a[5]+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-a[6])/(a[7]*a[7]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[7]*a[7]+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].zeroval==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand_interpolate(0)+jfactor_integrand_interpolate(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 0.25)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 0.5) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = wlike_gal*((1-a[8])*vlike_nb + a[8]*jfactor);
		}

		like_mw = star[i].vlike_mw*wlike_mw;

		surface_brightness_profile = pow(1+SQR(star[i].R/a[3]),-2); // Projected Plummer profile, where a[3] is the scale radius
		local_member_fraction = a[2]*surface_brightness_profile/(a[2]*surface_brightness_profile + 1);

		if (jfactor < 0) {
			die("jfactor = %g",jfactor);
			//jfactor = 0;
		}
		loglike += -log((1-local_member_fraction)*like_mw + local_member_fraction*like_gal); // this is the conditional likelihood L(V,W|R)
	}
	return loglike;
}

double Galaxy::LogLike_Test(double *a)
{
	// a[0] = velocity dispersion
	// a[1] = member fraction

	dispersion = a[0]; // necessary for calculating J-factors

	double loglike=0;
	double vlike_nb;
	double like_gal, like_mw;

	double jfactor, sum;
	int i,j;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		vlike_nb = exp(-0.5*SQR(star[i].v_avg-systemic_velocity)/(a[0]*a[0]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[0]*a[0]+SQR(star[i].sigv));

		if (star[i].rnorm==0) { jfactor = 0; like_gal = 0; }
		else {
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);

			if ((jfactor < 0) and (fabs(jfactor) < 1e-6)) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor: " << star[i].id << "v_sys: " << systemic_velocity << " sigma: " << dispersion << " jfactor: " << jfactor << endl;
				if (vlike_nb > 1e-3) die("wtf? star %i, jfactor %g, v = %g, disp = %g",i,jfactor,systemic_velocity,dispersion);
				jfactor = 0; // often the case for MW stars
			}
			like_gal = (1-a[1])*vlike_nb + a[1]*jfactor;
		}

		like_mw = star[i].vlike_mw;

		if (jfactor < 0) {
			die("jfactor = %g",jfactor);
		}
		loglike += -log((1-member_fraction)*like_mw + member_fraction*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_No_Binaries_No_MW(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion

	double loglike=0, like_nb;

	for (int i=0; i < nstars; i++)
	{
		like_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));
		loglike += -log(like_nb);
	}
	if (log_dispersion_prior) loglike += log(a[1]);

	//if (loglike*0.0 != 0.0) return 1e30;
	//else
	return loglike;
}

double Galaxy::LogLike_Binaries_No_MW(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors

	double loglike=0, like_nb;
	double jfactor;

	double sum;
	int i,j;
	double nfactor, v_avg, sigv;

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		//nfactor = calculate_nfactor(star[i].vlos_data,star[i].v_errors,v_avg,sigv);
		// if the assumed dispersion is small compared to the vcm stepsizes, then treat the R-function like a delta function and take vcm value closest to a[0]
		if (dispersion < ((star[i].rfactor_vcm_max-star[i].rfactor_vcm_min) / (star[i].rfactor_vcm_nn-1))) {
			int i_closest = (int) (star[i].rfactor_vcm_nn*(systemic_velocity - star[i].rfactor_vcm_min) / (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min));
			jfactor = star[i].rfactor_vals[i_closest];
		} else {
			// trapezoid rule to find jfactor
			sum = 0.5*(jfactor_integrand(0)+jfactor_integrand(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);
		}
		//if (dispersion < ((star[i].rfactor_vcm_max-star[i].rfactor_vcm_min) / (star[i].rfactor_vcm_nn-1))) cout << "sig=" << dispersion << " mu=" << systemic_velocity << " JFAC: " << jfactor << " " << jfactor_test << " vcm=" << star[i].vcm_vals[i_closest] << endl;

		if (star[i].nfactor <= 1e-30)
		{
			loglike += -log(jfactor*a[2]); // counted as a binary
		} else {
			nfactor = calculate_nfactor(star[i].vlos_data,star[i].v_errors,v_avg,sigv);
			like_nb = exp(-0.5*SQR(star[i].v_avg-a[0])/(a[1]*a[1]+SQR(star[i].sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(star[i].sigv));

			if (jfactor < 0) {
				if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor with nfactor not zero...skipped star: " << star[i].id << " sigma: " << a[1] << " jfactor: " << jfactor << endl;
				jfactor = 1e30; // this assures that the star is recorded as a binary (like_nb becomes negligible compared to this jfactor)
			}
			else {
				loglike += -log((1-a[2])*like_nb + a[2]*jfactor);
			}
		}
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_No_Binaries_No_MW_V_Sysparam(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = lambda1 (sysparam parameter)

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = a[2];

	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	dvector verr_sysparam;
	double v_avg, sigv;

	for (i=0; i < nstars; i++)
	{
		star_i = i;
		vlos_sysparam.input(star[i].n_epochs);
		verr_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			verr_sysparam[j] = star[i].v_errors[j];
			if (star[i].sysparam_i[j][0]==0) {
				if (sysparam_type[0]==0) vlos_sysparam[j] -= sysparam1;
				else if (sysparam_type[0]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam1*sysparam1);
				else die("sysparam type not recognized");
			}
			else if (star[i].sysparam_i[j][0] != -1) die("WTF?!");
		}
		nfactor = calculate_nfactor(vlos_sysparam,verr_sysparam,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));

		loglike += -log(like_nb);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_No_Binaries_No_MW_V_Sysparam2(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = lambda1 (sysparam parameter)
	// a[3] = lambda1 (sysparam parameter)

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = a[2];
	sysparam2 = a[3];

	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	dvector verr_sysparam;
	double v_avg, sigv;

	for (i=0; i < nstars; i++)
	{
		star_i = i;
		vlos_sysparam.input(star[i].n_epochs);
		verr_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			verr_sysparam[j] = star[i].v_errors[j];
			for (int kk=0; kk < 2; kk++) {
				if (star[i].sysparam_i[j][kk]==0) {
					if (sysparam_type[0]==0) vlos_sysparam[j] -= sysparam1;
					else if (sysparam_type[0]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam1*sysparam1);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==1) {
					if (sysparam_type[1]==0) vlos_sysparam[j] -= sysparam2;
					else if (sysparam_type[1]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam2*sysparam2);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk] != -1) die("WTF?!");
			}
		}
		nfactor = calculate_nfactor(vlos_sysparam,verr_sysparam,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));
		loglike += -log(like_nb);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_No_Binaries_No_MW_V_Sysparam3(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = lambda1 (sysparam parameter)
	// a[3] = lambda2 (sysparam parameter)
	// a[4] = lambda3 (sysparam parameter)

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = a[2];
	sysparam2 = a[3];
	sysparam3 = a[4];

	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	dvector verr_sysparam;
	double v_avg, sigv;

	for (i=0; i < nstars; i++)
	{
		star_i = i;
		vlos_sysparam.input(star[i].n_epochs);
		verr_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			verr_sysparam[j] = star[i].v_errors[j];
			for (int kk=0; kk < 2; kk++) {
				if (star[i].sysparam_i[j][kk]==0) {
					if (sysparam_type[0]==0) vlos_sysparam[j] -= sysparam1;
					else if (sysparam_type[0]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam1*sysparam1);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==1) {
					if (sysparam_type[1]==0) vlos_sysparam[j] -= sysparam2;
					else if (sysparam_type[1]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam2*sysparam2);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==2) {
					if (sysparam_type[2]==0) vlos_sysparam[j] -= sysparam3;
					else if (sysparam_type[2]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam3*sysparam3);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk] != -1) die("WTF?!");
			}
		}
		nfactor = calculate_nfactor(vlos_sysparam,verr_sysparam,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));
		loglike += -log(like_nb);
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_No_MW_V_Sysparam(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = binary fraction
	// a[3] = lambda1 (sysparam parameter)

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = a[3];

	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	dvector verr_sysparam;
	double v_avg, sigv;

	int ll;
	double lval;
	for (i=0; i < nstars; i++)
	{
		star_i = i;

		// if the assumed dispersion is small compared to the vcm stepsizes, then treat the R-function like a delta function and take vcm value closest to a[0]
		if (star[i].zeroval==0) { jfactor = 0; }
		else {
			// trapezoid rule to find jfactor
			sum = 0.5*(jfactor_integrand_interpolate_sysparam(0)+jfactor_integrand_interpolate_sysparam(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate_sysparam(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);
		}

		vlos_sysparam.input(star[i].n_epochs);
		verr_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			verr_sysparam[j] = star[i].v_errors[j];
			if (star[i].sysparam_i[j][0]==0) {
				if (sysparam_type[0]==0) vlos_sysparam[j] -= sysparam1;
				else if (sysparam_type[0]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam1*sysparam1);
				else die("sysparam type not recognized");
			}
			else if (star[i].sysparam_i[j][0] != -1) die("WTF?!");
		}
		nfactor = calculate_nfactor(vlos_sysparam,verr_sysparam,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));

		if (nfactor <= 1e-30) {
			loglike += -log(a[2]*jfactor); // counted as a binary
		} else if (jfactor < 0) {
			if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor with nfactor not zero...skipped star: " << star[i].id << " sigma: " << a[1] << " jfactor: " << jfactor << endl;
			jfactor = 1e30; // this assures that the star is recorded as a binary (like_nb becomes negligible compared to this jfactor)
		}
		else {
			loglike += -log((1-a[2])*like_nb + a[2]*jfactor);
		}
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_No_MW_V_Sysparam2(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = binary fraction
	// a[3] = lambda1 (sysparam parameter)
	// a[4] = lambda2 (sysparam parameter)

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = a[3];
	sysparam2 = a[4];
	//systemic_velocity = 65; // necessary for calculating J-factors
	//dispersion = 3.55; // necessary for calculating J-factors
	//sysparam1 = -1.7;
	//sysparam2 = -0.32;


	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	dvector verr_sysparam;
	double v_avg, sigv;

	interpolate_sysparams(2);

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		// if the assumed dispersion is small compared to the vcm stepsizes, then treat the R-function like a delta function and take vcm value closest to a[0]
		//if (star[i].zeroval==0) { jfactor = 0; }
		//else {
			// trapezoid rule to find jfactor
			sum = 0.5*(jfactor_integrand_interpolate_sysparam(0)+jfactor_integrand_interpolate_sysparam(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate_sysparam(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);
		//}
		//if (dispersion < ((star[i].rfactor_vcm_max-star[i].rfactor_vcm_min) / (star[i].rfactor_vcm_nn-1))) cout << "sig=" << dispersion << " mu=" << systemic_velocity << " JFAC: " << jfactor << " " << jfactor_test << " vcm=" << star[i].vcm_vals[i_closest] << endl;

		vlos_sysparam.input(star[i].n_epochs);
		verr_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			verr_sysparam[j] = star[i].v_errors[j];
			for (int kk=0; kk < 2; kk++) {
				if (star[i].sysparam_i[j][kk]==0) {
					if (sysparam_type[0]==0) vlos_sysparam[j] -= sysparam1;
					else if (sysparam_type[0]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam1*sysparam1);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==1) {
					if (sysparam_type[1]==0) vlos_sysparam[j] -= sysparam2;
					else if (sysparam_type[1]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam2*sysparam2);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk] != -1) die("WTF?!");
			}
		}
		nfactor = calculate_nfactor(vlos_sysparam,verr_sysparam,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));
		//if (nfactor <= 1e-30) {
			//loglike += -log(a[2]*jfactor); // counted as a binary
		if (jfactor < 0) {
			if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor with nfactor not zero...skipped star: " << star[i].id << " sigma: " << a[1] << " jfactor: " << jfactor << endl;
			jfactor = 1e30; // this assures that the star is recorded as a binary (like_nb becomes negligible compared to this jfactor)
		}
		else {
			loglike += -log((1-a[2])*like_nb + a[2]*jfactor);
		}
		//cout << "Star " << i << ": " << jfactor << endl;
	}
	//die();
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_No_MW_V_Sysparam3(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = binary fraction
	// a[3] = lambda1 (sysparam parameter)
	// a[4] = lambda2 (sysparam parameter)
	// a[5] = lambda3 (sysparam parameter)

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = a[3];
	sysparam2 = a[4];
	sysparam3 = a[5];

	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	dvector verr_sysparam;
	double v_avg, sigv;

	interpolate_sysparams(3);

//double wtf;
	for (i=0; i < nstars; i++)
	{
		star_i = i;

		// if the assumed dispersion is small compared to the vcm stepsizes, then treat the R-function like a delta function and take vcm value closest to a[0]
		//if (star[i].zeroval==0) { jfactor = 0; }
		//else {
			// trapezoid rule to find jfactor
			sum = 0.5*(jfactor_integrand_interpolate_sysparam(0)+jfactor_integrand_interpolate_sysparam(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate_sysparam(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);
		//}
		//if (dispersion < ((star[i].rfactor_vcm_max-star[i].rfactor_vcm_min) / (star[i].rfactor_vcm_nn-1))) cout << "sig=" << dispersion << " mu=" << systemic_velocity << " JFAC: " << jfactor << " " << jfactor_test << " vcm=" << star[i].vcm_vals[i_closest] << endl;

		vlos_sysparam.input(star[i].n_epochs);
		verr_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			verr_sysparam[j] = star[i].v_errors[j];
			for (int kk=0; kk < 2; kk++) {
				if (star[i].sysparam_i[j][kk]==0) {
					if (sysparam_type[0]==0) vlos_sysparam[j] -= sysparam1;
					else if (sysparam_type[0]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam1*sysparam1);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==1) {
					if (sysparam_type[1]==0) vlos_sysparam[j] -= sysparam2;
					else if (sysparam_type[1]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam2*sysparam2);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==2) {
					if (sysparam_type[2]==0) vlos_sysparam[j] -= sysparam3;
					else if (sysparam_type[2]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam3*sysparam3);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk] != -1) die("WTF?!");
			}
		}
		nfactor = calculate_nfactor(vlos_sysparam,verr_sysparam,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));
		if (nfactor <= 1e-30) {
			loglike += -log(a[2]*jfactor); // counted as a binary
			//wtf = -log(a[2]*jfactor); // counted as a binary
		} else if (jfactor < 0) {
			if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor with nfactor not zero...skipped star: " << star[i].id << " sigma: " << a[1] << " jfactor: " << jfactor << endl;
			jfactor = 1e30; // this assures that the star is recorded as a binary (like_nb becomes negligible compared to this jfactor)
			//wtf =0;
		}
		else {
			loglike += -log((1-a[2])*like_nb + a[2]*jfactor);
			//wtf = -log((1-a[2])*like_nb + a[2]*jfactor);
			//cout << "star " << i << ": " << wtf << " " << jfactor << " " << nfactor << endl;
		}
	}
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

double Galaxy::LogLike_Binaries_No_MW_V_Fixed_Sysparam3(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = binary fraction
	// a[3] = lambda1 (sysparam parameter)
	// a[4] = lambda2 (sysparam parameter)
	// a[5] = lambda3 (sysparam parameter)

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = a[3];
	sysparam2 = a[4];
	//sysparam3 = a[5];

	//systemic_velocity = 65; // necessary for calculating J-factors
	//dispersion = 3.55; // necessary for calculating J-factors
	//sysparam1 = -1.7;
	//sysparam2 = -0.32;

	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	dvector verr_sysparam;
	double v_avg, sigv;

	interpolate_sysparams(3);

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		// if the assumed dispersion is small compared to the vcm stepsizes, then treat the R-function like a delta function and take vcm value closest to a[0]
		//if (star[i].zeroval==0) { jfactor = 0; }
		//else {
			// trapezoid rule to find jfactor
			sum = 0.5*(jfactor_integrand_interpolate_sysparam(0)+jfactor_integrand_interpolate_sysparam(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate_sysparam(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);
		//}
		//if (dispersion < ((star[i].rfactor_vcm_max-star[i].rfactor_vcm_min) / (star[i].rfactor_vcm_nn-1))) cout << "sig=" << dispersion << " mu=" << systemic_velocity << " JFAC: " << jfactor << " " << jfactor_test << " vcm=" << star[i].vcm_vals[i_closest] << endl;

		vlos_sysparam.input(star[i].n_epochs);
		verr_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			verr_sysparam[j] = star[i].v_errors[j];
			for (int kk=0; kk < 2; kk++) {
				if (star[i].sysparam_i[j][kk]==0) {
					if (sysparam_type[0]==0) vlos_sysparam[j] -= sysparam1;
					else if (sysparam_type[0]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam1*sysparam1);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==1) {
					if (sysparam_type[1]==0) vlos_sysparam[j] -= sysparam2;
					else if (sysparam_type[1]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam2*sysparam2);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk]==2) {
					if (sysparam_type[2]==0) vlos_sysparam[j] -= sysparam3;
					else if (sysparam_type[2]==1) verr_sysparam[j] = sqrt(SQR(verr_sysparam[j]) + sysparam3*sysparam3);
					else die("sysparam type not recognized");
				}
				else if (star[i].sysparam_i[j][kk] != -1) die("WTF?!");
			}
		}
		nfactor = calculate_nfactor(vlos_sysparam,verr_sysparam,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));
		if (nfactor <= 1e-30) {
			loglike += -log(a[2]*jfactor); // counted as a binary
		} else if (jfactor < 0) {
			if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor with nfactor not zero...skipped star: " << star[i].id << " sigma: " << a[1] << " jfactor: " << jfactor << endl;
			jfactor = 1e30; // this assures that the star is recorded as a binary (like_nb becomes negligible compared to this jfactor)
		}
		else {
			loglike += -log((1-a[2])*like_nb + a[2]*jfactor);
		}
		//cout << "Star " << i << ": " << jfactor << endl;
	}
	//die();
	if (log_dispersion_prior) loglike += log(dispersion);
	return loglike;
}

/*
double Galaxy::LogLike_Binaries_No_MW_V_Fixed_Sysparam2(double *a)
{
	// a[0] = systemic velocity
	// a[1] = velocity dispersion
	// a[2] = binary fraction

	systemic_velocity = a[0]; // necessary for calculating J-factors
	dispersion = a[1]; // necessary for calculating J-factors
	sysparam1 = 1.0;
	sysparam2 = 0.5;

	double loglike=0, like_nb;
	double jfactor, nfactor;

	double sum;
	int i,j;
	dvector vlos_sysparam;
	double v_avg, sigv;

	interpolate_sysparams();

	for (i=0; i < nstars; i++)
	{
		star_i = i;

		// if the assumed dispersion is small compared to the vcm stepsizes, then treat the R-function like a delta function and take vcm value closest to a[0]
		if (star[i].zeroval==0) { jfactor = 0; }
		else {
			// trapezoid rule to find jfactor
			sum = 0.5*(jfactor_integrand_interpolate_sysparam(0)+jfactor_integrand_interpolate_sysparam(star[i].rfactor_vcm_nn-1));
			for (j=1; j < star[i].rfactor_vcm_nn-1; j++) sum += jfactor_integrand_interpolate_sysparam(j);
			jfactor = (star[i].rfactor_vcm_max-star[i].rfactor_vcm_min)*sum/(star[i].rfactor_vcm_nn-1);
		}
		//if (dispersion < ((star[i].rfactor_vcm_max-star[i].rfactor_vcm_min) / (star[i].rfactor_vcm_nn-1))) cout << "sig=" << dispersion << " mu=" << systemic_velocity << " JFAC: " << jfactor << " " << jfactor_test << " vcm=" << star[i].vcm_vals[i_closest] << endl;

		vlos_sysparam.input(star[i].n_epochs);
		for (j=0; j < star[i].n_epochs; j++) {
			vlos_sysparam[j] = star[i].vlos_data[j];
			if (star[i].sysparam_i[j]==0) vlos_sysparam[j] -= sysparam1;
			else if (star[i].sysparam_i[j]==1) vlos_sysparam[j] -= sysparam2;
			else if (star[i].sysparam_i[j] != -1) die("WTF?!");
		}
		nfactor = calculate_nfactor(vlos_sysparam,star[i].v_errors,v_avg,sigv);
		like_nb = nfactor * exp(-0.5*SQR(v_avg-a[0])/(a[1]*a[1]+SQR(sigv)))/SQRT_2PI/sqrt(a[1]*a[1]+SQR(sigv));
		if (nfactor <= 1e-30) {
			loglike += -log(a[2]*jfactor); // counted as a binary
		} else if (jfactor < 0) {
			if ((mpi_id==0) and (output_to_logfile)) logfile << "Negative j-factor with nfactor not zero...skipped star: " << star[i].id << " sigma: " << a[1] << " jfactor: " << jfactor << endl;
			jfactor = 1e30; // this assures that the star is recorded as a binary (like_nb becomes negligible compared to this jfactor)
		}
		else {
			//double loglike_term = -log((1-a[2])*like_nb + a[2]*jfactor);
			//cout << "i=" << i << " nfactor=" << nfactor << " like_nb=" << like_nb << " jfactor=" << jfactor << " loglike_i=" << loglike_term << endl;
			loglike += -log((1-a[2])*like_nb + a[2]*jfactor);
		}
	}
	return loglike;
}
*/

double Galaxy::logdv_likelihood_integrand(const int m)
{
	int i,j,k,l;
	double expfactor_plus, expfactor_minus;
	double dv_ij, sigsqk;

	expfactor_plus = 0;
	for (i=0; i < star[star_i].n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < star[star_i].n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(star[star_i].v_error_model[k]);
			}
			dv_ij = 0;
			for (l=j; l < i; l++) {
				if (l==k_longtime) dv_ij += star[star_i].dv_data[l] + dv_vals[m];
				else dv_ij += star[star_i].dv_data[l];
			}
			expfactor_plus += 0.5 * SQR(dv_ij) / (SQR(star[star_i].v_error_model[i]) + SQR(star[star_i].v_error_model[j]) + SQR(star[star_i].v_error_model[i]*star[star_i].v_error_model[j])*sigsqk);
		}
	}

	expfactor_minus = 0;
	for (i=0; i < star[star_i].n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < star[star_i].n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(star[star_i].v_error_model[k]);
			}
			dv_ij = 0;
			for (l=j; l < i; l++) {
				if (l==k_longtime) dv_ij += star[star_i].dv_data[l] - dv_vals[m];
				else dv_ij += star[star_i].dv_data[l];
			}
			expfactor_minus += 0.5 * SQR(dv_ij) / (SQR(star[star_i].v_error_model[i]) + SQR(star[star_i].v_error_model[j]) + SQR(star[star_i].v_error_model[i]*star[star_i].v_error_model[j])*sigsqk);
		}
	}

	return 0.5*logdv_likelihoods[year_number][star[star_i].mag_number][m]*nfactor_normalizing_factor*(exp(-expfactor_plus) + exp(-expfactor_minus));
}

double Galaxy::logdv_likelihood_integrand_no_errmodel(const int m)
{
	int i,j,k,l;
	double expfactor_plus, expfactor_minus;
	double dv_ij, sigsqk;

	expfactor_plus = 0;
	for (i=0; i < star[star_i].n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < star[star_i].n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(star[star_i].v_errors[k]);
			}
			dv_ij = 0;
			for (l=j; l < i; l++) {
				if (l==k_longtime) dv_ij += star[star_i].dv_data[l] + dv_vals[m];
				else dv_ij += star[star_i].dv_data[l];
			}
			expfactor_plus += 0.5 * SQR(dv_ij) / (SQR(star[star_i].v_errors[i]) + SQR(star[star_i].v_errors[j]) + SQR(star[star_i].v_errors[i]*star[star_i].v_errors[j])*sigsqk);
		}
	}

	expfactor_minus = 0;
	for (i=0; i < star[star_i].n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < star[star_i].n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(star[star_i].v_errors[k]);
			}
			dv_ij = 0;
			for (l=j; l < i; l++) {
				if (l==k_longtime) dv_ij += star[star_i].dv_data[l] - dv_vals[m];
				else dv_ij += star[star_i].dv_data[l];
			}
			expfactor_minus += 0.5 * SQR(dv_ij) / (SQR(star[star_i].v_errors[i]) + SQR(star[star_i].v_errors[j]) + SQR(star[star_i].v_errors[i]*star[star_i].v_errors[j])*sigsqk);
		}
	}

	return 0.5*logdv_likelihoods[year_number][star[star_i].mag_number][m]*nfactor_normalizing_factor*(exp(-expfactor_plus) + exp(-expfactor_minus));
}

double Galaxy::test_logdv_likelihood_integrand(const int m, const dvector &dv, const dvector &sig_errors)
{
	int i,j,k,l;
	double expfactor_plus, expfactor_minus;
	double dv_ij, sigsqk;
	int n_epochs = sig_errors.size();

	expfactor_plus = 0;
	for (i=0; i < n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(sig_errors[k]);
			}
			dv_ij = 0;
			for (l=j; l < i; l++) {
				if (l==k_longtime) dv_ij += dv[l] + dv_vals[m];
				else dv_ij += dv[l];
			}
			expfactor_plus += 0.5 * SQR(dv_ij) / (SQR(sig_errors[i]) + SQR(sig_errors[j]) + SQR(sig_errors[i]*sig_errors[j])*sigsqk);
		}
	}

	expfactor_minus = 0;
	for (i=0; i < n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(sig_errors[k]);
			}
			dv_ij = 0;
			for (l=j; l < i; l++) {
				if (l==k_longtime) dv_ij += dv[l] - dv_vals[m];
				else dv_ij += dv[l];
			}
			expfactor_minus += 0.5 * SQR(dv_ij) / (SQR(sig_errors[i]) + SQR(sig_errors[j]) + SQR(sig_errors[i]*sig_errors[j])*sigsqk);
		}
	}

	return 0.5*logdv_likelihoods[year_number][mag_number][m]*nfactor_normalizing_factor*(exp(-expfactor_plus) + exp(-expfactor_minus));
}

double Galaxy::LogLike_W_Only_Fixed_Metallicity_Params(double *a)
{
	// a[0] = binary fraction
	// a[1] = member fraction

	double loglike=0;
	double like_gal, like_mw;

	for (int i=0; i < nstars; i++)
	{
		if (star[i].no_metallicities==true) { like_gal = 1; like_mw = 1; }
		else {
			like_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			like_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { like_gal = 1; like_mw = 1; }

		loglike += -log((1-a[0])*like_mw + a[0]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_W_Only(double *a)
{
	// a[0] = member fraction
	// a[1] = mean metal equivalent width
	// a[2] = equivalent width dispersion
	// a[3] = MW mean metal equivalent width
	// a[4] = MW equivalent width dispersion

	double loglike=0;
	double like_gal, like_mw;

	for (int i=0; i < nstars; i++)
	{
		if (star[i].no_metallicities==true) { like_gal = 1; like_mw = 1; }
		else {
			like_gal = exp(-0.5*SQR(star[i].w_avg-a[1])/(a[2]*a[2]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[2]*a[2]+SQR(star[i].sigw));
			like_mw = exp(-0.5*SQR(star[i].w_avg-a[3])/(a[4]*a[4]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[4]*a[4]+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { like_gal = 1; like_mw = 1; }

		loglike += -log((1-a[0])*like_mw + a[0]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_DV_Fixed_Metallicity_Params_No_Period_Params(double *a)
{
	// a[0] = binary fraction
	// a[1] = member fraction

	double loglike=0;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;
	//mu_logP = 2.23;
	//sig_logP = 2.3;
	//interpolate_period_params();
	//

	for (int i=0; i < nstars; i++)
	{
		//star_i=i;

		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		like_gal = wlike_gal*((1-a[0]) + a[0]*star[i].jfactor_dv);
		//like_gal = wlike_gal*((1-a[0]) + a[0]*jfactor_interpolate());
		like_mw = wlike_mw;
		if (fix_member_fraction)
			loglike += -log((1-fixed_member_fraction)*like_mw + fixed_member_fraction*like_gal);
		else
			loglike += -log((1-a[1])*like_mw + a[1]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_DVW_No_Period_Params(double *a)
{
	// a[0] = member fraction
	// a[1] = mean metal equivalent width
	// a[2] = equivalent width dispersion
	// a[3] = MW mean metal equivalent width
	// a[4] = MW equivalent width dispersion
	// a[5] = binary fraction

	double loglike=0;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;
	//mu_logP = 2.23;
	//sig_logP = 2.3;
	//interpolate_period_params();

	for (int i=0; i < nstars; i++)
	{
		//star_i=i;

		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			//wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			//wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-a[1])/(a[2]*a[2]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[2]*a[2]+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-a[3])/(a[4]*a[4]+SQR(star[i].sigw)))/SQRT_2PI/sqrt(a[4]*a[4]+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		if (star[i].n_epochs==1) like_gal = wlike_gal;
		else like_gal = wlike_gal*((1-a[5]) + a[5]*star[i].jfactor_dv);
		//like_gal = wlike_gal*((1-a[5]) + a[5]*jfactor_interpolate());
		like_mw = wlike_mw;
		loglike += -log((1-a[0])*like_mw + a[0]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_DVW_logdvlike(double *a)
{
	// a[0] = member fraction
	// a[1] = mean metal equivalent width
	// a[2] = equivalent width dispersion
	// a[3] = MW mean metal equivalent width
	// a[4] = MW equivalent width dispersion
	// a[5] = binary fraction

	int ii,i,j,k;

	double loglike=0;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;
	double ccf_error, baseline_error;
	double like, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double sum;

	if (start==true) {
		start = false;
		for (int ii=0; ii < nstars; ii++)
		{
			if (star[ii].n_epochs==1) { likenbvec[ii]=1; likebvec=1; continue; }
			star_i = ii;
			k_longtime = star[ii].k_longtime;
			mag_number = star[ii].mag_number;
			year_number = star[ii].year_number;

			expfactor = 0;
			for (i=0; i < star[ii].n_epochs; i++) {
				for (j=0; j < i; j++) {
					sigsqk=0;
					for (k=0; k < star[ii].n_epochs; k++) {
						if ((k==i) or (k==j)) continue;
						sigsqk += 1.0/SQR(star[ii].v_errors[k]);
					}
					expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_errors[i]) + SQR(star[ii].v_errors[j]) + SQR(star[ii].v_errors[i]*star[ii].v_errors[j])*sigsqk);
				}
			}

			sigmsq_inv = 0;
			for (j=0; j < star[ii].n_epochs; j++)
				sigmsq_inv += 1.0/SQR(star[ii].v_errors[j]);
			sigmsq = 1.0/sigmsq_inv;

			nfactor_normalizing_factor = sqrt(M_2PI*sigmsq);
			for (i=0; i < star[ii].n_epochs; i++) nfactor_normalizing_factor /= SQRT_2PI*star[ii].v_errors[i];

			likenbvec[ii] = nfactor_normalizing_factor*exp(-expfactor);

			// trapezoid rule to find binary likelihood
			sum = 0.5*(logdv_likelihood_integrand_no_errmodel(0)+logdv_likelihood_integrand_no_errmodel(n_logdv_vals-1));
			for (j=1; j < n_logdv_vals-1; j++) sum += logdv_likelihood_integrand_no_errmodel(j);
			likebvec[ii] = (logdv_vals_max-logdv_vals_min)*sum/(n_logdv_vals-1);
	
			//cout << star[ii].id << " " << likebvec[ii] << " " << likenbvec[ii] << endl;
		}
	}

	for (int ii=0; ii < nstars; ii++)
	{

		if (star[ii].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			//wlike_gal = exp(-0.5*SQR(star[ii].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[ii].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[ii].sigw));
			//wlike_mw = exp(-0.5*SQR(star[ii].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[ii].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[ii].sigw));
			wlike_gal = exp(-0.5*SQR(star[ii].w_avg-a[1])/(a[2]*a[2]+SQR(star[ii].sigw)))/SQRT_2PI/sqrt(a[2]*a[2]+SQR(star[ii].sigw));
			wlike_mw = exp(-0.5*SQR(star[ii].w_avg-a[3])/(a[4]*a[4]+SQR(star[ii].sigw)))/SQRT_2PI/sqrt(a[4]*a[4]+SQR(star[ii].sigw));
		}
		if (star[ii].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		like_gal = wlike_gal*((1-a[5])*likenbvec[ii] + a[5]*likebvec[ii]);
		like_mw = wlike_mw*likenbvec[ii];
		loglike += -log((1-a[0])*like_mw + a[0]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_DV_Fixed_Metallicity_Params_logdvlike(double *a)
{
	// a[0] = binary fraction
	// a[1] = member fraction

	int ii,i,j,k;

	double loglike=0;
	double wlike_gal, wlike_mw;
	double like_gal, like_mw;
	double ccf_error, baseline_error;
	double like, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double sum;

	if (start==true) {
		start = false;
		for (int ii=0; ii < nstars; ii++)
		{
			if (star[ii].n_epochs==1) { likenbvec[ii]=1; likebvec=1; continue; }
			star_i = ii;
			k_longtime = star[ii].k_longtime;
			mag_number = star[ii].mag_number;
			year_number = star[ii].year_number;

			expfactor = 0;
			for (i=0; i < star[ii].n_epochs; i++) {
				for (j=0; j < i; j++) {
					sigsqk=0;
					for (k=0; k < star[ii].n_epochs; k++) {
						if ((k==i) or (k==j)) continue;
						sigsqk += 1.0/SQR(star[ii].v_errors[k]);
					}
					expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_errors[i]) + SQR(star[ii].v_errors[j]) + SQR(star[ii].v_errors[i]*star[ii].v_errors[j])*sigsqk);
				}
			}

			sigmsq_inv = 0;
			for (j=0; j < star[ii].n_epochs; j++)
				sigmsq_inv += 1.0/SQR(star[ii].v_errors[j]);
			sigmsq = 1.0/sigmsq_inv;

			nfactor_normalizing_factor = sqrt(M_2PI*sigmsq);
			for (i=0; i < star[ii].n_epochs; i++) nfactor_normalizing_factor /= SQRT_2PI*star[ii].v_errors[i];

			likenbvec[ii] = nfactor_normalizing_factor*exp(-expfactor);

			// trapezoid rule to find binary likelihood
			sum = 0.5*(logdv_likelihood_integrand_no_errmodel(0)+logdv_likelihood_integrand_no_errmodel(n_logdv_vals-1));
			for (j=1; j < n_logdv_vals-1; j++) sum += logdv_likelihood_integrand_no_errmodel(j);
			likebvec[ii] = (logdv_vals_max-logdv_vals_min)*sum/(n_logdv_vals-1);
	
			//cout << star[ii].id << " " << likebvec[ii] << " " << likenbvec[ii] << endl;
		}
	}

	for (int ii=0; ii < nstars; ii++)
	{

		if (star[ii].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[ii].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		like_gal = wlike_gal*((1-a[0])*likenbvec[ii] + a[0]*likebvec[ii]);
		like_mw = wlike_mw*likenbvec[ii];
		if (fix_member_fraction)
			loglike += -log((1-fixed_member_fraction)*like_mw + fixed_member_fraction*like_gal);
		else
			loglike += -log((1-a[1])*like_mw + a[1]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_DV_Fixed_Metallicity_Params(double *a)
{
	// a[0] = member fraction
	// a[1] = binary fraction
	// a[2] = mu_logP
	// a[3] = sig_logP

	double loglike=0;
	mu_logP = a[2];
	sig_logP = a[3];

	interpolate_period_params(); // find mu_logP, sig_logP values in jtable surrouding the given point (mu_logP,sig_logP)

	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	for (int i=0; i < nstars; i++)
	{
		star_i = i;

		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		like_gal = wlike_gal*((1-a[1]) + a[1]*jfactor_interpolate());
		like_mw = wlike_mw;
		loglike += -log((1-a[0])*like_mw + a[0]*like_gal);
	}
	return loglike;
}

void Galaxy::Count_Stars(void)
{
	int nn=0;
	for (int i=0; i < nstars; i++)
	{
		if ((star[i].w_avg < metallicity_limit) and (star[i].w_avg > metallicity_lowlimit))
			nn++;
	}
	cout << "Number of stars in sample: " << nn << endl;
	return;
}

double Galaxy::LogLike_DV_No_MW_No_Period_Params(double *a)
{
	// a[0] = binary fraction

	double loglike=0;
	double nfactor;
	double v_avg, sigv;

	for (int i=0; i < nstars; i++)
	{
		//if ((star[i].w_avg < metallicity_limit) and (star[i].w_avg > metallicity_lowlimit))
		nfactor = calculate_nfactor(star[i].vlos_data,star[i].v_errors,v_avg,sigv);
		loglike += -log((1-a[0])*nfactor + a[0]*star[i].dvlike);
	}
	return loglike;
}

double Galaxy::LogLike_DV_No_MW(double *a)
{
	// a[0] = binary fraction
	// a[1] = mu_logP
	// a[2] = sig_logP

	double loglike=0;

	mu_logP = a[1];				// necessary for calculating J-factors
	sig_logP = a[2];				// necessary for calculating J-factors

	interpolate_period_params(); // find mu_logP, sig_logP values in jtable surrouding the given point (mu_logP,sig_logP)

	for (int i=0; i < nstars; i++)
	{
		star_i = i;
		loglike += -log((1-a[0]) + a[0]*jfactor_interpolate());
	}
	return loglike;
}

double Galaxy::LogLike_DV_Fixed_Binary_Fraction(double *a)
{
	// a[0] = mu_logP
	// a[1] = sig_logP

	double loglike=0;

	mu_logP = a[0];				// necessary for calculating J-factors
	sig_logP = a[1];				// necessary for calculating J-factors

	interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)

	for (int i=0; i < nstars; i++)
	{
		star_i = i;
		//loglike += -log(jfactor_interpolate());
		loglike += -log((1-binfrac) + binfrac*jfactor_interpolate());
	}
	return loglike;
}

double Galaxy::LogLike_DV_Fixed_Binary_Fraction_Fixed_Metallicity_Params(double *a)
{
	// a[0] = member fraction
	// a[1] = mu_logP
	// a[2] = sig_logP

	double loglike=0;
	mu_logP = a[1];
	sig_logP = a[2];

	interpolate_period_params(); // find mu_logP, sig_logP values in jtable surrouding the given point (mu_logP,sig_logP)

	double wlike_gal, wlike_mw;
	double like_gal, like_mw;

	for (int i=0; i < nstars; i++)
	{
		star_i = i;

		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		like_gal = wlike_gal*((1-binfrac) + binfrac*jfactor_interpolate());
		like_mw = wlike_mw;
		loglike += -log((1-a[0])*like_mw + a[0]*like_gal);
	}
	return loglike;
}

double Galaxy::LogLike_DV_Error_Model(double *a)
{
	int ii,i,j,k;

	double loglike=0;
	double ccf_error, baseline_error;
	double nfactor, expfactor, sigsqk, sigmsq, sigmsq_inv;

	for (int ii=0; ii < nstars; ii++)
	{
		for (j=0; j < star[ii].n_epochs; j++)
		{
			baseline_error = (star[ii].channel_color[j]==0) ? 0.6 : 0.26;
			//baseline_error = (star[ii].channel_color[j]==0) ? a[4] : a[5];
			if (star[ii].channel_color[j]==0)
				ccf_error = a[0]*pow(1+star[ii].r_value[j],-a[1]); // two error parameters for red channel
			else
				ccf_error = a[2]*pow(1+star[ii].r_value[j],-a[3]); // two error parameters for blue channel
			star[ii].v_error_model[j] = sqrt(SQR(ccf_error) + SQR(baseline_error));
		}
		expfactor = 0;
		for (i=0; i < star[ii].n_epochs; i++) {
			for (j=0; j < i; j++) {
				sigsqk=0;
				for (k=0; k < star[ii].n_epochs; k++) {
					if ((k==i) or (k==j)) continue;
					sigsqk += 1.0/SQR(star[ii].v_error_model[k]);
				}
				expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_error_model[i]) + SQR(star[ii].v_error_model[j]) + SQR(star[ii].v_error_model[i]*star[ii].v_error_model[j])*sigsqk);
			}
		}

		sigmsq_inv = 0;
		for (j=0; j < star[ii].n_epochs; j++)
			sigmsq_inv += 1.0/SQR(star[ii].v_error_model[j]);
		sigmsq = 1.0/sigmsq_inv;

		nfactor = sqrt(M_2PI*sigmsq)*exp(-expfactor);
		for (i=0; i < star[ii].n_epochs; i++) nfactor /= SQRT_2PI*star[ii].v_error_model[i];

		loglike += -log(nfactor);

	}
	return loglike;
}

double Galaxy::LogLike_DV_logdvlike(double *a)
{
	int ii,i,j,k;

	double loglike=0;
	double ccf_error, baseline_error;
	double like, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double sum;

	if (start==true) {
		start = false;
		for (int ii=0; ii < nstars; ii++)
		{
			star_i = ii;
			k_longtime = star[ii].k_longtime;
			mag_number = star[ii].mag_number;
			year_number = star[ii].year_number;

			expfactor = 0;
			for (i=0; i < star[ii].n_epochs; i++) {
				for (j=0; j < i; j++) {
					sigsqk=0;
					for (k=0; k < star[ii].n_epochs; k++) {
						if ((k==i) or (k==j)) continue;
						sigsqk += 1.0/SQR(star[ii].v_errors[k]);
					}
					expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_errors[i]) + SQR(star[ii].v_errors[j]) + SQR(star[ii].v_errors[i]*star[ii].v_errors[j])*sigsqk);
				}
			}

			sigmsq_inv = 0;
			for (j=0; j < star[ii].n_epochs; j++)
				sigmsq_inv += 1.0/SQR(star[ii].v_errors[j]);
			sigmsq = 1.0/sigmsq_inv;

			nfactor_normalizing_factor = sqrt(M_2PI*sigmsq);
			for (i=0; i < star[ii].n_epochs; i++) nfactor_normalizing_factor /= SQRT_2PI*star[ii].v_errors[i];

			likenbvec[ii] = nfactor_normalizing_factor*exp(-expfactor);

			// trapezoid rule to find binary likelihood
			sum = 0.5*(logdv_likelihood_integrand_no_errmodel(0)+logdv_likelihood_integrand_no_errmodel(n_logdv_vals-1));
			for (j=1; j < n_logdv_vals-1; j++) sum += logdv_likelihood_integrand_no_errmodel(j);
			likebvec[ii] = (logdv_vals_max-logdv_vals_min)*sum/(n_logdv_vals-1);
			//cout << star[ii].id << " " << likebvec[ii] << " " << likenbvec[ii] << endl;
		}
	}

	for (int ii=0; ii < nstars; ii++)
	{
		like = (1-a[0])*likenbvec[ii] + a[0]*likebvec[ii];

		loglike += -log(like);

	}
	return loglike;
}

double Galaxy::LogLike_DV_Error_Model_with_Binaries(double *a)
{
	int ii,i,j,k;

	double loglike=0;
	double ccf_error, baseline_error;
	double like, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double sum;

	double like_b, like_nb;

	for (int ii=0; ii < nstars; ii++)
	{
		star_i = ii;
		k_longtime = star[ii].k_longtime;
		mag_number = star[ii].mag_number;
		year_number = star[ii].year_number;
		for (j=0; j < star[ii].n_epochs; j++)
		{
			baseline_error = (star[ii].channel_color[j]==0) ? 0.6 : 0.26;
			if (star[ii].channel_color[j]==0)
				ccf_error = a[0]*pow(1+star[ii].r_value[j],-a[1]); // two error parameters for red channel
			else
				ccf_error = a[2]*pow(1+star[ii].r_value[j],-a[3]); // two error parameters for blue channel
			star[ii].v_error_model[j] = sqrt(SQR(ccf_error) + SQR(baseline_error));
		}

		expfactor = 0;
		for (i=0; i < star[ii].n_epochs; i++) {
			for (j=0; j < i; j++) {
				sigsqk=0;
				for (k=0; k < star[ii].n_epochs; k++) {
					if ((k==i) or (k==j)) continue;
					sigsqk += 1.0/SQR(star[ii].v_error_model[k]);
				}
				expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_error_model[i]) + SQR(star[ii].v_error_model[j]) + SQR(star[ii].v_error_model[i]*star[ii].v_error_model[j])*sigsqk);
			}
		}

		sigmsq_inv = 0;
		for (j=0; j < star[ii].n_epochs; j++)
			sigmsq_inv += 1.0/SQR(star[ii].v_error_model[j]);
		sigmsq = 1.0/sigmsq_inv;

		nfactor_normalizing_factor = sqrt(M_2PI*sigmsq);
		for (i=0; i < star[ii].n_epochs; i++) nfactor_normalizing_factor /= SQRT_2PI*star[ii].v_error_model[i];

		like_nb = nfactor_normalizing_factor*exp(-expfactor);

		if ((star[ii].long_time_interval==false) or (star[ii].clipped_outliers==true)) like = like_nb;
		else {
			// trapezoid rule to find binary likelihood
			sum = 0.5*(logdv_likelihood_integrand(0)+logdv_likelihood_integrand(n_logdv_vals-1));
			for (j=1; j < n_logdv_vals-1; j++) sum += logdv_likelihood_integrand(j);
			like_b = (logdv_vals_max-logdv_vals_min)*sum/(n_logdv_vals-1);

			like = (1-a[4])*like_nb + a[4]*like_b;
		}

		loglike += -log(like);

	}
	return loglike;
}

double Galaxy::LogLike_DV_Error_Model_with_Binaries_Fixed_W_Params(double *a)
{
	int ii,i,j,k;

	double loglike=0;
	double ccf_error, baseline_error;
	double like, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double wlike_gal, wlike_mw;
	double sum;

	double like_b, like_nb, like_gal, like_mw;

	for (int ii=0; ii < nstars; ii++)
	{
		star_i = ii;
		k_longtime = star[ii].k_longtime;
		mag_number = star[ii].mag_number;
		year_number = star[ii].year_number;

		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		for (j=0; j < star[ii].n_epochs; j++)
		{
			baseline_error = (star[ii].channel_color[j]==0) ? 0.6 : 0.26;
			if (star[ii].channel_color[j]==0)
				ccf_error = a[0]*pow(1+star[ii].r_value[j],-a[1]); // two error parameters for red channel
			else
				ccf_error = a[2]*pow(1+star[ii].r_value[j],-a[3]); // two error parameters for blue channel
			star[ii].v_error_model[j] = sqrt(SQR(ccf_error) + SQR(baseline_error));
		}

		expfactor = 0;
		for (i=0; i < star[ii].n_epochs; i++) {
			for (j=0; j < i; j++) {
				sigsqk=0;
				for (k=0; k < star[ii].n_epochs; k++) {
					if ((k==i) or (k==j)) continue;
					sigsqk += 1.0/SQR(star[ii].v_error_model[k]);
				}
				expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_error_model[i]) + SQR(star[ii].v_error_model[j]) + SQR(star[ii].v_error_model[i]*star[ii].v_error_model[j])*sigsqk);
			}
		}

		sigmsq_inv = 0;
		for (j=0; j < star[ii].n_epochs; j++)
			sigmsq_inv += 1.0/SQR(star[ii].v_error_model[j]);
		sigmsq = 1.0/sigmsq_inv;

		nfactor_normalizing_factor = sqrt(M_2PI*sigmsq);
		for (i=0; i < star[ii].n_epochs; i++) nfactor_normalizing_factor /= SQRT_2PI*star[ii].v_error_model[i];

		like_nb = nfactor_normalizing_factor*exp(-expfactor);

		if ((star[ii].long_time_interval==false) or (star[ii].clipped_outliers==true)) like_gal = wlike_gal*like_nb;
		else {
			// trapezoid rule to find binary likelihood
			sum = 0.5*(logdv_likelihood_integrand(0)+logdv_likelihood_integrand(n_logdv_vals-1));
			for (j=1; j < n_logdv_vals-1; j++) sum += logdv_likelihood_integrand(j);
			like_b = (logdv_vals_max-logdv_vals_min)*sum/(n_logdv_vals-1);

			like_gal = wlike_gal*((1-a[5])*like_nb + a[5]*like_b);
		}
		like_mw = wlike_mw*like_nb;

		loglike += -log((1-a[4])*like_mw + a[4]*like_gal);

	}
	return loglike;
}

double Galaxy::LogLike_DV_Error_Model_with_Binaries_Fixed_W_Params_One_Channel(double *a)
{
	int ii,i,j,k;

	double loglike=0;
	double ccf_error, baseline_error;
	double like, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double wlike_gal, wlike_mw;
	double sum;

	double like_b, like_nb, like_gal, like_mw;

	for (int ii=0; ii < nstars; ii++)
	{
		star_i = ii;
		k_longtime = star[ii].k_longtime;
		mag_number = star[ii].mag_number;
		year_number = star[ii].year_number;

		if (star[i].no_metallicities==true) { wlike_gal = 1; wlike_mw = 1; }
		else {
			wlike_gal = exp(-0.5*SQR(star[i].w_avg-wavg_gal)/(SQR(wsig_gal)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_gal)+SQR(star[i].sigw));
			wlike_mw = exp(-0.5*SQR(star[i].w_avg-wavg_mw)/(SQR(wsig_mw)+SQR(star[i].sigw)))/SQRT_2PI/sqrt(SQR(wsig_mw)+SQR(star[i].sigw));
		}
		if (star[i].w_avg==1e30) { wlike_gal = 1; wlike_mw = 1; }

		for (j=0; j < star[ii].n_epochs; j++)
		{
			baseline_error = (star[ii].channel_color[j]==0) ? 0.6 : 0.26;
			ccf_error = a[0]*pow(1+star[ii].r_value[j],-a[1]); 
			star[ii].v_error_model[j] = sqrt(SQR(ccf_error) + SQR(baseline_error));
		}

		expfactor = 0;
		for (i=0; i < star[ii].n_epochs; i++) {
			for (j=0; j < i; j++) {
				sigsqk=0;
				for (k=0; k < star[ii].n_epochs; k++) {
					if ((k==i) or (k==j)) continue;
					sigsqk += 1.0/SQR(star[ii].v_error_model[k]);
				}
				expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_error_model[i]) + SQR(star[ii].v_error_model[j]) + SQR(star[ii].v_error_model[i]*star[ii].v_error_model[j])*sigsqk);
			}
		}

		sigmsq_inv = 0;
		for (j=0; j < star[ii].n_epochs; j++)
			sigmsq_inv += 1.0/SQR(star[ii].v_error_model[j]);
		sigmsq = 1.0/sigmsq_inv;

		nfactor_normalizing_factor = sqrt(M_2PI*sigmsq);
		for (i=0; i < star[ii].n_epochs; i++) nfactor_normalizing_factor /= SQRT_2PI*star[ii].v_error_model[i];

		like_nb = nfactor_normalizing_factor*exp(-expfactor);

		if ((star[ii].long_time_interval==false) or (star[ii].clipped_outliers==true)) like_gal = wlike_gal*like_nb;
		else {
			// trapezoid rule to find binary likelihood
			sum = 0.5*(logdv_likelihood_integrand(0)+logdv_likelihood_integrand(n_logdv_vals-1));
			for (j=1; j < n_logdv_vals-1; j++) sum += logdv_likelihood_integrand(j);
			like_b = (logdv_vals_max-logdv_vals_min)*sum/(n_logdv_vals-1);

			like_gal = wlike_gal*((1-a[3])*like_nb + a[3]*like_b);
		}
		like_mw = wlike_mw*like_nb;

		loglike += -log((1-a[2])*like_mw + a[2]*like_gal);

	}
	return loglike;
}

void Galaxy::test_2epoch_likelihood(void)
{
	int n_epochs = 2;
	double sig_error = 2;
	double sig2e = sig_error*sqrt(2);
	double like, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double sum;
	int i,j,k;

	k_longtime = 0;
	mag_number = 0;

	sigmsq_inv = 0;
	for (j=0; j < n_epochs; j++)
		sigmsq_inv += 1.0/SQR(sig_error);
	sigmsq = 1.0/sigmsq_inv;

	nfactor_normalizing_factor = sqrt(M_2PI*sigmsq);
	for (i=0; i < n_epochs; i++) nfactor_normalizing_factor /= SQRT_2PI*sig_error;

	int dv_nn = 500;
	double dv_min = -40, dv_max = 40;
	double dv, dv_step = (dv_max-dv_min) / (dv_nn-1);

	dvector dv_val(dv_nn), dv_like(dv_nn);
	dvector dv_vec(n_epochs-1);
	dvector sig_errors(n_epochs);
	sig_errors[0] = sig_error;
	sig_errors[1] = sig_error;
	for (i=0, dv = dv_min; i < dv_nn; i++, dv += dv_step)
	{
		dv_val[i] = dv;
		dv_vec[0] = dv;
		// trapezoid rule to find binary likelihood
		sum = 0.5*(test_logdv_likelihood_integrand(0,dv_vec,sig_errors)+test_logdv_likelihood_integrand(n_logdv_vals-1,dv_vec,sig_errors));
		for (j=1; j < n_logdv_vals-1; j++) sum += test_logdv_likelihood_integrand(j,dv_vec,sig_errors);
		dv_like[i] = (logdv_vals_max-logdv_vals_min)*sum/(n_logdv_vals-1);
		//die();
	}
	double like_check;
	for (i=0; i < dv_nn; i++)
	{
		//like_check = exp(-0.5*SQR(dv_val[i]/sig2e))/SQRT_2PI/sig2e;
		cout << dv_val[i] << " " << dv_like[i] << endl;
	}
}


double Galaxy::LogLike_DV_Error_Model_Extended(double *a)
{
	int ii,i,j,k;

	double loglike=0;
	double ccf_error, baseline_error;
	double nfactor, expfactor, sigsqk, sigmsq, sigmsq_inv;

	for (int ii=0; ii < nstars; ii++)
	{
		for (j=0; j < star[ii].n_epochs; j++)
		{
			baseline_error = (star[ii].channel_color[j]==0) ? 0.6 : 0.26;
			//baseline_error = (star[ii].channel_color[j]==0) ? a[4] : a[5];
			if (star[ii].channel_color[j]==0)
				ccf_error = a[0]*pow(a[4]+star[ii].r_value[j],-a[1]); // three error parameters for red channel
			else
				ccf_error = a[2]*pow(a[5]+star[ii].r_value[j],-a[3]); // three error parameters for blue channel
			//if (star[ii].channel_color[j]==0)
				//ccf_error = a[0]*pow(1+star[ii].r_value[j],-a[1]); // two error parameters for red channel
			//else
				//ccf_error = a[2]*pow(1+star[ii].r_value[j],-a[3]); // two error parameters for blue channel
			star[ii].v_error_model[j] = sqrt(SQR(ccf_error) + SQR(baseline_error));
		}
		expfactor = 0;
		for (i=0; i < star[ii].n_epochs; i++) {
			for (j=0; j < i; j++) {
				sigsqk=0;
				for (k=0; k < star[ii].n_epochs; k++) {
					if ((k==i) or (k==j)) continue;
					sigsqk += 1.0/SQR(star[ii].v_error_model[k]);
				}
				expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_error_model[i]) + SQR(star[ii].v_error_model[j]) + SQR(star[ii].v_error_model[i]*star[ii].v_error_model[j])*sigsqk);
			}
		}

		sigmsq_inv = 0;
		for (j=0; j < star[ii].n_epochs; j++)
			sigmsq_inv += 1.0/SQR(star[ii].v_error_model[j]);
		sigmsq = 1.0/sigmsq_inv;

		nfactor = sqrt(M_2PI*sigmsq)*exp(-expfactor);
		for (i=0; i < star[ii].n_epochs; i++) nfactor /= SQRT_2PI*star[ii].v_error_model[i];

		loglike += -log(nfactor);

	}
	return loglike;
}

double Galaxy::LogLike_DV_Error_Model_Metallicity(double *a)
{
	int ii,i,j,k;

	double loglike=0;
	double ccf_error, baseline_error;
	double nfactor, expfactor, sigsqk, sigmsq, sigmsq_inv;
	double alpha_red, x_red, alpha_blue, x_blue;
	double gamma_red, gamma_blue;

	for (int ii=0; ii < nstars; ii++)
	{
		for (j=0; j < star[ii].n_epochs; j++)
		{
			//alpha_red = a[0]*(1+a[1]*star[ii].w_avg);
			//x_red = a[2]*(1+a[3]*star[ii].w_avg);
			//alpha_blue = a[4]*(1+a[5]*star[ii].w_avg);
			//x_blue = a[6]*(1+a[7]*star[ii].w_avg);

			//alpha_red = a[0]*(1+a[1]*star[ii].w_avg+a[2]*SQR(star[ii].w_avg));
			//x_red = a[3];
			//alpha_blue = a[4]*(1+a[5]*star[ii].w_avg+a[6]*SQR(star[ii].w_avg));
			//x_blue = a[7];

			alpha_red = a[0]*(1+a[1]*star[ii].w_avg+a[2]*SQR(star[ii].w_avg));
			gamma_red = 1;
			x_red = a[3] + a[4]*star[ii].w_avg;
			alpha_blue = a[5]*(1+a[6]*star[ii].w_avg+a[7]*SQR(star[ii].w_avg));
			gamma_blue = 1;
			x_blue = a[8] + a[9]*star[ii].w_avg;

			baseline_error = (star[ii].channel_color[j]==0) ? 0.6 : 0.26;
			if (star[ii].channel_color[j]==0)
				ccf_error = alpha_red*pow(gamma_red+star[ii].r_value[j],-x_red); // two error parameters for red channel
			else
				ccf_error = alpha_blue*pow(gamma_blue+star[ii].r_value[j],-x_blue); // two error parameters for blue channel
			//if (star[ii].channel_color[j]==0)
				//ccf_error = alpha_red*pow(1+star[ii].r_value[j],-x_red); // two error parameters for red channel
			//else
				//ccf_error = alpha_blue*pow(1+star[ii].r_value[j],-x_blue); // two error parameters for blue channel
			star[ii].v_error_model[j] = sqrt(SQR(ccf_error) + SQR(baseline_error));
		}
		expfactor = 0;
		for (i=0; i < star[ii].n_epochs; i++) {
			for (j=0; j < i; j++) {
				sigsqk=0;
				for (k=0; k < star[ii].n_epochs; k++) {
					if ((k==i) or (k==j)) continue;
					sigsqk += 1.0/SQR(star[ii].v_error_model[k]);
				}
				expfactor += 0.5 * SQR(star[ii].vlos_data[i]-star[ii].vlos_data[j]) / (SQR(star[ii].v_error_model[i]) + SQR(star[ii].v_error_model[j]) + SQR(star[ii].v_error_model[i]*star[ii].v_error_model[j])*sigsqk);
			}
		}

		sigmsq_inv = 0;
		for (j=0; j < star[ii].n_epochs; j++)
			sigmsq_inv += 1.0/SQR(star[ii].v_error_model[j]);
		sigmsq = 1.0/sigmsq_inv;

		nfactor = sqrt(M_2PI*sigmsq)*exp(-expfactor);
		for (i=0; i < star[ii].n_epochs; i++) nfactor /= SQRT_2PI*star[ii].v_error_model[i];

		loglike += -log(nfactor);
	}
	return loglike;
}

void Galaxy::input_MW_Likelihood(char *datafile)
{
	bool use_flat_mwdist = false;
	include_MW_likelihood = true;
	if ((!use_gaussian_milky_way) and (strcmp(datafile,"NOMWFILE")==0)) { use_flat_mwdist = true; }
	if (use_flat_mwdist) {
		mwlike_vcm_min = 200;
		mwlike_vcm_max = 300;
		if (zero_systemic_velocity) { mwlike_vcm_min = -50; mwlike_vcm_max = 50; }
		//mwlike_vcm_min = -150;
		//mwlike_vcm_max = 350;
		mwlike_vcm_nn = 200; // won't get used
	} else if (use_gaussian_milky_way) {
		MW_mean_velocity = 30;
		MW_dispersion = 100;
		return;
	} else {
		MW_Likelihood.input(datafile);
		mwlike_vcm_min = MW_Likelihood.xmin();
		mwlike_vcm_max = MW_Likelihood.xmax();
		//mwlike_vcm_nn = MW_Likelihood.length();
		mwlike_vcm_nn = 2000;
	}
	//mwlike_vcm_nn = 2*MW_Likelihood.length(); // recommended for higher accuracy

	mwlike_vcm_vals.input(mwlike_vcm_nn);
	mwlike_vcm.input(mwlike_vcm_nn);

	double vcm, vcm_step;
	vcm_step = (mwlike_vcm_max-mwlike_vcm_min)/(mwlike_vcm_nn-1);
	int i;

	for (i=0, vcm=mwlike_vcm_min; i < mwlike_vcm_nn; i++, vcm += vcm_step)
	{
		mwlike_vcm_vals[i]=vcm;
		if (use_flat_mwdist) mwlike_vcm[i]=1.0/(mwlike_vcm_max-mwlike_vcm_min);
		else mwlike_vcm[i]=MW_Likelihood.splint(vcm);
	}
}

double Galaxy::mw_likelihood_integrand(const int j)
{
	double sigv = star[star_i].sigv;
	double ans = (mwlike_vcm[j]*exp(-0.5*SQR((mwlike_vcm_vals[j]-star[star_i].v_avg)/sigv))/SQRT_2PI/sigv);
	return ans;
}

double Galaxy::jfactor_integrand(const int j)
{
	return star[star_i].rfactor_vals[j]*exp(-0.5*SQR((star[star_i].vcm_vals[j]-systemic_velocity)/dispersion))/SQRT_2PI/dispersion;
}

double Galaxy::jfactor_integrand_interpolate(const int &j)
{
	return rfactor_interpolate(j)*exp(-0.5*SQR((star[star_i].vcm_vals[j]-systemic_velocity)/dispersion))/SQRT_2PI/dispersion;
}

double Galaxy::jfactor_integrand_interpolate_sysparam(const int &j)
{
	return rlfactor_interpolate(j)*exp(-0.5*SQR((star[star_i].vcm_vals[j]-systemic_velocity)/dispersion))/SQRT_2PI/dispersion;
	//return rlfactor_interpolate(j);
}

void Galaxy::interpolate_period_params(void)
{
	if (mu_logP == mu_logP_vals[0]) mu_logP_k=0;
	else if (mu_logP == mu_logP_vals[mu_logP_nn-1]) mu_logP_k=mu_logP_nn-2;
	else
	{
		int ku, kl, km;
		kl=-1;
		ku=mu_logP_nn;
		while (ku-kl > 1)
		{
			km=(ku+kl) >> 1;
			if (mu_logP >= mu_logP_vals[km]) kl=km;
			else ku=km;
		}
		mu_logP_k = kl;
	}

	if (sig_logP == sig_logP_vals[0]) sig_logP_l=0;
	else if (sig_logP == sig_logP_vals[sig_logP_nn-1]) sig_logP_l=sig_logP_nn-2;
	else
	{
		int lu, ll, lm;
		ll=-1;
		lu=sig_logP_nn;
		while (lu-ll > 1)
		{
			lm=(lu+ll) >> 1;
			if (sig_logP >= sig_logP_vals[lm]) ll=lm;
			else lu=lm;
		}
		sig_logP_l = ll;
	}

	if (mu_logP_k==-1) { // make sure it doesn't run off the edges of the rtable grid
		mu_logP_k = 0;
		tt = 0;
	} else if (mu_logP_k==mu_logP_nn-1) // make sure it doesn't run off the edges of the rtable grid
	{
		mu_logP_k = mu_logP_nn-2;
		tt = 1;
	} else {
		tt = (mu_logP - mu_logP_vals[mu_logP_k]) / (mu_logP_vals[mu_logP_k+1] - mu_logP_vals[mu_logP_k]);
	}

	if (sig_logP_l==-1) { // make sure it doesn't run off the edges of the rtable grid
		sig_logP_l = 0;
		uu = 0;
	} else if (sig_logP_l==sig_logP_nn-1) // make sure it doesn't run off the edges of the rtable grid
	{
		sig_logP_l = sig_logP_nn-2;
		uu = 1;
	} else {
		uu = (sig_logP - sig_logP_vals[sig_logP_l]) / (sig_logP_vals[sig_logP_l+1] - sig_logP_vals[sig_logP_l]);
	}
}

void Galaxy::interpolate_sysparams(int nsysparams)
{
	if (sysparam1 == sysparam_vals[0][0]) sysparam1_k=0;
	else if (sysparam1 == sysparam_vals[0][sysparam_nn[0]-1]) sysparam1_k=sysparam_nn[0]-2;
	else
	{
		int ku, kl, km;
		kl=-1;
		ku=sysparam_nn[0];
		while (ku-kl > 1)
		{
			km=(ku+kl) >> 1;
			if (sysparam1 >= sysparam_vals[0][km]) kl=km;
			else ku=km;
		}
		sysparam1_k = kl;
	}

	if (sysparam2 == sysparam_vals[1][0]) sysparam2_l=0;
	else if (sysparam2 == sysparam_vals[1][sysparam_nn[1]-1]) sysparam2_l=sysparam_nn[1]-2;
	else
	{
		int lu, ll, lm;
		ll=-1;
		lu=sysparam_nn[1];
		while (lu-ll > 1)
		{
			lm=(lu+ll) >> 1;
			if (sysparam2 >= sysparam_vals[1][lm]) ll=lm;
			else lu=lm;
		}
		sysparam2_l = ll;
	}

	if (sysparam1_k==-1) { // make sure it doesn't run off the edges of the rltable grid
		sysparam1_k = 0;
		tt = 0;
	} else if (sysparam1_k==sysparam_nn[0]-1) // make sure it doesn't run off the edges of the rtable grid
	{
		sysparam1_k = sysparam_nn[0]-2;
		tt = 1;
	} else {
		tt = (sysparam1 - sysparam_vals[0][sysparam1_k]) / (sysparam_vals[0][sysparam1_k+1] - sysparam_vals[0][sysparam1_k]);
	}

	if (sysparam2_l==-1) { // make sure it doesn't run off the edges of the rtable grid
		sysparam2_l = 0;
		uu = 0;
	} else if (sysparam2_l==sysparam_nn[1]-1) // make sure it doesn't run off the edges of the rtable grid
	{
		sysparam2_l = sysparam_nn[1]-2;
		uu = 1;
		if (sysparam2_l < 0) {
			sysparam2_l = 0;
			uu = 0;
		}
	} else {
		uu = (sysparam2 - sysparam_vals[1][sysparam2_l]) / (sysparam_vals[1][sysparam2_l+1] - sysparam_vals[1][sysparam2_l]);
	}

	if (nsysparams==3) {
		if (sysparam3 == sysparam_vals[2][0]) sysparam3_m=0;
		else if (sysparam3 == sysparam_vals[2][sysparam_nn[2]-1]) sysparam3_m=sysparam_nn[2]-2;
		else
		{
			int lu, ll, lm;
			ll=-1;
			lu=sysparam_nn[2];
			while (lu-ll > 1)
			{
				lm=(lu+ll) >> 1;
				if (sysparam3 >= sysparam_vals[2][lm]) ll=lm;
				else lu=lm;
			}
			sysparam3_m = ll;
		}
		if (sysparam3_m==-1) { // make sure it doesn't run off the edges of the rtable grid
			sysparam3_m = 0;
			ww = 0;
		} else if (sysparam3_m==sysparam_nn[2]-1) // make sure it doesn't run off the edges of the rtable grid
		{
			sysparam3_m = sysparam_nn[2]-2;
			ww = 1;
			if (sysparam3_m < 0) {
				sysparam3_m = 0;
				ww = 0;
			}
		} else {
			ww = (sysparam3 - sysparam_vals[2][sysparam3_m]) / (sysparam_vals[2][sysparam3_m+1] - sysparam_vals[2][sysparam3_m]);
		}
	}
	//cout << "SYS: " << sysparam1_k << " " << sysparam2_l << " " << sysparam3_m << endl;

	//if ((sysparam1_k==-1) or (sysparam2_l==-1)) die("wtf!!!!!!!!!!!!!!!");
	//if ((sysparam1_k==sysparam_nn[0]-1) or (sysparam2_l==sysparam_nn[1]-1)) die("wtf2!!!!!!!!!!!!!!!");
}

double Galaxy::calculate_nfactor(dvector& vlos_data, dvector& v_errors, double &v_avg, double &sigm_no_systematic)
{
	double systematic_err = 0; // maybe generalize this later to include a systematic error
	int n_epochs = vlos_data.size();
	dvector v_errors_no_systematic(n_epochs);
	int i,j,k;

	v_avg=0;
	double sigmsq_inv=0, expfactor, sigsqk, sigsqr_err_no_systematic, sigmsq_no_systematic;
	for (j=0; j < n_epochs; j++)
	{
		if (v_errors[j] < systematic_err) die("wtf? error smaller than systematic: %g",v_errors[j]); // this error should predominate
		sigsqr_err_no_systematic = SQR(v_errors[j]) - SQR(systematic_err);
		v_errors_no_systematic[j] = sqrt(sigsqr_err_no_systematic);
		v_avg += vlos_data[j]/sigsqr_err_no_systematic;
		sigmsq_inv += 1.0/sigsqr_err_no_systematic;
	}
	v_avg /= sigmsq_inv;
	sigmsq_no_systematic = 1.0/sigmsq_inv;
	sigm_no_systematic = sqrt(sigmsq_no_systematic);
	if (sigm_no_systematic==0) die("cannot have measurement errors equal to zero");

	// create normalizing n-factor
	expfactor = 0;
	for (i=0; i < n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(v_errors_no_systematic[k]);
			}
			expfactor += 0.5 * SQR(vlos_data[i]-vlos_data[j]) / (SQR(v_errors_no_systematic[i]) + SQR(v_errors_no_systematic[j]) + SQR(v_errors_no_systematic[i]*v_errors_no_systematic[j])*sigsqk);
		}
	}

	double nfactor_val = sqrt(M_2PI*sigmsq_no_systematic)*exp(-expfactor);
	for (i=0; i < n_epochs; i++) nfactor_val /= SQRT_2PI*v_errors_no_systematic[i];
	return nfactor_val;
}

void Galaxy::test_rfactor_interpolate(void)
{
	double vcm, rfactor;
	star_i=0;
	//mu_logP = 2.23;
	//sig_logP = 2.3;
	int i,j,k;
	double mu_logP_min=-1, mu_logP_max=5;
	double sig_logP_min=0.6, sig_logP_max=2.4;
	int mu_logP_nn=60, sig_logP_nn=60;
	double mu_logP_step = (mu_logP_max-mu_logP_min)/(mu_logP_nn-1);
	double sig_logP_step = (sig_logP_max-sig_logP_min)/(sig_logP_nn-1);

	//k = star[star_i].rfactor_vcm_nn/2;
	k = 325;

	for (i=0, mu_logP=mu_logP_min; i < mu_logP_nn; i++, mu_logP += mu_logP_step)
	{
		for (j=0, sig_logP=sig_logP_min; j < sig_logP_nn; j++, sig_logP += sig_logP_step)
		{
			interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)
			rfactor = rfactor_interpolate(k);
			cout << mu_logP << " " << sig_logP << " " << rfactor << endl;
			//cout << mu_logP << " " << sig_logP << " " << rfactor << " " << tt << " " << uu << endl;
		}
	}

	die();
}

void Galaxy::test_jtable_interpolate(void)
{
	double jfactor;
	star_i=0;
	//mu_logP = 2.23;
	//sig_logP = 2.3;
	int i,j;
	double mu_logP_min=-1, mu_logP_max=4.5;
	double sig_logP_min=0.5, sig_logP_max=4;
	int mu_logP_nn=60, sig_logP_nn=60;
	double mu_logP_step = (mu_logP_max-mu_logP_min)/(mu_logP_nn-1);
	double sig_logP_step = (sig_logP_max-sig_logP_min)/(sig_logP_nn-1);

	for (i=0, mu_logP=mu_logP_min; i < mu_logP_nn; i++, mu_logP += mu_logP_step)
	{
		for (j=0, sig_logP=sig_logP_min; j < sig_logP_nn; j++, sig_logP += sig_logP_step)
		{
			interpolate_period_params(); // find mu_logP, sig_logP values in rtable surrouding the given point (mu_logP,sig_logP)
			jfactor = jfactor_interpolate();
			cout << mu_logP << " " << sig_logP << " " << jfactor << endl;
			//cout << mu_logP << " " << sig_logP << " " << rfactor << " " << tt << " " << uu << endl;
		}
	}

	die();
}

Galaxy::~Galaxy()
{
	if (star != NULL)
		delete[] star;
	if (wt != NULL) {
		for (int i=0; i < 16; i++)
			delete[] wt[i];
		delete[] wt;
	}
	if (logdv_likelihoods != NULL) {
		for (int i=0; i < n_logdvlike_years; i++)
			delete[] logdv_likelihoods[i];
		delete[] logdv_likelihoods;
	}
	return;
}

char *advance(char *p)
{
	while ((*++p) and ((!isalpha(*p)) or (*p=='e'))) /* This advances to the next flag (if there is one) */
		;
	return (--p);
}

void find_velocity_params_from_sigma_clip(char *datafile, double &mu, double &sig, bool use_walker_cut)
{
	if (mpi_id==0) {
		string datafile_str(datafile);
		string sigmaclip_command = "binmc ";
		if (record_positions==true) sigmaclip_command += "-X ";
		if (no_metallicities==true) sigmaclip_command += "-Y ";
		if (include_v_sysparams==true) {
			sigmaclip_command += "-O ";
		}
		sigmaclip_command += "-D:" + datafile_str + " >/dev/null";
		string bestfit_filename = datafile_str + ".bestfit";
		//cout << sigmaclip_command << endl;
		system(sigmaclip_command.c_str());
		ifstream bestfit_file(bestfit_filename.c_str());
		bestfit_file >> mu >> sig;
		cout << "3-sigma clip results: systemic velocity = " << mu << ", dispersion = " << sig << endl << endl;
	}
#ifdef USE_MPI
	MPI_Bcast(&mu,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
	MPI_Bcast(&sig,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
#endif
}

void append_nstars_to_output_file(string &output_file, int nstars)
{
	string nstars_string;
	stringstream nstars_str;
	nstars_str << nstars;
	nstars_str >> nstars_string;
	output_file += "." + nstars_string;
}

