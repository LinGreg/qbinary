#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <vector>
#include <pthread.h>
#include "romberg_noclass.h"
#include "errors.h"
#include "spline.h"
#include "vector.h"
#include "rand.h"
#include "mathexpr.h"
#include "binmc.h"
#include "gamma.h"
#include "lumrspl.h"
#include "brent.h"
#include "minimize.h"
#include "sort_old.h"
#include <sys/stat.h>

#ifdef USE_OPENMP
#include <omp.h>
#endif

#ifdef USE_MPI
#include "mpi.h"
#endif

using namespace std;

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

const double auyr_to_kms = 4.741;

double systematic_error;

const double max_rgb_radius = 0.3; // in AU; refine this later, it actually depends on the star's mass

int seed;

const double mu_e_fiducial=0.25; //from O96
const double sig_e_fiducial=0.12; //from O96
const double mu_logP_fiducial=2.24; //from O96
const double sig_logP_fiducial=2.3; //from O96

const double flat_logP_min = -2;
const double flat_logP_max = 5;

double MW_mean_equivalent_width = 0.8; // calcium equivalent width. Note, this might vary depending on the portion of the sky observed (closer to MW disk --> higher metallicities)
double MW_equivalent_width_dispersion = 0.1;

// default metallicity parameters
double mean_equivalent_width = 0.3;
double equivalent_width_dispersion = 0.1;
double equivalent_width_error = 0.05;

// error model defaults (can be changed by loading an input file with 'Q:<filename>' command)
double alpha_red = 40;
double x_red = 1.1;
double alpha_blue = 38;
double x_blue = 1.2;

const double baseline_red = 0.6;
const double baseline_blue = 0.26;

double half_light_radius = -10; // must be set using the 'h' argument
double field_radius = -10;

double mean_sigerr;
double sig_sigerr;
double min_sigerr;

double mu_logP;
double sig_logP;
double mu_e;
double sig_e;
double binary_fraction;

Random randseq;

char *advance(char *p);
void print_options(void);
void load_input_params(char *input_filename, char *mwlikelihood_file, double &binfrac);

enum Mode
{
	Nothing,
	Generate_Velocities,
	Generate_Magnitude_Distribution,
	Generate_R_Functions,
	Generate_R_Tables,
	Generate_DV_JFactors,
	Generate_DV_JTables,
	Dispersion_Posterior_Real_Dataset,
	Make_Fake_Galaxy,
	Testing	// for when I'm testing out new functions
};

enum Period_Distribution
{
	LogNormal,
	Flat
} period_distribution;

lumr_mass_Spline_age lumr_mass_spline_age;
Spline MW_Likelihood;
Spline qdist_white_dwarf_spline;

double minimum_mass; // from magnitude limit
bool radius_from_isochrone;

const double logPmin = -7.00, logPmax = 7.44;
double v_error;
double minmass, maxmass, min_hb_agb_mass, max_hb_agb_mass, max_rgb_mass;

int epochs;
double gdisp, systemic_velocity, Rstar, M_bigstar;
bool fix_q, fix_eccentricity;
bool fix_absolute_magnitude;
bool ignore_hb_agb_stars;
bool use_error_distribution;
bool use_milky_way_likelihood;
bool use_milky_way_gaussian_likelihood;
bool include_metal_widths;
double abs_magnitude_value = -1e30;
double q_value = -1e30;
double eccentricity = -1e30;
int n_simulated_stars;
dvector time_intervals;
dvector preset_errors;
double max_absolute_magnitude;
double member_fraction;
double distance_kpc;
double limiting_magnitude;
double max_radius;
double v_cutoff_ratio; // this is v_cutoff / dispersion
double time_interval; 
bool continue_rtables;
bool continue_star;
bool use_experimental_qdist;
bool use_experimental_edist;
bool include_positions;
bool output_to_logfile;
bool output_primary_mass;
bool accurate_rfactors;
bool fix_theta;
bool fix_psi;
int mu_logP_jj_cont, sig_logP_kk_cont;
string star_id_cont;
int star_id_offset;
bool include_error_params;
bool use_tonry_davis_rvalues;
bool load_time_interval_input_file;
bool load_preset_errors_input_file;
bool include_white_dwarfs;
bool include_systematic_params;
bool make_posterior;
bool check_wtime;
string star_id_prefix;
int mpi_np, mpi_id;
int n_sys_params;
dvector sys_param_min, sys_param_max, sys_param_step, sys_param_vals, sys_param_epoch;
ivector sysparam_type;
ivector sysparam_nn;
int sysparam1, sysparam2, sysparam3;

ofstream musigdisp;

int main(int argc, char *argv[])
{
	// defaults if not using MPI
	mpi_np = 1;
	mpi_id = 0;

#ifdef USE_MPI
	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &mpi_np);
	MPI_Comm_rank(MPI_COMM_WORLD, &mpi_id);
#endif

	Mode mode;
	vector<Mode> modes;

	v_cutoff_ratio = 3;
	mu_logP = mu_logP_fiducial;
	sig_logP = sig_logP_fiducial;
	mu_e = mu_e_fiducial;
	sig_e = sig_e_fiducial;
	fix_q = false;
	fix_eccentricity = false;
	fix_absolute_magnitude = false;
	use_error_distribution = false;
	continue_rtables = false;
	continue_star = false;
	use_milky_way_likelihood = false;
	use_milky_way_gaussian_likelihood = false;
	use_experimental_qdist = false;
	use_experimental_edist = false;
	ignore_hb_agb_stars = true;
	include_metal_widths = false;
	include_positions = false;
	member_fraction = 1;
	output_to_logfile = false;
	output_primary_mass = false;
	period_distribution = LogNormal;
	accurate_rfactors = true;
	bool galaxy_name_specified = false;
	bool multi_thread = false;
	include_systematic_params = false;
	int n_threads = 1;
	int add_to_seed = 0;
	star_id_offset = 0;
	n_sys_params = 0;
	include_error_params = false;
	use_tonry_davis_rvalues = false;
	load_time_interval_input_file = false;
	load_preset_errors_input_file = false;
	include_white_dwarfs = false;
	star_id_prefix = "";
	check_wtime = true;
	fix_theta = false;
	fix_psi = false;
	make_posterior = false;

	double binfrac, metallicity, age;

	string directory_prefix = "vlos.";
	string dirname = "test";
	char dirchar[30] = "";
	char datafile[30] = "";
	char sysparamfile[30] = "";
	char input_param_file[30] = "";
	char galaxy_char[30] = "";
	string galaxy_name;
	char mwlikelihood_file[30] = "";
	char time_interval_filename[30] = "";
	char preset_error_filename[30] = "";
	char errmodel_params_filename_char[30] = "";
	char star_prefix_char[20] = "";
	bool read_script = false;
	char scriptfilename[30] = "";
	string errmodel_params_filename = "";

	// the following are only used if they are set in command-line arguments explicitly;
	// otherwise absolute magnitude is used
	distance_kpc=1e30;
	limiting_magnitude=1e30;

	//defaults
	age=10; // in Gyr
	metallicity = 0.001;	// log Z
	binfrac=1;
	time_interval=1; // in years
	M_bigstar = 0; // setting mass to 0 means it will be drawn from Kroupa IMF
	Rstar = -1; radius_from_isochrone = true; // setting R to -1 means the radius will be determined as fn. of mass
	max_absolute_magnitude = 1;
	gdisp = 0;
	systemic_velocity = 0;
	v_error = -10; // not defined until after command-line arg's
	epochs = 2;
	n_simulated_stars = 100; // default number of stars

	systematic_error = 0;
	mean_sigerr = 5.9;
	sig_sigerr = 2;
	min_sigerr = 2.6;

	seed = -32;
	time_intervals.input(epochs-1);

	for (int i = 1; i < argc; i++)    // Process command-line arguments 
	{
		if ((*argv[i] == '-') and (isalpha(*(argv[i]+1))))
		{
			while (int c = *++argv[i])
			{
				switch (c) {
				case 'i': fix_theta = true; break;
				case 'I': fix_psi = true; break;
				////case 'I':
					//if (sscanf(argv[i], "I:%s", input_param_file)==1) {
						//load_input_params(input_param_file,mwlikelihood_file,binfrac);
						//argv[i] += (1 + strlen(input_param_file));
					//}
					//else
						//load_input_params("fakesculptor.params",mwlikelihood_file,binfrac);
					//break;
				case 'G':
					modes.push_back(Make_Fake_Galaxy);
					if (sscanf(argv[i], "G:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'u':
					if (sscanf(argv[i], "u:%s", star_prefix_char)==1) {
						argv[i] += (1 + strlen(star_prefix_char));
						star_id_prefix.assign(star_prefix_char);
						if (star_id_prefix != "") star_id_prefix += "."; // divides prefix from ID number
					}
					break;
				case 'y': modes.push_back(Testing); break;
				case 'A': ignore_hb_agb_stars = false; break;
				case 'z': period_distribution = Flat; break;
				case 'D':
					modes.push_back(Dispersion_Posterior_Real_Dataset);
					if (sscanf(argv[i], "D:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
					}
					break;
				case 'O':
					include_systematic_params = true;
					if (*(argv[i]+1)=='2') {
						n_sys_params = 2; // used for sigma clipping called by mcmc
					} else {
						n_sys_params = 1;
						if (sscanf(argv[i], "O:%s", sysparamfile)==1) {
							argv[i] += (1 + strlen(sysparamfile));
						}
					}
					break;
				case 's':
					if (sscanf(argv[i], "S%lf", &systematic_error)==0) die("invalid systematic error");
					if (systematic_error < 0) die("negative systematic error not allowed");
					argv[i] = advance(argv[i]);
					break;
				case 'U': modes.push_back(Generate_Magnitude_Distribution); break;
				case 'g':
					galaxy_name_specified = true;
					if (sscanf(argv[i], "g:%s", galaxy_char)==1) {
						argv[i] += (1 + strlen(galaxy_char));
					}
					galaxy_name.assign(galaxy_char);
					break;
				case 'o': output_to_logfile = true; break;
				case 'n':
					if (sscanf(argv[i], "n%i", &n_simulated_stars)==0) die("invalid number of stars");
					if (n_simulated_stars <= 0) die("negative number of stars not allowed");
					argv[i] = advance(argv[i]);
					break;
				case 'X': include_positions = true; break;
				case 'h':
					if (*(argv+1)) sscanf(argv[i], "h%lf", &half_light_radius);
					include_positions = true;
					argv[i] = advance(argv[i]);
					break;
				case 'K': accurate_rfactors = false; break; // use if you want a very rough calculation of R-factors
				case 'Y': include_metal_widths = false; break;
				case 'H':
					sscanf(argv[i], "H%lf", &field_radius); // in kpc
					include_positions = true;
					break;
				case 'C':
					continue_rtables = true;
					if (sscanf(argv[i], "C%i,%i", &mu_logP_jj_cont, &sig_logP_kk_cont)==0) die("invalid continue params");
					argv[i] = advance(argv[i]);
					break;
				//case 'i':
					//if (sscanf(argv[i], "i%i", &star_id_offset)==0) die("invalid star ID offset");
					//argv[i] = advance(argv[i]);
					//break;
				case 'c':
					if (sscanf(argv[i], "c%lf", &v_cutoff_ratio)==0) die("invalid cutoff ratio");
					if (v_cutoff_ratio < 0) die("invalid cutoff ratio; must be greater than 0");
					argv[i] = advance(argv[i]);
					break;
				case 'a':
					if (sscanf(argv[i], "a%lf", &age)==0) die("invalid age");
					if (age < 1) die("invalid age; must be greater than 1 Gyr");
					argv[i] = advance(argv[i]);
					break;
				case 'b':
					if (sscanf(argv[i], "b%lf", &binfrac)==0) die("invalid binary fraction");
					if ((binfrac < 0) or (binfrac > 1)) die("invalid binary fraction; must be within interval [0,1]");
					argv[i] = advance(argv[i]);
					break;
				case 'e':
					if (sscanf(argv[i], "e%lf", &v_error)==0) {
						if (sscanf(argv[i], "e:%s", preset_error_filename)==1) {
							load_preset_errors_input_file = true;
							argv[i] += (1 + strlen(preset_error_filename));
						} else die("invalid error value");
					} else {
						if (v_error==-10) use_error_distribution = true; // if no error is specified, draw sig_err from distribution
						argv[i] = advance(argv[i]);
					}
					break;
				case 'x':
					if (sscanf(argv[i], "x%lf", &eccentricity)==0) die("invalid eccentricity");
					if (eccentricity==-1e30) use_experimental_edist = true;
					else fix_eccentricity = true;
					argv[i] = advance(argv[i]);
					break;
				case 'p':
					if (sscanf(argv[i], "p%lf", &mu_logP)==0) die("invalid mu_logP value");
					argv[i] = advance(argv[i]);
					break;
				case 'P':
					if (sscanf(argv[i], "P%lf", &sig_logP)==0) die("invalid sigma_logP value");
					argv[i] = advance(argv[i]);
					break;
				case 'T':
					if (sscanf(argv[i], "T%lf", &time_interval)==0) {
						if (sscanf(argv[i], "T:%s", time_interval_filename)==1) {
							load_time_interval_input_file = true;
							argv[i] += (1 + strlen(time_interval_filename));
						} else die("invalid time interval");
					} else {
						time_intervals[0] = time_interval;
					}
					argv[i] = advance(argv[i]);
					break;
				case 'f':
					if (sscanf(argv[i], "f%lf", &member_fraction)==0) die("invalid member fraction");
					argv[i] = advance(argv[i]);
					break;
				case 'W': use_milky_way_gaussian_likelihood = true; break;
				case 'F':
					if (sscanf(argv[i], "F:%s", mwlikelihood_file)==1) {
						argv[i] += (1 + strlen(mwlikelihood_file));
						use_milky_way_likelihood = true;
					}
					argv[i] = advance(argv[i]);
					break;	
				case 'V':
					if (sscanf(argv[i], "V%lf", &systemic_velocity)==0) die("invalid systemic velocity");
					argv[i] = advance(argv[i]);
					break;
				case 'd':
					if (sscanf(argv[i], "d%lf", &gdisp)==0) die("invalid velocity dispersion");
					argv[i] = advance(argv[i]);
					break;
				case 'k':
					if (sscanf(argv[i], "k%lf", &distance_kpc)==0) die("invalid distance");
					argv[i] = advance(argv[i]);
					break;
				case 't':
					multi_thread = true;
					if (sscanf(argv[i], "t%i", &n_threads)==0) die("invalid number of threads");
					argv[i] = advance(argv[i]);
					break;
				case 'q':
					if (sscanf(argv[i], "q%lf", &q_value)==0) die("invalid mass ratio");
					if (q_value==-1e30) use_experimental_qdist = true;
					else fix_q = true;
					argv[i] = advance(argv[i]);
					break;
				case 'E':
					if (sscanf(argv[i], "E%i", &epochs)==0) die("invalid number of epochs");
					//if ((epochs < 1) or (epochs > 10)) die("number of epochs must be between 1 and 10");
					time_intervals.input(epochs-1);
					argv[i] = advance(argv[i]);
					break;
				case 'Q':
					use_tonry_davis_rvalues = true;
					if (sscanf(argv[i], "Q:%s", errmodel_params_filename_char)==1) {
						argv[i] += (1 + strlen(errmodel_params_filename_char));
						errmodel_params_filename.assign(errmodel_params_filename_char);
					}
					argv[i] = advance(argv[i]);
					break;
				case 'm':
					if (sscanf(argv[i], "m%lf", &M_bigstar) != 0) {
						output_primary_mass = true;
					}
					argv[i] = advance(argv[i]);
					break;
				case 'M':
					if (sscanf(argv[i], "M%lf", &abs_magnitude_value)==0) die("invalid absolute magnitude for primary star");
					fix_absolute_magnitude = true;
					argv[i] = advance(argv[i]);
					break;
				case 'l':
					if (sscanf(argv[i], "l%lf", &limiting_magnitude)==0) die("invalid magnitude limit");
					argv[i] = advance(argv[i]);
					break;
				case 'L':
					if (sscanf(argv[i], "L%lf", &max_absolute_magnitude)==0) die("invalid absolute magnitude limit");
					argv[i] = advance(argv[i]);
					break;
				case 'v':
					modes.push_back(Generate_Velocities);
					if (*(argv[i]+1)=='2') {
						output_primary_mass = true;
						if (sscanf(argv[i], "v2:%s", dirchar)==1) {
							argv[i] += (2 + strlen(dirchar));
							dirname.assign(dirchar);
						}
					} else {
						if (sscanf(argv[i], "v:%s", dirchar)==1) {
							argv[i] += (1 + strlen(dirchar));
							dirname.assign(dirchar);
						}
					}
					break;
				case 'r':
					if (*(argv[i]+1)=='2') {
						modes.push_back(Generate_R_Tables);
						if (sscanf(argv[i], "R2:%s", datafile)==1) {
							argv[i] += (2 + strlen(datafile));
							dirname.assign(datafile);
						}
						argv[i] = advance(argv[i]);
						break;
					} else {
						modes.push_back(Generate_R_Functions);
						if (sscanf(argv[i], "r:%s", datafile)==1) {
							argv[i] += (1 + strlen(datafile));
							dirname.assign(datafile);
						}
						argv[i] = advance(argv[i]);
					}
					break;
				case 'R':
					if (sscanf(argv[i], "R%lf", &Rstar)==0) die("invalid stellar radius");
					radius_from_isochrone = false;
					argv[i] = advance(argv[i]);
					break;
				case 'j':
					if (*(argv[i]+1)=='2') {
						modes.push_back(Generate_DV_JFactors);
						make_posterior = true;
						if (sscanf(argv[i], "j2:%s", datafile)==1) {
							argv[i] += (2 + strlen(datafile));
							dirname.assign(datafile);
						}
						argv[i] = advance(argv[i]);
						break;
					} else {
						modes.push_back(Generate_DV_JFactors);
						if (sscanf(argv[i], "j:%s", datafile)==1) {
							argv[i] += (1 + strlen(datafile));
							dirname.assign(datafile);
						}
						argv[i] = advance(argv[i]);
						break;
					}
				case 'Z': include_error_params = false; break; // no good reason to assign 'Z' to this, except I've run out of letters
				case 'N':
					if (sscanf(argv[i], "N%i", &add_to_seed)==0) die("invalid random seed number");
					seed -= add_to_seed;
					argv[i] = advance(argv[i]);
					break;
				case 'J':
					modes.push_back(Generate_DV_JTables);
					if (sscanf(argv[i], "J:%s", datafile)==1) {
						argv[i] += (1 + strlen(datafile));
						dirname.assign(datafile);
					}
					argv[i] = advance(argv[i]);
					break;
				case 'w': include_white_dwarfs = true; break;
				}
			}
		} else {
			if (sscanf(argv[i], "%s", scriptfilename)==1) {
				argv[i] += (1 + strlen(scriptfilename));
				read_script = true;
			}
		}
	}

	if (read_script)
	{
		int nwords;
		string line;
		vector<string> words;
		stringstream* ws = NULL;
		ifstream scriptfile(scriptfilename);

		while (!scriptfile.eof()) {
			bool transform_name = false;
			bool transform_latex_name = false;
			getline(scriptfile,line);
			words.clear();
			if (line.empty()) continue;
			if (line[0]=='#') continue;
			string linestring(line);
			remove_comments(linestring);
			istringstream linestream(linestring);
			string word;
			while (linestream >> word)
				words.push_back(word);
			nwords = words.size();
			if (ws != NULL) delete[] ws;
			ws = new stringstream[nwords];
			for (int i=0; i < nwords; i++) ws[i] << words[i];
			remove_equal_sign(nwords,words,ws); // in case there's an equal sign for setting variables, get rid of it
			if (words[0]=="binfrac")
			{
				if (nwords != 2) die("one argument required for 'binfrac' (binary fraction setting)");
				if (!(ws[1] >> binfrac)) die("incorrect format for binary fraction");
				if ((binfrac < 0) or (binfrac > 1)) die("binary fraction must be between 0 and 1");
			}
			else if (words[0]=="nstars")
			{
				if (nwords != 2) die("one argument required for 'nstars' (number of stars)");
				if (!(ws[1] >> n_simulated_stars)) die("incorrect format for number of stars");
				if (n_simulated_stars <= 0) die("number of stars must be greater than 0");
			}
			else if (words[0]=="n_epochs")
			{
				if (nwords != 2) die("one argument required for 'n_epochs' (number of epochs)");
				if (!(ws[1] >> epochs)) die("incorrect format for number of epochs");
				if (epochs <= 0) die("number of epochs must be greater than 0");
				time_intervals.input(epochs-1);
			}
			else if (words[0]=="v_error")
			{
				if (nwords != 2) die("one argument required for 'v_error' (line-of-sight velocity measurement errors)");
				if (!(ws[1] >> v_error)) die("incorrect format for v_error");
				if (v_error < 0) die("measurement error must be greater than or equal to 0");
			}
			else if (words[0]=="vsys")
			{
				if (nwords != 2) die("one argument required for 'vsys' (systemic velocity)");
				if (!(ws[1] >> systemic_velocity)) die("incorrect format for vsys");
			}
			else if (words[0]=="vdisp")
			{
				if (nwords != 2) die("one argument required for 'vdisp' (velocity dispersion)");
				if (!(ws[1] >> gdisp)) die("incorrect format for vdisp");
			}
			else if (words[0]=="time_interval")
			{
				if (nwords != 2) die("one argument required for 'time_interval'");
				if (!(ws[1] >> time_interval)) die("incorrect format for time_interval");
				if (time_interval < 0) die("time interval must be greater than or equal to 0");
			}
			else if (words[0]=="load_time_intervals")
			{
				if (nwords != 2) die("one argument required for 'load_time_intervals' (filename)");
				load_time_interval_input_file = true;
				strcpy(time_interval_filename,words[1].c_str());
			}
			else if (words[0]=="data_label")
			{
				if (nwords != 2) die("one argument required for 'data_label' (label)");
				dirname = words[1];
				strcpy(datafile,dirname.c_str());
			}
			else if (words[0]=="mu_logP")
			{
				if (nwords != 2) die("one argument required for 'mu_logP' (mean log-period)");
				if (!(ws[1] >> mu_logP)) die("incorrect format for mu_logP");
			}
			else if (words[0]=="sig_logP")
			{
				if (nwords != 2) die("one argument required for 'sig_logP' (dispersion of log-period)");
				if (!(ws[1] >> sig_logP)) die("incorrect format for sig_logP");
			}
			else if (words[0]=="dist_kpc")
			{
				if (nwords != 2) die("one argument required for 'dist_kpc' (distance to star population in kpc)");
				if (!(ws[1] >> distance_kpc)) die("incorrect format for dist_kpc");
			}
			else if (words[0]=="vmag_limit")
			{
				if (nwords != 2) die("one argument required for 'vmag_limit' (limiting M_V magnitude)");
				if (!(ws[1] >> limiting_magnitude)) die("incorrect format for vmag_limit");
			}
			else if (words[0]=="include_metal_widths")
			{
				if (nwords != 2) die("one argument required for 'include_metal_widths' (on/off)");
				if (words[1]=="on") include_metal_widths = true;
				else if (words[1]=="off") include_metal_widths = false;
				else die("must specify 'on' or 'off' for command 'include_metal_widths'");
			}
			else if (words[0]=="stellar_age")
			{
				if (nwords != 2) die("one argument required for 'stellar_age'");
				if (!(ws[1] >> age)) die("incorrect format for stellar_age");
			}
			else if (words[0]=="nthreads")
			{
				if (nwords != 2) die("one argument required for 'nthreads'");
				if (!(ws[1] >> n_threads)) die("incorrect format for number of threads");
				multi_thread = true;
			}
			else if (words[0]=="generate_mockdata")
			{
				modes.push_back(Generate_Velocities);
			}
			else if (words[0]=="generate_dvlikes")
			{
				modes.push_back(Generate_DV_JFactors);
			}
			else if (words[0]=="generate_vlikes")
			{
				modes.push_back(Generate_R_Functions);
			}
			else if (words[0]=="random_seed")
			{
				int add_to_seed;
				if (nwords != 2) die("one argument required for 'random_seed'");
				if (!(ws[1] >> add_to_seed)) die("incorrect format for random seed");
				seed -= add_to_seed;
			}
			else
			{
				die("command '%s' not recognized",line.c_str());
			}

			delete[] ws; ws = NULL;
		}
	}

	randseq.set_random_seed(seed);
	if ((v_error==-10) and (!use_error_distribution)) v_error=0;

	if ((include_metal_widths) and ((mean_equivalent_width==-1e30) or (equivalent_width_dispersion==-1e30))) die("If metal widths are included, you must give mean width and dispersion of width using -w and -W");

	if (include_white_dwarfs) qdist_white_dwarf_spline.input("qdist_wd");

	if (galaxy_name_specified) {
		if (galaxy_name=="sculptor") {
			distance_kpc = 86;
			systemic_velocity = 111.5;
			gdisp = 8.7;
		} else if (galaxy_name=="fornax") {
			distance_kpc = 140;
			systemic_velocity = 55;
			gdisp = 11.8;
		} else if (galaxy_name=="carina") {
			distance_kpc = 103;
			systemic_velocity = 223;
			gdisp = 7.3;
		} else if (galaxy_name=="sextans") {
			distance_kpc = 87;
			systemic_velocity = 224;
			gdisp = 8.2;
		} else die("unknown galaxy specified");
	}

	if (use_tonry_davis_rvalues) {
		if (errmodel_params_filename != "") {
			ifstream errmodel_params_file(errmodel_params_filename.c_str());
			errmodel_params_file >> alpha_red >> x_red >> alpha_blue >> x_blue;
		}
	}

	if (load_time_interval_input_file) {
		dvector time_increments(epochs-1);
		ifstream time_interval_file(time_interval_filename);
		for (int i=0; i < epochs-1; i++) {
			time_interval_file >> time_increments[i];
			time_intervals[i] = 0;
			for (int j=0; j <= i; j++)
				time_intervals[i] += time_increments[j];
		}
	} else {
		time_intervals[0] = time_interval;
		for (int i=1; i < epochs-1; i++)
			time_intervals[i] = time_intervals[i-1] + time_interval;
	}

	if (load_preset_errors_input_file) {
		ifstream preset_error_file(preset_error_filename);
		preset_errors.input(epochs);
		for (int i=0; i < epochs; i++) {
			preset_error_file >> preset_errors[i];
		}
	} 

	if (include_systematic_params) {
		string sysparamfile_string(sysparamfile);
		if (sysparamfile_string != "") {
			ifstream sysparam_file(sysparamfile);
			sysparam_file >> n_sys_params;
			if ((n_sys_params < 1) or (n_sys_params > 3)) die("Invalid number of offset parameters; must be between 1 and 3");
			sys_param_min.input(n_sys_params);
			sys_param_max.input(n_sys_params);
			sys_param_step.input(n_sys_params);
			sysparam_nn.input(n_sys_params);
			sysparam_type.input(n_sys_params);
			for (int i=0; i < n_sys_params; i++) {
				sysparam_file >> sysparam_type[i];  // type=0 for an offset, type=1 for systematic error parameter
				sysparam_file >> sys_param_min[i] >> sys_param_max[i] >> sysparam_nn[i];
				if (sysparam_nn[i] > 1) sys_param_step[i] = (sys_param_max[i]-sys_param_min[i])/(sysparam_nn[i]-1);
				else sys_param_step[i] = 0;
			}
			if ((mode==Generate_Velocities) or (mode==Dispersion_Posterior_Real_Dataset)) {
				// next parameters give the offsets for the i'th measurement of each star when generating simulated data sets
				sys_param_vals.input(n_sys_params);
				sys_param_epoch.input(n_sys_params);
				for (int i=0; i < n_sys_params; i++) {
					sysparam_file >> sys_param_epoch[i];
					sysparam_file >> sys_param_vals[i];
				}
				for (int j=0; j < 10; j++) {
					int n_sysparams_this_epoch=0;
					for (int kk=0; kk < n_sys_params; kk++) {
						if (sys_param_epoch[kk]==j) {
							n_sysparams_this_epoch++;
						}
					}
					if (n_sysparams_this_epoch > 2) die("cannot have more than two systematic error parameters associated with one epoch");
				}
			}

			string sysparam_table_filename = dirname + ".sysparam_table";
			if (mpi_id==0) {
				int i,m;
				double lambda;
				ofstream sysparam_file;
				sysparam_file.open(sysparam_table_filename.c_str());
				sysparam_file << n_sys_params << endl;
				for (i=0; i < n_sys_params; i++) {
					sysparam_file << sysparam_type[i] << " ";
				}
				sysparam_file << endl << endl;
				for (i=0; i < n_sys_params; i++) {
					sysparam_file << sysparam_nn[i] << endl;
					for (m=0, lambda=sys_param_min[i]; m < sysparam_nn[i]; m++, lambda += sys_param_step[i])
						sysparam_file << lambda << endl;
					sysparam_file << endl;
				}
				sysparam_file.close();
			}
			sysparam_file.close();
		}
	}
	
	string directory = directory_prefix + dirname;
	string vbinfile, logvbinfile, vfile, dvfile, logdvfile, vdatafile, vfile_nobin, dvfile_nobin, params_file;
	vbinfile = directory + "/vbin";
	logvbinfile = directory + "/logvbin";
	vfile = directory + "/vdata";
	vfile_nobin = directory + "/vdata.nobin";
	vdatafile = dirname;
	logdvfile = directory + "/logdvdata";
	params_file = directory + "/params";
	dvfile = directory + "/dvdata";
	dvfile_nobin = directory + "/dvdata.nobin";
	string mkdirstring = "if [ -e ", mkdirstring_rfactors = mkdirstring, mkdirstring_rtables = mkdirstring, mkdirstring_jtables = mkdirstring, mkdirstring_rltables = mkdirstring;
	mkdirstring += directory + " ]; then rm -r " + directory + "; fi; mkdir " + directory;
	mkdirstring_rfactors += "rfactors." + dirname + " ]; then rm -r " + "rfactors." + dirname + "; fi; mkdir " + "rfactors." + dirname;
	mkdirstring_rtables += "rtables." + dirname + " ]; then rm -r " + "rtables." + dirname + "; fi; mkdir " + "rtables." + dirname;
	mkdirstring_jtables += "jtables." + dirname + " ]; then rm -r " + "jtables." + dirname + "; fi; mkdir " + "jtables." + dirname;
	mkdirstring_rltables += "rltables." + dirname + " ]; then rm -r " + "rltables." + dirname + "; fi; mkdir " + "rltables." + dirname;

	lumr_mass_spline_age.input(metallicity);
	lumr_mass_spline_age.create_lum_r_splines_at_fixed_age(age);
	//lumr_mass_spline_age.plot_lumr(20, 10);

	if (use_milky_way_likelihood) MW_Likelihood.input(mwlikelihood_file);

	minmass = lumr_mass_spline_age.min_mass();
	double min_rgb_mass = lumr_mass_spline_age.min_rgb_mass_at_fixed_age();
	max_rgb_mass = lumr_mass_spline_age.max_rgb_mass_at_fixed_age();
	max_hb_agb_mass = lumr_mass_spline_age.max_hb_agb_mass_at_fixed_age();
	min_hb_agb_mass = lumr_mass_spline_age.min_hb_agb_mass_at_fixed_age();
	//cout << "RGB mass range: " << min_rgb_mass << " to " << max_rgb_mass << ", AGB mass range: " << min_hb_agb_mass << " to " << max_hb_agb_mass << endl;
	maxmass = (ignore_hb_agb_stars==true) ? max_rgb_mass : max_hb_agb_mass; // we don't want stars in the AGB mass range if ignore_hb_agb_stars==true
	//if (ignore_hb_agb_stars) warn("we're assuming datasets without AGB stars, since identifying AGB stars requires color info (put this in!)\nAlso, look into including dust extinction when going from apparent to absolute magnitudes!");
	
	if ((distance_kpc < 1e30) and (limiting_magnitude < 1e30)) {
		max_absolute_magnitude = find_absolute_magnitude(distance_kpc, limiting_magnitude);
	}

	minimum_mass = find_stellar_mass(max_absolute_magnitude);
	if (fix_absolute_magnitude) M_bigstar = find_stellar_mass(abs_magnitude_value);

	for (int m=0; m < modes.size(); m++) {
		mode = modes[m];
		if (mode==Make_Fake_Galaxy)
		{
			output_fake_galaxy_data_real_errors(binfrac,datafile);
		}
		else if (mode==Testing)
		{
			find_compact_binary_ratio(0.87);
		}
		else if (mode==Dispersion_Posterior_Real_Dataset)
		{
			plot_sigma_posterior(datafile);
		}
		else if (mode==Generate_Velocities)
		{
			if (mpi_id==0) {
				system(mkdirstring.c_str());
				generate_velocities(binfrac,vbinfile,logvbinfile,vfile,logdvfile,dvfile,vdatafile,add_to_seed);
				ofstream params_out(params_file.c_str());
				params_out << "Number of stars = " << n_simulated_stars << endl;
				params_out << "Systemic velocity = " << systemic_velocity << endl;
				params_out << "Velocity dispersion = " << gdisp << endl;
				params_out << "Member fraction = " << member_fraction << endl;
				if (member_fraction < 1) params_out << "Milky way likelihood file: " << mwlikelihood_file << endl;
				params_out << "Binary fraction = " << binfrac << endl;
				params_out << "Mean binary period (mu_logP) = " << mu_logP << endl;
				params_out << "Spread of binary periods (sigma_logP) = " << sig_logP << endl;
				params_out << "Stellar radii: ";
				if (radius_from_isochrone) params_out << "calculated from isochrone" << endl;
				else params_out << "fixed at " << Rstar << " AU" << endl;
				params_out << "Number of epochs = " << epochs << endl;
				params_out << "Measurement error = " << v_error << endl;
				if (include_metal_widths) {
					params_out << endl;
					params_out << "Average metal width = " << mean_equivalent_width << endl;
					params_out << "Metal width dispersion = " << equivalent_width_dispersion << endl;
					params_out << "Average MW metal width = " << MW_mean_equivalent_width << endl;
					params_out << "Metal MW width dispersion = " << MW_equivalent_width_dispersion << endl;
				}
				if (include_positions) {
					params_out << endl;
					params_out << "Plummer scale radius = " << half_light_radius << endl;
					params_out << "field radius = " << field_radius << endl;
					double NN = (member_fraction/(1-member_fraction))*(1+SQR(field_radius/half_light_radius));
					params_out << "N parameter (ratio of galaxy to MW number density at r=0) = " << NN << endl;
				}
				if ((distance_kpc < 1e30) and (limiting_magnitude < 1e30)) {
					params_out << endl;
					params_out << "distance in kpc: " << distance_kpc << endl;
					params_out << "magnitude limit = " << limiting_magnitude << endl;
				}
				if (fix_absolute_magnitude)
					params_out << "absolute magnitude fixed at M = " << abs_magnitude_value << endl;
				else
					params_out << "absolute magnitude limit = " << max_absolute_magnitude << endl;
				if (include_systematic_params) {
					for (int i=0; i < n_sys_params; i++) {
						if (sysparam_type[i]==0) params_out << "velocity offset " << i << ": lambda" << i << " = " << sys_param_vals[i] << " (epoch " << sys_param_epoch[i] << ")" << endl;
						else params_out << "systematic error " << i << ": syserr" << i << " = " << sys_param_vals[i] << " (epoch " << sys_param_epoch[i] << ")" << endl;
					}
				}
				params_out << "Random seed: " << seed << endl;

			}
		}
		else if (mode==Generate_R_Functions)
		{
			if (include_systematic_params==false) {
				if (distance_kpc < 1e30) ;
				else cerr << "*WARNING*: Distance to galaxy has not been set; will assume given magnitudes are absolute magnitude values\n\n";
				if (mpi_id==0) system(mkdirstring_rfactors.c_str());
				if ((systemic_velocity==0) and (gdisp==0)) find_approximate_velocity_params(datafile,systemic_velocity,gdisp);
				
				R_Factors rfactors(systemic_velocity,gdisp,distance_kpc);
				rfactors.generate_rfactors(datafile,dirname,n_threads);
			} else {
				if (distance_kpc < 1e30) ;
				else cerr << "*WARNING*: Distance to galaxy has not been set; will assume given magnitudes are absolute magnitude values\n\n";
				if (mpi_id==0) system(mkdirstring_rltables.c_str());
				if ((systemic_velocity==0) and (gdisp==0)) find_approximate_velocity_params(datafile,systemic_velocity,gdisp);

				R_Factors rfactors(systemic_velocity,gdisp,distance_kpc);
				rfactors.generate_rfactors_sysparams(datafile,dirname,n_threads);
			}
		}
		else if (mode==Generate_R_Tables)
		{
			if (distance_kpc < 1e30) ;
			else cerr << "*WARNING*: Distance to galaxy has not been set; will assume given magnitudes are absolute magnitude values\n\n";

			int mu_logP_nn=10;
			double mu_logP_min, mu_logP_max;
			//double sigmu=1.69545; // for MW prior
			//mu_logP_min = mu_logP_fiducial - 2*sigmu; // used for MW prior
			//mu_logP_max = mu_logP_fiducial + 2*sigmu; // used for MW prior
			mu_logP_min = -1.2;
			mu_logP_max = 5.7;

			int sig_logP_nn=6;
			double sig_logP_min=0.5, sig_logP_max=4;

			if ((systemic_velocity==0) and (gdisp==0)) find_approximate_velocity_params(datafile,systemic_velocity,gdisp);
			if (mpi_id==0) {
				if ((continue_rtables==false) and (continue_star==false)) system(mkdirstring_rtables.c_str());
			}
			R_Factors rtables(systemic_velocity,gdisp,distance_kpc);
			rtables.set_musig_table_params(mu_logP_min,mu_logP_max,mu_logP_nn,sig_logP_min,sig_logP_max,sig_logP_nn);
			rtables.generate_rtables(datafile,dirname,n_threads);
		}
		else if (mode==Generate_DV_JFactors)
		{
			if (distance_kpc < 1e30) ;
			else cerr << "*WARNING*: Distance to galaxy has not been set; will assume given magnitudes are absolute magnitude values\n\n";
			
			R_Factors jfactors(distance_kpc);
			jfactors.generate_jfactors(datafile,dirname,n_threads);
		}
		else if (mode==Generate_DV_JTables)
		{
			if (mpi_id==0) {
				if (distance_kpc < 1e30) ;
				else cerr << "*WARNING*: Distance to galaxy has not been set; will assume given magnitudes are absolute magnitude values\n\n";
				system(mkdirstring_jtables.c_str());
			}
			
			int mu_logP_nn=10;
			double mu_logP_min, mu_logP_max;
			//double sigmu=1.69545; // for MW prior
			//mu_logP_min = mu_logP_fiducial - 2*sigmu; // used for MW prior
			//mu_logP_max = mu_logP_fiducial + 2*sigmu; // used for MW prior
			mu_logP_min = -1.2;
			mu_logP_max = 5.7;

			int sig_logP_nn=6;
			double sig_logP_min=0.5, sig_logP_max=4;

			R_Factors dv_jtables(distance_kpc);
			dv_jtables.set_musig_table_params(mu_logP_min,mu_logP_max,mu_logP_nn,sig_logP_min,sig_logP_max,sig_logP_nn);
			dv_jtables.generate_jtables(datafile,dirname,n_threads);
		}
		else if (mode==Generate_Magnitude_Distribution)
		{
			generate_magnitude_distribution(distance_kpc);
		}
		else
		{
			print_options();
		}
	}
#ifdef USE_MPI
	MPI_Finalize();
#endif
}

void remove_comments(string& instring)
{
	string instring_copy(instring);
	instring.clear();
	size_t comment_pos = instring_copy.find("#");
	if (comment_pos != string::npos) {
		instring = instring_copy.substr(0,comment_pos);
	} else instring = instring_copy;
}

void remove_equal_sign(int& nwords, vector<string>& words, stringstream *ws)
{
	int pos;
	if ((pos = words[0].find('=')) != string::npos) {
		// there's an equal sign in the first word, so remove it and separate into two words
		words.push_back("");
		for (int i=nwords-1; i > 0; i--) words[i+1] = words[i];
		words[1] = words[0].substr(pos+1);
		words[0] = words[0].substr(0,pos);
		nwords++;
		if (ws != NULL) delete[] ws;
		ws = new stringstream[nwords];
		for (int i=0; i < nwords; i++) ws[i] << words[i];
	}
	else if ((nwords == 3) and (words[1]=="="))
	{
		// there's an equal sign in the second of three words (indicating a parameter assignment), so remove it and reduce to two words
		string word1,word2;
		word1=words[0]; word2=words[2];
		words.clear();
		words.push_back(word1);
		words.push_back(word2);
		nwords = 2;
		delete[] ws;
		ws = new stringstream[nwords];
		ws[0] << words[0]; ws[1] << words[1];
	}
}

void generate_velocities(double binfrac, const string vbinfilename, const string logvbinfilename, const string vfilename, const string logdv_filename, const string dv_filename, const string vdatafilename, const int add_to_seed)
{
	seed -= add_to_seed;
	dvector vbin(epochs), vlos(epochs), vlos_nb(epochs);
	dvector v_errors(epochs);
	dvector width(epochs), width_errors(epochs);
	dvector rvalue(epochs);
	ivector channel(epochs);
	double dv, width_no_error;
	double vlos_no_error;
	if (load_preset_errors_input_file) {
		for (int i=0; i < epochs; i++) v_errors[i]=preset_errors[i];
	} else {
		if ((!use_error_distribution) and (!use_tonry_davis_rvalues)) for (int i=0; i < epochs; i++) v_errors[i]=v_error;
	}

	double logP, q, e, phi, M, a, r, dphidt, p, vcm, mem_p;
	double period, time, time_year, time_evolved_year, newtime, newtime_years;
	double newphi, newr, newdphidt, orientation_cosine;
	double theta, psi;	// Euler angles
	double roche_lobe_radius;
	double magnitude, projected_R, projected_phi;
	double verr;

	int i,j;
	int ii, n_epochs;
	dvector times(epochs), times_zero_offset(epochs);

	string vdatafilename_bins_final = vfilename + ".bin";
	string vdatafilename_bins = vfilename + ".bin.tmp";
	string vnbdatafilename = vfilename + "_nb";
	string dvdatafilename = vfilename + ".dv";
	string walker_datafilename = vfilename + ".vel";
	string xvals_filename = vfilename + ".xvals";
	ofstream vbinfile(vbinfilename.c_str());
	ofstream logvbinfile(logvbinfilename.c_str());
	ofstream vfile(vfilename.c_str());
	ofstream vdatafile(vdatafilename.c_str());
	ofstream vnbdatafile(vnbdatafilename.c_str());
	ofstream vdatafile_bins(vdatafilename_bins.c_str());
	ofstream dvdatafile(dv_filename.c_str());
	ofstream logdvfile(logdv_filename.c_str());
	ofstream xvalsfile(xvals_filename.c_str());
	string binary_params_filename = vfilename + ".binparams";
	ofstream binary_params_out(binary_params_filename.c_str());
	ofstream walker_datafile;
	if (use_tonry_davis_rvalues) {
		walker_datafile.open(walker_datafilename.c_str());
		walker_datafile << "# ID # Fiber # Julian Date (days) # RA_2000 # Dec_2000 # V (mag) # error_V (mag) # V-I (mag) # error_{V-I} (mag) # vhelio (km/s) # error_vhelio (km/s) # Mg (Ang.) # error_MG (Ang.) # number of observations of this star # probability of membership # Tonry-Davis R # <S/N>/pix--all repeat observations are listed immediately under the first observation # <S/N> per pixel" << endl;
	}

	int nbins = 0;

	ofstream rfile;
	if (include_positions) rfile.open("rdata");

	double white_dwarf_rand=0;
	bool has_white_dwarf;

	vdatafile << n_simulated_stars << endl;
	for (i=0; i < n_simulated_stars; i++)
	{
			n_epochs = epochs;
			p = randseq.RandomNumber2();
			mem_p = randseq.RandomNumber2();
			if (mem_p > member_fraction) // then it's a MW star
			{
				for (;;)
				{
					M = gen_m_kroupa();
					magnitude = magnitude_from_mass(M); // this is probably bogus for MW stars, and bear in mind this won't work if apparent magnitude is selected
					// since distance will be much smaller than galaxy distance but for just doing a simulation using absolute magnitude, it's probably fine.
					// If you want to do better, then define a min_apparent_magnitude (so there's a range of magnitudes) and use "besancon.maghist" for the distribution of magnitudes
					if (magnitude == -1e30) { continue; } // cuts out stars beyond limiting magnitude
					break;
				}
				times_zero_offset[0]=0;
				for (j=1; j < n_epochs; j++)
					times_zero_offset[j] = time_intervals[j-1];	// time_intervals is in years

				vlos_no_error = generate_nonmember_velocity();
				for (j=0; j < n_epochs; j++)
				{
					vlos[j] = vlos_no_error;
					if (use_tonry_davis_rvalues) vlos[j] += generate_velocity_error_from_rvalue(v_errors[j],rvalue[j],channel[j]);
					else vlos[j] += generate_velocity_error(v_errors[j]);
					if (include_systematic_params) {
						for (int kk=0; kk < n_sys_params; kk++) {
							if (sys_param_epoch[kk]==j) {
								if (sysparam_type[kk]==0) {
									vlos[j] += sys_param_vals[kk];
								} else {
									vlos[j] += sys_param_vals[kk]*randseq.NormalDeviate();
								}
							}
						}
					}
					vlos_nb[j] = vlos[j]; // assuming MW stars are not binaries (a bit dicey perhaps!)
				}
				if (include_metal_widths)
				{
					width_no_error = MW_mean_equivalent_width + MW_equivalent_width_dispersion*randseq.NormalDeviate();
					for (j=0; j < n_epochs; j++)
					{
						width[j] = width_no_error;
						if (equivalent_width_error != 0) width[j] += equivalent_width_error*randseq.NormalDeviate();
					}
				}
				if (include_positions) {
					projected_R = generate_nonmember_position();
					projected_phi = M_2PI*randseq.RandomNumber2();
				}
			}
			else
			{
				vcm = systemic_velocity + gdisp*randseq.NormalDeviate(); // velocity due to galaxy's intrinsic dispersion and systemic velocity
				
				if (p < binfrac) // if binary generate/calculate binary parameters
				{
					nbins++;
				 for (;;)
				 {
					if (fix_absolute_magnitude)
					{
						M = M_bigstar;
						magnitude = abs_magnitude_value;
					}
					else
					{
						M = (M_bigstar==0) ? gen_m_kroupa() : M_bigstar;
						magnitude = magnitude_from_mass(M);
						if (magnitude == -1e30) { continue; } // cuts out stars beyond limiting magnitude
					}	

					if (radius_from_isochrone) Rstar = find_stellar_radius(M);
					if (Rstar==-1) continue;

					// Binary Parameters      
					logP = gen_logP(); 
					period = pow(10.0,logP);
					if (include_white_dwarfs) white_dwarf_rand = randseq.RandomNumber2();
					if (white_dwarf_rand < 0.69) { // because ~ 31% of giants in binary systems have white dwarf companions
						has_white_dwarf = false;
						if (fix_q) q = q_value;
						else if (use_experimental_qdist) q = gen_q_experimental(logP);
						else q = gen_q(logP);
					} else {
						has_white_dwarf = true;
						q = gen_q_white_dwarf();
					}

					if (fix_eccentricity) e = eccentricity;
					else if (use_experimental_edist) e = gen_e_experimental(logP);
					else e = gen_e(logP);

					a = primary_semimajor_axis(logP, M, q);
					roche_lobe_radius = roche_lobe_radius_approximation(a,q);

					if (has_white_dwarf) {
						if (max_rgb_radius > roche_lobe_radius) continue;
					} else {
						if (Rstar > roche_lobe_radius) continue;
					}

					generate_euler_angles(theta,psi); 
					if (fix_theta) theta = M_HALFPI;
					if (fix_psi) psi = M_HALFPI;
					phi = gen_phi(e,time);
					vbin[0] = vbin_los(a,period,e,theta,psi,phi);

					double x = sin(theta)*q*pow(2.0/(1+q),0.66666666666667);
					binary_params_out << "star " << i << ": mass=" << M << " vcm=" << vcm << " time0=" << time << " theta=" << theta << " psi=" << psi << " logP=" << logP << " q=" << q << " e=" << e << " x=" << x << " Rstar=" << Rstar << " roche_lobe_R=" << roche_lobe_radius << endl;

					//if (period < 15) n_epochs = 15;
					//if (period < 1.5) n_epochs = 10;
					//if (period < 0.3) n_epochs = 5;
					times[0] = time*period;
					times_zero_offset[0] = 0;
					for (j=1; j < n_epochs; j++)
					{
						newtime_years = times[0] + time_intervals[j-1];	// time_intervals is in years
						newtime = newtime_years/period - (int)(newtime_years/period);
						phi = evolve_phi(e,newtime);
						vbin[j] = vbin_los(a,period,e,theta,psi,phi);
						times[j] = newtime_years;
						times_zero_offset[j] = times[j] - times[0];
					}
					break;
				 }
			  // Note calculation starts over if resulting semimajor axis
			  // is disallowed by the stellar radius
			}
			else
			{
				//binary_params_out << "star " << i << ": not a binary" << endl;
				if (fix_absolute_magnitude)
				{
					M = M_bigstar;
					magnitude = abs_magnitude_value;
				}
				else
				{
					for (;;)
					{
						M = (M_bigstar==0) ? gen_m_kroupa() : M_bigstar;
						magnitude = magnitude_from_mass(M);
						if (magnitude == -1e30) continue; // cuts out stars beyond limiting magnitude
						break;
					}
				}	
				for (j=0; j < n_epochs; j++) vbin[j] = 0;
				times_zero_offset[0]=0;
				for (j=1; j < n_epochs; j++)
					times_zero_offset[j] = time_intervals[j-1];	// time_intervals is in years
			}
			for (j=0; j < n_epochs; j++)
			{
				verr = 0;
				if (use_tonry_davis_rvalues) verr += generate_velocity_error_from_rvalue(v_errors[j],rvalue[j],channel[j]);
				else verr += generate_velocity_error(v_errors[j]);
				if (include_systematic_params) {
					for (int kk=0; kk < n_sys_params; kk++) {
						if (sys_param_epoch[kk]==j) {
							if (sysparam_type[kk]==0) {
								verr += sys_param_vals[kk];
							} else {
								verr += sys_param_vals[kk]*randseq.NormalDeviate();
							}
						}
					}
				}
				vlos[j] = vbin[j] + vcm + verr;
				vlos_nb[j] = vcm + verr;
			}

			if (include_metal_widths)
			{
				width_no_error = mean_equivalent_width + equivalent_width_dispersion*randseq.NormalDeviate();
				for (j=0; j < n_epochs; j++)
				{
					width[j] = width_no_error;
					if (equivalent_width_error != 0) width[j] += equivalent_width_error*randseq.NormalDeviate();
				}
			}
			if (include_positions) {
				projected_R = generate_position_from_plummer_profile();
				projected_phi = M_2PI*randseq.RandomNumber2();
			}
		}
		
		if (mem_p < member_fraction)
		{
			vbinfile << abs(vbin[0]) << endl;
			logvbinfile << log10(abs(vbin[0])) << endl;
		}
		vfile << vlos[0] << endl;

		if (include_positions) rfile << projected_R << endl;

		ii = star_id_offset + i;
		if (output_primary_mass) {
			if (include_positions) vdatafile << star_id_prefix << "ep" << n_epochs << "." << ii << " " << M << " " << projected_R << " " << n_epochs << endl;
			else vdatafile << star_id_prefix << "ep" << n_epochs << "." << ii << " " << M << " " << n_epochs << endl;
		} else {
			if (include_positions) {
				vdatafile << star_id_prefix << "ep" << n_epochs << "." << ii << " " << magnitude << " " << projected_R << " " << n_epochs << endl;
				vnbdatafile << star_id_prefix << "ep" << n_epochs << "." << ii << " " << magnitude << " " << projected_R << " " << n_epochs << endl;
			}
			else {
				vdatafile << star_id_prefix << "ep" << n_epochs << "." << ii << " " << magnitude << " " << n_epochs << endl;
				vnbdatafile << star_id_prefix << "ep" << n_epochs << "." << ii << " " << magnitude << " " << n_epochs << endl;
			}
		}
		if (include_metal_widths) {
			for (j=0; j < n_epochs; j++) {
				vdatafile << times_zero_offset[j] << " " << vlos[j] << " " << v_errors[j] << " " << width[j] << " " << equivalent_width_error;
				vnbdatafile << times_zero_offset[j] << " " << vlos_nb[j] << " " << v_errors[j] << " " << width[j] << " " << equivalent_width_error;
				if (use_tonry_davis_rvalues) {
					vdatafile << " " << rvalue[j] << " " << channel[j];
					vnbdatafile << " " << rvalue[j] << " " << channel[j];
				}
				if (include_systematic_params) {
					int output_sysparam_flags = 0;
					for (int kk=0; kk < n_sys_params; kk++) {
						if (sys_param_epoch[kk]==j) {
							vdatafile << " " << kk;
							vnbdatafile << " " << kk;
							output_sysparam_flags++;
						}
					}
					if (output_sysparam_flags > 2) die("cannot have more than two systematic error parameters associated with one epoch");
					while (output_sysparam_flags < 2) {
						vdatafile << " -1"; // indicate that we are not adding one of the systematic error parameters to this epoch
						vnbdatafile << " -1"; // indicate that we are not adding one of the systematic error parameters to this epoch
						output_sysparam_flags++;
					}
				}
				vdatafile << endl;
				vnbdatafile << endl;
			}
		} else {
			for (j=0; j < n_epochs; j++) {
				vdatafile << times_zero_offset[j] << " " << vlos[j] << " " << v_errors[j];
				vnbdatafile << times_zero_offset[j] << " " << vlos_nb[j] << " " << v_errors[j];
				if (include_systematic_params) {
					int output_sysparam_flags = 0;
					for (int kk=0; kk < n_sys_params; kk++) {
						if (sys_param_epoch[kk]==j) {
							vdatafile << " " << kk;
							vnbdatafile << " " << kk;
							output_sysparam_flags++;
						}
					}
					if (output_sysparam_flags > 2) die("cannot have more than two systematic error parameters associated with one epoch");
					while (output_sysparam_flags++ < 2) {
						vdatafile << " -1"; // indicate that we are not adding one of the systematic error parameters to this epoch
						vnbdatafile << " -1"; // indicate that we are not adding one of the systematic error parameters to this epoch
						output_sysparam_flags++;
					}
				}
				vdatafile << endl;
				vnbdatafile << endl;
			}
		}

		if (p < binfrac) { // it's a binary
			if (output_primary_mass) {
				if (include_positions) vdatafile_bins << star_id_prefix << "ep" << n_epochs << "." << ii << " " << M << " " << projected_R << " " << n_epochs << endl;
				else vdatafile_bins << star_id_prefix << "ep" << n_epochs << "." << ii << " " << M << " " << n_epochs << endl;
			} else {
				if (include_positions) vdatafile_bins << star_id_prefix << "ep" << n_epochs << "." << ii << " " << magnitude << " " << projected_R << " " << n_epochs << endl;
				else vdatafile_bins << star_id_prefix << "ep" << n_epochs << "." << ii << " " << magnitude << " " << n_epochs << endl;
			}
			if (include_metal_widths) {
				for (j=0; j < n_epochs; j++)
					vdatafile_bins << times_zero_offset[j] << " " << vlos[j] << " " << v_errors[j] << " " << width[j] << " " << equivalent_width_error << " " << endl;
			} else {
				for (j=0; j < n_epochs; j++)
					vdatafile_bins << times_zero_offset[j] << " " << vlos[j] << " " << v_errors[j] << endl;
			}
			double x = sin(theta)*q*pow(2.0/(1+q),0.66666666666667);
			xvalsfile << x << endl; // The velocity semiamplitude, which will be inferred at star level in hierarchical modeling
		}

		if (use_tonry_davis_rvalues) {
			string channel_str;
			for (j=0; j < n_epochs; j++) {
				double days_zero_offset = 365*times_zero_offset[j];
				if (channel[j]==0) channel_str = "R";
				else channel_str = "B";
				walker_datafile << star_id_prefix << "ep" << n_epochs << "." << ii << " " << channel_str << " " << days_zero_offset << " 0 0 " << magnitude << " 0 0 0 " << vlos[j] << " " << v_errors[j] << " " << width[j] << " " << equivalent_width_error << " " << n_epochs << " 1 " << rvalue[j] << " 1000" << endl;
			}
		}

		if (n_epochs > 1) {
			dv = vlos[1]-vlos[0];
			logdvfile << log10(abs(dv)) << endl;
			dvdatafile << dv << endl;
		}
		if ((n_simulated_stars > 10000) and (i>0) and (i % 100000 == 0)) cerr << i << " stars generated" << endl;
	}
	vdatafile_bins.close();
	ofstream nbin_file("nbins_tmp");
	nbin_file << nbins << endl;
	nbin_file.close();
	string mkvdata_bin_file = "cat nbins_tmp " + vdatafilename_bins + " >" + vdatafilename_bins_final + "; rm nbins_tmp " + vdatafilename_bins;
	system(mkvdata_bin_file.c_str());
	// The following functions are for testing the x-distribution formula which is used in the hierarchical modeling
	/*
	ofstream xdistfile("xdist.dat");
	double xmin,xmax,x,xstep,px;
	int n_xsteps;
	xmin=0;
	xmax=1.0;
	n_xsteps = 100;
	xstep = (xmax-xmin)/(n_xsteps-1);
	for (i=0, x=xmin; i < n_xsteps; i++, x += xstep)
	{
		px = xdist_integral(x);
		xdistfile << x << " " << px << endl;
	}
	*/
}

// The following functions are for testing the x-distribution formula which is used in the hierarchical modeling
double qroot_wcube;

double qroot_equation(const double q)
{
	return qroot_wcube*SQR(1+q) - 4*q*q*q;
}

double find_qroot(const double w)
{
	double q;
	qroot_wcube = CUBE(w);
	q = BrentsMethod(qroot_equation,0,1,1e-5);
	return q;
}

double qdist(const double q)
{
	const double mu=0.5;
	const double sig=0.3;
	double qnorm = (erf((mu)/(M_SQRT2*sig)) + erf((1.0-mu)/(M_SQRT2*sig)))/2;
	return exp(-SQR((q-mu)/sig)/2)/(sig*SQRT_2PI)/qnorm;
}

double xdist_integral_xval;

double xdist_integrand(const double theta)
{
	double q;
	q = find_qroot(xdist_integral_xval/sin(theta));
	return 2*qdist(q)*pow((1+q)/2,5.0/3.0)/(1+q/3.0);
}

double xdist_integral(const double x)
{
	xdist_integral_xval = x;
	double px;
	px = romberg(xdist_integrand,asin(x),M_HALFPI,1e-5,5);
	return px;
}

int star_i;
int size_val;
Spline like_vcm_spline[2];
double like_integrand(double vcm)
{
	return like_vcm_spline[size_val].splint(vcm);
}

double jfactor_test_integrand(double vcm)
{
	return like_vcm_spline[size_val].splint(vcm)*exp(-0.5*SQR((vcm-systemic_velocity)/gdisp))/SQRT_2PI/gdisp;
}

R_Factors::R_Factors(const double distance) // if only velocity differences are used, galaxy dispersion and systemic velocity aren't required
{
	systematic_err = systematic_error;
	galaxy_distance = distance;

	binsize_min = 0.05; // will be scaled by the measurement error
	binsize_max = 0.10; // will be scaled by the measurement error

	binsizes.input(2);

	initial_fractional_accuracy = 2e-3; // the accuracy may vary from star to star
	test_iterations=5; // number of iterations before convergence testing begins
	binsizes_test_iterations=3; // specifies how many of the most recent iterations will we use to compare the different binsizes (to make sure they converge to the same value)
	nonmember_threshold = 80; // stars with velocities that differ from galaxy's systemic velocity by this amount or more are assumed nonmembers
	iteration_threshold = 80;
}

R_Factors::R_Factors(const double vcm_mean_guess, const double vcm_dispersion_guess, const double distance)
{
	systematic_err = systematic_error;
	galaxy_distance = distance;

	// size of vcm window = vcm_window_size*vcm_scale_approx (6.0 accommodates dispersions ~twice)
	vcm_window_size = 6;
	n_tail = 4; // gives the scale (in units of dispersion) at which the tail "takes over", so larger stepsizes are used (v_tail = n_tail*sigm)
	binsize_min = 0.36; // will be scaled by the measurement error
	binsize_max = 0.6; // will be scaled by the measurement error
	vcm_nn_min = 100;

	vcm_mean_approx = vcm_mean_guess;
	vcm_dispersion_approx = vcm_dispersion_guess;

	vcm_min = vcm_mean_approx - vcm_window_size*vcm_dispersion_approx;
	vcm_max = vcm_mean_approx + vcm_window_size*vcm_dispersion_approx;
	initial_vcm_window_divisions = 20;

	binsizes.input(2);
	tail_binsizes.input(2);

	initial_fractional_accuracy = 4e-3; // the accuracy may vary from star to star
	test_iterations=5; // number of iterations before convergence testing begins
	binsizes_test_iterations=3; // specifies how many of the most recent iterations will we use to compare the different binsizes (to make sure they converge to the same value)
	iteration_threshold = 80;
	double max_binary_vlos = 60; // the largest conceivable binary velocity; used to set nonmember_threshold below
	nonmember_threshold = 2*vcm_dispersion_approx + max_binary_vlos; // stars with velocities that differ from galaxy's systemic velocity by more than this amount are assumed nonmembers
}

void R_Factors::set_musig_table_params(const double mumin, const double mumax, const int mu_nn, const double sigmin, const double sigmax, const int sig_nn)
{
	mu_logP_min = mumin;
	mu_logP_max = mumax;
	mu_logP_nn = mu_nn;
	sig_logP_min = sigmin;
	sig_logP_max = sigmax;
	sig_logP_nn = sig_nn;
}

//ofstream paramout;

void R_Factors::generate_jfactors(const string datafile_str, const string rdir, const int n_threads)
{
	//paramout.open((datafile_str+".dists").c_str());
	double clocktime0, wtime;
#ifdef USE_OPENMP
	if (check_wtime) clocktime0 = omp_get_wtime();
#endif

	int i,j,k;
	double primary_mass, stellar_radius, magnitude, abs_magnitude;
	double sigm_no_systematic;
	int n_simulated_stars_proc, n_simulated_stars_thread, nstars_remainder;

	Random_Sequence thread_random_sequence[n_threads];
	int mpi_process_index = mpi_id*n_threads;
	for (k=0; k < n_threads; k++) {
		thread_random_sequence[k].input(seed-mpi_process_index-k);
	}

	string logfilename;
	if ((mpi_id==0) and (output_to_logfile)) {
		logfilename = "log_" + rdir;
		logfile.open(logfilename.c_str());
	}

	string jfactor_filename = "jfactors." + rdir;
	ofstream jfactor_file;
	if (mpi_id==0) jfactor_file.open(jfactor_filename.c_str());

	dv_like.input(2);
	dv_bincount = new int[2];
	dv_bincount_part = new int[2];
	dv_bincount_sum = new int[2];
	dv_bin_area.input(2);
	sq_bin_radii.input(2);

	ifstream datafile(datafile_str.c_str());
	string star_id;
	double wdata, werr, dt, R, offset;
	int nstars;
	datafile >> nstars;
	for (int ii=0; ii < nstars; ii++)
	{
		datafile >> star_id >> magnitude;
		if (include_positions) datafile >> R;
		datafile >> epochs;
		if (epochs==1) die("stars with only one epoch are not allowed in this method");
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "star ID: " << star_id << endl;
			else cout << "star ID: " << star_id << endl;
		}
		initial_n_simulated_stars = (epochs==2) ? 60000 :
									(epochs==3) ? 200000 :
									(epochs==4) ? 600000 : 1000000;

		dvector vbin(epochs), vlos_nb(epochs), vlos_data(epochs);
		dvector time_interval_years;
		ndim = epochs - 1; // number of dv variables (v2-v1, v3-v1, etc.)
		dvector dv(ndim), dv_data(ndim);
		time_interval_years.input(ndim);
		dvector v_errors(epochs), v_errors_no_systematic(epochs);
		double r_value, channel;

		for (j=0; j < epochs; j++)
		{
			datafile >> dt;
			if (j>0) time_interval_years[j-1] = dt;
			datafile >> vlos_data[j] >> v_errors[j];
			if (include_metal_widths) datafile >> wdata >> werr; // we don't use these
			if (include_error_params) datafile >> r_value >> channel;
			if (include_systematic_params) datafile >> offset; // don't use this
			//cout << dt << " " << vlos_data[j] << " " << v_errors[j] << " " << wdata << " " << werr << endl;
		}
		for (j=0; j < ndim; j++)
			dv_data[j] = vlos_data[j+1] - vlos_data[0];

		calculate_verr_no_systematic(vlos_data,v_errors,v_errors_no_systematic);
		calculate_nfactor(vlos_data,v_errors,nfactor,like_nb,sigm_no_systematic);

		// for this method, like_nb is irrelevant; you really should have a separate function that only uses the nfactor to decide accuracy and n_simulated_stars
		determine_dvlike_accuracy_and_n_simulated_stars(nfactor,sigm_no_systematic);
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "nfactor = " << nfactor << endl;
			else cout << "nfactor = " << nfactor << endl;
		}

		if (output_primary_mass) {
			primary_mass = magnitude; // in this mode, the data file is assumed to contain the primary mass directly, not the magnitude
			if (radius_from_isochrone) stellar_radius = find_stellar_radius(primary_mass);
			else stellar_radius = Rstar;
		} else {
			if (magnitude==-99) { abs_magnitude = -99; primary_mass = -99; stellar_radius = -99; }
			else {
				if (galaxy_distance != 1e30)
					abs_magnitude = find_absolute_magnitude(galaxy_distance,magnitude);
				else abs_magnitude = magnitude;
				primary_mass = find_stellar_mass(abs_magnitude);
				if (radius_from_isochrone) stellar_radius = find_stellar_radius(primary_mass);
				else stellar_radius = Rstar;
			}
		}

		for (i=0, size=size_min; i < 2; i++, size += size_step) {
			binsizes[i] = size;
		}
		n_times_increased_binsize = 0;
		n_times_decreased_binsize = 0;

		// the bin "area" is found by using the formula for the volume of a hypersphere
		c_n = pow(M_PI,0.5*ndim) / Gamma(0.5*ndim+1);
		for (k=0; k < 2; k++)
		{
			dv_bin_area[k] = c_n * pow(0.5*binsizes[k],ndim);
			sq_bin_radii[k] = 0.25*SQR(binsizes[k]);
		}

		for (k=0; k < 2; k++) {
			dv_bincount[k]=0;
			dv_bincount_part[k]=0;
			dv_like[k]=0;
		}

		bool converged = false;

		int iterations=0; // start at 0
		int iterations_since_accuracy_reset=0;
		new_binsize = true;
		double jfactor, max_dvlike=0, normsq;
		int n_times_zero_pts_in_bin = 0;
		zero_jfactor = false;
		total_n_simulated_stars=0;
		n_simulated_stars = initial_n_simulated_stars;

		dv_like_vals = new vector<double>[2];

		pthread_t thread[n_threads];
		Thread_Data threads_data[n_threads];
		for (k=0; k < n_threads; k++)
			threads_data[k].input(n_simulated_stars_thread, thread_random_sequence[k], dv_data, v_errors_no_systematic, time_interval_years, sq_bin_radii, magnitude, primary_mass, stellar_radius);

		while (converged==false)
		{
			iterations++;

			iterations_since_accuracy_reset++;
			if (iterations_since_accuracy_reset >= iteration_threshold) {
				// since there are too many iterations, we reduce the required accuracy and increase number of simulated stars; this prevents runaway dvlike calculations taking forever
				if (fractional_accuracy < 0.5) {
					fractional_accuracy *= 2;
					initial_n_simulated_stars *= 2;
					n_simulated_stars *= 2;
				} else {
					initial_n_simulated_stars *= 5;
					n_simulated_stars *= 5;
				}
				iterations_since_accuracy_reset = 0;
			}

			nstars_remainder = n_simulated_stars % mpi_np;
			n_simulated_stars_proc = n_simulated_stars / mpi_np;
			if (nstars_remainder > 0) {
				n_simulated_stars_proc++;
				n_simulated_stars = mpi_np * n_simulated_stars_proc;
			}

			nstars_remainder = n_simulated_stars_proc % n_threads;
			n_simulated_stars_thread = n_simulated_stars_proc / n_threads;
			if (nstars_remainder > 0) {
				n_simulated_stars_thread++;
				n_simulated_stars_proc = n_threads * n_simulated_stars_thread;
				n_simulated_stars = mpi_np * n_simulated_stars_proc;
			}

			for (k=0; k < n_threads; k++) threads_data[k].update(n_simulated_stars_thread, sq_bin_radii);
			for (k=0; k < n_threads; k++) pthread_create(&(thread[k]), NULL, bin_dv_multi_thread, (void*) (threads_data+k));
			for (k=0; k < n_threads; k++) pthread_join(thread[k],NULL);

			// this records the randseq number generator info so they can be taken up by each thread the next time around
			for (k=0; k < n_threads; k++) threads_data[k].output_random_sequence(thread_random_sequence[k]);

			for (k=0; k < 2; k++) {
				dv_bincount_part[k]=0;
			}

			for (k=0; k < n_threads; k++) {
				//cout << "thread " << k << " idum: " << threads_data[k].rand.idum << ", idum2: " << threads_data[k].rand.idum2 << ", bincount: " << threads_data[k].dv_bincount[0] << endl;
				for (j=0; j < 2; j++) {
					dv_bincount_part[j] += threads_data[k].dv_bincount[j];
				}
				// if make_posterior is on, have it output posterior points from all threads to a file (one file for each MPI process), then clear the postpts vectors.
			}
#ifdef USE_MPI
			MPI_Allreduce(dv_bincount_part, dv_bincount_sum, 2, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#else
			for (j=0; j < 2; j++) {
				dv_bincount_sum[j] = dv_bincount_part[j];
			}
#endif

			total_n_simulated_stars += n_simulated_stars;
			for (j=0; j < 2; j++) {
				dv_bincount[j] += dv_bincount_sum[j];
				dv_like[j] = dv_bincount[j] / (2*total_n_simulated_stars*dv_bin_area[j]);
			}

			test_dv_convergence(iterations,converged,false);
			adapt_stepsizes(iterations,iterations_since_accuracy_reset,max_dvlike,n_times_zero_pts_in_bin,converged);
		}

		for (k=0; k < 2; k++)
			if (dv_like[k]==0) dv_like[k] = 1e-30;

		if (mpi_id==0) {
			if (output_to_logfile) logfile << "log_dv_like, dv_like, bprob, nfactor:" << endl;
			else cout << "log_dv_like, dv_like, bprob, nfactor:" << endl;
		}
		for (k=0; k < 2; k++)
		{
			double jfac = dv_like[k]/nfactor;
			if (isinf(jfac)) jfac = dv_like[k]*1e30;
			double bprob = jfac/(1 + jfac);
			if (mpi_id==0) {
				if (output_to_logfile) logfile << log(dv_like[k]) << " " << dv_like[k] << " " << bprob << " " << nfactor << endl;
				else cout << log(dv_like[k]) << " " << dv_like[k] << " " << bprob << " " << nfactor << endl;
			}
		}

		if (mpi_id==0) {
			jfactor = dv_like[0]/nfactor;
			jfactor_file << star_id << " " << jfactor << " " << nfactor << " " << dv_like[0] << endl;
			if (output_to_logfile) logfile << endl;
			else cout << endl;
		}
		if (dv_like[0] > max_dvlike) max_dvlike = dv_like[0];
		delete[] dv_like_vals;
	}
#ifdef USE_OPENMP
	if ((mpi_id==0) and (check_wtime)) {
		wtime = omp_get_wtime() - clocktime0;
		if (output_to_logfile) logfile << "total wall time: " << wtime << endl;
		else cout << "total wall time: " << wtime << endl;
	}
#endif
	if ((mpi_id==0) and (output_to_logfile)) {
		logfile.close();
		string mvlog_command = "cp " + logfilename + " dvlike_log_finished/";
		system(mvlog_command.c_str());
		remove(logfilename.c_str());
	}
	delete[] dv_bincount;
	delete[] dv_bincount_sum;
	delete[] dv_bincount_part;
	//paramout.close();
}

void R_Factors::generate_jtables(const string datafile_str, const string rdir, const int n_threads)
{
	double clocktime0, wtime;
#ifdef USE_OPENMP
	if (check_wtime) clocktime0 = omp_get_wtime();
#endif

	int i,j,k;
	double primary_mass, stellar_radius, magnitude, abs_magnitude;
	double sigm_no_systematic;
	int n_simulated_stars_proc, n_simulated_stars_thread, nstars_remainder;

	Random_Sequence thread_random_sequence[n_threads];
	int mpi_process_index = mpi_id*n_threads;
	for (k=0; k < n_threads; k++) {
		thread_random_sequence[k].input(seed-mpi_process_index-k);
	}

	string logfilename;
	if (output_to_logfile) {
		logfilename = "log_" + rdir;
		logfile.open(logfilename.c_str());
	}

	string nstarfilename = "jtables." + rdir + "/nstars";

	double sig_logP_step;
	if (sig_logP_nn > 1)
		sig_logP_step = (sig_logP_max-sig_logP_min)/(sig_logP_nn-1);
	else sig_logP_step = 0;

	double mu_logP_step = (mu_logP_max-mu_logP_min)/(mu_logP_nn-1);
	dvector mu_logP_vals(mu_logP_nn), sig_logP_vals(sig_logP_nn);
	int jj, kk;
	for (jj=0, mu_logP=mu_logP_min; jj < mu_logP_nn; jj++, mu_logP += mu_logP_step)
		mu_logP_vals[jj] = mu_logP;
	for (kk=0, sig_logP=sig_logP_min; kk < sig_logP_nn; kk++, sig_logP += sig_logP_step)
		sig_logP_vals[kk] = sig_logP;

	string musig_table_filename = "jtables." + rdir + "/musig_table";
	if (mpi_id==0) {
		ofstream musig_file;
		musig_file.open(musig_table_filename.c_str());
		musig_file << mu_logP_nn << endl;
		for (jj=0; jj < mu_logP_nn; jj++)
			musig_file << mu_logP_vals[jj] << endl;
		musig_file << endl;
		musig_file << sig_logP_nn << endl;
		for (kk=0; kk < sig_logP_nn; kk++)
			musig_file << sig_logP_vals[kk] << endl;
		musig_file.close();
	}

	dv_like.input(2);
	dv_bincount_part = new int[2];
	dv_bincount_sum = new int[2];
	dv_bincount = new int[2];
	dv_bin_area.input(2);
	sq_bin_radii.input(2);

	  // Generate n stars
	ifstream datafile(datafile_str.c_str());
	string star_id;
	double wdata, werr, dt, R;
	int nstars;
	datafile >> nstars;
	for (int ii=0; ii < nstars; ii++)
	{
		datafile >> star_id >> magnitude;
		if (include_positions) datafile >> R;
		datafile >> epochs;
		if (epochs==1) die("stars with only one epoch are not allowed in this method");
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "star ID: " << star_id << endl;
			else cout << "star ID: " << star_id << endl;
		}
		initial_n_simulated_stars = (epochs==2) ? 60000 :
									(epochs==3) ? 200000 :
									(epochs==4) ? 600000 : 1000000;

		dvector vbin(epochs), vlos_nb(epochs), vlos_data(epochs);
		dvector time_interval_years;
		ndim = epochs - 1; // number of dv variables (v2-v1, v3-v1, etc.)
		dvector dv(ndim), dv_data(ndim);
		time_interval_years.input(ndim);
		dvector v_errors(epochs), v_errors_no_systematic(epochs);
		double r_value, channel, offset;

		for (j=0; j < epochs; j++)
		{
			datafile >> dt;
			if (j>0) time_interval_years[j-1] = dt;
			datafile >> vlos_data[j] >> v_errors[j];
			if (include_metal_widths) datafile >> wdata >> werr; // we don't use these
			if (include_error_params) datafile >> r_value >> channel;
			if (include_systematic_params) datafile >> offset; // don't use this
		}
		for (j=0; j < ndim; j++)
			dv_data[j] = vlos_data[j+1] - vlos_data[0];

		calculate_verr_no_systematic(vlos_data,v_errors,v_errors_no_systematic);
		calculate_nfactor(vlos_data,v_errors,nfactor,like_nb,sigm_no_systematic);

		string jtable_filename = "jtables." + rdir + "/jtable." + star_id;
		string jtable_plot_filename = "jtables." + rdir + "/jtable_plot." + star_id;
		ofstream jtable_file;
		ofstream jtable_plot;
		if (mpi_id==0) {
			jtable_file.open(jtable_filename.c_str());
			jtable_plot.open(jtable_plot_filename.c_str());
		}
		
		// for this method, like_nb is irrelevant; you really should have a separate function that only uses the nfactor to decide accuracy and n_simulated_stars
		determine_dvlike_accuracy_and_n_simulated_stars(nfactor,sigm_no_systematic);
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "nfactor = " << nfactor << endl;
			else cout << "nfactor = " << nfactor << endl;
		}

		if (galaxy_distance != 1e30)
			abs_magnitude = find_absolute_magnitude(galaxy_distance,magnitude);
		else abs_magnitude = magnitude;
		primary_mass = find_stellar_mass(abs_magnitude);
		stellar_radius = find_stellar_radius(primary_mass);

		bool converged;

		int iterations;
		int iterations_since_accuracy_reset;
		double jfactor, max_dvlike=0, normsq;
		int n_times_zero_pts_in_bin = 0;
		zero_jfactor = false;

		for (jj=0; jj < mu_logP_nn; jj++)
		{
			mu_logP = mu_logP_vals[jj];
			for (kk=0; kk < sig_logP_nn; kk++)
			{
				sig_logP = sig_logP_vals[kk];

				if (zero_jfactor==true) {
					if (mpi_id==0) {
						jtable_file << "1e-30" << endl;
						jtable_plot << mu_logP << " " << sig_logP << " 1e-30 " << nfactor << " 1e-30" << endl;
					}
					zero_jfactor = false;
					continue;
				}

				dv_like_vals = new vector<double>[2];
				for (k=0; k < 2; k++) {
					dv_bincount[k]=0;
					dv_bincount_part[k]=0;
					dv_like[k]=0;
				}

				for (i=0, size=size_min; i < 2; i++, size += size_step)
					binsizes[i] = size;
				n_times_increased_binsize = 0;
				n_times_decreased_binsize = 0;

				// the bin "area" is found by using the formula for the volume of a hypersphere
				c_n = pow(M_PI,0.5*ndim) / Gamma(0.5*ndim+1);
				for (k=0; k < 2; k++)
				{
					dv_bin_area[k] = c_n * pow(0.5*binsizes[k],ndim);
					sq_bin_radii[k] = 0.25*SQR(binsizes[k]);
				}

				converged = false;
				iterations = 0;
				iterations_since_accuracy_reset = 0;
				new_binsize = true;
				total_n_simulated_stars = 0;
				// below, we set to initial_n_simulated_stars because different binsizes may be used for different period param's, and thus n_simulated_stars may be different
				n_simulated_stars = initial_n_simulated_stars;

				pthread_t thread[n_threads];
				Thread_Data threads_data[n_threads];
				for (k=0; k < n_threads; k++)
					threads_data[k].input(n_simulated_stars_thread, thread_random_sequence[k], dv_data, v_errors_no_systematic, time_interval_years, sq_bin_radii, magnitude, primary_mass, stellar_radius);

				int total_n_simulated_stars_thread = 0;
				while (converged==false)
				{
					iterations++;

					iterations_since_accuracy_reset++;
					if (iterations_since_accuracy_reset >= iteration_threshold) {
						// since there are too many iterations, we reduce the required accuracy and increase number of simulated stars; this prevents runaway dvlike calculations taking forever
						if (fractional_accuracy < 0.5) {
							fractional_accuracy *= 2;
							initial_n_simulated_stars *= 2;
							n_simulated_stars *= 2;
						} else {
							initial_n_simulated_stars *= 5;
							n_simulated_stars *= 5;
						}
						iterations_since_accuracy_reset = 0;
					}

					nstars_remainder = n_simulated_stars % mpi_np;
					n_simulated_stars_proc = n_simulated_stars / mpi_np;
					if (nstars_remainder > 0) {
						n_simulated_stars_proc++;
						n_simulated_stars = mpi_np * n_simulated_stars_proc;
					}

					nstars_remainder = n_simulated_stars_proc % n_threads;
					n_simulated_stars_thread = n_simulated_stars_proc / n_threads;
					if (nstars_remainder > 0) {
						n_simulated_stars_thread++;
						n_simulated_stars_proc = n_threads * n_simulated_stars_thread;
						n_simulated_stars = mpi_np * n_simulated_stars_proc;
					}

					for (k=0; k < n_threads; k++) threads_data[k].update(n_simulated_stars_thread, sq_bin_radii);
					for (k=0; k < n_threads; k++) pthread_create(&(thread[k]), NULL, bin_dv_multi_thread, (void*) (threads_data+k));
					for (k=0; k < n_threads; k++) pthread_join(thread[k],NULL);

					// this records the randseq number generator info so they can be taken up by each thread the next time around
					for (k=0; k < n_threads; k++) threads_data[k].output_random_sequence(thread_random_sequence[k]);

					for (k=0; k < 2; k++) {
						dv_bincount_part[k]=0;
					}
					for (k=0; k < n_threads; k++) {
						//cout << "thread " << k << " idum: " << threads_data[k].rand.idum << ", bincount: " << threads_data[k].dv_bincount[0] << endl;
						for (j=0; j < 2; j++)
							dv_bincount_part[j] += threads_data[k].dv_bincount[j];
					}
#ifdef USE_MPI
					MPI_Allreduce(dv_bincount_part, dv_bincount_sum, 2, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
#else
					for (j=0; j < 2; j++) {
						dv_bincount_sum[j] = dv_bincount_part[j];
					}
#endif
					total_n_simulated_stars += n_simulated_stars;

					double comp;
					for (j=0; j < 2; j++) {
						dv_bincount[j] += dv_bincount_sum[j];
						dv_like[j] = dv_bincount[j] / (2*total_n_simulated_stars*dv_bin_area[j]);
					}

					test_dv_convergence(iterations,converged,true);
					adapt_stepsizes(iterations,iterations_since_accuracy_reset,max_dvlike,n_times_zero_pts_in_bin,converged);
				}

				if ((dv_like[0]==0) and (jj > 1) and (max_dvlike <= 1e-30)) {
					zero_jfactor = true; // if we've gone far enough in the grid and the likelihood is still zero, then we admit we're just not resolving the likelihood and give up
					if (mpi_id==0) {
						if (output_to_logfile) logfile << "The likelihood is still zero, so we're giving up and outputting zero for all (mu_logP,sig_logP) values.\n\n";
						else cout << "The likelihood is still zero, so we're giving up and outputting zero for all (mu_logP,sig_logP) values.\n\n";
						cerr << "Warning: The likelihood for star " << star_id << " is still zero, so we're giving up and outputting zero for all (mu_logP,sig_logP) values.\n\n";
					}
				}

				for (k=0; k < 2; k++)
					if (dv_like[k]==0) dv_like[k] = 1e-30;

				if (mpi_id==0) {
					if (output_to_logfile) logfile << "dv_like, bprob, nfactor:" << endl;
					else cout << "dv_like, bprob, nfactor:" << endl;
				}
				for (k=0; k < 2; k++)
				{
					double jfac = dv_like[k]/nfactor;
					double bprob = jfac/(1 + jfac);
					if (mpi_id==0) {
						if (output_to_logfile) logfile << dv_like[k] << " " << bprob << " " << nfactor << endl;
						else cout << dv_like[k] << " " << bprob << " " << nfactor << endl;
					}
				}

				jfactor = dv_like[0]/nfactor;
				if (mpi_id==0) {
					jtable_file << jfactor << endl;
					jtable_plot << mu_logP << " " << sig_logP << " " << jfactor << " " << nfactor << " " << dv_like[0] << endl;
				}
				if (dv_like[0] > max_dvlike) max_dvlike = dv_like[0];
				
				if (mpi_id==0) {
					if (output_to_logfile) logfile << endl;
					else cout << endl;
				}
				delete[] dv_like_vals;
			}
		}
		if (mpi_id==0) output_nstars(nstarfilename,ii);

	}
#ifdef USE_OPENMP
	if ((mpi_id==0) and (check_wtime)) {
		wtime = omp_get_wtime() - clocktime0;
		if (output_to_logfile) logfile << "total wall time: " << wtime << endl;
		else cout << "total wall time: " << wtime << endl;
	}
#endif
	if ((mpi_id==0) and (output_to_logfile)) {
		logfile.close();
		string mvlog_command = "cp " + logfilename + " dvlike_log_finished/";
		system(mvlog_command.c_str());
		remove(logfilename.c_str());
	}
	delete[] dv_bincount;
	delete[] dv_bincount_part;
	delete[] dv_bincount_sum;
}

void R_Factors::generate_rfactors(const string datafile_str, const string rdir, const int n_threads)
{
	double clocktime0, wtime;
#ifdef USE_OPENMP
	if (check_wtime) clocktime0 = omp_get_wtime();
#endif
	int i,j,k,l;
	double primary_mass, stellar_radius, magnitude, abs_magnitude;
	double sigm_no_systematic;

	Random_Sequence thread_random_sequence[n_threads];
	int mpi_process_index = mpi_id*n_threads;
	for (k=0; k < n_threads; k++) {
		thread_random_sequence[k].input(seed-mpi_process_index-k);
	}

	string logfilename;
	if (output_to_logfile) {
		logfilename = "log_" + rdir;
		logfile.open(logfilename.c_str());
	}

	sq_bin_radii.input(2);
	sq_bin_radii_tail.input(2);
	int n_simulated_stars_proc, n_simulated_stars_thread, nstars_remainder;

	  // Generate n stars
	ifstream datafile(datafile_str.c_str());
	string rnormfilename = "rfactors." + rdir + "/rnorms";
	string nstarfilename = "rfactors." + rdir + "/nstars";
	ofstream rnorm_file;
	ofstream nstar_file;
	
	if (mpi_id==0) {
		rnorm_file.open(rnormfilename.c_str());
		nstar_file.open(nstarfilename.c_str());
	}
	string star_id;
	double wdata, werr, dt, R;
	int nstars;
	bool nonmember;
	datafile >> nstars;
	for (int ii=0; ii < nstars; ii++)
	{
		nonmember = false; // assume first it's a member; this will be set to zero only if the jfactors calculated are exactly zero (no points binned)

		datafile >> star_id >> magnitude;
		if (include_positions) datafile >> R;
		datafile >> epochs;
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "star ID: " << star_id << endl;
			else cout << "star ID: " << star_id << endl;
		}
		n_simulated_stars = (epochs==1) ? 10000 :
									(epochs==2) ? 20000 :
									(epochs==3) ? 200000 : 800000;

		dvector vbin(epochs), vlos_nb(epochs), vlos_data(epochs);
		dvector times(epochs);
		dvector time_interval_years;
		if (epochs > 1)
			time_interval_years.input(epochs-1);
		else time_interval_years.input(1);
		dvector v_errors(epochs), v_errors_no_systematic(epochs);
		double r_value, channel, offset;

		for (j=0; j < epochs; j++)
		{
			datafile >> dt;
			if (j>0) time_interval_years[j-1] = dt;
			datafile >> vlos_data[j] >> v_errors[j];
			if (include_metal_widths) datafile >> wdata >> werr; // we don't use these
			if (include_error_params) datafile >> r_value >> channel;
			if (include_systematic_params) datafile >> offset; // don't use this
		}

		string rfilename = "rfactors." + rdir + "/rfactor." + star_id;
		string like_nb_filename = "rfactors." + rdir + "/like_nb." + star_id;

		calculate_verr_no_systematic(vlos_data,v_errors,v_errors_no_systematic);
		calculate_nfactor(vlos_data,v_errors,nfactor,like_nb,sigm_no_systematic);

		use_large_tail_bins = false; // I will probably get rid of the tail-bin option altogether, but I'm keeping it around just in case I decide to revive it
		determine_accuracy_and_n_simulated_stars(like_nb,nfactor,sigm_no_systematic);
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "nfactor = " << nfactor << " like_nb = " << like_nb << " vcm_nn = " << vcm_nn << endl;
			else cout << "nfactor = " << nfactor << " like_nb = " << like_nb << " vcm_nn = " << vcm_nn << endl;
		}

		// output rfactor without binaries, for testing purposes
		string rfactor_nb_filename = rfilename + ".nb";
		ofstream rfile_nb;
		if (mpi_id==0) rfile_nb.open(rfactor_nb_filename.c_str());
		double rfactor_nb;
		for (k=0, vcm=vcm_min; k < vcm_nn; k++, vcm += vcm_step)
		{
			rfactor_nb = exp(-0.5*SQR((star_v_avg-vcm)/sigm_no_systematic))/sigm_no_systematic/SQRT_2PI;
			if (mpi_id==0) rfile_nb << vcm << " " << rfactor_nb << endl;
		}

		for (i=0, size=size_min; i < 2; i++, size += size_step) {
			binsizes[i] = size;
			tail_binsizes[i] = (use_large_tail_bins==true) ? 2*size : size;
		}

		if (abs(star_v_avg-vcm_mean_approx) > nonmember_threshold) // star too far away in velocity-space to be binary, so it is not a member
		{
			if (mpi_id==0) {
				output_rfactor_files(rfilename,false); // output zero rfactors
				rnorm_file << star_id << " " << nfactor << " 0\n";
				if (output_to_logfile) logfile << "j=0\n\n";
				else cout << "j=0\n\n";
			}
			continue;
		}

		for (k=0; k < 2; k++) {
			vcm_like[k].input(vcm_nn);
			vcm_like[k]=0;
			vcm_bincount[k] = new int[vcm_nn];
			vcm_bincount_part[k] = new int[vcm_nn];
			vcm_bincount_sum[k] = new int[vcm_nn];
			for (j=0; j < vcm_nn; j++) {
				 vcm_bincount[k][j] = 0;
				 vcm_bincount_part[k][j] = 0;
			}
		}

		// the bin "area" is found by using the formula for the volume of a hypersphere
		c_n = pow(M_PI,0.5*epochs) / Gamma(0.5*epochs+1);
		for (k=0; k < 2; k++)
		{
			vcm_bin_area[k] = c_n * pow(0.5*binsizes[k],epochs);
			vcm_bin_area_tail[k] = c_n * pow(0.5*tail_binsizes[k],epochs);
			sq_bin_radii[k] = 0.25*SQR(binsizes[k]);
			sq_bin_radii_tail[k] = 0.25*SQR(tail_binsizes[k]);
		}

		if (galaxy_distance != 1e30)
			abs_magnitude = find_absolute_magnitude(galaxy_distance,magnitude);
		else abs_magnitude = magnitude;
		primary_mass = find_stellar_mass(abs_magnitude);
		stellar_radius = find_stellar_radius(primary_mass);

		bool converged = false;

		int iterations=0; // start at 0
		int iterations_since_accuracy_reset=0;
		double normsq;
		vcm_vals.input(vcm_nn);
		for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step) vcm_vals[i] = vcm;
		jvals = new vector<double>[2];

		// determine indices for which the tail takes over
		double vdist_left, vdist_right;
		vdist_left = 1e30;
		vdist_right = 1e30;
		int ktail_left = -1, ktail_right = vcm_nn;
		for (k=0, vcm=vcm_min; k < vcm_nn; k++, vcm += vcm_step)
		{
			if (vcm < star_v_avg - n_tail*sigm_no_systematic) {
				if (abs(vcm-star_v_avg) < vdist_left) { ktail_left = k; vdist_left = abs(vcm-star_v_avg); }
			} else if (vcm > star_v_avg + n_tail*sigm_no_systematic) {
				if (abs(vcm-star_v_avg) < vdist_right) { ktail_right = k; vdist_right = abs(vcm-star_v_avg); }
			}
		}

		total_n_simulated_stars=0;

		pthread_t thread[n_threads];
		Thread_Data threads_data[n_threads];
		for (k=0; k < n_threads; k++)
			threads_data[k].input(n_simulated_stars_thread, thread_random_sequence[k], vlos_data, v_errors_no_systematic, time_interval_years, sq_bin_radii, magnitude, primary_mass, stellar_radius, true, vcm_nn, vcm_min, vcm_step, ktail_left, ktail_right, 0, NULL, 0);

		while (converged==false)
		{
			iterations++;
			iterations_since_accuracy_reset++;
			if (iterations_since_accuracy_reset >= 40) {
				// since there are too many iterations, we reduce the required accuracy and increase number of simulated stars; this prevents runaway rfactor calculations taking forever
				if (fractional_accuracy < 0.5) {
					fractional_accuracy *= 5;
					n_simulated_stars *= 2;
				} else {
					n_simulated_stars *= 5;
				}
				iterations_since_accuracy_reset = 0;
			}

			nstars_remainder = n_simulated_stars % mpi_np;
			n_simulated_stars_proc = n_simulated_stars / mpi_np;
			if (nstars_remainder > 0) {
				n_simulated_stars_proc++;
				n_simulated_stars = mpi_np * n_simulated_stars_proc;
			}

			nstars_remainder = n_simulated_stars_proc % n_threads;
			n_simulated_stars_thread = n_simulated_stars_proc / n_threads;
			if (nstars_remainder > 0) {
				n_simulated_stars_thread++;
				n_simulated_stars_proc = n_threads * n_simulated_stars_thread;
				n_simulated_stars = mpi_np * n_simulated_stars_proc;
			}

			for (k=0; k < n_threads; k++) threads_data[k].update(n_simulated_stars_thread, sq_bin_radii, sq_bin_radii_tail);
			for (k=0; k < n_threads; k++) pthread_create(&(thread[k]), NULL, bin_vcm_multi_thread, (void*) (threads_data+k));
			for (k=0; k < n_threads; k++) pthread_join(thread[k],NULL);

			// this records the randseq number generator info so they can be taken up by each thread the next time around
			for (k=0; k < n_threads; k++) threads_data[k].output_random_sequence(thread_random_sequence[k]);

			for (k=0; k < 2; k++) {
				for (j=0; j < vcm_nn; j++) {
					 vcm_bincount_part[k][j] = 0;
				}
			}

			for (k=0; k < n_threads; k++) {
				for (l=0; l < 2; l++) {
					for (j=0; j < vcm_nn; j++) {
						vcm_bincount_part[l][j] += threads_data[k].vcm_bincount[l][j];
					}
				}
			}
#ifdef USE_MPI
			for (l=0; l < 2; l++) {
				MPI_Allreduce(vcm_bincount_part[l], vcm_bincount_sum[l], vcm_nn, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
			}
#else
			for (l=0; l < 2; l++) {
				for (j=0; j < vcm_nn; j++) {
					vcm_bincount_sum[l][j] = vcm_bincount_part[l][j];
				}
			}
#endif

			total_n_simulated_stars += n_simulated_stars;
			for (l=0; l < 2; l++) {
				for (j=0; j < vcm_nn; j++) {
					vcm_bincount[l][j] += vcm_bincount_sum[l][j];
				}
			}

			calculate_normalized_likelihood();
			test_convergence(iterations,converged,nonmember,false);
			if (mpi_id==0) output_rfactor_files(rfilename,true);
		}

		if (nonmember==false) {
			if (mpi_id==0) {
				if (output_to_logfile) logfile << "jfac, bprob, nfactor:" << endl;
				else cout << "jfac, bprob, nfactor:" << endl;
			}
			for (k=0; k < 2; k++)
			{
				size_val = k;
				double jint_test = romberg_nocrash(jfactor_test_integrand,vcm_min,vcm_max,1e-5,5);
				double jfac = jint_test/nfactor;
				double bprob = jfac/(like_nb + jfac);
				if (mpi_id==0) {
					if (output_to_logfile) logfile << jfac << " " << bprob << " " << nfactor << endl;
					else cout << jfac << " " << bprob << " " << nfactor << endl;
				}
			}
		}
		if (mpi_id==0) {
			rnorm_file << star_id << " " << nfactor << " " << jvals[1][iterations-1] << endl;
			nstar_file.open(nstarfilename.c_str());
			nstar_file << ii+1 << endl;	// this keeps track of how many stars are done so far, so incomplete data sets can be analyzed
			nstar_file.close();
			if (output_to_logfile) logfile << endl;
			else cout << endl;
		}
		delete[] jvals;
	}
#ifdef USE_OPENMP
	if ((mpi_id==0) and (check_wtime)) {
		wtime = omp_get_wtime() - clocktime0;
		if (output_to_logfile) logfile << "total wall time: " << wtime << endl;
		else cout << "total wall time: " << wtime << endl;
	}
#endif
	if ((mpi_id==0) and (output_to_logfile)) {
		logfile.close();
		string mvlog_command = "cp " + logfilename + " rfactor_log_finished/";
		system(mvlog_command.c_str());
		remove(logfilename.c_str());
	}
	for (k=0; k < 2; k++) {
		delete[] vcm_bincount[k];
		delete[] vcm_bincount_part[k];
		delete[] vcm_bincount_sum[k];
	}
}

void R_Factors::generate_rfactors_sysparams(const string datafile_str, const string rdir, const int n_threads)
{
	double clocktime0, wtime;
#ifdef USE_OPENMP
	if (check_wtime) clocktime0 = omp_get_wtime();
#endif
	int i,j,k,l,m,n,p;
	double primary_mass, stellar_radius, magnitude, abs_magnitude;

	Random_Sequence thread_random_sequence[n_threads];
	int mpi_process_index = mpi_id*n_threads;
	for (k=0; k < n_threads; k++) {
		thread_random_sequence[k].input(seed-mpi_process_index-k);
	}

	string logfilename;
	if (output_to_logfile) {
		logfilename = "log_" + rdir;
		if (mpi_id==0) logfile.open(logfilename.c_str());
	}

	sq_bin_radii.input(2);
	sq_bin_radii_tail.input(2);
	int n_simulated_stars_proc, n_simulated_stars_thread, nstars_remainder;

	  // Generate n stars
	ifstream datafile(datafile_str.c_str());
	string nstarfilename = "rltables." + rdir + "/nstars";
	ofstream nstar_file;
	
	if (mpi_id==0) {
		nstar_file.open(nstarfilename.c_str());
	}
	string star_id;
	double wdata, werr, dt, R;
	int nstars;
	bool nonmember;
	int n_sysparam_star;
	int like_i, likeval_i, n_likevals, nlikes;
	datafile >> nstars;
	for (int ii=0; ii < nstars; ii++)
	{
		nonmember = false; // assume first it's a member; this will be set to zero only if the jfactors calculated are exactly zero (no points binned)

		datafile >> star_id >> magnitude;
		if (include_positions) datafile >> R;
		datafile >> epochs;
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "star ID: " << star_id << endl;
			else cout << "star ID: " << star_id << endl;
		}
		if (mpi_id==0) {
			string mkdirstring = "mkdir rltables." + rdir + "/rltable." + star_id;
			system(mkdirstring.c_str());
		}

		n_simulated_stars = (epochs==1) ? 10000 :
									(epochs==2) ? 20000 :
									(epochs==3) ? 200000 : 800000;

		dvector vbin(epochs), vlos_nb(epochs), vlos_data(epochs);
		imatrix sysparam_i(epochs,2);
		dvector times(epochs);
		dvector time_interval_years;
		if (epochs > 1)
			time_interval_years.input(epochs-1);
		else time_interval_years.input(1);
		dvector v_errors(epochs), v_errors_no_systematic(epochs);
		double r_value, channel;

		n_sysparam_star = 0;
		for (j=0; j < epochs; j++)
		{
			datafile >> dt;
			if (j>0) time_interval_years[j-1] = dt;
			datafile >> vlos_data[j] >> v_errors[j];
			if (include_metal_widths) datafile >> wdata >> werr; // we don't use these
			if (include_error_params) datafile >> r_value >> channel;
			for (int kk=0; kk < 2; kk++) {
				datafile >> sysparam_i[j][kk];
				if (sysparam_i[j][kk] != -1) {
					if (n_sysparam_star==0) {
						n_sysparam_star++;
						sysparam1 = sysparam_i[j][kk];
					} else if (n_sysparam_star==1) {
						if (sysparam_i[j][kk] != sysparam1) {
							n_sysparam_star++;
							sysparam2 = sysparam_i[j][kk];
						}
					} else if (n_sysparam_star==2) {
						if ((sysparam_i[j][kk] != sysparam1) and (sysparam_i[j][kk] != sysparam2)) {
							n_sysparam_star++;
							sysparam3 = sysparam_i[j][kk];
						}
					} else if (n_sysparam_star==3) {
						if ((sysparam_i[j][kk] != sysparam1) and (sysparam_i[j][kk] != sysparam2) and (sysparam_i[j][kk] != sysparam3)) die("Only a maximum of three sysparams are currently allowed");
					}
				}
			}
		}

		calculate_verr_no_systematic(vlos_data,v_errors,v_errors_no_systematic);

		nlikes = 1;
		if (n_sysparam_star>=1) nlikes *= sysparam_nn[sysparam1];
		if (n_sysparam_star>=2) nlikes *= sysparam_nn[sysparam2];
		if (n_sysparam_star==3) nlikes *= sysparam_nn[sysparam3];
		nfactor_sysparam = new double[nlikes];
		like_nb_sysparam = new double[nlikes];
		double *sigm_no_systematic = new double[nlikes];

		dvector vlos_sysparam(epochs);
		dvector verr_sysparam(epochs);
		if (n_sysparam_star==0) {
			calculate_nfactor(vlos_data,v_errors,nfactor_sysparam[0],like_nb_sysparam[0],sigm_no_systematic[0]);
		} else if (n_sysparam_star==1) {
			double lambda1;
			for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
				for (j=0; j < epochs; j++) {
					vlos_sysparam[j] = vlos_data[j];
					verr_sysparam[j] = v_errors[j];
					for (int kk=0; kk < 2; kk++) {
						if (sysparam_i[j][kk]==sysparam1) {
							if (sysparam_type[sysparam1]==0) vlos_sysparam[j] -= lambda1;
							else verr_sysparam[j] = sqrt(SQR(verr_sysparam[j])+lambda1*lambda1);
						}
					}
				}
				calculate_nfactor(vlos_sysparam,verr_sysparam,nfactor_sysparam[m],like_nb_sysparam[m],sigm_no_systematic[m]);
			}
			if (mpi_id==0) {
				if (output_to_logfile) logfile << "nfactors: ";
				else cout << "nfactors: ";
				for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
					if (output_to_logfile) logfile << nfactor_sysparam[m] << " ";
					else cout << nfactor_sysparam[m] << " ";
				}
				if (output_to_logfile) logfile << endl;
				else cout << endl;
			}
		} else if (n_sysparam_star==2) {
			double lambda1, lambda2, normsq_term;
			like_i=0;
			for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
				for (n=0, lambda2=sys_param_min[sysparam2]; n < sysparam_nn[sysparam2]; n++, lambda2 += sys_param_step[sysparam2]) {
					for (j=0; j < epochs; j++) {
						vlos_sysparam[j] = vlos_data[j];
						verr_sysparam[j] = v_errors[j];
						for (int kk=0; kk < 2; kk++) {
							if (sysparam_i[j][kk]==sysparam1) {
								if (sysparam_type[sysparam1]==0) vlos_sysparam[j] -= lambda1;
								else verr_sysparam[j] = sqrt(SQR(verr_sysparam[j])+lambda1*lambda1);
							} else if (sysparam_i[j][kk]==sysparam2) {
								if (sysparam_type[sysparam2]==0) vlos_sysparam[j] -= lambda2;
								else verr_sysparam[j] = sqrt(SQR(verr_sysparam[j])+lambda2*lambda2);
							}
						}
					}
					calculate_nfactor(vlos_sysparam,verr_sysparam,nfactor_sysparam[like_i],like_nb_sysparam[like_i],sigm_no_systematic[like_i]);
					like_i++;
				}
			}
			if (mpi_id==0) {
				like_i=0;
				if (output_to_logfile) logfile << "nfactors: ";
				else cout << "nfactors: ";
				for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
					for (n=0, lambda2=sys_param_min[sysparam2]; n < sysparam_nn[sysparam2]; n++, lambda2 += sys_param_step[sysparam2]) {
						if (output_to_logfile) logfile << nfactor_sysparam[like_i] << " ";
						else cout << nfactor_sysparam[like_i] << " ";
						like_i++;
					}
					if (output_to_logfile) logfile << endl;
					else cout << endl;
				}
			}
		} else if (n_sysparam_star==3) {
			double lambda1, lambda2, lambda3, normsq_term;
			like_i=0;
			for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
				for (n=0, lambda2=sys_param_min[sysparam2]; n < sysparam_nn[sysparam2]; n++, lambda2 += sys_param_step[sysparam2]) {
					for (l=0, lambda3=sys_param_min[sysparam3]; l < sysparam_nn[sysparam3]; l++, lambda3 += sys_param_step[sysparam3]) {
						for (j=0; j < epochs; j++) {
							vlos_sysparam[j] = vlos_data[j];
							verr_sysparam[j] = v_errors[j];
							for (int kk=0; kk < 2; kk++) {
								if (sysparam_i[j][kk]==sysparam1) {
									if (sysparam_type[sysparam1]==0) vlos_sysparam[j] -= lambda1;
									else verr_sysparam[j] = sqrt(SQR(verr_sysparam[j])+lambda1*lambda1);
								} else if (sysparam_i[j][kk]==sysparam2) {
									if (sysparam_type[sysparam2]==0) vlos_sysparam[j] -= lambda2;
									else verr_sysparam[j] = sqrt(SQR(verr_sysparam[j])+lambda2*lambda2);
								} else if (sysparam_i[j][kk]==sysparam3) {
									if (sysparam_type[sysparam3]==0) vlos_sysparam[j] -= lambda3;
									else verr_sysparam[j] = sqrt(SQR(verr_sysparam[j])+lambda3*lambda3);
								}
							}
						}
						calculate_nfactor(vlos_sysparam,verr_sysparam,nfactor_sysparam[like_i],like_nb_sysparam[like_i],sigm_no_systematic[like_i]);
						like_i++;
					}
				}
			}
			like_i=0;
			if (mpi_id==0) {
				if (output_to_logfile) logfile << "nfactors: ";
				else cout << "nfactors: ";
				for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
					for (n=0, lambda2=sys_param_min[sysparam2]; n < sysparam_nn[sysparam2]; n++, lambda2 += sys_param_step[sysparam2]) {
						for (l=0, lambda3=sys_param_min[sysparam3]; l < sysparam_nn[sysparam3]; l++, lambda3 += sys_param_step[sysparam3]) {
							if (output_to_logfile) logfile << nfactor_sysparam[like_i] << " ";
							else cout << nfactor_sysparam[like_i] << " ";
							like_i++;
						}
					}
					if (output_to_logfile) logfile << endl;
					else cout << endl;
				}
			}
		}


		use_large_tail_bins = false; // I will probably get rid of the tail-bin option altogether, but I'm keeping it around just in case I decide to revive it
		double smallest_sigm = 1e30;
		double largest_sigm = -1e30;
		for (m=0; m < nlikes; m++) {
			if (sigm_no_systematic[m] < smallest_sigm) smallest_sigm = sigm_no_systematic[m];
			if (sigm_no_systematic[m] > largest_sigm) largest_sigm = sigm_no_systematic[m];
		}
		determine_accuracy_and_n_simulated_stars_sysparam(n_sysparam_star,smallest_sigm);
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "nfactor = " << nfactor_sysparam[0] << " like_nb = " << like_nb_sysparam[0] << " vcm_nn = " << vcm_nn << endl;
			else cout << "nfactor = " << nfactor_sysparam[0] << " like_nb = " << like_nb_sysparam[0] << " vcm_nn = " << vcm_nn << endl;
		}

		for (i=0, size=size_min; i < 2; i++, size += size_step) {
			binsizes[i] = size;
			tail_binsizes[i] = (use_large_tail_bins==true) ? 2*size : size;
		}

		string zerofilename = "rltables." + rdir + "/zerofile." + star_id;
		ofstream zerofile;
		if (mpi_id==0) zerofile.open(zerofilename.c_str());
		if (abs(star_v_avg-vcm_mean_approx) > nonmember_threshold) // star too far away in velocity-space to be binary, so it is not a member
		{
			if (mpi_id==0) {
				zerofile << "0" << endl; // indicates the R-factor is zero
				zerofile.close();
				if (output_to_logfile) logfile << "j=0\n\n";
				else cout << "j=0\n\n";
			}
#ifdef USE_MPI
			MPI_Barrier(MPI_COMM_WORLD);
#endif
			continue;
		}
		if (mpi_id==0) {
			zerofile << "1" << endl; // indicates the R-factor is nonzero
			zerofile << n_sysparam_star << endl;
			if (n_sysparam_star >= 1) zerofile << sysparam1 << " ";
			if (n_sysparam_star >= 2) zerofile << sysparam2 << " ";
			if (n_sysparam_star >= 3) zerofile << sysparam3 << " ";
			zerofile << endl;
			zerofile.close();
			//string dirstring = "rltables." + rdir + "/rltable." + star_id;
			//string mkdirstring = "mkdir rltables." + rdir + "/rltable." + star_id;
			//system(mkdirstring.c_str());

			//struct stat sb;
			//stat(dirstring.c_str(),&sb);
			//if (S_ISDIR(sb.st_mode)==false)
				//mkdir(dirstring.c_str(),S_IRWXU | S_IRWXG);
		}
#ifdef USE_MPI
		MPI_Barrier(MPI_COMM_WORLD);
#endif

		n_likevals = vcm_nn;
		if (n_sysparam_star>=1) n_likevals *= sysparam_nn[sysparam1];
		if (n_sysparam_star>=2) n_likevals *= sysparam_nn[sysparam2];
		if (n_sysparam_star==3) n_likevals *= sysparam_nn[sysparam3];
		for (k=0; k < 2; k++) {
			vcm_like[k].input(n_likevals);
			vcm_like[k]=0;
			vcm_bincount[k] = new int[n_likevals];
			vcm_bincount_part[k] = new int[n_likevals];
			vcm_bincount_sum[k] = new int[n_likevals];
			for (j=0; j < n_likevals; j++) {
				 vcm_bincount[k][j] = 0;
				 vcm_bincount_part[k][j] = 0;
			}
		}

		// the bin "area" is found by using the formula for the volume of a hypersphere
		c_n = pow(M_PI,0.5*epochs) / Gamma(0.5*epochs+1);
		for (k=0; k < 2; k++)
		{
			vcm_bin_area[k] = c_n * pow(0.5*binsizes[k],epochs);
			vcm_bin_area_tail[k] = c_n * pow(0.5*tail_binsizes[k],epochs);
			sq_bin_radii[k] = 0.25*SQR(binsizes[k]);
			sq_bin_radii_tail[k] = 0.25*SQR(tail_binsizes[k]);
		}

		if (galaxy_distance != 1e30)
			abs_magnitude = find_absolute_magnitude(galaxy_distance,magnitude);
		else abs_magnitude = magnitude;
		primary_mass = find_stellar_mass(abs_magnitude);
		stellar_radius = find_stellar_radius(primary_mass);

		bool converged = false;

		int iterations=0; // start at 0
		int iterations_since_accuracy_reset=0;
		double normsq;
		jvals = new vector<double>[nlikes];

		string vcm_table_filename = "rltables." + rdir + "/rltable." + star_id + "/vcm_table";
		ofstream vcm_table_file;
		if (mpi_id==0) {
			vcm_table_file.open(vcm_table_filename.c_str());
			vcm_table_file << vcm_nn << endl;
		}
		vcm_vals.input(vcm_nn);
		for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step)
		{
			vcm_vals[i] = vcm;
			if (mpi_id==0) {
				vcm_table_file << vcm << endl;
			}
		}
		if (mpi_id==0) vcm_table_file.close();

		// determine indices for which the tail takes over
		double vdist_left, vdist_right;
		vdist_left = 1e30;
		vdist_right = 1e30;
		int ktail_left = -1, ktail_right = vcm_nn;
		for (k=0, vcm=vcm_min; k < vcm_nn; k++, vcm += vcm_step)
		{
			if (vcm < star_v_avg - n_tail*largest_sigm) {
				if (abs(vcm-star_v_avg) < vdist_left) { ktail_left = k; vdist_left = abs(vcm-star_v_avg); }
			} else if (vcm > star_v_avg + n_tail*largest_sigm) {
				if (abs(vcm-star_v_avg) < vdist_right) { ktail_right = k; vdist_right = abs(vcm-star_v_avg); }
			}
		}

		total_n_simulated_stars=0;

		pthread_t thread[n_threads];
		Thread_Data threads_data[n_threads];
		for (k=0; k < n_threads; k++) {
			if (n_sysparam_star <= 1)
				threads_data[k].input(n_simulated_stars_thread, thread_random_sequence[k], vlos_data, v_errors_no_systematic, time_interval_years, sq_bin_radii, magnitude, primary_mass, stellar_radius, true, vcm_nn, vcm_min, vcm_step, ktail_left, ktail_right, n_sysparam_star, sysparam_i, sysparam_nn[sysparam1]);
			else if (n_sysparam_star == 2)
				threads_data[k].input(n_simulated_stars_thread, thread_random_sequence[k], vlos_data, v_errors_no_systematic, time_interval_years, sq_bin_radii, magnitude, primary_mass, stellar_radius, true, vcm_nn, vcm_min, vcm_step, ktail_left, ktail_right, n_sysparam_star, sysparam_i, sysparam_nn[sysparam1], sysparam_nn[sysparam2]);
			else if (n_sysparam_star == 3)
				threads_data[k].input(n_simulated_stars_thread, thread_random_sequence[k], vlos_data, v_errors_no_systematic, time_interval_years, sq_bin_radii, magnitude, primary_mass, stellar_radius, true, vcm_nn, vcm_min, vcm_step, ktail_left, ktail_right, n_sysparam_star, sysparam_i, sysparam_nn[sysparam1], sysparam_nn[sysparam2], sysparam_nn[sysparam3]);
		}

		while (converged==false)
		{
			iterations++;
			iterations_since_accuracy_reset++;
			if (iterations_since_accuracy_reset >= 40) {
				// since there are too many iterations, we reduce the required accuracy and increase number of simulated stars; this prevents runaway rfactor calculations taking forever
				if (fractional_accuracy < 0.5) {
					fractional_accuracy *= 5;
					n_simulated_stars *= 2;
				} else {
					n_simulated_stars *= 5;
				}
				iterations_since_accuracy_reset = 0;
			}

			nstars_remainder = n_simulated_stars % mpi_np;
			n_simulated_stars_proc = n_simulated_stars / mpi_np;
			if (nstars_remainder > 0) {
				n_simulated_stars_proc++;
				n_simulated_stars = mpi_np * n_simulated_stars_proc;
			}

			nstars_remainder = n_simulated_stars_proc % n_threads;
			n_simulated_stars_thread = n_simulated_stars_proc / n_threads;
			if (nstars_remainder > 0) {
				n_simulated_stars_thread++;
				n_simulated_stars_proc = n_threads * n_simulated_stars_thread;
				n_simulated_stars = mpi_np * n_simulated_stars_proc;
			}

			for (k=0; k < n_threads; k++) threads_data[k].update(n_simulated_stars_thread, sq_bin_radii, sq_bin_radii_tail);
			for (k=0; k < n_threads; k++) pthread_create(&(thread[k]), NULL, bin_vcm_multi_thread, (void*) (threads_data+k));
			for (k=0; k < n_threads; k++) pthread_join(thread[k],NULL);

			// this records the randseq number generator info so they can be taken up by each thread the next time around
			for (k=0; k < n_threads; k++) threads_data[k].output_random_sequence(thread_random_sequence[k]);

			for (k=0; k < 2; k++) {
				for (j=0; j < n_likevals; j++) {
					 vcm_bincount_part[k][j] = 0;
				}
			}

			if (n_sysparam_star==0) {
				for (k=0; k < n_threads; k++) {
					for (l=0; l < 2; l++) {
						for (j=0; j < vcm_nn; j++) {
							vcm_bincount_part[l][j] += threads_data[k].vcm_bincount[l][j];
						}
					}
				}
			} else if (n_sysparam_star==1) {
				for (k=0; k < n_threads; k++) {
					for (l=0; l < 2; l++) {
						likeval_i = 0;
						for (m=0; m < sysparam_nn[sysparam1]; m++) {
							for (j=0; j < vcm_nn; j++) {
								vcm_bincount_part[l][likeval_i] += threads_data[k].vcm_bincount1[l][m][j];
								likeval_i++;
							}
						}
						if (likeval_i != n_likevals) die("number of likevals not reached");
					}
				}
			} else if (n_sysparam_star==2) {
				for (k=0; k < n_threads; k++) {
					for (l=0; l < 2; l++) {
						likeval_i = 0;
						for (m=0; m < sysparam_nn[sysparam1]; m++) {
							for (n=0; n < sysparam_nn[sysparam2]; n++) {
								for (j=0; j < vcm_nn; j++) {
									vcm_bincount_part[l][likeval_i] += threads_data[k].vcm_bincount2[l][m][n][j];
									likeval_i++;
								}
							}
						}
					}
				}
			} else if (n_sysparam_star==3) {
				for (k=0; k < n_threads; k++) {
					for (l=0; l < 2; l++) {
						likeval_i = 0;
						for (m=0; m < sysparam_nn[sysparam1]; m++) {
							for (n=0; n < sysparam_nn[sysparam2]; n++) {
								for (p=0; p < sysparam_nn[sysparam3]; p++) {
									for (j=0; j < vcm_nn; j++) {
										vcm_bincount_part[l][likeval_i] += threads_data[k].vcm_bincount3[l][m][n][p][j];
										likeval_i++;
									}
								}
							}
						}
					}
				}
			}

#ifdef USE_MPI
			for (l=0; l < 2; l++) {
				MPI_Allreduce(vcm_bincount_part[l], vcm_bincount_sum[l], n_likevals, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
			}
#else
			for (l=0; l < 2; l++) {
				for (j=0; j < n_likevals; j++) {
					vcm_bincount_sum[l][j] = vcm_bincount_part[l][j];
				}
			}
#endif

			total_n_simulated_stars += n_simulated_stars;
			for (l=0; l < 2; l++) {
				for (j=0; j < n_likevals; j++) {
					vcm_bincount[l][j] += vcm_bincount_sum[l][j];
				}
			}

			calculate_normalized_likelihood_sysparam(n_sysparam_star);
			test_convergence_sysparam(iterations,converged,nonmember,false,n_sysparam_star);
			//if (mpi_id==0) output_rfactor_files(rfilename,true);
		}

		if (mpi_id==0) {
			for (i=0; i < vcm_nn; i++) {
				stringstream istr;
				string istring;
				istr << i;
				istr >> istring;
				string rltable_filename = "rltables." + rdir + "/rltable." + star_id + "/rltable." + istring;
				ofstream rltable_file(rltable_filename.c_str());
				if (!(rltable_file.is_open())) die("cannot open rltable file for writing");
				if (n_sysparam_star==0) {
					rltable_file << vcm_like[1][i] << endl;
				} else if (n_sysparam_star==1) {
					likeval_i = i;
					for (m=0; m < sysparam_nn[sysparam1]; m++) {
						rltable_file << vcm_like[1][likeval_i] << endl;
						likeval_i += vcm_nn;
					}
				} else if (n_sysparam_star==2) {
					likeval_i = i;
					like_i=0;
					for (m=0; m < sysparam_nn[sysparam1]; m++) {
						for (n=0; n < sysparam_nn[sysparam2]; n++) {
							rltable_file << vcm_like[1][likeval_i] << " ";
							likeval_i += vcm_nn;
							like_i++;
						}
						rltable_file << endl;
					}
				} else if (n_sysparam_star==3) {
					likeval_i = i;
					like_i=0;
					for (m=0; m < sysparam_nn[sysparam1]; m++) {
						for (n=0; n < sysparam_nn[sysparam2]; n++) {
							for (l=0; l < sysparam_nn[sysparam3]; l++) {
								rltable_file << vcm_like[1][likeval_i] << " ";
								likeval_i += vcm_nn;
								like_i++;
							}
							rltable_file << endl;
						}
						rltable_file << endl;
					}
				}
			}

			string nfilename = "rltables." + rdir + "/rltable." + star_id + "/nfactors";
			ofstream nfile(nfilename.c_str());
			if (n_sysparam_star==0) {
				string rfilename = "rltables." + rdir + "/rltable." + star_id + "/rfunctions";
				nfile << nfactor_sysparam[0] << endl;
				ofstream rfile(rfilename.c_str());
				for (i=0; i < vcm_nn; i++)
					rfile << vcm << " " << vcm_like[1][i] << endl;
			} else if (n_sysparam_star==1) {
				likeval_i=0;
				for (m=0; m < sysparam_nn[sysparam1]; m++) {
					nfile << nfactor_sysparam[m] << endl;
					stringstream mstr;
					mstr << m;
					string mstring;
					mstr >> mstring;
					string rfilename = "rltables." + rdir + "/rltable." + star_id + "/rfunctions." + mstring;
					ofstream rfile(rfilename.c_str());
					for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step) {
						rfile << vcm << " " << vcm_like[1][likeval_i] << endl;
						likeval_i++;
					}
				}
				if (likeval_i != n_likevals) die("number of likevals not reached when outputting rfunctions");
			} else if (n_sysparam_star==2) {
				likeval_i=0;
				like_i=0;
				for (m=0; m < sysparam_nn[sysparam1]; m++) {
					for (n=0; n < sysparam_nn[sysparam2]; n++) {
						nfile << nfactor_sysparam[like_i] << " ";
						stringstream mstr, nstr;
						mstr << m;
						nstr << n;
						string mstring, nstring;
						mstr >> mstring;
						nstr >> nstring;
						string rfilename = "rltables." + rdir + "/rltable." + star_id + "/rfunctions." + mstring + "." + nstring;
						ofstream rfile(rfilename.c_str());
						for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step) {
							rfile << vcm << " " << vcm_like[1][likeval_i] << endl;
							likeval_i++;
						}
						like_i++;
					}
					nfile << endl;
				}
				if (likeval_i != n_likevals) die("number of likevals not reached when outputting rfunctions");
			} else if (n_sysparam_star==3) {
				likeval_i=0;
				like_i=0;
				for (m=0; m < sysparam_nn[sysparam1]; m++) {
					for (n=0; n < sysparam_nn[sysparam2]; n++) {
						for (p=0; p < sysparam_nn[sysparam3]; p++) {
							nfile << nfactor_sysparam[like_i] << " ";
							stringstream mstr, nstr, pstr;
							mstr << m;
							nstr << n;
							pstr << p;
							string mstring, nstring, pstring;
							mstr >> mstring;
							nstr >> nstring;
							pstr >> pstring;
							string rfilename = "rltables." + rdir + "/rltable." + star_id + "/rfunctions." + mstring + "." + nstring + "." + pstring;
							ofstream rfile(rfilename.c_str());
							for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step) {
								rfile << vcm << " " << vcm_like[1][likeval_i] << endl;
								likeval_i++;
							}
							like_i++;
						}
						nfile << endl;
					}
				}
				if (likeval_i != n_likevals) die("number of likevals not reached when outputting rfunctions");
			}
			nfile.close();
		}

		if (nonmember==false) {
			if (mpi_id==0) {
				if (output_to_logfile) logfile << "jfac, bprob, nfactor (n=0):" << endl;
				else cout << "jfac, bprob, nfactor (n=0):" << endl;
			}
			for (k=0; k < 2; k++)
			{
				like_vcm_spline[k].input(vcm_vals.array(),vcm_like[k].array(0),vcm_nn);
				size_val = k;
				double jint_test = romberg_nocrash(jfactor_test_integrand,vcm_min,vcm_max,1e-5,5);
				double jfac = jint_test/nfactor_sysparam[0];
				double bprob = jfac/(like_nb_sysparam[0] + jfac);
				if (mpi_id==0) {
					if (output_to_logfile) logfile << jfac << " " << bprob << " " << nfactor_sysparam[0] << endl;
					else cout << jfac << " " << bprob << " " << nfactor_sysparam[0] << endl;
				}
			}
		}
		if (mpi_id==0) {
			//rnorm_file << star_id << " " << nfactor << " " << jvals[1][iterations-1] << endl;
			nstar_file.open(nstarfilename.c_str());
			nstar_file << ii+1 << endl;	// this keeps track of how many stars are done so far, so incomplete data sets can be analyzed
			nstar_file.close();
			if (output_to_logfile) logfile << endl;
			else cout << endl;
		}
		delete[] jvals;
		delete[] nfactor_sysparam;
		delete[] like_nb_sysparam;
		delete[] sigm_no_systematic;
	}
#ifdef USE_OPENMP
	if ((mpi_id==0) and (check_wtime)) {
		wtime = omp_get_wtime() - clocktime0;
		if (output_to_logfile) logfile << "total wall time: " << wtime << endl;
		else cout << "total wall time: " << wtime << endl;
	}
#endif
	if ((mpi_id==0) and (output_to_logfile)) {
		logfile.close();
		string mvlog_command = "cp " + logfilename + " rfactor_log_finished/";
		system(mvlog_command.c_str());
		remove(logfilename.c_str());
	}
	for (k=0; k < 2; k++) {
		delete[] vcm_bincount[k];
		delete[] vcm_bincount_part[k];
		delete[] vcm_bincount_sum[k];
	}
}

void R_Factors::calculate_verr_no_systematic(const dvector& vlos_data, const dvector& v_errors, dvector& v_errors_no_systematic)
{
	int n_epochs = v_errors.size();
	star_v_avg = 0;
	double sigmsq_inv = 0;
	for (int j=0; j < n_epochs; j++)
	{
		if (v_errors[j] < systematic_err) die("wtf? error smaller than systematic: %g",v_errors[j]); // this error should predominate
		v_errors_no_systematic[j] = sqrt(SQR(v_errors[j]) - SQR(systematic_err));
		star_v_avg += vlos_data[j]/SQR(v_errors_no_systematic[j]);
		sigmsq_inv += 1.0/SQR(v_errors_no_systematic[j]);
	}
	star_v_avg /= sigmsq_inv;
}

void R_Factors::calculate_nfactor(const dvector& vlos, const dvector& verrs, double &nfactor_val, double &like_nb_val, double &sigm_no_sys)
{
	int n_epochs = vlos.size();
	dvector verrs_no_sys(n_epochs);
	int i,j,k;

	double sigmsq_inv, expfactor, sigsqk, sigsqr_err_no_systematic, sigmsq_no_systematic;
	double v_avg = 0; sigmsq_inv = 0;
	for (j=0; j < n_epochs; j++)
	{
		if (verrs[j] < systematic_err) die("wtf? error smaller than systematic: %g",verrs[j]); // this error should predominate
		sigsqr_err_no_systematic = SQR(verrs[j]) - SQR(systematic_err);
		verrs_no_sys[j] = sqrt(sigsqr_err_no_systematic);
		v_avg += vlos[j]/sigsqr_err_no_systematic;
		sigmsq_inv += 1.0/sigsqr_err_no_systematic;
	}
	v_avg /= sigmsq_inv;
	sigmsq_no_systematic = 1.0/sigmsq_inv;
	sigm_no_sys = sqrt(sigmsq_no_systematic);
	if (sigm_no_sys==0) die("cannot have measurement errors equal to zero");

	// create normalizing n-factor
	expfactor = 0;
	for (i=0; i < n_epochs; i++) {
		for (j=0; j < i; j++) {
			sigsqk=0;
			for (k=0; k < n_epochs; k++) {
				if ((k==i) or (k==j)) continue;
				sigsqk += 1.0/SQR(verrs_no_sys[k]);
			}
			expfactor += 0.5 * SQR(vlos[i]-vlos[j]) / (SQR(verrs_no_sys[i]) + SQR(verrs_no_sys[j]) + SQR(verrs_no_sys[i]*verrs_no_sys[j])*sigsqk);
		}
	}

	nfactor_val = sqrt(M_2PI*sigmsq_no_systematic)*exp(-expfactor);
	for (i=0; i < n_epochs; i++) nfactor_val /= SQRT_2PI*verrs_no_sys[i];
	//if (nfactor_val < 1e-30) nfactor_val = 1e-30; // for numerical stability; clearly binary part dominates
	if (nfactor_val == 0.0) nfactor_val = 1e-30; // for numerical stability; clearly binary part dominates
		// so exact value of nfactor_val doesn't matter--it's ridiculously small regardless
	like_nb_val = nfactor_val*exp(-0.5*SQR(v_avg-vcm_mean_approx)/(SQR(sigm_no_sys)+SQR(vcm_dispersion_approx)))/sqrt(SQR(sigm_no_sys)+SQR(vcm_dispersion_approx))/SQRT_2PI;
}

void R_Factors::generate_rtables(const string datafile_str, const string rdir, const int n_threads)
{
	int i,j,k,l;
	double primary_mass, stellar_radius, magnitude, abs_magnitude;
	double sigm_no_systematic;
	double rfactor;

	Random_Sequence thread_random_sequence[n_threads];
	int mpi_process_index = mpi_id*n_threads;
	for (k=0; k < n_threads; k++) {
		thread_random_sequence[k].input(seed-mpi_process_index-k);
	}

	string logfilename;
	if ((mpi_id==0) and (output_to_logfile)) {
		logfilename = "log_" + rdir;
		logfile.open(logfilename.c_str());
	}

	string nstarfilename = "rtables." + rdir + "/nstars";
	ofstream nstar_file;

	double sig_logP_step;
	if (sig_logP_nn > 1)
		sig_logP_step = (sig_logP_max-sig_logP_min)/(sig_logP_nn-1);
	else sig_logP_step = 0;

	double mu_logP_step = (mu_logP_max-mu_logP_min)/(mu_logP_nn-1);
	dvector mu_logP_vals(mu_logP_nn), sig_logP_vals(sig_logP_nn);
	int jj, kk;
	for (jj=0, mu_logP=mu_logP_min; jj < mu_logP_nn; jj++, mu_logP += mu_logP_step)
		mu_logP_vals[jj] = mu_logP;
	for (kk=0, sig_logP=sig_logP_min; kk < sig_logP_nn; kk++, sig_logP += sig_logP_step)
		sig_logP_vals[kk] = sig_logP;

	string musig_table_filename = "rtables." + rdir + "/musig_table";
	if (mpi_id==0) {
		ofstream musig_file;
		musig_file.open(musig_table_filename.c_str());
		musig_file << mu_logP_nn << endl;
		for (jj=0; jj < mu_logP_nn; jj++)
			musig_file << mu_logP_vals[jj] << endl;
		musig_file << endl;
		musig_file << sig_logP_nn << endl;
		for (kk=0; kk < sig_logP_nn; kk++)
			musig_file << sig_logP_vals[kk] << endl;
		musig_file.close();
	}

	//vcm_like = new dvector[2];
	//vcm_bincount = new int*[2];
	//vcm_bincount_part = new int*[2];
	//vcm_bincount_sum = new int*[2];
	//vcm_bin_area = new double[2];
	//vcm_bin_area_tail = new double[2];
	sq_bin_radii.input(2);
	sq_bin_radii_tail.input(2);
	int n_simulated_stars_proc, n_simulated_stars_thread, nstars_remainder;

	  // Generate n stars
	ifstream datafile(datafile_str.c_str());
	string star_id;
	double wdata, werr, dt, R;
	int nstars;
	bool nonmember;
	bool continued = false;
	datafile >> nstars;
	for (int ii=0; ii < nstars; ii++)
	{
		datafile >> star_id >> magnitude;
		if (include_positions) datafile >> R;
		datafile >> epochs;
		if ((continue_rtables==true) and (continue_star==false)) { continue_star = true; star_id_cont = star_id; } // this causes it to not try to make a directory for the first star, if we're continuing the rtable
		n_simulated_stars = (epochs==1) ? 10000 :
									(epochs==2) ? 20000 :
									(epochs==3) ? 200000 : 800000;

		dvector vbin(epochs), vlos_nb(epochs), vlos_data(epochs);
		dvector times(epochs);
		dvector time_interval_years;
		if (epochs > 1)
			time_interval_years.input(epochs-1);
		else time_interval_years.input(1);
		dvector v_errors(epochs), v_errors_no_systematic(epochs);
		double r_value, channel, offset;

		for (j=0; j < epochs; j++)
		{
			datafile >> dt;
			if (j>0) time_interval_years[j-1] = dt;
			datafile >> vlos_data[j] >> v_errors[j];
			if (include_metal_widths) datafile >> wdata >> werr; // we don't use these
			if (include_error_params) datafile >> r_value >> channel;
			if (include_systematic_params) datafile >> offset; // don't use this
		}
		if ((continue_star==true) and (continued==false)) {
			if (star_id != star_id_cont) continue;
			else continued = true;
		} else if (continued==true) continue_star = false; // after continued star, set continue_star=false to make new dirs
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "star ID: " << star_id << endl;
			else cout << "star ID: " << star_id << endl;
		}

		calculate_verr_no_systematic(vlos_data,v_errors,v_errors_no_systematic);
		calculate_nfactor(vlos_data,v_errors,nfactor,like_nb,sigm_no_systematic);
	
		use_large_tail_bins = true;
		determine_accuracy_and_n_simulated_stars(like_nb,nfactor,sigm_no_systematic);

		for (i=0, size=size_min; i < 2; i++, size += size_step) {
			binsizes[i] = size;
			tail_binsizes[i] = (use_large_tail_bins==true) ? 2*size : size;
		}

		string zerofilename = "rtables." + rdir + "/zerofile." + star_id;
		ofstream zerofile;
		if (mpi_id==0) zerofile.open(zerofilename.c_str());
		if (abs(star_v_avg-vcm_mean_approx) > nonmember_threshold) // star too far away in velocity-space to be binary, so it is not a member
		{
			if (mpi_id==0) {
				zerofile << "0" << endl; // indicates the R-factor is zero
				zerofile.close();
				if (output_to_logfile) logfile << "j=0\n\n";
				else cout << "j=0\n\n";
			}
			continue;
		}
		if (mpi_id==0) {
			zerofile << "1" << endl; // indicates the R-factor is nonzero
			zerofile.close();
			string mkdirstring = "mkdir rtables." + rdir + "/rtable." + star_id;
			if (continue_star==false) system(mkdirstring.c_str());
		}

		for (k=0; k < 2; k++) {
			vcm_like[k].input(vcm_nn);
			vcm_like[k]=0;
			vcm_bincount[k] = new int[vcm_nn];
			vcm_bincount_part[k] = new int[vcm_nn];
			vcm_bincount_sum[k] = new int[vcm_nn];
		}

		// the bin "area" is found by using the formula for the volume of a hypersphere
		c_n = pow(M_PI,0.5*epochs) / Gamma(0.5*epochs+1);
		for (k=0; k < 2; k++)
		{
			vcm_bin_area[k] = c_n * pow(0.5*binsizes[k],epochs);
			vcm_bin_area_tail[k] = c_n * pow(0.5*tail_binsizes[k],epochs);
			sq_bin_radii[k] = 0.25*SQR(binsizes[k]);
			sq_bin_radii_tail[k] = 0.25*SQR(tail_binsizes[k]);
		}

		if (galaxy_distance != 1e30)
			abs_magnitude = find_absolute_magnitude(galaxy_distance,magnitude);
		else abs_magnitude = magnitude;
		primary_mass = find_stellar_mass(abs_magnitude);
		stellar_radius = find_stellar_radius(primary_mass);

		bool converged;
		int iterations;
		int iterations_since_accuracy_reset;
		double normsq;

		string rtable_dirname;
		string vcm_table_filename = "rtables." + rdir + "/rtable." + star_id + "/vcm_table";
		ofstream vcm_table_file;
		if (mpi_id==0) {
			vcm_table_file.open(vcm_table_filename.c_str());
			vcm_table_file << vcm_nn << endl;
		}
		vector<string> rtable_filenames(vcm_nn);
		vcm_vals.input(vcm_nn);
		dmatrix *rtable = new dmatrix[vcm_nn];
		for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step)
		{
			vcm_vals[i] = vcm;
			if (mpi_id==0) {
				vcm_table_file << vcm << endl;
			}
			rtable[i].input(mu_logP_nn,sig_logP_nn);
		}
		if (mpi_id==0) vcm_table_file.close();

		// determine indices for which the tail takes over
		double vdist_left, vdist_right;
		vdist_left = 1e30;
		vdist_right = 1e30;
		ktail_left = -1;
		ktail_right = vcm_nn;
		for (k=0, vcm=vcm_min; k < vcm_nn; k++, vcm += vcm_step)
		{
			if (vcm < star_v_avg - n_tail*sigm_no_systematic) {
				if (abs(vcm-star_v_avg) < vdist_left) { ktail_left = k; vdist_left = abs(vcm-star_v_avg); }
			} else if (vcm > star_v_avg + n_tail*sigm_no_systematic) {
				if (abs(vcm-star_v_avg) < vdist_right) { ktail_right = k; vdist_right = abs(vcm-star_v_avg); }
			}
		}

		if (mpi_id==0) {
			if (output_to_logfile) {
				logfile << "nfactor = " << nfactor << " like_nb = " << like_nb << " vcm_nn = " << vcm_nn << endl;
				logfile << "v_avg = " << star_v_avg << " sigm = " << sigm_no_systematic << " vdist_left = " << vdist_left << " vdist_right = " << vdist_right << endl;
			}
			else
			{
				cout << "nfactor = " << nfactor << " like_nb = " << like_nb << " vcm_nn = " << vcm_nn << endl;
				cout << "v_avg = " << star_v_avg << " sigm = " << sigm_no_systematic << " vdist_left = " << vdist_left << " vdist_right = " << vdist_right << endl;
			}
		}

		for (jj=0; jj < mu_logP_nn; jj++)
		{
			mu_logP = mu_logP_vals[jj];
			for (kk=0; kk < sig_logP_nn; kk++)
			{
				nonmember = false; // assume first it's a member; this will be set to zero only if the jfactors calculated are exactly zero (no points binned)

				sig_logP = sig_logP_vals[kk];
				stringstream jjstr, kkstr;
				jjstr << jj;
				kkstr << kk;
				string jjstring, kkstring;
				jjstr >> jjstring;
				kkstr >> kkstring;
				string rfilename = "rtables." + rdir + "/rtable." + star_id + "/rfunctions." + jjstring + "." + kkstring;
				if ((continue_rtables==true) and ((jj < mu_logP_jj_cont) or (kk < sig_logP_kk_cont))) {
					ifstream rfile(rfilename.c_str());
					for (i=0; i < vcm_nn; i++)
						rfile >> vcm >> rtable[i][jj][kk];
					continue;
				}
				jvals = new vector<double>[2];
				for (k=0; k < 2; k++) {
					vcm_like[k]=0;
					for (j=0; j < vcm_nn; j++) {
						 vcm_bincount[k][j] = 0;
					}
				}

				converged = false;
				iterations=0;
				iterations_since_accuracy_reset=0;
				total_n_simulated_stars=0;

				pthread_t thread[n_threads];
				Thread_Data threads_data[n_threads];
				for (k=0; k < n_threads; k++)
					threads_data[k].input(n_simulated_stars_thread, thread_random_sequence[k], vlos_data, v_errors_no_systematic, time_interval_years, sq_bin_radii, magnitude, primary_mass, stellar_radius, true, vcm_nn, vcm_min, vcm_step, ktail_left, ktail_right, 0, NULL, 0);

				while (converged==false)
				{
					iterations++;

					iterations_since_accuracy_reset++;
					if (iterations_since_accuracy_reset >= 40) {
						// since there are too many iterations, we reduce the required accuracy and increase number of simulated stars; this prevents runaway rfactor calculations taking forever
						if (fractional_accuracy < 0.5) {
							fractional_accuracy *= 5;
							n_simulated_stars *= 2;
						} else {
							n_simulated_stars *= 5;
						}
						iterations_since_accuracy_reset = 0;
					}

					nstars_remainder = n_simulated_stars % mpi_np;
					n_simulated_stars_proc = n_simulated_stars / mpi_np;
					if (nstars_remainder > 0) {
						n_simulated_stars_proc++;
						n_simulated_stars = mpi_np * n_simulated_stars_proc;
					}

					nstars_remainder = n_simulated_stars_proc % n_threads;
					n_simulated_stars_thread = n_simulated_stars_proc / n_threads;
					if (nstars_remainder > 0) {
						n_simulated_stars_thread++;
						n_simulated_stars_proc = n_threads * n_simulated_stars_thread;
						n_simulated_stars = mpi_np * n_simulated_stars_proc;
					}

					for (k=0; k < n_threads; k++) threads_data[k].update(n_simulated_stars_thread, sq_bin_radii, sq_bin_radii_tail);
					for (k=0; k < n_threads; k++) pthread_create(&(thread[k]), NULL, bin_vcm_multi_thread, (void*) (threads_data+k));
					for (k=0; k < n_threads; k++) pthread_join(thread[k],NULL);

					// this records the randseq number generator info so they can be taken up by each thread the next time around
					for (k=0; k < n_threads; k++) threads_data[k].output_random_sequence(thread_random_sequence[k]);

					for (l=0; l < 2; l++) {
						for (j=0; j < vcm_nn; j++) {
							 vcm_bincount_part[l][j] = 0;
						}
					}
					for (k=0; k < n_threads; k++) {
						for (l=0; l < 2; l++) {
							for (j=0; j < vcm_nn; j++) {
								vcm_bincount_part[l][j] += threads_data[k].vcm_bincount[l][j];
							}
						}
					}
#ifdef USE_MPI
					for (l=0; l < 2; l++) {
						MPI_Allreduce(vcm_bincount_part[l], vcm_bincount_sum[l], vcm_nn, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
					}
#else
					for (l=0; l < 2; l++) {
						for (j=0; j < vcm_nn; j++) {
							vcm_bincount_sum[l][j] = vcm_bincount_part[l][j];
						}
					}
#endif
					total_n_simulated_stars += n_simulated_stars;
					for (l=0; l < 2; l++) {
						for (j=0; j < vcm_nn; j++) {
							vcm_bincount[l][j] += vcm_bincount_sum[l][j];
						}
					}

					calculate_normalized_likelihood();
					test_convergence(iterations,converged,nonmember,true);
					ofstream rfile;
					if (mpi_id==0) rfile.open(rfilename.c_str());
					for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step)
					{
						rfactor = vcm_like[1][i] / nfactor;
						if (rfactor < 0) die("Negative rfactor!");
						if (mpi_id==0) rfile << vcm << " " << rfactor << endl;
						rtable[i][jj][kk] = rfactor;
					}
					if (mpi_id==0) rfile.close();
				}

				if (nonmember==false)
				{
					if (mpi_id==0) {
						if (output_to_logfile) logfile << "jfac, bprob, nfactor:" << endl;
						else cout << "jfac, bprob, nfactor:" << endl;
					}
					for (k=0; k < 2; k++)
					{
						size_val = k;
						double jint_test = trapezoid_rule(jfactor_test_integrand,vcm_min,vcm_max,vcm_nn);
						double jfac = jint_test/nfactor;
						double bprob = jfac/(like_nb + jfac);
						if (mpi_id==0) {
							if (output_to_logfile) logfile << mu_logP << " " << sig_logP << " " << jfac << " " << bprob << " " << nfactor << endl;
							else cout << mu_logP << " " << sig_logP << " " << jfac << " " << bprob << " " << nfactor << endl;
						}
					}
				}
				delete[] jvals;
			}
		}
		if (mpi_id==0)
		{
			for (i=0; i < vcm_nn; i++)
			{
				stringstream istr;
				string istring;
				istr << i;
				istr >> istring;
				string rtable_filename = "rtables." + rdir + "/rtable." + star_id + "/rtable." + istring;
				string rtable_plot_filename = "rtables." + rdir + "/rtable." + star_id + "/rtable_plot." + istring;
				ofstream rtable_file(rtable_filename.c_str());
				ofstream rtable_plot(rtable_plot_filename.c_str());
				for (jj=0; jj < mu_logP_nn; jj++) {
					for (kk=0; kk < sig_logP_nn; kk++) {
						rtable_file << rtable[i][jj][kk] << endl;
						rtable_plot << mu_logP_vals[jj] << " " << sig_logP_vals[kk] << " " << rtable[i][jj][kk] << endl;
					}
				}
			}
		}
		int stars_finished = ii+1;
		stringstream nstars_str;
		nstars_str << stars_finished;
		string nstars_string;
		nstars_str >> nstars_string;
		if (mpi_id==0) {
			nstar_file.open(nstarfilename.c_str());
			nstar_file << nstars_string << endl;	// this keeps track of how many stars are done so far, so incomplete data sets can be analyzed
			nstar_file.close();
			if (output_to_logfile) logfile << endl;
			else cout << endl;
		}
		delete[] rtable;
	}
	if (mpi_id==0) {
		if (output_to_logfile) {
			logfile.close();
			string mvlog_command = "cp " + logfilename + " ./rtable_log_finished/";
			system(mvlog_command.c_str());
			remove(logfilename.c_str());
		}
	}
	for (k=0; k < 2; k++) {
		delete[] vcm_bincount[k];
		delete[] vcm_bincount_part[k];
		delete[] vcm_bincount_sum[k];
	}
}

//double post_psi, post_time, post_e, post_q, post_logP;

void *bin_dv_multi_thread(void *dataptr)
{
	Thread_Data& thread_data = *((Thread_Data*) dataptr);
	Thread_Data *thread_dataptr = (Thread_Data*) dataptr;

	int i,j,k,ii;
	double normsq, normsq_neg;
	dvector vbin(thread_data.epochs);
	lvector times(thread_data.epochs);
	dvector dv(thread_data.epochs-1);
	dvector vlos(thread_data.epochs);
	dvector neg_dvdata(thread_data.ndim);
	neg_dvdata = -thread_data.data;
	for (i=0; i < thread_data.n_simulated_stars_thread; i++)
	{
		generate_binary_velocities_multi_thread(thread_data, vbin, times);
		for (j=0; j < thread_data.epochs; j++) {
			vlos[j] = vbin[j] + thread_data.v_errors[j]*normal_deviate(thread_data.rand);
		}

		normsq=0;
		normsq_neg=0;
		for (j=0; j < thread_data.ndim; j++) {
			dv[j] = vlos[j+1] - vlos[0];
			normsq += SQR(dv[j] - thread_data.data[j]);
			normsq_neg += SQR(dv[j] - neg_dvdata[j]);
		}

		for (k=0; k < 2; k++) {
			if (normsq < thread_data.sq_bin_radii[k]) {
				thread_data.dv_bincount[k]++;
				if (make_posterior) {
					for (ii=0; ii < 5; ii++) thread_data.postpts[i].push_back(thread_data.postpt[i]);
				}
				//paramout << "1 " << post_psi << " " << post_time << " " << post_e << " " << post_q << " " << post_logP << " 1" << endl;

			}
			if (normsq_neg < thread_data.sq_bin_radii[k]) {
				thread_data.dv_bincount[k]++;
				if (make_posterior) {
					for (ii=0; ii < 5; ii++) thread_data.postpts[i].push_back(thread_data.postpt[i]);
				}
			}
		}
	}
}

void *bin_vcm_multi_thread(void *dataptr)
{
	Thread_Data& thread_data = *((Thread_Data*) dataptr);
	Thread_Data *thread_dataptr = (Thread_Data*) dataptr;

	int i,j,k,l,p,kk;
	double normsq, vcm, error_term;
	dvector vbin(thread_data.epochs);
	lvector times(thread_data.epochs);
	dvector verr(thread_data.epochs);
	dvector normdev(thread_data.epochs);
	for (i=0; i < thread_data.n_simulated_stars_thread; i++)
	{
		generate_binary_velocities_multi_thread(thread_data, vbin, times);
		for (j=0; j < thread_data.epochs; j++) {
			normdev[j] = normal_deviate(thread_data.rand);
			verr[j] = thread_data.v_errors[j]*normdev[j];
		}

		if (thread_data.n_sys_params==0) {
			for (k=0, vcm=thread_data.vcm_min; k < thread_data.vcm_nn; k++, vcm += thread_data.vcm_step)
			{
				normsq=0;
				for (j=0; j < thread_data.epochs; j++)
					normsq += SQR(vbin[j] + verr[j] - (thread_data.data[j]-vcm));

				for (l=0; l < 2; l++)
				{
					if ((k > thread_data.ktail_left) and (k < thread_data.ktail_right)) {
						if (normsq < thread_data.sq_bin_radii[l]) thread_data.vcm_bincount[l][k]++;
					} else {
						if (normsq < thread_data.sq_bin_radii_tail[l]) thread_data.vcm_bincount[l][k]++;
					}
				}
			}
		} else if (thread_data.n_sys_params==1) {
			int m;
			double lambda1, normsq_term;
			for (k=0, vcm=thread_data.vcm_min; k < thread_data.vcm_nn; k++, vcm += thread_data.vcm_step)
			{
				for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1])
				{
					normsq=0;
					for (j=0; j < thread_data.epochs; j++) {
						normsq_term = vbin[j] - (thread_data.data[j]-vcm);
						error_term = verr[j];
						for (kk=0; kk < 2; kk++) {
							if (thread_data.offset_i[j][kk]==sysparam1) {
								if (sysparam_type[sysparam1]==0) normsq_term += lambda1;
								else error_term = sqrt(SQR(thread_data.v_errors[j]) + SQR(lambda1))*normdev[j];
							}
						}
						normsq_term += error_term;
						normsq += SQR(normsq_term);
					}

					for (l=0; l < 2; l++)
					{
						if ((k > thread_data.ktail_left) and (k < thread_data.ktail_right)) {
							if (normsq < thread_data.sq_bin_radii[l]) thread_data.vcm_bincount1[l][m][k]++;
						} else {
							if (normsq < thread_data.sq_bin_radii_tail[l]) thread_data.vcm_bincount1[l][m][k]++;
						}
					}
				}
			}
		} else if (thread_data.n_sys_params==2) {
			int m,n;
			double lambda1, lambda2, normsq_term;
			for (k=0, vcm=thread_data.vcm_min; k < thread_data.vcm_nn; k++, vcm += thread_data.vcm_step)
			{
				for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
					for (n=0, lambda2=sys_param_min[sysparam2]; n < sysparam_nn[sysparam2]; n++, lambda2 += sys_param_step[sysparam2]) {
						normsq=0;
						for (j=0; j < thread_data.epochs; j++) {
							normsq_term = vbin[j] - (thread_data.data[j]-vcm);
							error_term = verr[j];
							for (kk=0; kk < 2; kk++) {
								if (thread_data.offset_i[j][kk]==sysparam1) {
									if (sysparam_type[sysparam1]==0) normsq_term += lambda1;
									else error_term = sqrt(SQR(thread_data.v_errors[j]) + SQR(lambda1))*normdev[j];
								}
								else if (thread_data.offset_i[j][kk]==sysparam2) {
									if (sysparam_type[sysparam2]==0) normsq_term += lambda2;
									else error_term = sqrt(SQR(thread_data.v_errors[j]) + SQR(lambda2))*normdev[j];
								}
							}
							normsq_term += error_term;
							normsq += SQR(normsq_term);
						}

						for (l=0; l < 2; l++)
						{
							if ((k > thread_data.ktail_left) and (k < thread_data.ktail_right)) {
								if (normsq < thread_data.sq_bin_radii[l]) thread_data.vcm_bincount2[l][m][n][k]++;
							} else {
								if (normsq < thread_data.sq_bin_radii_tail[l]) thread_data.vcm_bincount2[l][m][n][k]++;
							}
						}
					}
				}
			}
		} else if (thread_data.n_sys_params==3) {
			int m,n;
			double lambda1, lambda2, lambda3, normsq_term;
			for (k=0, vcm=thread_data.vcm_min; k < thread_data.vcm_nn; k++, vcm += thread_data.vcm_step)
			{
				for (m=0, lambda1=sys_param_min[sysparam1]; m < sysparam_nn[sysparam1]; m++, lambda1 += sys_param_step[sysparam1]) {
					for (n=0, lambda2=sys_param_min[sysparam2]; n < sysparam_nn[sysparam2]; n++, lambda2 += sys_param_step[sysparam2]) {
						for (p=0, lambda3=sys_param_min[sysparam3]; p < sysparam_nn[sysparam3]; p++, lambda3 += sys_param_step[sysparam3]) {
							normsq=0;
							for (j=0; j < thread_data.epochs; j++) {
								normsq_term = vbin[j] - (thread_data.data[j]-vcm);
								error_term = verr[j];
								for (kk=0; kk < 2; kk++) {
									if (thread_data.offset_i[j][kk]==sysparam1) {
										if (sysparam_type[sysparam1]==0) normsq_term += lambda1;
										else error_term = sqrt(SQR(thread_data.v_errors[j]) + SQR(lambda1))*normdev[j];
									}
									else if (thread_data.offset_i[j][kk]==sysparam2) {
										if (sysparam_type[sysparam2]==0) normsq_term += lambda2;
										else error_term = sqrt(SQR(thread_data.v_errors[j]) + SQR(lambda2))*normdev[j];
									}
									else if (thread_data.offset_i[j][kk]==sysparam3) {
										if (sysparam_type[sysparam3]==0) normsq_term += lambda3;
										else error_term = sqrt(SQR(thread_data.v_errors[j]) + SQR(lambda3))*normdev[j];
									}
								}
								normsq_term += error_term;
								normsq += SQR(normsq_term);
							}
							for (l=0; l < 2; l++)
							{
								if ((k > thread_data.ktail_left) and (k < thread_data.ktail_right)) {
									if (normsq < thread_data.sq_bin_radii[l]) thread_data.vcm_bincount3[l][m][n][p][k]++;
								} else {
									if (normsq < thread_data.sq_bin_radii_tail[l]) thread_data.vcm_bincount3[l][m][n][p][k]++;
								}
							}
						}
					}
				}
			}
		}
	}
}

void R_Factors::calculate_normalized_likelihood()
{
	// normalize the likelihood
	int i,k;
	for (k=0; k < 2; k++) {
		for (i=0; i < vcm_nn; i++) {
			if ((i>ktail_left) and (i<ktail_right)) {
				vcm_like[k][i] = vcm_bincount[k][i] * 1.0/(total_n_simulated_stars*vcm_bin_area[k]);
			} else {
				vcm_like[k][i] = vcm_bincount[k][i] * 1.0/(total_n_simulated_stars*vcm_bin_area_tail[k]); // lower resolution needed at tail
			}
		}
	}
}

void R_Factors::calculate_normalized_likelihood_sysparam(int n_sysparam_star)
{
	// normalize the likelihood
	int i,k,n,like_i;
	int nlikes = 1;
	if (n_sysparam_star>=1) nlikes *= sysparam_nn[sysparam1];
	if (n_sysparam_star>=2) nlikes *= sysparam_nn[sysparam2];
	if (n_sysparam_star>=3) nlikes *= sysparam_nn[sysparam3];
	for (k=0; k < 2; k++) {
		like_i=0;
		for (n=0; n < nlikes; n++) {
			for (i=0; i < vcm_nn; i++) {
				if ((i>ktail_left) and (i<ktail_right)) {
					vcm_like[k][like_i] = vcm_bincount[k][like_i] * 1.0/(total_n_simulated_stars*vcm_bin_area[k]);
				} else {
					vcm_like[k][like_i] = vcm_bincount[k][like_i] * 1.0/(total_n_simulated_stars*vcm_bin_area_tail[k]); // lower resolution needed at tail
				}
				like_i++;
			}
		}
	}
}

void R_Factors::test_convergence(int iterations, bool &converged, bool &nonmember, const bool print_musigvals)
{
	int i,k;
	dvector jint(2);
	double jint_test, jfac, bprob;
	bool zero_jfactors = true;
	for (k=0; k < 2; k++) {
		like_vcm_spline[k].input(vcm_vals,vcm_like[k]);
		size_val = k;
		jint[k] = trapezoid_rule(like_integrand,vcm_min,vcm_max,vcm_nn);
		if ((zero_jfactors==true) and (jint[k] != 0)) zero_jfactors = false;

		jvals[k].push_back(jint[k]);
		jint_test = trapezoid_rule(jfactor_test_integrand,vcm_min,vcm_max,vcm_nn);
		jfac = jint_test/nfactor;
		bprob = jfac/(like_nb + jfac);

		if (mpi_id==0) {
			if (print_musigvals) {
				if (output_to_logfile) logfile << mu_logP << " " << sig_logP << " " << "iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k] << " bprob=" << bprob << endl;
				else cout << mu_logP << " " << sig_logP << " " << "iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k] << " bprob=" << bprob << endl;
			}
			else {
				if (output_to_logfile) logfile << "iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k] << " bprob=" << bprob << endl;
				else cout << "iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k] << " bprob=" << bprob << endl;
			}
		}
	}
	//cout << "JVALS: ";
	//for (int i=0; i < jvals[0].size(); i++) cout << jvals[0][i] << " ";
	//cout << endl;

	if (zero_jfactors==true)
	{
		// if all the jfactors are exactly zero, this star is probably not a member
		nonmember = true;
		converged = true;
	}
	else
	{
		if (iterations > test_iterations) // now test for convergence; if the J-factor isn't much different from the J-factor on the previous iteration several iterations, we're done
		{
			int last_comp = iterations-test_iterations-1;
			for (i=iterations-2; i >= last_comp; i--)
			{
				if (abs((jint[1]-jvals[1][i])/jint[1]) > fractional_accuracy) break; // use largest binsize
				if (i==last_comp) {
					double min_jval=1e30, max_jval=-1e30, javg, jdif;
					for (k=0; k < 2; k++) {
						if (jint[k] < min_jval) min_jval = jint[k];
						if (jint[k] > max_jval) max_jval = jint[k];
					}
					javg = 0.5*(min_jval+max_jval);
					jdif = (max_jval-min_jval)/javg;
					if (mpi_id==0) {
						if (output_to_logfile) logfile << "jmin/max " << min_jval << " " << max_jval << " " << jdif << endl;
						else cout << "jmin/max " << min_jval << " " << max_jval << " " << jdif << endl;
					}
					if (jdif < 10*fractional_accuracy) { converged = true; }
					// only finishes if the different binsizes give same j-factor within 10*accuracy, otherwise get more points
				}
			}
		}
	}
	return;
}

void R_Factors::test_convergence_sysparam(int iterations, bool &converged, bool &nonmember, const bool print_musigvals, int n_sysparam_star)
{
	int i,k,n;
	bool sub_convergence;
	int nlikes = 1;
	if (n_sysparam_star>=1) nlikes *= sysparam_nn[sysparam1];
	if (n_sysparam_star>=2) nlikes *= sysparam_nn[sysparam2];
	if (n_sysparam_star>=3) nlikes *= sysparam_nn[sysparam3];
	dvector jint[2];
	for (i=0; i < 2; i++) jint[i].input(nlikes);
	double jint_test, jfac, bprob;
	bool zero_jfactors = true;
	bool max_nfactor_zero_jval = true;
	int likevals_i;

	double nfactor_max=-1e30;
	for (n=0; n < nlikes; n++) {
		if (nfactor_sysparam[n] > nfactor_max) {
			nfactor_max = nfactor_sysparam[n];
			nlike_max_nfactor = n;
		}
	}


	for (k=0; k < 2; k++) {
		for (n=0, likevals_i=0; n < nlikes; n++, likevals_i += vcm_nn) {
			like_vcm_spline[k].input(vcm_vals.array(),vcm_like[k].array(likevals_i),vcm_nn);
			size_val = k;
			jint[k][n] = trapezoid_rule(like_integrand,vcm_min,vcm_max,vcm_nn);
			if ((zero_jfactors==true) and (jint[k][n] != 0)) zero_jfactors = false;
			if ((n==nlike_max_nfactor) and (jint[k][n] != 0)) max_nfactor_zero_jval = false;

			if (k==1) jvals[n].push_back(jint[k][n]);

			jint_test = trapezoid_rule(jfactor_test_integrand,vcm_min,vcm_max,vcm_nn);
			jfac = jint_test/nfactor_sysparam[n];
			bprob = jfac/(like_nb_sysparam[n] + jfac);

			if (mpi_id==0) {
				if (print_musigvals) {
					if (output_to_logfile) logfile << mu_logP << " " << sig_logP << " " << "n=" << n << " iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k][n] << " bprob=" << bprob << endl;
					else cout << mu_logP << " " << sig_logP << " " << "n=" << n << " iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k][n] << " bprob=" << bprob << endl;
				}
				else {
					if (output_to_logfile) logfile << "n=" << n << " iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k][n] << " bprob=" << bprob << endl;
					else cout << "n=" << n << " iter=" << iterations << " binsize=" << binsizes[k] << " jint=" << jint[k][n] << " bprob=" << bprob << endl;
				}
			}
		}
	}
	//cout << "JVALS: ";
	//for (int i=0; i < jvals[0].size(); i++) cout << jvals[0][i] << " ";
	//cout << endl;
	if (zero_jfactors==true)
	{
		// if all the jfactors are exactly zero, this star is probably not a member
		nonmember = true;
		converged = true;
	}
	else if (max_nfactor_zero_jval==true)
	{
		converged = false; // Probably a binary member star, and we need to keep adding points until all the jfactors for different assumed systematic parameters are non-zero
	}
	else
	{
		if (iterations > test_iterations) // now test for convergence; if the J-factor isn't much different from the J-factor on the previous iteration several iterations, we're done
		{
			sub_convergence = true;
			//for (n=0; n < nlikes; n++) {
				n = nlike_max_nfactor;
				int last_comp = iterations-test_iterations-1;
				for (i=iterations-2; i >= last_comp; i--)
				{
					if (abs((jint[1][n]-jvals[n][i])/jint[1][n]) > fractional_accuracy) { sub_convergence = false; break; } // use largest binsize
					if (i==last_comp) {
						double min_jval=1e30, max_jval=-1e30, javg, jdif;
						for (k=0; k < 2; k++) {
							if (jint[k][n] < min_jval) min_jval = jint[k][n];
							if (jint[k][n] > max_jval) max_jval = jint[k][n];
						}
						javg = 0.5*(min_jval+max_jval);
						jdif = (max_jval-min_jval)/javg;
						 if (mpi_id==0) {
							 if (output_to_logfile) logfile << "jmin/max " << min_jval << " " << max_jval << " " << jdif << endl;
							 else cout << "jmin/max " << min_jval << " " << max_jval << " " << jdif << endl;
						 }
						if (jdif >= 10*fractional_accuracy) { sub_convergence = false; }
						// only finishes if the different binsizes give same j-factor within 10*accuracy, otherwise get more points
					}
				}
			//}
			if (sub_convergence==true) converged = true;
		}
	}
	return;
}

void R_Factors::test_dv_convergence(int iterations, bool &converged, const bool print_musigvals)
{
	if (iterations==1) iterations_converged = 0; // iterations_converged will tell us how many iterations the series has been converging, while the binsizes haven't converged to same likelihood
	int i,k;
	double jfac, bprob;
	for (k=0; k < 2; k++)
	{
		jfac = dv_like[k]/nfactor;
		bprob = jfac/(1 + jfac);
		dv_like_vals[k].push_back(dv_like[k]);

		if (mpi_id==0) {
			if (print_musigvals) {
				if (output_to_logfile) logfile << mu_logP << " " << sig_logP << " " << "iter=" << iterations << " binsize=" << binsizes[k] << " dv_like=" << dv_like[k] << " bprob=" << bprob << endl;
				else cout << mu_logP << " " << sig_logP << " " << "iter=" << iterations << " binsize=" << binsizes[k] << " dv_like=" << dv_like[k] << " bprob=" << bprob << endl;
			}
			else {
				//cout << "iter=" << iterations << " binsize=" << binsizes[k] << " dv_like=" << dv_like[k] << " bprob=" << bprob << endl;
				if (output_to_logfile) logfile << "iter=" << iterations << " binsize=" << binsizes[k] << " dv_like=" << dv_like[k] << " bprob=" << bprob << endl;
				else cout << "iter=" << iterations << " binsize=" << binsizes[k] << " dv_like=" << dv_like[k] << " bprob=" << bprob << endl;
			}
		}
	}

	bool binsizes_converge; // we want to make sure the different binsizes converge to approximately the same likelihood; if they don't, our bins are too large
	if (iterations > test_iterations) // now test for convergence; if the J-factor isn't much different from the J-factor on the previous iteration several iterations, we're done
	{
		binsizes_converge = true; // we'll test if this is really true in the following lines
		int last_comp = iterations-test_iterations-1; // last_comp refers to the last calculated value we'll compare to, counting back from our most recent one (*not* the last calculation we've done)
		for (i=iterations-2; i >= last_comp; i--)
		{
			if (abs((dv_like[0]-dv_like_vals[0][i])/dv_like[0]) > fractional_accuracy) { iterations_converged = 0; binsizes_converge = false; break; } // use smallest binsize
			if (i >= iterations-1-binsizes_test_iterations) {
				double min_dvlike_val=1e30, max_dvlike_val=-1e30, javg, jdif;
				for (k=0; k < 2; k++) {
					if (dv_like_vals[k][i+1] < min_dvlike_val) min_dvlike_val = dv_like_vals[k][i+1];
					if (dv_like_vals[k][i+1] > max_dvlike_val) max_dvlike_val = dv_like_vals[k][i+1];
				}
				javg = 0.5*(min_dvlike_val+max_dvlike_val);
				jdif = (max_dvlike_val-min_dvlike_val)/javg;
				if (mpi_id==0) {
					if (output_to_logfile) logfile << "jmin/max " << min_dvlike_val << " " << max_dvlike_val << " " << jdif << endl;
					else cout << "jmin/max, jdif for iteration " << i+1 << ": " << min_dvlike_val << " " << max_dvlike_val << " " << jdif << endl;
				}

				// if the number of stars is more than 1 billion, we won't test for binsize convergence--some accuracy must be sacrificed, or else it never finishes!
				if (n_simulated_stars < 1000000000) { 
					if (n_times_decreased_binsize < 4) { // we'll loosen the accuracy if the binsize has been reduced 4 times already
						if (jdif > 3*fractional_accuracy) { binsizes_converge = false; } // the 1.7*fractional accuracy is ideal, but too slow for more than 2 epochs
					} else {
						if (jdif > 10*fractional_accuracy) { binsizes_converge = false; } // the 1.7*fractional accuracy is ideal, but too slow for more than 2 epochs
					}
				}
				// only finishes if the different binsizes give same j-factor within fractional_accuracy, otherwise get more points
			}
			if (i==last_comp) { iterations_converged++; // the series has converged again; if the binsizes aren't converging to the same value, we'll need to reduce the binsizes and start again
				if (mpi_id==0) {
					if (output_to_logfile) logfile << "series has converged " << iterations_converged << " times\n";
					else cout << "series has converged " << iterations_converged << " times\n";
				}
			}
		}
		if (binsizes_converge==true) converged = true;
	}
	return;
}

void R_Factors::adapt_stepsizes(int &iterations, int &iterations_since_accuracy_reset, const double &max_dvlike, int &n_times_zero_pts_in_bin, bool &converged)
{
	int k;
	if ((converged==false) and (iterations_converged==3))
	{
		// this means the likelihoods are converging, but the different bin sizes are converging to different values--this happens when the bins are too large,
		// so we'll make them smaller and start over again
		iterations = 0;
		iterations_since_accuracy_reset = 0;
		total_n_simulated_stars = 0;
		for (int k=0; k < 2; k++)
		{
			dv_bincount[k] = 0;
			dv_like[k] = 0;
			binsizes[k] *= 0.5; // cut the binsizes in half
			dv_bin_area[k] = c_n * pow(0.5*binsizes[k],ndim);
			sq_bin_radii[k] = 0.25*SQR(binsizes[k]);
		}
		n_simulated_stars *= ipow(2,ndim);
		n_times_decreased_binsize++;
		delete[] dv_like_vals;
		dv_like_vals = new vector<double>[2];
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "NOTE: The different bins are not converging to the same likelihood value, so we will start again with smaller bins.\n\n";
			else cout << "NOTE: The different bins are not converging to the same likelihood value, so we will start again with smaller bins.\n\n";
		}
	}
	else if ((n_times_increased_binsize >= 2) and (dv_bincount[0]==0) and (dv_bincount[1]==0)) {
		for (k=0; k < 2; k++)
			dv_like[k] = 0;
		converged = true;
		if (mpi_id==0) {
			if (output_to_logfile) logfile << "NOTE: After increasing the binsize two times, no points are falling in, so the likelihood is zero for all intents and purposes.\n\n";
			else cout << "NOTE: After increasing the binsize two times, no points are falling in, so the likelihood is zero for all intents and purposes.\n\n";
		}
	}
	else if (dv_bincount[0]==0)
	{
		if ((dv_bincount[1]==0) and (n_simulated_stars > 1000000) and (max_dvlike > 5e-5)) {
			for (k=0; k < 2; k++)
				dv_like[k] = 0;
			converged = true;
			if (mpi_id==0) {
				if (output_to_logfile) logfile << "NOTE: The likelihood is decreasing to small values, and no points are falling in, so the likelihood is zero for all intents and purposes.\n\n";
				else cout << "NOTE: The likelihood is decreasing to small values, and no points are falling in, so the likelihood is zero for all intents and purposes.\n\n";
			}
		} else if (new_binsize==true) {
			n_simulated_stars *= ipow(3,ndim);
			if (max_dvlike <= 1e-30) {
				n_times_zero_pts_in_bin++;
				if (n_times_zero_pts_in_bin < 3) { // we only want to do this a couple times
					if (fractional_accuracy < 0.5)
						fractional_accuracy *= 2;
				}
			}
			new_binsize = false;
			if (mpi_id==0) {
				if (output_to_logfile) logfile << "NOTE: No points are falling inside bin, so we will try increasing the number of simulated stars to " << n_simulated_stars << " before doubling the binsize.\n\n";
				else cout << "NOTE: No points are falling inside bin, so we will try increasing the number of simulated stars to " << n_simulated_stars << " before doubling the binsize.\n\n";
			}
		} else {
			// this means we need a larger binsize, because no points are falling in
			iterations = 0;
			iterations_since_accuracy_reset = 0;
			total_n_simulated_stars = 0;
			for (int k=0; k < 2; k++)
			{
				dv_bincount[k] = 0;
				dv_like[k] = 0;
				binsizes[k] *= 2; // double the binsize
				dv_bin_area[k] = c_n * pow(0.5*binsizes[k],ndim);
				sq_bin_radii[k] = 0.25*SQR(binsizes[k]);
			}
			n_simulated_stars *= 2;
			n_times_increased_binsize++;
			delete[] dv_like_vals;
			dv_like_vals = new vector<double>[2];
			if (mpi_id==0) {
				if (output_to_logfile) logfile << "NOTE: Since no points are falling inside bin, we will double the binsize and start again.\n\n";
				else cout << "NOTE: Since no points are falling inside bin, we will double the binsize and start again.\n\n";
			}
			new_binsize = true;
		}
	}
}

void R_Factors::output_nstars(const string &nstarfilename, const int &ii)
{
	ofstream nstar_file(nstarfilename.c_str());
	int stars_finished = ii+1;
	stringstream nstars_str;
	nstars_str << stars_finished;
	string nstars_string;
	nstars_str >> nstars_string;
	nstar_file << nstars_string << endl;	// this keeps track of how many stars are done so far, so incomplete data sets can be analyzed
	nstar_file.close();
}

void R_Factors::output_like_nb_file(const string like_nb_filename, const double v_avg, const double sigm)
{
	ofstream like_nb_file(like_nb_filename.c_str());
	double like_vcm_nb_analytic;
	for (int i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step) {
		like_vcm_nb_analytic = exp(-0.5*SQR(v_avg-vcm)/(SQR(sigm)))/sigm/SQRT_2PI;
		like_nb_file << vcm << " " << like_vcm_nb_analytic << endl;
	}
}

void R_Factors::output_rfactor_files(const string rfilename, bool nonzero_rfactor)
{
	int i,k;
	ofstream rfile[2];
	double rfactor;
	for (k=0; k < 2; k++) {
		stringstream kstr;
		string kstring, rfilename_k;
		kstr << k;
		kstr >> kstring;
		rfilename_k = rfilename + "." + kstring;
		if (k==1) {
			rfilename_k = rfilename;
		} // best resolution file doesn't need a ".#" at the end
		rfile[k].open(rfilename_k.c_str());
		for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step)
		{
			if (nonzero_rfactor)
				rfactor = vcm_like[k][i] / nfactor;
			else rfactor = 0;
			rfile[k] << vcm << " " << rfactor << endl;
		}
		rfile[k].close();
	}
}

/*
void R_Factors::output_rfactor_files_offset(const string rfilename, bool nonzero_rfactor)
{
	int i,k;
	ofstream rfile[2];
	double rfactor;
	for (k=0; k < 2; k++) {
		stringstream kstr;
		string kstring, rfilename_k;
		kstr << k;
		kstr >> kstring;
		rfilename_k = rfilename + "." + kstring;
		if (k==1) {
			rfilename_k = rfilename;
		} // best resolution file doesn't need a ".#" at the end
		rfile[k].open(rfilename_k.c_str());
		for (i=0, vcm=vcm_min; i < vcm_nn; i++, vcm += vcm_step)
		{
			if (nonzero_rfactor)
				rfactor = vcm_like[k][i] / nfactor;
			else rfactor = 0;
			rfile[k] << vcm << " " << rfactor << endl;
		}
		rfile[k].close();
	}
}
*/

void R_Factors::determine_dvlike_accuracy_and_n_simulated_stars(const double &nfac, const double &sigm)
{
	 // To deal with outlier stars: if like_nb is small, the binary likelihood will be too, so we'll need more simulated stars to map out the space.
	 // Note that if the star is a nonmember, the nfac can be large (implying multi-epoch observations do not reveal it as a binary) and like_nb can be very small
	 // (since v_avg is far from systemic velocity). In this case, the binary likelihood will be small so we cannot use the nfac alone as our criteria of whether
	 // to gather more points and/or enlarge binsizes and/or lighten the accuracy requirement. Hence like_nb is used.
	size_max = binsize_max*sigm;
	fractional_accuracy = initial_fractional_accuracy;
	if (nfac < 1e-2) {
		if (nfac > 5e-5) {
			initial_n_simulated_stars *= ipow(3,ndim);
			size_max *= 2;
		} else if (nfac > 1e-8) {
			initial_n_simulated_stars *= ipow(6,ndim);
			fractional_accuracy = initial_fractional_accuracy * 2;
			size_max *= 3;
		} else if (nfac > 1e-12) {
			initial_n_simulated_stars *= ipow(10,ndim);
			fractional_accuracy = initial_fractional_accuracy * 3;
			size_max *= 4;
		} else if (nfac > 1e-15) {
			initial_n_simulated_stars *= ipow(20,ndim);
			fractional_accuracy = initial_fractional_accuracy * 4;
			size_max *= 4;
		} else {
			initial_n_simulated_stars *= ipow(20,ndim);
			fractional_accuracy = initial_fractional_accuracy * 5; // this is a compromise, otherwise it takes forever-- *reduce this to get a more accurate R-factor for the obvious binaries*
			size_max *= 4;
		}
		if (accurate_rfactors==false) fractional_accuracy *= 2;
	}
	// we want the bin sizes to be in proportion to the vcm_step that's been selected
	size_min = size_max*binsize_min/binsize_max;
	size_step = size_max-size_min;

	if (mpi_id==0) {
		if (output_to_logfile) logfile << "fractional accuracy: " << fractional_accuracy << ", number of simulated stars: " << initial_n_simulated_stars << endl;
		else cout << "fractional accuracy: " << fractional_accuracy << ", number of simulated stars: " << initial_n_simulated_stars << endl;
	}
}

void R_Factors::determine_accuracy_and_n_simulated_stars(const double &like_nb, const double &nfac, const double &sigm)
{
	vcm_window_divisions = initial_vcm_window_divisions;
	vcm_nn = vcm_window_divisions*vcm_window_size;
	//size_min = binsize_min*sigm;
	//size_max = binsize_max*sigm;
	//size_step = size_max-size_min;
	vcm_step = (vcm_max-vcm_min)/(vcm_nn-1);
	for (;;) {
		vcm_step = (vcm_max-vcm_min)/(vcm_nn-1);
		if (vcm_step > binsize_max*sigm) vcm_nn += 20;
		else break;
	}

	 // To deal with outlier stars: if like_nb is small, the binary likelihood will be too, so we'll need more simulated stars to map out the space.
	 // Note that if the star is a nonmember, the nfac can be large (implying multi-epoch observations do not reveal it as a binary) and like_nb can be very small
	 // (since v_avg is far from systemic velocity). In this case, the binary likelihood will be small so we cannot use the nfac alone as our criteria of whether
	 // to gather more points and/or enlarge binsizes and/or lighten the accuracy requirement. Hence like_nb is used.
	fractional_accuracy = initial_fractional_accuracy;
	if (like_nb < 1e-10)
	{
		use_large_tail_bins = false;
		if (accurate_rfactors) {
			if (epochs < 3) n_simulated_stars *= ipow(10,epochs);
			else n_simulated_stars *= 200;
		} else {
			if (epochs < 3) n_simulated_stars *= ipow(6,epochs);
			else n_simulated_stars *= 40;
		}
		if (nfac > 5e-5) {
			// in this case, it's probably a nonmember so we don't need good accuracy
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 10;
			else fractional_accuracy = initial_fractional_accuracy * 25;
			vcm_nn /= 2;
		} else if (nfac > 1e-9) {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			n_simulated_stars *= 5; // this takes a looooong time to calculate
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 5;
			else fractional_accuracy = initial_fractional_accuracy * 10;
			// the rfunction in this case is generally much broader than sigm suggests, so we go back to wide vcm spacing
			vcm_nn /= 2;
		} else {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			n_simulated_stars *= 5; // this takes a looooong time to calculate
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 5; // this is a compromise, otherwise it takes forever-- *reduce this to get a more accurate R-factor for the obvious binaries*
			else fractional_accuracy = initial_fractional_accuracy * 20; // this is a compromise, otherwise it takes forever-- *reduce this to get a more accurate R-factor for the obvious binaries*
			// the rfunction in this case is generally much broader than sigm suggests, so we go back to wide vcm spacing
			vcm_nn = vcm_nn_min;
			vcm_nn /= 4;
		}
	}
	else if (like_nb < 5e-7)
	{
		if (accurate_rfactors) {
			if (epochs < 3) n_simulated_stars *= ipow(8,epochs);
			else n_simulated_stars *= 100;
		} else {
			if (epochs < 3) n_simulated_stars *= ipow(4,epochs);
			else n_simulated_stars *= 20;
		}
		if (nfac > 5e-5) {
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 2;
			else fractional_accuracy = initial_fractional_accuracy * 5;
		} else {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 5;
			else fractional_accuracy = initial_fractional_accuracy * 10;
			// the rfunction in this case is generally broader than sigm suggests, so we go back to wider vcm spacing
			vcm_nn /= 2;
			use_large_tail_bins = false;
		}
	}
	else if (like_nb < 5e-4)
	{
		if (accurate_rfactors) {
			if (epochs < 3) n_simulated_stars *= ipow(4,epochs);
			else n_simulated_stars *= 20;
		} else {
			if (epochs < 3) n_simulated_stars *= ipow(2,epochs);
			else n_simulated_stars *= 4;
		}
		if (nfac < 5e-5) {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			if (accurate_rfactors) {
				fractional_accuracy = initial_fractional_accuracy * 2;
				n_simulated_stars *= 4;
			} else {
				fractional_accuracy = initial_fractional_accuracy * 5;
				n_simulated_stars *= 2;
			}
			// the rfunction in this case is generally broader than sigm suggests, so we go back to wider vcm spacing
			vcm_nn /= 2;
			use_large_tail_bins = false;
		}
	}
	else if (like_nb < 5e-3)
	{
		if (epochs > 2) n_simulated_stars *= pow(2,epochs);
	}
	if (vcm_nn < vcm_nn_min) vcm_nn = vcm_nn_min;
	vcm_step = (vcm_max-vcm_min)/(vcm_nn-1); // in case vcm_nn has been changed

	// we want the bin sizes to be in proportion to the vcm_step that's been selected
	size_max = vcm_step * sqrt(epochs); // distance between points in the N-dimensional velocity-space (N epochs)
	size_min = size_max*binsize_min/binsize_max;
	size_step = size_max-size_min;

	if (mpi_id==0) {
		if (output_to_logfile) logfile << "fractional accuracy: " << fractional_accuracy << ", number of simulated stars: " << n_simulated_stars << ", min_binsize: " << size_min << ", max_binsize: " << size_max << endl;
		else cout << "fractional accuracy: " << fractional_accuracy << ", number of simulated stars: " << n_simulated_stars << ", min_binsize: " << size_min << ", max_binsize: " << size_max << endl;

	}
}

void R_Factors::determine_accuracy_and_n_simulated_stars_sysparam(int n_sysparam_star, const double &sigm)
{
	vcm_window_divisions = initial_vcm_window_divisions;
	vcm_nn = vcm_window_divisions*vcm_window_size;
	//size_min = binsize_min*sigm;
	//size_max = binsize_max*sigm;
	//size_step = size_max-size_min;
	vcm_step = (vcm_max-vcm_min)/(vcm_nn-1);
	for (;;) {
		vcm_step = (vcm_max-vcm_min)/(vcm_nn-1);
		if (vcm_step > binsize_max*sigm) vcm_nn += 20;
		else break;
	}

	double nfactor_max=-1e30, like_nb_max;
	int nlikes = 1;
	if (n_sysparam_star>=1) nlikes *= sysparam_nn[sysparam1];
	if (n_sysparam_star>=2) nlikes *= sysparam_nn[sysparam2];
	if (n_sysparam_star>=3) nlikes *= sysparam_nn[sysparam3];
	for (int n=0; n < nlikes; n++) {
		if (nfactor_sysparam[n] > nfactor_max) {
			nfactor_max = nfactor_sysparam[n];
			like_nb_max = like_nb_sysparam[n];
			nlike_max_nfactor = n;
		}
	}

	 // To deal with outlier stars: if like_nb is small, the binary likelihood will be too, so we'll need more simulated stars to map out the space.
	 // Note that if the star is a nonmember, the nfactor_max can be large (implying multi-epoch observations do not reveal it as a binary) and like_nb can be very small
	 // (since v_avg is far from systemic velocity). In this case, the binary likelihood will be small so we cannot use the nfactor_max alone as our criteria of whether
	 // to gather more points and/or enlarge binsizes and/or lighten the accuracy requirement. Hence like_nb is used.
	fractional_accuracy = initial_fractional_accuracy;
	if (like_nb_max < 1e-10)
	{
		use_large_tail_bins = false;
		if (accurate_rfactors) {
			if (epochs < 3) n_simulated_stars *= ipow(10,epochs);
			else n_simulated_stars *= 200;
		} else {
			if (epochs < 3) n_simulated_stars *= ipow(6,epochs);
			else n_simulated_stars *= 40;
		}
		if (nfactor_max > 5e-5) {
			// in this case, it's probably a nonmember so we don't need good accuracy
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 10;
			else fractional_accuracy = initial_fractional_accuracy * 25;
			vcm_nn /= 2;
		} else if (nfactor_max > 1e-9) {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			n_simulated_stars *= 5; // this takes a looooong time to calculate
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 5;
			else fractional_accuracy = initial_fractional_accuracy * 10;
			// the rfunction in this case is generally much broader than sigm suggests, so we go back to wide vcm spacing
			vcm_nn /= 2;
		} else {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			n_simulated_stars *= 5; // this takes a looooong time to calculate
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 5; // this is a compromise, otherwise it takes forever-- *reduce this to get a more accurate R-factor for the obvious binaries*
			else fractional_accuracy = initial_fractional_accuracy * 20; // this is a compromise, otherwise it takes forever-- *reduce this to get a more accurate R-factor for the obvious binaries*
			// the rfunction in this case is generally much broader than sigm suggests, so we go back to wide vcm spacing
			vcm_nn = vcm_nn_min;
			vcm_nn /= 4;
		}
	}
	else if (like_nb_max < 5e-7)
	{
		if (accurate_rfactors) {
			if (epochs < 3) n_simulated_stars *= ipow(8,epochs);
			else n_simulated_stars *= 100;
		} else {
			if (epochs < 3) n_simulated_stars *= ipow(4,epochs);
			else n_simulated_stars *= 20;
		}
		if (nfactor_max > 5e-5) {
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 2;
			else fractional_accuracy = initial_fractional_accuracy * 5;
		} else {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			if (accurate_rfactors) fractional_accuracy = initial_fractional_accuracy * 5;
			else fractional_accuracy = initial_fractional_accuracy * 10;
			// the rfunction in this case is generally broader than sigm suggests, so we go back to wider vcm spacing
			vcm_nn /= 2;
			use_large_tail_bins = false;
		}
	}
	else if (like_nb_max < 5e-4)
	{
		if (accurate_rfactors) {
			if (epochs < 3) n_simulated_stars *= ipow(4,epochs);
			else n_simulated_stars *= 20;
		} else {
			if (epochs < 3) n_simulated_stars *= ipow(2,epochs);
			else n_simulated_stars *= 4;
		}
		if (nfactor_max < 5e-5) {
			// it's probably a binary so we don't want to reduce the accuracy *too* much
			if (accurate_rfactors) {
				fractional_accuracy = initial_fractional_accuracy * 2;
				n_simulated_stars *= 4;
			} else {
				fractional_accuracy = initial_fractional_accuracy * 5;
				n_simulated_stars *= 2;
			}
			// the rfunction in this case is generally broader than sigm suggests, so we go back to wider vcm spacing
			vcm_nn /= 2;
			use_large_tail_bins = false;
		}
	}
	else if (like_nb_max < 5e-3)
	{
		if (epochs > 2) n_simulated_stars *= pow(2,epochs);
	}
	if (vcm_nn < vcm_nn_min) vcm_nn = vcm_nn_min;
	vcm_step = (vcm_max-vcm_min)/(vcm_nn-1); // in case vcm_nn has been changed

	// we want the bin sizes to be in proportion to the vcm_step that's been selected
	size_max = vcm_step * sqrt(epochs); // distance between points in the N-dimensional velocity-space (N epochs)
	size_min = size_max*binsize_min/binsize_max;
	size_step = size_max-size_min;

	if (mpi_id==0) {
		if (output_to_logfile) logfile << "fractional accuracy: " << fractional_accuracy << ", number of simulated stars: " << n_simulated_stars << ", min_binsize: " << size_min << ", max_binsize: " << size_max << endl;
		else cout << "fractional accuracy: " << fractional_accuracy << ", number of simulated stars: " << n_simulated_stars << ", min_binsize: " << size_min << ", max_binsize: " << size_max << endl;
	}
}

void generate_binary_velocities_multi_thread(Thread_Data &thread_data, dvector &vbin,  lvector &times)
{
	double logP, period, q, e, a, roche_lobe_radius, theta, psi, phi, time, newtime;
	for (;;)
	{
		if (thread_data.magnitude==-99) {
			double magnitude, abs_magnitude;
			for (;;) {
				// this is a hack for Sextans to handle stars without magnitude info; draws thread_data.randseq magnitudes consistent with its magnitude distribution (using a Gaussian fit)
				magnitude = 19.84 + 1.27*normal_deviate(thread_data.rand);
				if ((magnitude >= 18) and (magnitude <= 20.4)) break;
			}
			abs_magnitude = find_absolute_magnitude(87,magnitude); // 87 kpc is distance to Sextans
			if (!output_primary_mass) thread_data.primary_mass = find_stellar_mass(abs_magnitude); // if the datafile contained the primary mass directly, then it's already stored in thread_data.primary_mass
			if (radius_from_isochrone) thread_data.stellar_radius = find_stellar_radius(thread_data.primary_mass);
		}

		// Binary Parameters      

		logP = gen_logP(thread_data.rand); 
		if (fix_q) q = q_value;
		else if (use_experimental_qdist) q = gen_q_experimental(logP,thread_data.rand);
		else q = gen_q(logP,thread_data.rand);
		a = primary_semimajor_axis(logP, thread_data.primary_mass, q);
		roche_lobe_radius = roche_lobe_radius_approximation(a,q);

		// approximation for case of observed secondary with massive compact primaries
		// do this more accurately so Rstar is maximum RGB radius as function of mass!
		//if ((q>1) and (Rstar != 0)) Rstar = 1;

		if (thread_data.stellar_radius > roche_lobe_radius) continue;

		if (fix_eccentricity) e = eccentricity;
		else if (use_experimental_edist) e = gen_e_experimental(logP,thread_data.rand);
		else e = gen_e(logP,thread_data.rand);
		generate_euler_angles(theta,psi,thread_data.rand); 
		if (fix_theta) theta = M_HALFPI;
		if (fix_psi) psi = M_HALFPI;

		phi = gen_phi_multi_thread(e,time,thread_data.rand);
		period = pow(10.0,logP);
		vbin[0] = vbin_los(a,period,e,theta,psi,phi);

		times[0] = time*period;
		for (int j=1; j < thread_data.epochs; j++)
		{
			times[j] = times[0] + thread_data.time_interval_years[j-1];	// time_intervals is in years
			newtime = times[j]/period - (int)(times[j]/period);
			phi = evolve_phi_multi_thread(e,newtime);
			vbin[j] = vbin_los(a,period,e,theta,psi,phi);
		}
		break;
	}
	if (make_posterior) {
		thread_data.postpt[0] = psi;
		thread_data.postpt[1] = time;
		thread_data.postpt[2] = e;
		thread_data.postpt[3] = q; // change this to x, the partial velocity amplitude
		thread_data.postpt[4] = logP;
	}
	return;
}

/************* finding a prior in the period distribution parameters **************/

double sigma_mu_value, logP_value;
double sigma_logP_min, sigma_logP_max;
dvector logP_datapoints;
int n_logP_datapoints;

double binary_period_likelihood_integrand(double sigma)
{
	double sqrwidth = SQR(sigma) + SQR(sigma_mu_value);
	return exp(-0.5*SQR(logP_value-mu_logP_fiducial)/sqrwidth)/sqrt(M_2PI*sqrwidth)/(sigma_logP_max-sigma_logP_min);
}

double binary_period_loglikelihood(double sigma_mu)
{
	sigma_mu_value = sigma_mu;
	double like, loglike=0;
	for (int i=0; i < n_logP_datapoints; i++)
	{
		logP_value = logP_datapoints[i];
		like = romberg(binary_period_likelihood_integrand,sigma_logP_min,sigma_logP_max,1e-4,5);
		//double sqrwidth = SQR(sig_logP) + SQR(sigma_mu);
		//like = exp(-0.5*SQR(logP_value-mu_logP_fiducial)/sqrwidth)/sqrt(M_2PI*sqrwidth);
		loglike -= log10(like);
	}
	return loglike;
}

void plot_binary_period_loglikelihood(void)
{
	sigma_logP_min = 0.5; sigma_logP_max = 2.3;
	create_duquennoy_mayor_dataset(1000);
	double loglike, sigmu, sigmu_step, sigmu_min = 1, sigmu_max = 3;
	int i, sigmu_nn = 30;
	sigmu_step = (sigmu_max-sigmu_min)/(sigmu_nn-1);

	BrentsMinMethod maxlike;
	double sigmin=0, sigmax=10;
	maxlike.set_interval(sigmu_min,sigmu_max);
	double best_fit_sigmu = maxlike.minimize(binary_period_loglikelihood);
	cout << best_fit_sigmu << endl;
}

void create_duquennoy_mayor_dataset(int nn_logP_datapoints)
{
	n_logP_datapoints = nn_logP_datapoints;
	logP_datapoints.input(n_logP_datapoints);
	for (int i=0; i < n_logP_datapoints; i++)
		logP_datapoints[i] = mu_logP_fiducial + sig_logP_fiducial*randseq.NormalDeviate();
}

/***************************** dataset structures ********************************/

struct star
{
	bool clipped;
	bool giant;
	bool binary;
	bool preset_times;
	double magnitude;
	string id;
	int n_epochs;
	dvector times;
	dvector times_zero_offset;
	dvector delta_t;
	dvector vlos, vlos_noerr;
	dvector sig_err;
	dvector r_value;
	ivector channel;
	double R; // position from center of galaxy
	dvector metallicity, metallicity_err;
	double v_average, sig_err_average;
	star(int nn) { clipped=false; giant=false; n_epochs = nn; vlos_noerr.input(nn); vlos.input(nn); metallicity.input(nn); metallicity_err.input(nn); sig_err.input(nn); r_value.input(nn); channel.input(nn); delta_t.input(nn-1); magnitude = 1e30; preset_times=false;}
	star() { clipped=false; giant=false; magnitude = -1e30; }
	void input(int id_int, int nn) { stringstream idstr; idstr << id_int; idstr >> id; n_epochs = nn; vlos_noerr.input(nn); vlos.input(nn); metallicity.input(nn); metallicity_err.input(nn); sig_err.input(nn); r_value.input(nn); channel.input(nn); delta_t.input(nn-1); magnitude = -1e30; preset_times=false; }
	void input(string idd, int nn) { id=idd; n_epochs = nn; vlos_noerr.input(nn); vlos.input(nn); metallicity.input(nn); metallicity_err.input(nn); sig_err.input(nn); r_value.input(nn); channel.input(nn); delta_t.input(nn-1); magnitude = -1e30; preset_times=false; }
	void input(string idd, int nn, double mag) { id=idd; n_epochs = nn; vlos_noerr.input(nn); vlos.input(nn); metallicity.input(nn); metallicity_err.input(nn); sig_err.input(nn); r_value.input(nn); channel.input(nn); delta_t.input(nn-1); magnitude = mag; preset_times=true; times.input(nn); times_zero_offset.input(nn); }
	void input(string idd, int nn, double mag, double distance) { id=idd; n_epochs = nn; vlos_noerr.input(nn); vlos.input(nn); metallicity.input(nn); metallicity_err.input(nn); sig_err.input(nn); r_value.input(nn); channel.input(nn); delta_t.input(nn-1); magnitude = find_absolute_magnitude(distance,mag); preset_times=true; times.input(nn); times_zero_offset.input(nn); }
	double velocity(int j) { return vlos[j]; }
	~star() {}

	double find_v_average()
	{
		double sigsqr_err_average_no_systematic, sigsqr_err_no_systematic;
		double sig_sqr_average_inv = 0;
		v_average = 0;
		for (int i=0; i < n_epochs; i++)
		{
			if (sig_err[i] < systematic_error) die("wtf? err smaller than systematic: %g",sig_err[i]); // this error should predominate
			else if (sig_err[i] == 0) { cout << "star " << id << ", epoch " << i << endl; die("cannot have a measurement error of zero"); }
			else sigsqr_err_no_systematic = SQR(sig_err[i]) - SQR(systematic_error);
			v_average += vlos[i]/sigsqr_err_no_systematic;
			sig_sqr_average_inv += 1.0/sigsqr_err_no_systematic;
		}
		v_average /= sig_sqr_average_inv;
		sigsqr_err_average_no_systematic = 1.0/sig_sqr_average_inv;
		sig_err_average = sqrt(sigsqr_err_average_no_systematic + SQR(systematic_error));
	}

	void assign_velocities_errors(void)
	{
		double logP, q, e, phi, M, a, r, dphidt, x, mem_p, dispvel, sysvel;
		double period, time, time_year, time_evolved_year, newtime, newtime_years;
		double newphi, newr, newdphidt, orientation_cosine;
		double theta, psi;	// Euler angles
		double roche_lobe_radius;
		double threshold_frac, threshold_frac_noerr, threshold_frac_only_err, threshold_frac_err, k_value, k_value_approx, beta_value;

		// Generate input dispersion velocities
		int i,j;
		dvector vbin(n_epochs);
		times.input(n_epochs);
		times_zero_offset.input(n_epochs);
		double v_err;
		double abs_magnitude;

		dispvel = gdisp*randseq.NormalDeviate();
		if (systematic_error != 0) sysvel = systematic_error*randseq.NormalDeviate();
		else sysvel = 0;
		
		x = randseq.RandomNumber2();
		if (x < binary_fraction) // if binary generate/calculate binary parameters
		{
			binary=true;
			for (;;)
			{
				// Binary Parameters      

				if (magnitude==-99) {
					for (;;) {
						// this is a hack for Sextans to handle stars without magnitude info; draws randseq magnitudes consistent with its magnitude distribution (using a Gaussian fit)
						magnitude = 19.84 + 1.27*randseq.NormalDeviate();
						if ((magnitude >= 18) or (magnitude <= 20.4)) break;
					}
					abs_magnitude = find_absolute_magnitude(87,magnitude); // Sextans is 87 kpc away
					M = find_stellar_mass(abs_magnitude);
				}
				else if (magnitude != -1e30) {
					if (distance_kpc != 1e30)
						abs_magnitude = find_absolute_magnitude(distance_kpc,magnitude);
					else abs_magnitude = magnitude;
					M = find_stellar_mass(abs_magnitude);
				}
				else M = (M_bigstar==0) ? gen_m_kroupa() : M_bigstar;
				if (radius_from_isochrone) Rstar = find_stellar_radius(M);
				if (Rstar==-1) continue;
				if (magnitude != -1e30) ;
				else if (fix_absolute_magnitude) magnitude = abs_magnitude_value;
				else
				{
					magnitude = magnitude_from_mass(M);
					if (magnitude == -1e30) { continue; } // cuts out stars beyond limiting magnitude
					if ((giant==true) and (magnitude > 2)) { continue; }
				}

				logP = gen_logP(); 
				period = pow(10.0,logP);
				if (fix_q) q = q_value;
				else if (use_experimental_qdist) q = gen_q_experimental(logP);
				else q = gen_q(logP);

				if (fix_eccentricity) e = eccentricity;
				else if (use_experimental_edist) e = gen_e_experimental(logP);
				else e = gen_e(logP);

				a = primary_semimajor_axis(logP, M, q);
				roche_lobe_radius = roche_lobe_radius_approximation(a,q);

				// approximation for case of observed secondary with massive compact primaries
				// do this more accurately so Rstar is maximum RGB radius as function of mass!
				//if ((q>1) and (Rstar != 0)) Rstar = 1.5;

				if (Rstar > roche_lobe_radius) continue;

				generate_euler_angles(theta,psi); 
				phi = gen_phi(e,time);
				vbin[0] = vbin_los(a,period,e,theta,psi,phi);

				times[0] = time*period;
				times_zero_offset[0] = 0;
				for (j=1; j < n_epochs; j++)
				{
					newtime_years = times[j-1] + delta_t[j-1];	// delta_t is in years
					newtime = newtime_years/period - (int)(newtime_years/period);
					phi = evolve_phi(e,newtime);
					vbin[j] = vbin_los(a,period,e,theta,psi,phi);
					times[j] = newtime_years;
					times_zero_offset[j] = times[j] - times[0];
				}
				break;
		  }
		  // Note calculation starts over if resulting semimajor axis
		  // is disallowed by the stellar radius
		}
		else
		{
			binary=false;
			times[0] = 0;
			for (j=1; j < n_epochs; j++)
				times[j] = times[j-1] + delta_t[j-1];	// delta_t is in years
			times_zero_offset = times;
			for (j=0; j < n_epochs; j++) {
				vbin[j] = 0;	// not a binary
			}
		}
		for (j=0; j < n_epochs; j++)
		{
			vlos_noerr[j] = vbin[j] + dispvel + systemic_velocity;
			//if (use_tonry_davis_rvalues) {
				//v_err = generate_velocity_error_from_rvalue(sig_err[j],r_value[j],channel[j]);
				//cout << v_err << " " << sig_err[j] << " " << r_value[j] << " " << channel[j] << endl;
			//}
			//else v_err = generate_velocity_error(sig_err[j]);
			v_err = generate_velocity_error(sig_err[j]);
			vlos[j] = vlos_noerr[j] + sysvel + v_err;
		}
		find_v_average();
	}
	void output_data(ofstream &data_out, ofstream &walker_out)
	{
		data_out << id << " " << magnitude << " ";
		if (include_positions) data_out << R << " ";
		data_out << n_epochs << endl;
		for (int j=0; j < n_epochs; j++)
		{
			data_out << times_zero_offset[j] << " " << vlos[j] << " " << sig_err[j];
			if (include_metal_widths) data_out << " " << metallicity[j] << " " << metallicity_err[j];
			data_out << endl;

			string channel_str;
			for (j=0; j < n_epochs; j++) {
				double days_zero_offset = 365*times_zero_offset[j];
				if (channel[j]==0) channel_str = "R";
				else channel_str = "B";
				walker_out << id << " " << channel_str << " " << days_zero_offset << " 0 0 " << magnitude << " 0 0 0 " << vlos[j] << " " << sig_err[j] << " " << metallicity[j] << " " << metallicity_err[j] << " " << n_epochs << " 1 " << r_value[j] << " 1000" << endl;
			}
		}
	}
	void output_data(ofstream &data_out)
	{
		data_out << id << " " << magnitude << " ";
		if (include_positions) data_out << R << " ";
		data_out << n_epochs << endl;
		for (int j=0; j < n_epochs; j++)
		{
			data_out << times_zero_offset[j] << " " << vlos[j] << " " << sig_err[j];
			if (include_metal_widths) data_out << " " << metallicity[j] << " " << metallicity_err[j];
			data_out << endl;
		}
	}
};

struct dataset
{
	int nn;
	star *stars;
	bool make_binary_correction;
	Spline2D binary_dispratio_spline;
	Spline f_prior_spline;

	void plot_f_prior(void) { f_prior_spline.printall(100); }

	dataset() { make_binary_correction = false;}
	dataset(int n1, int n2, int n3, int n4)
	{
		nn=n1+n2+n3+n4;
		stars = new star[nn];
		int i;
		for (i=0; i < n1; i++) { stars[i].input(i,1); stars[i].assign_velocities_errors(); }
		for (i=0; i < n2; i++) { stars[n1+i].input(n1+i,2); stars[n1+i].assign_velocities_errors(); }
		for (i=0; i < n3; i++) { stars[n1+n2+i].input(n1+n2+i,3); stars[n1+n2+i].assign_velocities_errors(); }
		for (i=0; i < n4; i++) { stars[n1+n2+n3+i].input(n1+n2+n3+i,4); stars[n1+n2+n3+i].assign_velocities_errors(); }
		make_binary_correction=false;
	}
	void set_magnitude_limit(double maglim)
	{ 
		max_absolute_magnitude = maglim;
		minimum_mass = find_stellar_mass(max_absolute_magnitude);
	}
	void output_data(string filename)
	{
		string walker_filename = filename + ".vel";
		ofstream data_out(filename.c_str());
		data_out << nn << endl;

		if (use_tonry_davis_rvalues) {
			ofstream walker_out(walker_filename.c_str());
			for (int i=0; i < nn; i++)
			{
				stars[i].output_data(data_out,walker_out);
			}
		} else {
			for (int i=0; i < nn; i++)
			{
				stars[i].output_data(data_out);
			}
		}
	}
	void output_data_separate_by_epoch(string filename)
	{
		string filename_1epoch = filename + ".1epoch";
		string filename_2epoch = filename + ".2epoch";
		string filename_3epoch = filename + ".3epoch";
		string filename_4epoch = filename + ".4epoch";
		ofstream out_1epoch(filename_1epoch.c_str());
		ofstream out_2epoch(filename_2epoch.c_str());
		ofstream out_3epoch(filename_3epoch.c_str());
		ofstream out_4epoch(filename_4epoch.c_str());
		int nn_1epoch=0, nn_2epoch=0, nn_3epoch=0, nn_4epoch=0;
		int e3file=0, e4file=0;
		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==1) nn_1epoch++;
			if (stars[i].n_epochs==2) nn_2epoch++;
			if (stars[i].n_epochs==3) nn_3epoch++;
			if (stars[i].n_epochs==4) nn_4epoch++;
		}
		out_1epoch << nn_1epoch << endl;
		out_1epoch << nn_2epoch << endl;
		out_1epoch << nn_3epoch << endl;
		out_1epoch << nn_4epoch << endl;

		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==1)
				stars[i].output_data(out_1epoch);
			if (stars[i].n_epochs==2)
				stars[i].output_data(out_2epoch);
			if (stars[i].n_epochs==3)
				stars[i].output_data(out_3epoch);
			if (stars[i].n_epochs==4)
				stars[i].output_data(out_4epoch);
		}
	}


	void input(string filename)
	{
		double n_epochs;
		double magnitude, date, offset;
		int sysparam;
		string id;
		ifstream file(filename.c_str());
		file >> nn;
		stars = new star[nn];
		for (int i=0; i < nn; i++)
		{
			file >> id >> magnitude;
			if (include_positions) file >> stars[i].R;
			file >> n_epochs;
			stars[i].input(id,n_epochs,magnitude);
			for (int j=0; j < n_epochs; j++)
			{
				file >> stars[i].times[j] >> stars[i].vlos[j] >> stars[i].sig_err[j];
				if (include_metal_widths) file >> stars[i].metallicity[j] >> stars[i].metallicity_err[j];
				if (include_error_params) file >> stars[i].r_value[j] >> stars[i].channel[j];
				if (include_systematic_params) {
					for (int ll=0; ll < 2; ll++) {
						file >> sysparam;
						if ((sysparam != -1) and (sys_param_vals.size() > 0)) {
							if (sysparam_type[sysparam]==0) stars[i].vlos[j] -= sys_param_vals[sysparam];
							else if (sysparam_type[sysparam]==1) stars[i].sig_err[j] = sqrt(SQR(stars[i].sig_err[j]) + sysparam*sysparam);
							else die("sysparam type %i not recognized",sysparam_type[sysparam]);
						}
					}
				}
				
				stars[i].times_zero_offset[j] = stars[i].times[j] - stars[i].times[0];
				if (j>0) stars[i].delta_t[j-1] = stars[i].times[j] - stars[i].times[0];
			}
			stars[i].find_v_average();
		}
	}
	int dv_clip(void)
	{
		return 0;
		int n_clipped=0;
		for (int i=0; i < nn; i++)
		{
			for (int j=0; j < stars[i].n_epochs; j++)
			{
				for (int k=0; k < j; k++)
				{
					if (fabs(stars[i].vlos[j] - stars[i].vlos[k]) > 2*sqrt(SQR(stars[i].sig_err[j])+SQR(stars[i].sig_err[k])))
					{
						stars[i].clipped=true;
						n_clipped++;
					}
					if (stars[i].clipped) break;
				}
				if (stars[i].clipped) break;
			}
			stars[i].find_v_average();
		}
		return n_clipped;
	}
	void plot_dverrs(string filename_no_extension)
	{
		string filename = filename_no_extension + ".dverrs";
		ofstream dverrs_file(filename.c_str());
		string filename_id = filename_no_extension + ".dverrs_id";
		ofstream dverrs_id_file(filename_id.c_str());
		double dv,dverr;
		for (int i=0; i < nn; i++)
		{
			for (int j=0; j < stars[i].n_epochs; j++)
			{
				for (int k=0; k < j; k++)
				{
					dv =  fabs(stars[i].vlos[j] - stars[i].vlos[k]);
					dverr =  dv / sqrt(SQR(stars[i].sig_err[j])+SQR(stars[i].sig_err[k]));
					dverrs_file << dverr << endl;
					dverrs_id_file << dverr << " " << dv << " " << stars[i].id << endl;
				}
			}
		}
	}
	void output_vavgs_errs(string filename_no_extension)
	{
		string filename = filename_no_extension + ".vavgs";
		ofstream vavgs_file(filename.c_str());
		for (int i=0; i < nn; i++)
		{
			vavgs_file << stars[i].id << " " << stars[i].v_average << " " << stars[i].sig_err_average << endl;
		}
	}

	void output_maxdv(string filename_no_extension)
	{
		double dv, maxdv, sig2e, maxdv_over_sig;
		string filename_dv_over_sig = filename_no_extension + ".dverrs";
		string filename = filename_no_extension + ".maxdv";
		string filename_id = filename_no_extension + ".dverrs_id";

		//string filename_3days = filename_no_extension + ".maxdv.3days";
		//string filename_9days = filename_no_extension + ".maxdv.9days";
		//string filename_gt9days = filename_no_extension + ".maxdv.gt9days";
		//string filename_1year = filename_no_extension + ".maxdv.1year";
		//string filename_2year = filename_no_extension + ".maxdv.2year";
		////string filename_dv_over_sig_3days = filename_no_extension + ".maxdvsig.3days";
		//string filename_dv_over_sig_9days = filename_no_extension + ".maxdvsig.9days";
		//string filename_dv_over_sig_gt9days = filename_no_extension + ".maxdvsig.gt9days";
		//string filename_dv_over_sig_1year = filename_no_extension + ".maxdvsig.1year";
		//string filename_dv_over_sig_2year = filename_no_extension + ".maxdvsig.2year";

		ofstream dv_over_sig_file(filename_dv_over_sig.c_str());
		ofstream dv_over_sig_id_file(filename_id.c_str());
		for (int i=0; i < nn; i++)
		{
			double dv_over_sig;
			if (stars[i].n_epochs==1) continue;
			for (int j=0; j < stars[i].n_epochs-1; j++)
			{
				dv = fabs(stars[i].vlos[j+1] - stars[i].vlos[j]);
				sig2e = sqrt(SQR(stars[i].sig_err[j]) + SQR(stars[i].sig_err[j+1]));
				dv_over_sig = dv/sig2e;
				dv_over_sig_file << dv_over_sig << endl;
				dv_over_sig_id_file << dv_over_sig << " " << dv << " " << stars[i].id << endl;
			}
		}
		dv_over_sig_file.close();

		ofstream maxdv_file(filename.c_str());
		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==1) continue;
			maxdv=0;
			for (int j=0; j < stars[i].n_epochs-1; j++)
			{
				dv = fabs(stars[i].vlos[j+1] - stars[i].vlos[j]);
				if (dv > maxdv) maxdv=dv;
			}
			if (maxdv==0) continue;
			maxdv_file << maxdv << endl;
		}
		maxdv_file.close();

		/*
		ofstream maxdv_file_9days(filename_9days.c_str());
		ofstream maxdv_file_dv_over_sig_9days(filename_dv_over_sig_9days.c_str());
		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==1) continue;
			maxdv=0;
			for (int j=0; j < stars[i].n_epochs-1; j++)
			{
				//if ((stars[i].delta_t[j] > 0.013) and (stars[i].delta_t[j] < 0.025)) {
				if (stars[i].delta_t[j] < 0.025) {
					dv = fabs(stars[i].vlos[j+1] - stars[i].vlos[j]);
					if (dv > maxdv) {
						maxdv=dv;
						sig2e = sqrt(SQR(stars[i].sig_err[j]) + SQR(stars[i].sig_err[j+1]));
						maxdv_over_sig = maxdv/sig2e;
					}
				}
			}
			if (maxdv==0) continue;
			maxdv_file_9days << maxdv << endl;
			maxdv_file_dv_over_sig_9days << maxdv_over_sig << endl;
		}
		maxdv_file_9days.close();
		maxdv_file_dv_over_sig_9days.close();

		ofstream maxdv_file_1year(filename_1year.c_str());
		ofstream maxdv_file_dv_over_sig_1year(filename_dv_over_sig_1year.c_str());
		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==1) continue;
			maxdv=0;
			for (int j=0; j < stars[i].n_epochs-1; j++)
			{
				if ((stars[i].delta_t[j] > 0.9) and (stars[i].delta_t[j] < 1.1)) {
					dv = fabs(stars[i].vlos[j+1] - stars[i].vlos[j]);
					if (dv > maxdv) {
						maxdv=dv;
						sig2e = sqrt(SQR(stars[i].sig_err[j]) + SQR(stars[i].sig_err[j+1]));
						maxdv_over_sig = maxdv/sig2e;
					}
				}
			}
			if (maxdv==0) continue;
			maxdv_file_1year << maxdv << endl;
			maxdv_file_dv_over_sig_1year << maxdv_over_sig << endl;
		}
		maxdv_file_1year.close();
		maxdv_file_dv_over_sig_1year.close();

		ofstream maxdv_file_2year(filename_2year.c_str());
		ofstream maxdv_file_dv_over_sig_2year(filename_dv_over_sig_2year.c_str());
		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==1) continue;
			maxdv=0;
			for (int j=0; j < stars[i].n_epochs-1; j++)
			{
				if (stars[i].delta_t[j] > 1.9) {
					dv = fabs(stars[i].vlos[j+1] - stars[i].vlos[j]);
					if (dv > maxdv) {
						maxdv=dv;
						sig2e = sqrt(SQR(stars[i].sig_err[j]) + SQR(stars[i].sig_err[j+1]));
						maxdv_over_sig = maxdv/sig2e;
					}
				}
			}
			if (maxdv==0) continue;
			maxdv_file_2year << maxdv << endl;
			maxdv_file_dv_over_sig_2year << maxdv_over_sig << endl;
		}
		maxdv_file_2year.close();
		maxdv_file_dv_over_sig_2year.close();

		ofstream maxdv_file_gt9days(filename_gt9days.c_str());
		ofstream maxdv_file_dv_over_sig_gt9days(filename_dv_over_sig_gt9days.c_str());
		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==1) continue;
			maxdv=0;
			for (int j=0; j < stars[i].n_epochs-1; j++)
			{
				if (stars[i].delta_t[j] > 0.025) {
					dv = fabs(stars[i].vlos[j+1] - stars[i].vlos[j]);
					if (dv > maxdv) {
						maxdv=dv;
						sig2e = sqrt(SQR(stars[i].sig_err[j]) + SQR(stars[i].sig_err[j+1]));
						maxdv_over_sig = maxdv/sig2e;
					}
				}
			}
			if (maxdv==0) continue;
			maxdv_file_gt9days << maxdv << endl;
			maxdv_file_dv_over_sig_gt9days << maxdv_over_sig << endl;
		}
		maxdv_file_gt9days.close();
		maxdv_file_dv_over_sig_gt9days.close();
		*/
	}

	void input(string filename, double distance)
	{
		double n_epochs;
		double magnitude;
		string id;
		ifstream file(filename.c_str());
		file >> nn;
		stars = new star[nn];
		for (int i=0; i < nn; i++)
		{
			file >> id >> magnitude;
			if (include_positions) file >> stars[i].R;
			file >> n_epochs;
			stars[i].input(id,n_epochs,magnitude,distance);
			for (int j=0; j < n_epochs; j++)
			{
				file >> stars[i].times[j] >> stars[i].vlos[j] >> stars[i].sig_err[j];
				if (include_metal_widths) file >> stars[i].metallicity[j] >> stars[i].metallicity_err[j];
				if (include_error_params) file >> stars[i].r_value[j] >> stars[i].channel[j];
				stars[i].times_zero_offset[j] = stars[i].times[j] - stars[i].times[0];
				if (j>0) stars[i].delta_t[j-1] = stars[i].times[j] - stars[i].times[0];
			}
			stars[i].find_v_average();
		}
	}

	void reassign_velocities(void)
	{
		for (int i=0; i < nn; i++)
		{
			stars[i].assign_velocities_errors();
			stars[i].find_v_average();
		}
	}
	void input(string filename_1epoch, string filename_2epoch, string filename_3epoch, string filename_4epoch)
	{
		double id, magnitude, date;
		int nn1, nn2, nn3, nn4;
		ifstream file_1epoch(filename_1epoch.c_str());
		ifstream file_2epoch(filename_2epoch.c_str());
		ifstream file_3epoch(filename_3epoch.c_str());
		ifstream file_4epoch(filename_4epoch.c_str());
		file_1epoch >> nn1;
		file_2epoch >> nn2;
		file_3epoch >> nn3;
		file_4epoch >> nn4;
		nn = nn1+nn2+nn3+nn4;
		stars = new star[nn];
		int i;
		double ep, R;
		for (i=0; i < nn1; i++)
		{
			stars[i].input(i,1);
			file_1epoch >> stars[i].id >> magnitude;
			file_1epoch >> R;
			file_1epoch >> ep;
			file_1epoch >> date >> stars[i].vlos[0] >> stars[i].sig_err[0];
			stars[i].find_v_average();
		}
		for (i=nn1; i < nn1+nn2; i++)
		{
			stars[i].input(i,2);
			file_2epoch >> stars[i].id >> magnitude;
			file_2epoch >> R;
			file_2epoch >> ep;
			file_2epoch >> date >> stars[i].vlos[0] >> stars[i].sig_err[0];
			file_2epoch >> date >> stars[i].vlos[1] >> stars[i].sig_err[1];
			stars[i].find_v_average();
		}
		for (i=nn1+nn2; i < nn1+nn2+nn3; i++)
		{
			stars[i].input(i,3);
			file_3epoch >> stars[i].id >> magnitude;
			file_3epoch >> R;
			file_3epoch >> ep;
			file_3epoch >> date >> stars[i].vlos[0] >> stars[i].sig_err[0];
			file_3epoch >> date >> stars[i].vlos[1] >> stars[i].sig_err[1];
			file_3epoch >> date >> stars[i].vlos[2] >> stars[i].sig_err[2];
			stars[i].find_v_average();
		}
		for (i=nn1+nn2+nn3; i < nn; i++)
		{
			stars[i].input(i,4);
			file_4epoch >> stars[i].id >> magnitude;
			file_4epoch >> R;
			file_4epoch >> ep;
			file_4epoch >> date >> stars[i].vlos[0] >> stars[i].sig_err[0];
			file_4epoch >> date >> stars[i].vlos[1] >> stars[i].sig_err[1];
			file_4epoch >> date >> stars[i].vlos[2] >> stars[i].sig_err[2];
			file_4epoch >> date >> stars[i].vlos[3] >> stars[i].sig_err[3];
			stars[i].find_v_average();
		}
		make_binary_correction=false;
	}
	void input(int n1, int n2, int n3, int n4)
	{
		nn = n1+n2+n3+n4;
		stars = new star[nn];
		int i;
		for (i=0; i < n1; i++) { stars[i].input(i,1); stars[i].assign_velocities_errors(); }
		for (i=n1; i < n2; i++) { stars[n1+i].input(i,2); stars[n1+i].assign_velocities_errors(); }
		for (i=n2; i < n3; i++) { stars[n1+n2+i].input(i,3); stars[n1+n2+i].assign_velocities_errors(); }
		for (i=n3; i < n4; i++) { stars[n1+n2+n3+i].input(i,4); stars[n1+n2+n3+i].assign_velocities_errors(); }
		make_binary_correction=false;
	}
	void input(int n1, int n1_giants, int n2, int n2_giants, int n3, int n3_giants, int n4, int n4_giants)
	{
		nn = n1+n2+n3+n4;
		stars = new star[nn];
		int i;
		for (i=0; i < n1; i++)
		{
			stars[i].input(i,1);
			if (i < n1_giants) stars[i].giant=true;
			stars[i].assign_velocities_errors();
		}
		for (i=0; i < n2; i++)
		{
			stars[n1+i].input(n1+i,2);
			if (i < n2_giants) stars[n1+i].giant=true;
			stars[n1+i].assign_velocities_errors();
		}
		for (i=0; i < n3; i++)
		{
			stars[n1+n2+i].input(n1+n2+i,3);
			if (i < n3_giants) stars[n1+n2+i].giant=true;
			stars[n1+n2+i].assign_velocities_errors();
		}
		for (i=0; i < n4; i++)
		{
			stars[n1+n2+n3+i].input(n1+n2+n3+i,4);
			if (i < n4_giants) stars[n1+n2+n3+i].giant=true;
			stars[n1+n2+n3+i].assign_velocities_errors();
		}
		make_binary_correction=false;
	}
	void load_binary_correction_splines(char *fs0_file, char *sigr_fs0_file, char *f_prior_file)
	{
		binary_dispratio_spline.input(fs0_file,sigr_fs0_file);
		f_prior_spline.input(f_prior_file);
		make_binary_correction=true;
	}

	double threshold_fraction_2epoch_subset(double threshold)
	{
		double dv, f;
		int n_greater_than_threshold=0;
		int n_2epoch=0;
		for (int i=0; i < nn; i++)
		{
			if (stars[i].n_epochs==2) {
				n_2epoch++;
				dv = fabs(stars[i].vlos_noerr[1] - stars[i].vlos_noerr[0]);
				if (dv > threshold) n_greater_than_threshold++;
			}
		}
		f = ((double) n_greater_than_threshold ) / ((double) n_2epoch);
		return f;
	}

	int sigma_clip(double sigma_ratio, double dispersion)
	{

		double quadsum, mean_velocity;
		int i;
		double meanv_norm=0, meanv_weighted_sum=0;

		// first, find the mean velocity from the data points, weighting by errors assuming the input dispersion
		for (i=0; i < nn; i++)
		{
			if (stars[i].clipped==true) continue;
			quadsum = SQR(dispersion) + SQR(stars[i].sig_err_average);
			meanv_norm += 1.0/quadsum;
			meanv_weighted_sum += stars[i].v_average/quadsum;
		}
		mean_velocity = meanv_weighted_sum / meanv_norm;

		double v_cut;
		int n_clipped=0;
		for (i=0; i < nn; i++)
		{
			if (stars[i].clipped==true) continue;
			v_cut = sigma_ratio*sqrt(SQR(dispersion) + SQR(stars[i].sig_err_average));
			if (fabs(stars[i].v_average - mean_velocity) > v_cut)
			{
				stars[i].clipped=true;
				//cout << "clipped star ID: " << stars[i].id << " v_avg = " << stars[i].v_average << endl;
				n_clipped++;
			}
		}

		return n_clipped;
	}

	double chi_squared_zero_mean(double dispersion)
	{
		double quadsum, chisq=0;
		int i;
		for (i=0; i < nn; i++)
		{
			if (stars[i].clipped==true) continue;
			quadsum = SQR(dispersion) + SQR(stars[i].sig_err_average);
			chisq += log(quadsum) + SQR(stars[i].v_average)/quadsum;
		}

		return chisq;
	}

	double statistical_mean_velocity(double dispersion, double &norm)
	{
		double quadsum, mean_velocity, chisq=0;
		int i;
		double meanv_norm=0, meanv_weighted_sum=0;

		// first, find the mean velocity from the data points, weighting by errors assuming the input dispersion
		for (i=0; i < nn; i++)
		{
			if (stars[i].clipped==true) continue;
			quadsum = SQR(dispersion) + SQR(stars[i].sig_err_average);
			meanv_norm += 1.0/quadsum;
			meanv_weighted_sum += stars[i].v_average/quadsum;
		}
		mean_velocity = meanv_weighted_sum / meanv_norm;
		norm=meanv_norm;
		return mean_velocity;
	}

	double log_posterior(double dispersion) // marginalized over mean
	{
		double quadsum, mean_velocity, chisq=0;
		int i;
		double meanv_norm, logprior;

		mean_velocity = statistical_mean_velocity(dispersion,meanv_norm);

		for (i=0; i < nn; i++)
		{
			if (stars[i].clipped==true) continue;
			//cout << stars[i].v_average << " " << stars[i].sig_err_average << endl;
			quadsum = SQR(dispersion) + SQR(stars[i].sig_err_average);
			chisq += log(quadsum) + SQR(stars[i].v_average - mean_velocity)/quadsum;
		}

		chisq += log(meanv_norm); // this factor comes from marginalizing over the mean velocity
		chisq *= 0.5;

		logprior = 0; // corresponds to a flat prior in sigma

		chisq -= logprior;
		return chisq;
	}
	void rough_MW_cut(void) // makes a histogram, finds galaxy by locating bin with most stars, then cuts out stars 50 km/s away from galaxy (gets rid of most MW stars)
	{
		dvector v_data(nn);
		int i,j;
		for (i=0; i < nn; i++) v_data[i] = stars[i].v_average;
		sort(v_data);
		double vmin, vmax;
		vmin = v_data[0];
		vmax = v_data[nn-1];
		int nbins = 60;
		int *bin_count;
		double *bin_left, *bin_right, *bin_center;
		bin_count = new int[nbins];
		bin_left = new double[nbins];
		bin_right = new double[nbins];
		bin_center = new double[nbins];

		double v, vstep;
		vstep = (vmax-vmin)/nbins;
		for (i=0, v=vmin; i < nbins; i++, v += vstep)
		{
			bin_count[i] = 0;
			bin_left[i] = v;
			bin_right[i] = v + vstep;
			bin_center[i] = v + 0.5*vstep;
		}

		for (j=0; j < nn; j++)
		{
			v = stars[j].v_average;
			for (int i=0; i < nbins; i++) {
				if ((v > bin_left[i]) and (v <= bin_right[i]))
					bin_count[i]++;
			}
		}
		int bincount_max = 0, imax = 0;
		for (i=0; i < nbins; i++) {
			if (bin_count[i] > bincount_max) { imax=i; bincount_max = bin_count[i]; }
		}

		double vgal = bin_center[imax];
		vmin = vgal - 50;
		vmax = vgal + 50;
		for (int i=0; i < nn; i++) {
			if ((stars[i].v_average < vmin) or (stars[i].v_average > vmax)) stars[i].clipped = true;
		}

		delete[] bin_count;
		delete[] bin_left;
		delete[] bin_right;
		delete[] bin_center;
	}
	double log_posterior_f(double dispersion, double threshold_fraction) // marginalized over mean
	{
		double quadsum, mean_velocity, binary_correction, chisq=0;
		int i;
		double meanv_norm, log_sig_prior, log_f_prior;

		mean_velocity = statistical_mean_velocity(dispersion,meanv_norm);
		binary_correction = binary_dispratio_spline.splint(dispersion,threshold_fraction);

		for (i=0; i < nn; i++)
		{
			if (stars[i].clipped==true) continue;
			quadsum = SQR(dispersion) + SQR(binary_correction) + SQR(stars[i].sig_err_average);
			chisq += log(quadsum) + SQR(stars[i].v_average - mean_velocity)/quadsum;
		}
		chisq += log(meanv_norm); // this factor comes from marginalizing over the mean velocity
		chisq *= 0.5;

		log_sig_prior = 0; // corresponds to a flat prior in sigma
		log_f_prior = log(f_prior_spline.splint(threshold_fraction));

		chisq -= log_sig_prior;
		chisq -= log_f_prior;
		return chisq;
	}

	~dataset()
	{
		delete[] stars;
	}
};

dataset dsph_dataset;

double log_postnorm, postnorm;
double log_posterior_sf(const dvector &sf) { return dsph_dataset.log_posterior_f(sf[0],sf[1]); }
double log_posterior_marginalized_mean(double dispersion) { return dsph_dataset.log_posterior(dispersion); }
double normalized_posterior(double sig) { double logpos = log_posterior_marginalized_mean(sig) - log_postnorm; return (exp(-logpos))/postnorm; }

void find_approximate_velocity_params(char *datafile, double &vsys, double &disp)
{
	dsph_dataset.input(datafile);
	dsph_dataset.rough_MW_cut(); // cuts out most of the MW stars

	BrentsMinMethod logpos;
	double sigmin=0, sigmax=15;
	logpos.set_interval(sigmin,sigmax);
	double best_fit_dispersion, dispratio, meanv_norm, mean_velocity;
	int n_clipped;
	bool first_round = true;
	for (;;)
	{
		best_fit_dispersion = logpos.minimize(log_posterior_marginalized_mean);
		n_clipped = dsph_dataset.sigma_clip(v_cutoff_ratio,best_fit_dispersion);
		mean_velocity = dsph_dataset.statistical_mean_velocity(best_fit_dispersion,meanv_norm);
		if (n_clipped==0) break;
	}
	vsys = mean_velocity;
	disp = best_fit_dispersion;
	if (mpi_id==0) cout << "3-sigma clip results: systemic velocity = " << vsys << ", dispersion = " << disp << endl << endl;
}

void plot_sigma_posterior(const string filename)
{
	ofstream postout((filename + ".sigpost").c_str());
	ofstream disp_nocut_out("bestfitdisp.nocut");
	string bestfit_file = filename + ".bestfit";
	ofstream bestfit_out(bestfit_file.c_str());

	dsph_dataset.input(filename.c_str());
	dsph_dataset.output_vavgs_errs(filename);
	dsph_dataset.plot_dverrs(filename.c_str());
	dsph_dataset.rough_MW_cut(); // cuts out most of the MW stars
	BrentsMinMethod logpos;
	double sigmin=0, sigmax=15;
	logpos.set_interval(sigmin,sigmax);
	double best_fit_dispersion, dispratio, meanv_norm, mean_velocity;
	int n_clipped;
	bool first_round = true;
	for (;;)
	{
		best_fit_dispersion = logpos.minimize(log_posterior_marginalized_mean);
		n_clipped = dsph_dataset.sigma_clip(v_cutoff_ratio,best_fit_dispersion);
		mean_velocity = dsph_dataset.statistical_mean_velocity(best_fit_dispersion,meanv_norm);
		if (mpi_id==0) cout << "dispersion: " << best_fit_dispersion << ", mean velocity: " << mean_velocity << ", outliers: " << n_clipped << endl;
		if (first_round) { disp_nocut_out << best_fit_dispersion << endl; first_round = false; }
		if (n_clipped==0) break;
	}
	bestfit_out << mean_velocity << " " << best_fit_dispersion << endl;
	log_postnorm = log_posterior_marginalized_mean(best_fit_dispersion);
	postnorm = 1;
	postnorm = romberg_open(normalized_posterior,sigmin,sigmax,1e-5,5);

	int i, sig_nn = 300;
	double logpos_norm, pos, sig, sigstep = (sigmax-sigmin)/(sig_nn-1);
	for (i=0, sig=sigmin; i < sig_nn; i++, sig += sigstep)
	{
		pos = normalized_posterior(sig);
		postout << sig << " " << pos << endl;
	}
}

void output_fake_galaxy_data_real_errors(double binfrac, char *datafile)
{
	string datafile_str(datafile);
	string datafile_out = datafile_str + ".fake";
	binary_fraction = binfrac;

	dataset Galaxy;
	Galaxy.input(datafile);
	Galaxy.reassign_velocities();
	Galaxy.output_data(datafile_out.c_str());
	//Galaxy.output_data_separate_by_epoch(datafile_out.c_str());
	//Galaxy.output_vavgs_errs(datafile_out.c_str());
	Galaxy.output_maxdv(datafile_out.c_str());
}

void generate_magnitude_distribution(double distance)
{
	double mass, loglum, abs_mag, mag;
	int i=0;
	minimum_mass = .4;
	do
	{
		mass = gen_m_kroupa();
		cout << mass << endl;
		if (mass > max_rgb_mass) continue;
		loglum = lumr_mass_spline_age.interpolate_lum_at_fixed_age(mass);
		abs_mag = 4.83 - 2.5*loglum;
		mag = abs_mag + 10 + 5*log(distance)/log(10);
		cout << mag << endl;
		i++;
	}
	while (i < n_simulated_stars);
}

inline double find_stellar_radius(double mass)
{
	return lumr_mass_spline_age.interpolate_radius_at_fixed_age(mass);
}

double ndev(double min, double max, Random_Sequence &rand)
{
  // Generates normal deviates with mean 0 and st. dev. 1 with boundaries (min,max)
  // Transform to general normal deviate with mu+sigma*ndev()
  // Returns 0 (delta function) for min=max

  double u,v;
  if (min==max) return 0;
  else
    {
      do {
			u = min+(max-min)*(RandomNumber_2(rand));
			v = RandomNumber_2(rand);
      } while (v > exp(-0.5*u*u));
      return u;
    }
}

double generate_velocity_error(double &sigma_error)
{
	if (use_error_distribution)
	{
		double min = (min_sigerr-mean_sigerr)/sig_sigerr;
		double max = (20-mean_sigerr)/sig_sigerr;

		sigma_error = mean_sigerr+sig_sigerr*randseq.ndev(min,max);
		return (sigma_error*randseq.NormalDeviate());
	}
	else
		return (sigma_error*randseq.NormalDeviate());
}

double generate_velocity_error_from_rvalue(double &sigma_error, double &rvalue, int &channel)
{
	double baseline_error, ccf_error;

	// Now we will create a reasonably realistic distribution of R-values
	const double mu_rval=12.0;
	const double sig_rval=8.0;
	double min = (-mu_rval+4)/sig_rval;
	double max = 28.0/sig_rval; // these give roughly the range of R-values seen in walker's data
	rvalue = mu_rval+sig_rval*randseq.ndev(min,max);

	double channel_rand = randseq.RandomNumber2();
	if (channel_rand < 0.5) channel = 0; // red
	else channel = 1; // blue

	if (channel==0) {
		ccf_error = alpha_red*pow(1+rvalue,-x_red); // two error parameters for red channel
		baseline_error = baseline_red;
	} else {
		ccf_error = alpha_blue*pow(1+rvalue,-x_blue); // two error parameters for blue channel
		baseline_error = baseline_blue;
	}
	sigma_error = sqrt(SQR(ccf_error) + SQR(baseline_error));
	return (sigma_error*randseq.NormalDeviate());
}

double normal_deviate(Random_Sequence &rand)
{
	double fac,rsq,v1,v2;

	if (rand.idum < 0) rand.iset=0;
	if (rand.iset == 0) {
		do {
			v1=2.0*RandomNumber_2(rand)-1.0;
			v2=2.0*RandomNumber_2(rand)-1.0;
			rsq=v1*v1+v2*v2;
		} while (rsq >= 1.0 || rsq == 0.0);
		fac=sqrt(-2.0*log(rsq)/rsq);
		rand.gset=v1*fac;
		rand.iset=1;
		return v2*fac;
	} else {
		rand.iset=0;
		return rand.gset;
	}
}

double find_absolute_magnitude(double distance, double magnitude)
{
	return (magnitude - 10 - 5*log(distance)/log(10));
}

double magnitude_from_mass(double mass)
{
	double log_luminosity, abs_magnitude, magnitude;
	if (mass < minmass) return -1e30;
	if (mass > maxmass) return -1e30; // no red giants beyond AGB

	abs_magnitude = lumr_mass_spline_age.interpolate_vmag_at_fixed_age(mass);
	if (abs_magnitude > max_absolute_magnitude) return -1e30;	// will reject this star as too faint to observe
	if (distance_kpc==1e30)
		magnitude = abs_magnitude;	// if distance not defined, use absolute magnitude
	else
		magnitude = abs_magnitude + 10 + 5*log(distance_kpc)/log(10);
	return magnitude;
}

double loglum_val, mag_val;

double find_stellar_mass(double absolute_mag) // this function doesn't include AGB stars
{
	double mass;
	mag_val = absolute_mag;

	if (absolute_mag < -2.6) { warn("absolute magnitude out of range"); mass = maxmass; } else
	mass = BrentsMethod(mass_mag_root,minmass,max_rgb_mass,1e-5); // we can't distinguish AGB stars from RGB stars without color information, so we assume they're RGB here (need to add color info!)

	return mass;
}

double mass_mag_root(double mass)
{
	double ans = mag_val - lumr_mass_spline_age.interpolate_vmag_at_fixed_age(mass);
	return ans;
}

double find_compact_binary_ratio(const double observed_mass)
{
	int iterations = 5000;
	int n_simulated_stars = 100000;
	double q, primary_mass, secondary_mass, logP;
	double roche_lobe_radius;
	double binsize = 0.01;
	double bin_leftside = observed_mass - 0.5*binsize;
	double bin_rightside = observed_mass + 0.5*binsize;
	double secondary_to_primary_ratio;
	int primary_bincount = 0, secondary_bincount = 0;
	minimum_mass = 0;
	maxmass = 100; // now we are allowing compact objects, so there is no RGB mass limit here
	double white_dwarf_mass, final_mass_ratio, a;
	int i, it;
	for (it=0; it < iterations; it++)
	{
		for (i=0; i < n_simulated_stars; i++)
		{
			for (;;) {
				primary_mass = gen_m_kroupa();
				logP = gen_logP(); 
				q = gen_q(logP);
				a = primary_semimajor_axis(logP,primary_mass,q);
				roche_lobe_radius = roche_lobe_radius_approximation(a,q);
				if (roche_lobe_radius < max_rgb_radius) continue;
				else break;
			}
			secondary_mass = primary_mass * q;
			//if ((primary_mass > 0.6) and (primary_mass < 1.0)) cout << primary_mass << endl;
			//if ((secondary_mass > 0.6) and (secondary_mass < 1.0)) cout << secondary_mass << endl;
			//if (primary_mass > 18) {
				if (q<0.99) {
				if ((primary_mass > bin_leftside) and (primary_mass < bin_rightside)) primary_bincount++;
				//if (primary_mass > 7) {
					if ((secondary_mass > bin_leftside) and (secondary_mass < bin_rightside)) { // note, these are stars that *were* the secondary, but are now typically the primary because their companions shed mass on their way to becoming a white dwarf
						secondary_bincount++;
						white_dwarf_mass = 0.109*primary_mass + 0.394; // this is the initial-final mass relation for white dwarfs and their progenitors
						//final_mass_ratio = (white_dwarf_mass < secondary_mass) ? white_dwarf_mass / secondary_mass
												//: secondary_mass / white_dwarf_mass;
						final_mass_ratio = white_dwarf_mass / secondary_mass;
						cout << final_mass_ratio << endl;
					}
				//}
			}
		}
		if (it % 10 == 0) cerr << it << " iterations\n";
		//secondary_to_primary_ratio = ((double) secondary_bincount)/primary_bincount;
		//cout << secondary_to_primary_ratio << endl;
	}
}

double gen_m_kroupa ()
{
  // Generates mass from Kroupa IMF

  double x, x0, xmin, mass;
  do {
	  x0 = randseq.RandomNumber2();
	  x = x0*0.067396 + 0.932604;	// maps (0,1) --> (0.932604,1), so that minimum mass = 0.5

		if (minimum_mass < 0.01)
			xmin = 0;
		else if (minimum_mass < 0.08)
			xmin = 3.79530*(pow(minimum_mass,0.7) - 0.03981);
		else if (minimum_mass < 0.5)
			xmin = 0.496658 + 0.07514*(7.54272 - pow(minimum_mass,-0.8));
		else if (minimum_mass < 1.0)
			xmin = 0.932603 + 0.018948*(3.24900 - pow(minimum_mass,-1.7));
		else
			xmin = 0.975220 + 0.024779*(1.0 - pow(minimum_mass,-1.3));

		x = xmin + x0*(1-xmin);	// maps (0,1) --> (xmin,1)
		
		if (x < 0.496659)
			mass = pow(0.263484*x + 0.039811, 1.428571);
		else if ((x >= 0.496659) and (x < 0.932604))
			mass = pow(7.54272 - 13.30827*(x-0.496659), -1.25);
		else if ((x >= 0.932604) and (x < 0.975220))
			mass = pow(3.249 - 52.772*(x-0.932604), -0.588235);
		else
			mass = pow(1 - 40.3551*(x-0.975220), -0.769230);
	} while (mass > maxmass); // ensures that dead stars aren't being generated (stars that are too massive to still be burning, given the age of the star population)

	return mass;
}

double generate_nonmember_velocity(void)
{
	double vmin, vmax, v;
	if (use_milky_way_likelihood)
	{
		vmin = MW_Likelihood.xmin();
		vmax = MW_Likelihood.xmax();
		double y, ymax;
		ymax = MW_Likelihood.ymax();
		ymax += 0.1; //just to make sure in case maximum of splined distribution is slightly higher than max y value
		for (;;) {
			v = (vmax-vmin)*randseq.RandomNumber2() + vmin;
			y = ymax*randseq.RandomNumber2();
			if (y < MW_Likelihood.splint(v)) break; // rejection method
		}
	} else if (use_milky_way_gaussian_likelihood) {
		v = 30 + 100*randseq.NormalDeviate();
	} else { // use flat distribution
		vmin = -50;
		vmax = 50;
		v = (vmax-vmin)*randseq.RandomNumber2() + vmin;
	}
	return v;
}

double generate_nonmember_position(void)
{
	if (field_radius==-10) die("radius of field must be set to generate positions");
	double x, R;
	x = randseq.RandomNumber2();
	R = field_radius*sqrt(x);
	return R;
}

double generate_position_from_plummer_profile(void)
{
	if (field_radius==-10) die("radius of field must be set to generate positions");
	if (half_light_radius==-10) die("Plummer scale radius must be set to generate positions");
	double x, R;
	x = randseq.RandomNumber2();
	R = half_light_radius*sqrt(x/(1-x+SQR(half_light_radius/field_radius)));
	return R;
}

double gen_logP(void)
{
  // Generates value of logP sampled from normal distribution

  double ans;

  if (period_distribution==LogNormal)
  {
	  ans = mu_logP+sig_logP*randseq.NormalDeviate();
	  if ((ans < logPmin) or (ans > logPmax)) return gen_logP();	// very short periods cause numerical error (they are too improbable anyway...)
  }
  else if (period_distribution==Flat)
  {
	  ans = flat_logP_min + (flat_logP_max-flat_logP_min)*randseq.RandomNumber2();
  }
  return ans;
}

inline double gen_logP(Random_Sequence &rand)
{
  // Generates value of logP sampled from normal distribution

  double ans;

  if (period_distribution==LogNormal)
  {
	  ans = mu_logP+sig_logP*normal_deviate(rand);
	  if ((ans < logPmin) or (ans > logPmax)) return gen_logP(rand);	// very short periods cause numerical error (they are too improbable anyway...)
  }
  else if (period_distribution==Flat)
  {
	  ans = flat_logP_min + (flat_logP_max-flat_logP_min)*RandomNumber_2(rand);
  }
  return ans;
}

inline double gen_q_white_dwarf (void)
{
  // Generates value of q (mass ratio, m_sec/m_prim)
  // for systems with a white dwarf secondary
	
	double x,q,y,ymax;
	for (;;) {
		x = randseq.RandomNumber2();
		q = 0.58 - log(1-x*0.9966)/5.3;
		ymax = 9.2*exp(-5.3*(q-0.58)); // this function lies above the splined q-distribution, so it works for rejection method
		y = ymax*randseq.RandomNumber2();
		if (y < qdist_white_dwarf_spline.splint(q)) break; // rejection method
	}
	return q;
}

inline double gen_q (double logP)
{
  // Generates value of q (mass ratio, m_sec/m_prim) sampled
  // from normal distribution

	if (logP < 0.91483) // 3000 days; from Mazeh (1992)
	{
		return randseq.RandomNumber2();	// uniform distribution in q
	}
	else
	{
		double mu, sig, min, max;

		mu=0.23; //from O96
		sig=0.42; //from O96
		  
		min = -mu/sig;
		max = (1-mu)/sig;
		//max = (20-mu)/sig;
		return mu+sig*randseq.ndev(min,max);
	}
}

inline double gen_q(double logP, Random_Sequence &rand)
{
  // Generates value of q (mass ratio, m_sec/m_prim) sampled
  // from normal distribution

	if (logP < 0.91483) // 3000 days; from Mazeh (1992)
	{
		return RandomNumber_2(rand);	// uniform distribution in q
	}
	else
	{
		double mu, sig, min, max;

		mu=0.23; //from O96
		sig=0.42; //from O96
		  
		min = -mu/sig;
		max = (1-mu)/sig;
		return mu+sig*ndev(min,max,rand);
	}
}

void plot_new_qdist(void)
{
	ofstream qvals("qvals.old");
	double q;
	for (;;) {
		//q = gen_q_experimental(0.1);
		q = gen_q(10);
		qvals << q << endl;
	}
}

double gen_q_myfit (double logP) // uses a lognormal spliced to salpeter; is a better fit to Duquennoy & Mayor, but not particularly important unless you want to extend q to q > 1
{
  // Generates value of q (mass ratio)
	double x,q;
	if (logP < 0.91483) // 3000 days; from Mazeh (1992)
	{
		q = randseq.RandomNumber2();	// uniform distribution in q
	}
	else
	{
		do { // you should change the proportions so you don't have to reject points; do this later
			x = randseq.RandomNumber2();
			if (x < 0.618)
			{
				// log-normal for q < 0.55
				double mu, sig, min, max, logq;

				mu=-0.66997;
				sig=0.906386;
				  
				min = -100;
				max = (-0.597837-mu)/sig;
				logq = mu+sig*randseq.ndev(min,max);
				q=exp(logq);
			}
			else
			{
				x = randseq.RandomNumber2();
				q = 0.55 * pow(1-x,-0.76923); // salpeter IMF for q > 0.55
			}
		} while (q > 1);
	}

  return q;

}

const double q_cutoff=1.75;
const double q_lower_cutoff=0.125;

inline double gen_q_experimental (double logP, Random_Sequence &rand)
{
  // Generates value of q (mass ratio, m_sec/m_prim) sampled
  // from normal distribution

	double mu, sig, min, max;

	mu=0.5;
	sig=0.3;
	  
	min = -mu/sig;
	max = (1-mu)/sig;
	//max = (20-mu)/sig;
	return mu+sig*ndev(min,max,rand);
}

inline double gen_q_experimental (double logP)
{
  // Generates value of q (mass ratio, m_sec/m_prim) sampled
  // from normal distribution

	double mu, sig, min, max;

	mu=0.5;
	sig=0.3;
	  
	min = -mu/sig;
	max = (1-mu)/sig;
	//max = (20-mu)/sig;
	return mu+sig*randseq.ndev(min,max);
}

/*
double gen_q_experimental (double logP)
{
  // Generates value of q (mass ratio)
	double x,q;
	if (logP < 0.91483) // 3000 days; from Mazeh (1992)
	{
		double x,y;
		do {
			x = randseq.RandomNumber2();	// uniform distribution in q
			if (x < 0.5) q = 2*x; // for mass ratios less than 1
			else {
				y = 2*(1-x); // now y is a randseq deviate between 0 and 1
				q = 1.0/y; // for mass ratios greater than 1
			}
		} while ((q > q_cutoff) or (q < q_lower_cutoff));
	}
	else
	{
		do {
			x = randseq.RandomNumber2();
			if (x < 0.618)
			{
				// log-normal for q < 0.55
				double mu, sig, min, max, logq;

				mu=-0.66997;
				sig=0.906386;
				  
				min = -100;
				max = (-0.597837-mu)/sig;
				logq = mu+sig*randseq.ndev(min,max);
				q=exp(logq);
			}
			else
			{
				x = randseq.RandomNumber2();
				q = 0.55 * pow(1-x,-0.76923); // salpeter IMF for q > 0.55
			}
		} while ((q > q_cutoff) or (q < q_lower_cutoff));
	}

  return q;

}
*/

double gen_e(double logP)
{
  // Generates a value of the eccentricity e.  Distribution of e depends
  // on period range, and is taken from DM.

  double x;

  if (logP > 0.4377) // P > 1000 d (from DM)
  {
      // f_e = 3/2e^(1/2) is simple enough to integrate analytically...
      // The cumulative distribution fxn is e^(3/2), so for a udev x,
      // the eccentricity is x^(2/3)
      
      x = randseq.RandomNumber2();
      
      return pow(x,2.0/3.0);
  }
  else if (logP > -1.5209) // 11 d < P < 1000 d (from DM)
  {
		 double min, max;
      // Approximates figure in DM with a Gaussian fxn

      min = -mu_e/sig_e;
      max = (1-mu_e)/sig_e;
      
      return mu_e+sig_e*randseq.ndev(min,max);
  }
  else // P < 11 d (from DM)
	return 0;
}

double gen_e_experimental(double logP)
{

	 double min, max;
	// Approximates figure in DM with a Gaussian fxn

	min = -mu_e/sig_e;
	max = (1-mu_e)/sig_e;
	
	return mu_e+sig_e*randseq.ndev(min,max);
}

double gen_e_experimental(double logP, Random_Sequence &rand)
{

	 double min, max;
	// Approximates figure in DM with a Gaussian fxn

	min = -mu_e/sig_e;
	max = (1-mu_e)/sig_e;
	
	return mu_e+sig_e*ndev(min,max,rand);
}

inline double gen_e(double logP, Random_Sequence &rand)
{
  // Generates a value of the eccentricity e.  Distribution of e depends
  // on period range, and is taken from DM.

  double x;

  if (logP > 0.4377) // P > 1000 d (from DM)
    {
      // f_e = 3/2e^(1/2) is simple enough to integrate analytically...
      // The cumulative distribution fxn is e^(3/2), so for a udev x,
      // the eccentricity is x^(2/3)
      
      x = RandomNumber_2(rand);
      
      return pow(x,2.0/3.0);
    }
  else if (logP > -1.5209) // 11 d < P < 1000 d (from DM)
    {
		 double min, max;
      // Approximates figure in DM with a Gaussian fxn

      min = -mu_e/sig_e;
      max = (1-mu_e)/sig_e;
      
      return mu_e+sig_e*ndev(min,max,rand);
    }
  else // P < 11 d (from DM)
    {
      return 0;
    }
}

inline double eta2phi (double eta, double e)
{
	return (2*atan(sqrt((1+e)/(1-e))*tan(eta/2)));
}

double timeval, eval;

double gen_phi (double e, double &time)
{
  // Generates a uniform deviate in the time, then finds the polar angle phi

	double eta,phi;

	eval = e;
	time = randseq.RandomNumber2();	// time is in fraction of period
	timeval = time;
	eta = BrentsMethod(anomaly_time_equation,0,6.28318530718,1e-4);
	phi = eta2phi(eta, e);

	return phi;
}

double evolve_phi (double e, double time)
{
  // Given a time (in units of the period), finds the polar angle phi

	double eta,phi;

	eval = e;
	timeval = time;
	eta = BrentsMethod(anomaly_time_equation,0,6.28318530718,1e-2);
	phi = eta2phi(eta, e);

	return phi;
}

double anomaly_time_equation(double anomaly)
{
	return ((anomaly - eval*sin(anomaly))/M_2PI - timeval);
}

inline double gen_phi_multi_thread(const double e, double &time, Random_Sequence &rand)
{
  // Generates a uniform deviate in the time, then finds the polar angle phi

	double eta,phi;

	time = RandomNumber_2(rand);	// time is in fraction of period
	if ((e < 1e30) and (e > -1e30)) ;
	else { cout << "wtf? e value crazy! we have e = " << e << endl; die(); }
	eta = find_eta_root(e,time,0,6.28318530718,1e-2);
	phi = eta2phi(eta, e);

	return phi;
}

inline double evolve_phi_multi_thread (const double e, const double time)
{
  // Given a time (in units of the period), finds the polar angle phi

  double eta,phi;

	eta = find_eta_root(e,time,0,6.28318530718,1e-2);
	phi = eta2phi(eta, e);

  return phi;
}

const int itmax = 100;
const double eps = 3.0e-8;

double find_eta_root(const double e_val, const double time_val, const double x1, const double x2, const double tol)
{
	double a = x1, b = x2, c = x2, d, e, min1, min2;
	double fa = (a - e_val*sin(a))/M_2PI - time_val;
	double fb = (b - e_val*sin(b))/M_2PI - time_val;
	double fc, p, q, r, s, tol1, xm;

	if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)) {
		cout << "fa=" << fa << ", fb=" << fb << endl;
		die("root must be bracketed in Brent's Method");
	}

	fc = fb;
	for (int it=0; it < itmax; it++) {
		if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
			c = a;
			fc = fa;
			e = d = b-a;
		}
		if (fabs(fc) < fabs(fb)) {
			a = b;
			b = c;
			c = a;
			fa = fb;
			fb = fc;
			fc = fa;
		}
		tol1 = 2.0*eps*fabs(b) + 0.5*tol;
		xm = 0.5*(c-b);
		if (fabs(xm) <= tol1 || fb == 0.0) return b;

		if (fabs(e) >= tol1 && fabs(fa) > fabs(fb))
		{
			s = fb / fa;
			if (a == c) {
				p = 2.0*xm*s;
				q = 1.0 - s;
			} else {
				q = fa/fc;
				r = fb/fc;
				p = s * (2.0*xm*q*(q-r) - (b-a)*(r-1.0));
				q = (q-1.0)*(r-1.0)*(s-1.0);
			}
			if (p > 0.0) q = -q;
			p = fabs(p);
			min1 = 3.0*xm*q - fabs(tol1*q);
			min2 = fabs(e*q);
			if (2.0*p < (min1 < min2 ? min1 : min2)) {
				e = d;
				d = p/q;
			} else {
				d = xm;
				e = d;
			}
		} else {
			d = xm;
			e = d;
		}
		a = b;
		fa = fb;
		if (fabs(d) > tol1)
			b += d;
		else
			b += SIGN(tol1,xm);
		fb = (b - e_val*sin(b))/M_2PI - time_val;
	}
	die("maximum number of iterations exceeded in Brent's Method");
	return 0.0;
}

double primary_semimajor_axis(double logP, double M, double q)
{
  // Calculates semimajor axis of ellipse formed by movement of primary
  // (i.e. the maximum separation between the primary and the center of
  // mass).

  return pow(pow(10,2*logP)*M*(1+q),1.0/3.0)*q/(q+1);
}

double orbital_separation_semimajor_axis(double logP, double M, double q)
{
  // The semimajor axis of the orbit (i.e. the maximum separation
  // of the primary and the secondary) is found by omitting the q/(q+1)
  // factor in the above equation. This is not needed for calculating
  // the primary star v_los, but we leave this function here in case it
  // is needed later

  return pow(pow(10,2*logP)*M*(1+q),1.0/3.0);
}

void generate_euler_angles(double &theta, double &psi)
{
	double costheta = 2*(randseq.RandomNumber2())-1;	// uniform deviate in cos(theta) because it's randseq solid angle
	theta = acos(costheta);
	psi = 2*M_PI*randseq.RandomNumber2();
}

void generate_euler_angles(double &theta, double &psi, Random_Sequence &rand)
{
	double costheta = 2*(RandomNumber_2(rand))-1;	// uniform deviate in cos(theta) because it's randseq solid angle
	theta = acos(costheta);
	psi = 2*M_PI*RandomNumber_2(rand);
}

long double vbin_los(double a, double period, double e, double theta, double psi, double phi)
{
	double axis_ratio, angular_factor;
	axis_ratio = sqrt(1-e*e);
	angular_factor = sin(theta)*(cos(psi-phi) + e*cos(psi));
	return auyr_to_kms*2*M_PI*a*angular_factor/(period*axis_ratio);
}

double vbin_tot(double a, double period, double e, double phi)
{
	double ans, e_factor;
	e_factor = (1+e*e+2*e*cos(phi))/(1-e*e);
	ans = M_2PI*a*sqrt(e_factor)/period;
	ans *= auyr_to_kms;
	return ans;
}

//double roche_lobe_radius_approximation(double a_orbit, double q, double e)
double roche_lobe_radius_approximation(double a_orbit, double q)
{
	double roche_circular_ratio, roche_lobe_radius;
	// Note, Eggleton's formula calculates the Roche lobe of the primary star, and q = mass_primary / mass_secondary
	// (which is opposite to our notation everywhere else). So we flip q --> 1/q here.
	q = 1.0 / q;
	roche_circular_ratio = 0.49*pow(q,2.0/3.0)/(0.6*pow(q,2.0/3.0) + log(1+pow(q,1.0/3.0)));	// Eggleton 1983
	//double roche_ratio = roche_circular_ratio*(1-0.16*e)/(1-e);	// Correction from eccentricity due to Church & Dischler 2009; may not be trustworthy
	//roche_lobe_radius = roche_ratio*a_orbit*(1-e);	// a_orbit*(1-e) is the pericenter
	roche_lobe_radius = roche_circular_ratio*a_orbit;
	return roche_lobe_radius;
}

void print_options(void)
{
	cout << "Command-line options: (this is not an exhaustive list. See code for all options.)\n\n";
	cout << "-v:<name>	Generate radial velocity data from simulated binary population.\n";
	cout << "-r:<name>	Generate R-factors for the stars in a given dataset.\n";
	cout << "-j:<name>	Generate J-factors (in dv) for the stars in a given dataset.\n";
	cout << "-r2:<name>	Generate R-tables (in mu_P, sigma_P) for stars in given dataset.\n";
	cout << "-D:<name>	Find velocity dispersion and mean velocity of a given dataset.\n";
	cout << "-G:<name>	Randomly resample velocities of a given dataset.\n";
	cout << "-n#	Number of stars in sample (default is 100).\n";
	cout << "-d#	Velocity dispersion of sample in km/s (not counting binary part; default=4).\n";
	cout << "-E#	Number of epochs recorded\n";
	cout << "-e#	Measurement error sigma_v for each velocity (default is 0 km/s)\n";
	cout << "-T#	Time interval between epochs in years (default is 1 year)\n";
	cout << "-k#	Distance to galaxy or star cluster in kpc. If the distance is\n";
	cout << "	specified, then stellar magnitudes will be listed as apparent magnitudes;\n";
	cout << "	if no distance is specified, then absolute magnitudes are given.\n";
	cout << "-V#	Systemic velocity of sample in km/s\n";
	cout << "-L#	Maximum absolute magnitude of the sample (if distance is specified, then\n";
	cout << "	set a limiting apparent magnitude using '-l' instead)\n";
	cout << "-l#	Apparent magnitude limit of sample (use only if the distance has been\n";
	cout << "	specified using option '-k' above)\n";
	cout << "-M#	Fix the absolute magnitude of every star to the specified value, rather\n";
	cout << "	than allowing a distribution of magnitudes (and hence, mass of the\n";
	cout << "	primary star). This option would be used instead of '-L' or '-l'.\n";
	cout << "-b#	Binary fraction of sample (default: b=1)\n";
	cout << "-p#	Period distribution parameter mu_logP, the mean log-period\n";
	cout << "	(default: mu_logP=2.24, from solar neighborhood type G stars)\n";
	cout << "-P#	Period distribution parameter sigma_logP, the width of the period dist-\n";
	cout << "	ribution in log-space (default: 2.3, from solar neighborhood type G stars)\n";
	cout << "-q#	Fix mass ratio of binaries to the specified value, rather than use a\n";
	cout << "	distribution of mass ratios.\n";
	cout << "-x#	Fix eccentricity of binaries to the specified value, rather than use a\n";
	cout << "	distribution of eccentricities.\n";
	cout << "-m#	Fix mass of primary star to specified value in units of M_solar, instead\n";
	cout << "	of determining each star's mass from its luminosity (or from IMF for sim data)\n";
	cout << "-R#	Fix each stellar radius to specified value in units of R_solar, instead\n";
	cout << "	of determining each star's radius from its mass\n";
	cout << "-u#	Minimum allowed log-period, logPmin; this is essentially a cutoff on the\n";
	cout << "	short-period end of the period distribution (default: logPmin = -7.00.\n";
	cout << "	Note, this cutoff is only important if stellar radius is fixed to 0 using\n";
	cout << "	'-r0'; then, Roche-lobe overflow doesn't suppress short-period binaries.)\n";
	cout << "-U#	Maximum allowed log-period, logPmax; this is a cutoff on the long-period end\n";
	cout << "	of the period distribution (default: logPmax = 7.44, from Duquennoy & Mayor)\n";
	cout << "-a#	Specify age of star population in Gyr, which must be between 1 and 10 Gyr\n";
	cout << "	(Default 10 Gyr; beyond several Gyr, results are same regardless of age)\n";
	return;
}

char *advance(char *p)
{
	while ((*++p) and ((!isalpha(*p)) or (*p=='e'))) /* This advances to the next flag (if there is one) */
		;
	return (--p);
}

void load_input_params(char *input_filename, char *mwlikelihood_file, double &binfrac)
{
	include_metal_widths = true;
	include_positions = true;
	use_milky_way_likelihood = true;
	ifstream input_file(input_filename);
	input_file >> n_simulated_stars;
	input_file >> systemic_velocity;
	input_file >> gdisp;
	input_file >> member_fraction;
	input_file >> mwlikelihood_file;
	input_file >> binfrac;
	input_file >> mu_logP;
	input_file >> sig_logP;
	input_file >> epochs;
	input_file >> v_error;
	input_file >> mean_equivalent_width;
	input_file >> equivalent_width_dispersion;
	input_file >> MW_mean_equivalent_width;
	input_file >> MW_equivalent_width_dispersion;
	input_file >> half_light_radius;
	input_file >> field_radius;
	input_file >> distance_kpc;
	input_file >> limiting_magnitude;
	input_file.close();
}
