
double IncGamma(const double a, const double x);
double IncGammaUp(const double a, const double x);
double Gamma(const double xx);
double gammln(const double xx);
void gcf(double &gammcf, const double a, const double x, double &gln);
void gser(double &gamser, const double a, const double x, double &gln);
double erffc(const double x);
double erff(const double x);

