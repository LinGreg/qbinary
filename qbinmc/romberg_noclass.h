
#ifndef ROMBERG_NOCLASS_H
#define ROMBERG_NOCLASS_H

double trapzd(double (*func)(const double), const double a, const double b, const int n);
double midinf(double (*func)(const double), const double aa, const double bb, const int n);
double midpnt(double (*func)(const double), const double a, const double b, const int n);
void RombergPolyExtrapolate(double xa[], double ya[], const int n, const double x, double &y, double &dy);

double romberg(double (*func)(const double), const double a, const double b, const double eps, const int k);
double romberg_open(double (*func)(const double), const double a, const double b, const double eps, const int k);
double romberg_improper(double (*func)(const double), const double a, const double b, const double eps, const int k);
double romberg(double (*func)(const double), const double a, const double b, const double eps, const int k, const double min_error);
double romberg_nocrash(double (*func)(const double), const double a, const double b, const double eps, const int k);

double qtrap(double (*func)(double), const double a, const double b, const double EPS);
double trapezoid_rule(double (*func)(double), const double a, const double b, const int npoints);
double riemann_sum(double (*func)(double), const double a, const double b, const int npoints);

#endif // ROMBERG_NOCLASS_H
