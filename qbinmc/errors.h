// ERRORS.H: Contains functions for error checking

#ifndef ERRORS_H
#define ERRORS_H

#define DEBUG(x) cout << #x " = " << x << endl
#define TRACE(s) cerr << #s << endl; s

void die(void);
void die(char *, ...);
void warn(char *, ...);
void warn(bool, char *, ...);
void interrupt(void);
void interrupt(char *);
void openerror(char *);
void readerror(char *);
void writeerror(char *);

void warn_openerror(char *);
void warn_readerror(char *);
void warn_writeerror(char *);

#endif // ERRORS_H
