#ifndef MATHEXPR
#define MATHEXPR

#define ln10 2.30258509299
#define M_2PI 6.28318530718
#define M_3PI 9.42477796077
#define M_4PI 12.5663706144
#define SQRT_2PI 2.50662827463
#define ln10_reciprocal 0.434294481903
#define M_HALFPI 1.57079632679

inline double dmin(const double &a, const double &b) { return (a < b ? a : b); }
inline double dmax(const double &a, const double &b) { return (a > b ? a : b); }
inline int imin(const int &a, const int &b) { return (a < b ? a : b); }
inline int imax(const int &a, const int &b) { return (a > b ? a : b); }
inline double SQR(const double s) { return s*s; }
inline double CUBE(const double s) { return s*s*s; }
inline int SIGN(const double &a) { return (a < 0 ? -1 : a > 0 ? 1 : 0); }
inline int ipow(const int &n, const int &p) { int nn=1; for (int i=0; i < p; i++) nn *= n; return nn; }

class MathExpr
{
	public:
	MathExpr() {}
	inline double dmin(const double &a, const double &b) { return (a < b ? a : b); }
	inline double dmax(const double &a, const double &b) { return (a > b ? a : b); }
	inline int imin(const int &a, const int &b) { return (a < b ? a : b); }
	inline int imax(const int &a, const int &b) { return (a > b ? a : b); }
	inline double SQR(const double s) { return s*s; }
	inline double CUBE(const double s) { return s*s*s; }
	inline int sign(const double &a) { return (a < 0 ? -1 : a > 0 ? 1 : 0); }
	double log10(const double x) { return (ln10_reciprocal*log(x)); }

};

#endif //MATHEXPR

