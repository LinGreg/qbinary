#ifndef RAND_H
#define RAND_H

#include <cmath>

class Random
{
	private:
	int seed;
	bool first_normal_deviate;
	double gset;

	public:
	Random() { seed = -1; first_normal_deviate = true; }
	Random(const int& seed_in) { seed = seed_in; first_normal_deviate = true; }
	void set_random_seed(const int& seed_in) { seed = seed_in; first_normal_deviate = true; }
	double get_random_seed() { return seed; }

	double RandomNumber1();
	double RandomNumber2();
	double NormalDeviate();
	double ndev(double min, double max);
};

struct Random_Sequence
{
	int idum, idum2, iy, iset;
	int iv[32];
	double gset;
	Random_Sequence() { idum = -10; idum2 = 10; iy = 0; }
	void input(int idum_in) { idum = idum_in; idum2 = fabs(idum_in); iy = 0; }
	void input(const Random_Sequence& rand_in) { idum = rand_in.idum; iset = rand_in.iset; gset = rand_in.gset; idum2 = fabs(idum); iy = 0; }
};

double RandomNumber_2(Random_Sequence&);


#endif // RAND_H
