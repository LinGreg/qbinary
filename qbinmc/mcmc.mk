
CC   := g++ -w -g
#CC   := g++ -w -O3 -DUSE_OPENMP -fopenmp
#CC   := mpicxx.openmpi -DUSE_MPI -DUSE_OPENMP -w -O3 -fopenmp

objects = mcmc.o mcmchdr.o GregsMathHdr.o errors.o spline.o

mcmc: $(objects)
	$(CC) -o mcmc $(objects) -lm

mcmc.o: mcmc.cpp mcmchdr.h mcmc.h
	$(CC) -c mcmc.cpp

errors.o: errors.cpp errors.h
	$(CC) -c errors.cpp

mcmchdr.o: mcmchdr.cpp mcmchdr.h GregsMathHdr.h random.h
	$(CC) -c mcmchdr.cpp

spline.o: spline.cpp spline.h
	$(CC) -c spline.cpp

GregsMathHdr.o: GregsMathHdr.cpp GregsMathHdr.h
	$(CC) -c GregsMathHdr.cpp

clean:
	rm mcmc $(objects)

clmain:
	rm mcmc.o

