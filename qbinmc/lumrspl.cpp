#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <sstream>
#include <cmath>
#include "lumrspl.h"
#include "spline.h"
#include "vector.h"
#include "errors.h"
#include "sort_old.h"
using namespace std;

void lumr_mass_Spline_age::input(double metallicity)
{
	cout << setprecision(12);
	stringstream metallicity_str;
	string metallicity_string;
	metallicity_str << metallicity;
	metallicity_str >> metallicity_string;

	double age, age_previous, mass, vmag, lum, r;
	string lum_splinefilename = "lum_mass.z" + metallicity_string;
	string lum_hb_agb_splinefilename = "lum_mass.hb_agb.z" + metallicity_string;
	string vmag_splinefilename = "vmag_mass.z" + metallicity_string;
	string vmag_hb_agb_splinefilename = "vmag_mass.hb_agb.z" + metallicity_string;
	string r_splinefilename = "radius_mass.z" + metallicity_string; // NOTE: the stellar radii in this file need to be in units of AU

	ifstream vmag_splinefile(vmag_splinefilename.c_str());
	ifstream vmag_hb_agb_splinefile(vmag_hb_agb_splinefilename.c_str());
	ifstream lum_splinefile(lum_splinefilename.c_str());
	ifstream lum_hb_agb_splinefile(lum_hb_agb_splinefilename.c_str());
	ifstream r_splinefile(r_splinefilename.c_str());
	vector<double> *mass_vec;
	vector<double> *hb_agb_mass_vec;
	vector<double> age_vec;
	age_previous = -1000; //uninitialized value
	while (lum_splinefile >> age >> mass >> lum)
	{
		if (age != age_previous) { age_vec.push_back(age); }
		age_previous=age;
	}
	n_age = age_vec.size();
	lum_splinefile.close();

	mass_vec = new vector<double>[n_age];
	vector<double> *mass_vec_ptr = mass_vec;
	double age_i=-1;
	age_previous = -1000; //uninitialized value
	lum_splinefile.open(lum_splinefilename.c_str());
	int yo=0;
	while (lum_splinefile >> age >> mass >> lum)
	{
		if (age != age_previous) { age_i++; if (age_previous != -1000) mass_vec_ptr++; yo=0;}
		//cout << age << " " << mass << " " << " " << lum << endl;
		//mass_vec[age_i].push_back(mass);
		mass_vec_ptr->push_back(mass);
		//cout << mass << " " << (*mass_vec_ptr)[yo] << endl;
		age_previous=age;
		yo++;
	}
	lum_splinefile.close();

	hb_agb_mass_vec = new vector<double>[n_age];
	vector<double> *hb_agb_mass_vec_ptr = hb_agb_mass_vec;
	age_i=-1;
	age_previous = -1000; //uninitialized value
	while (lum_hb_agb_splinefile >> age >> mass >> lum)
	{
		//cout << "WOAH " << age << " " << mass << " " << " " << lum << endl;
		if (age != age_previous) { age_i++; if (age_previous != -1000) hb_agb_mass_vec_ptr++; }
		//hb_agb_mass_vec[age_i].push_back(mass);
		hb_agb_mass_vec_ptr->push_back(mass);
		age_previous=age;
	}
	lum_hb_agb_splinefile.close();
	age_vals.input(n_age);
	int i;
	for (i=0; i < n_age; i++) age_vals[i] = age_vec[i];

	mass_vals = new dvector[n_age];
	hb_agb_mass_vals = new dvector[n_age];
	int j;
	for (j=0; j < n_age; j++)
	{
		mass_vals[j].input(mass_vec[j].size());
		hb_agb_mass_vals[j].input(hb_agb_mass_vec[j].size());
		for (i=0; i < mass_vec[j].size(); i++) mass_vals[j][i] = mass_vec[j][i];
		for (i=0; i < hb_agb_mass_vec[j].size(); i++) hb_agb_mass_vals[j][i] = hb_agb_mass_vec[j][i];
	}

	lumr_point = new lumr_points_mass[n_age];
	for (j=0; j < n_age; j++)
	{
		lumr_point[j].mass_vals.input(mass_vals[j]);
		lumr_point[j].vmagvals.input(mass_vals[j].size());
		lumr_point[j].lumvals.input(mass_vals[j].size());
		lumr_point[j].rvals.input(mass_vals[j].size());
	}

	lumr_hb_agb_point = new lumr_points_mass[n_age];
	for (j=0; j < n_age; j++)
	{
		lumr_hb_agb_point[j].mass_vals.input(hb_agb_mass_vals[j]);
		lumr_hb_agb_point[j].vmagvals.input(hb_agb_mass_vals[j].size());
		lumr_hb_agb_point[j].lumvals.input(hb_agb_mass_vals[j].size());
	}

	dvector max_rgb_mass_vals(n_age);
	dvector vmag_at_max_rgb_mass_vals(n_age);
	dvector lum_at_max_rgb_mass_vals(n_age);
	dvector radius_at_max_rgb_mass_vals(n_age);
	lum_splinefile.open(lum_splinefilename.c_str());
	for (i=0; i < n_age; i++)
	{
		for (j=0; j < mass_vals[i].size(); j++)
		{
			vmag_splinefile >> age >> mass >> vmag;
			//cout << age << " " << mass << " " << vmag << endl;
			lumr_point[i].vmagvals[j] = vmag;
			lum_splinefile >> age >> mass >> lum;
			//cout << age << " " << mass << " " << lum << " " << vmag << endl;
			lumr_point[i].lumvals[j] = lum;
			r_splinefile >> age >> mass >> r;
			lumr_point[i].rvals[j] = r;
			if (j==mass_vals[i].size()-1)
			{
				max_rgb_mass_vals[i] = mass;
				vmag_at_max_rgb_mass_vals[i] = vmag;
				lum_at_max_rgb_mass_vals[i] = lum;
				//cout << mass << " " << lum << endl;
				radius_at_max_rgb_mass_vals[i] = r;
			}
		}
	}
	vmag_splinefile.close();
	lum_splinefile.close();
	r_splinefile.close();

	max_rgb_mass_spline.input(age_vals,max_rgb_mass_vals);
	dvector sorted_masses(max_rgb_mass_vals);
	dvector sorted_age_vals(age_vals);
	sort(sorted_masses,sorted_age_vals);
	max_rgb_age_spline.input(sorted_masses,sorted_age_vals);
	//max_rgb_age_spline.printall(300);
	//die();

	lum_at_max_rgb_mass_spline.input(age_vals,lum_at_max_rgb_mass_vals);
	dvector sorted_masses_2(max_rgb_mass_vals);
	sort(sorted_masses_2,lum_at_max_rgb_mass_vals);
	lum_at_max_rgb_age_spline.input(sorted_masses_2,lum_at_max_rgb_mass_vals);

	radius_at_max_rgb_mass_spline.input(age_vals,radius_at_max_rgb_mass_vals);
	dvector sorted_masses_3(max_rgb_mass_vals);
	sort(sorted_masses_3,radius_at_max_rgb_mass_vals);
	radius_at_max_rgb_age_spline.input(sorted_masses_3,radius_at_max_rgb_mass_vals);

	vmag_at_max_rgb_mass_spline.input(age_vals,vmag_at_max_rgb_mass_vals);
	dvector sorted_masses_4(max_rgb_mass_vals);
	sort(sorted_masses_4,vmag_at_max_rgb_mass_vals);
	vmag_at_max_rgb_age_spline.input(sorted_masses_4,vmag_at_max_rgb_mass_vals);

	dvector max_hb_agb_mass_vals(n_age);
	dvector lum_at_max_hb_agb_mass_vals(n_age);
	dvector vmag_at_max_hb_agb_mass_vals(n_age);
	dvector min_hb_agb_mass_vals(n_age);
	dvector lum_at_min_hb_agb_mass_vals(n_age);
	dvector vmag_at_min_hb_agb_mass_vals(n_age);
	lum_hb_agb_splinefile.open(lum_hb_agb_splinefilename.c_str());
	for (i=0; i < n_age; i++)
	{
		for (j=0; j < hb_agb_mass_vals[i].size(); j++)
		{
			lum_hb_agb_splinefile >> age >> mass >> lum;
			lumr_hb_agb_point[i].lumvals[j] = lum;
			vmag_hb_agb_splinefile >> age >> mass >> vmag;
			lumr_hb_agb_point[i].vmagvals[j] = vmag;
			if (j==0)
			{
				min_hb_agb_mass_vals[i] = mass;
				vmag_at_min_hb_agb_mass_vals[i] = vmag;
				lum_at_min_hb_agb_mass_vals[i] = lum;
			}
			if (j==hb_agb_mass_vals[i].size()-1)
			{
				max_hb_agb_mass_vals[i] = mass;
				vmag_at_max_hb_agb_mass_vals[i] = vmag;
				lum_at_max_hb_agb_mass_vals[i] = lum;
			}
		}
	}
	lum_hb_agb_splinefile.close();
	vmag_hb_agb_splinefile.close();

	dvector sorted_age_vals_2(age_vals);
	max_hb_agb_mass_spline.input(age_vals,max_hb_agb_mass_vals);
	dvector sorted_hb_agb_masses_1(max_hb_agb_mass_vals);
	sorted_age_vals.input(age_vals);
	sort(sorted_hb_agb_masses_1,sorted_age_vals_2);
	max_hb_agb_age_spline.input(sorted_hb_agb_masses_1,sorted_age_vals_2);

	lum_at_max_hb_agb_mass_spline.input(age_vals,lum_at_max_hb_agb_mass_vals);
	dvector sorted_hb_agb_masses_2(max_hb_agb_mass_vals);
	sort(sorted_hb_agb_masses_2,lum_at_max_hb_agb_mass_vals);
	lum_at_max_hb_agb_age_spline.input(sorted_hb_agb_masses_2,lum_at_max_hb_agb_mass_vals);

	min_hb_agb_mass_spline.input(age_vals,min_hb_agb_mass_vals);
	dvector sorted_hb_agb_masses_3(min_hb_agb_mass_vals);
	dvector sorted_age_vals_3(age_vals);
	sort(sorted_hb_agb_masses_3,sorted_age_vals_3);
	min_hb_agb_age_spline.input(sorted_hb_agb_masses_3,sorted_age_vals_3);

	lum_at_min_hb_agb_mass_spline.input(age_vals,lum_at_min_hb_agb_mass_vals);
	dvector sorted_hb_agb_masses_4(min_hb_agb_mass_vals);
	sort(sorted_hb_agb_masses_4,lum_at_min_hb_agb_mass_vals);
	lum_at_min_hb_agb_age_spline.input(sorted_hb_agb_masses_4,lum_at_min_hb_agb_mass_vals);

	vmag_at_max_hb_agb_mass_spline.input(age_vals,vmag_at_max_hb_agb_mass_vals);
	dvector sorted_hb_agb_masses_5(max_hb_agb_mass_vals);
	sort(sorted_hb_agb_masses_5,vmag_at_max_hb_agb_mass_vals);
	vmag_at_max_hb_agb_age_spline.input(sorted_hb_agb_masses_5,vmag_at_max_hb_agb_mass_vals);

	vmag_at_min_hb_agb_mass_spline.input(age_vals,vmag_at_min_hb_agb_mass_vals);
	dvector sorted_hb_agb_masses_6(min_hb_agb_mass_vals);
	sort(sorted_hb_agb_masses_6,vmag_at_min_hb_agb_mass_vals);
	vmag_at_min_hb_agb_age_spline.input(sorted_hb_agb_masses_6,vmag_at_min_hb_agb_mass_vals);

	for (i=0; i < n_age; i++)
	{
		lumr_point[i].create_lum_mass_spline();
		lumr_point[i].create_vmag_mass_spline();
		lumr_point[i].create_r_mass_spline();
	}
	for (i=0; i < n_age; i++)
	{
		lumr_hb_agb_point[i].create_lum_mass_spline();
		lumr_hb_agb_point[i].create_vmag_mass_spline();
	}
	delete[] mass_vec;
	delete[] hb_agb_mass_vec;
}

lumr_mass_Spline_age::~lumr_mass_Spline_age()
{
	if (mass_vals != NULL) delete[] mass_vals;
	if (hb_agb_mass_vals != NULL) delete[] hb_agb_mass_vals;

	if (lumr_point != NULL)
		delete[] lumr_point;
	if (lumr_hb_agb_point != NULL)
		delete[] lumr_hb_agb_point;
}

void lumr_mass_Spline_age::set_age(double age)
{
	fixed_age = age;
	max_rgb_mass = max_rgb_mass_spline.splint(age);
	radius_at_max_rgb_mass = radius_at_max_rgb_mass_spline.splint(age);
	lum_at_max_rgb_mass = lum_at_max_rgb_mass_spline.splint(age);
	vmag_at_max_rgb_mass = vmag_at_max_rgb_mass_spline.splint(age);
	max_hb_agb_mass = max_hb_agb_mass_spline.splint(age);
	lum_at_max_hb_agb_mass = lum_at_max_hb_agb_mass_spline.splint(age);
	vmag_at_max_hb_agb_mass = vmag_at_max_hb_agb_mass_spline.splint(age);
	min_hb_agb_mass = min_hb_agb_mass_spline.splint(age);
	lum_at_min_hb_agb_mass = lum_at_min_hb_agb_mass_spline.splint(age);
	vmag_at_min_hb_agb_mass = vmag_at_min_hb_agb_mass_spline.splint(age);
	//cout << "lum: " << lum_at_max_rgb_mass << " " << lum_at_min_hb_agb_mass << " " << lum_at_max_hb_agb_mass << endl;
	//cout << "vmag: " << vmag_at_max_rgb_mass << " " << vmag_at_min_hb_agb_mass << " " << vmag_at_max_hb_agb_mass << endl;
}

double lumr_mass_Spline_age::interpolate_lum_at_fixed_age(double mass)
{
	double lum;
	if (mass-max_rgb_mass <= 1e-7) return lum_rgb_at_fixed_age_spline.splint(mass);
	else if (mass < min_hb_agb_mass) lum = lum_at_max_rgb_mass;
	else if (mass < max_hb_agb_mass + 1e-7) lum = lum_hb_agb_at_fixed_age_spline.splint(mass);
	else die("mass too large! mass = %g, max_mass = %g",mass,max_rgb_mass);
	return lum;
}

double lumr_mass_Spline_age::interpolate_vmag_at_fixed_age(double mass)
{
	//double lum, vmag, vmag_bol;
	//if (mass-max_rgb_mass <= 1e-7) lum = lum_rgb_at_fixed_age_spline.splint(mass);
	//else if (mass < min_hb_agb_mass) lum = lum_at_max_rgb_mass;
	//else if (mass < max_hb_agb_mass + 1e-7) lum = lum_hb_agb_at_fixed_age_spline.splint(mass);
	//else die("mass too large! mass = %g, max_mass = %g",mass,max_rgb_mass);
	//vmag_bol = 4.74 - 2.5*lum;
	//vmag = vmag_bol + 0.25; // this is the approximate bolometric correction for stars in this mass range

	double vmag_spl;
	if (include_hb_agb_stars==false) {
		if (mass-max_rgb_mass <= 1e-7) vmag_spl = vmag_rgb_at_fixed_age_spline.splint(mass);
		else if (mass < min_hb_agb_mass) vmag_spl = vmag_at_max_rgb_mass;
		else die("mass of HB/AGB star not allowed");
	} else {
		if (mass-max_rgb_mass <= 1e-7) vmag_spl = vmag_rgb_at_fixed_age_spline.splint(mass);
		else if (mass < min_hb_agb_mass) vmag_spl = vmag_at_max_rgb_mass;
		else if (mass < max_hb_agb_mass + 1e-7) vmag_spl = vmag_hb_agb_at_fixed_age_spline.splint(mass);
		else die("mass too large! mass = %g, max_mass = %g",mass,max_rgb_mass);
	}
	//cout << vmag << " " << vmag_spl << endl;
	return vmag_spl;
}

double lumr_mass_Spline_age::interpolate_radius_at_fixed_age(double mass)
{
	double radius;
	if (mass < max_rgb_mass) radius = radius_rgb_at_fixed_age_spline.splint(mass);
	else radius = radius_rgb_at_fixed_age_spline.y_at_xmax();
	return radius;
}

void lumr_mass_Spline_age::create_lum_r_splines_at_fixed_age(double age)
{
	set_age(age);
	double min_age_dist=1e30; // uninitialized value
	double age_dist;
	int closest_age_i;
	for (int i=0; i < n_age; i++)
	{
		age_dist = abs(age-age_vals[i]);
		if (age_dist < min_age_dist) { min_age_dist = age_dist; closest_age_i = i; }
	}
	//for (int i=0; i < lumr_point[closest_age_i].lumvals.size(); i++) cout << lumr_point[closest_age_i].lumvals[i] << endl;
	//for (int i=0; i < lumr_point[closest_age_i].vmagvals.size(); i++) cout << mass_vals[closest_age_i][i] << " " << lumr_point[closest_age_i].vmagvals[i] << endl;
	lum_rgb_at_fixed_age_spline.input(mass_vals[closest_age_i],lumr_point[closest_age_i].lumvals);
	vmag_rgb_at_fixed_age_spline.input(mass_vals[closest_age_i],lumr_point[closest_age_i].vmagvals);
	radius_rgb_at_fixed_age_spline.input(mass_vals[closest_age_i],lumr_point[closest_age_i].rvals);
	vmag_hb_agb_at_fixed_age_spline.input(lumr_hb_agb_point[closest_age_i].mass_vals,lumr_hb_agb_point[closest_age_i].vmagvals);
	lum_hb_agb_at_fixed_age_spline.input(lumr_hb_agb_point[closest_age_i].mass_vals,lumr_hb_agb_point[closest_age_i].lumvals);
	max_rgb_mass = lum_rgb_at_fixed_age_spline.xmax();
	min_hb_agb_mass = lum_hb_agb_at_fixed_age_spline.xmin();
	max_hb_agb_mass = lum_hb_agb_at_fixed_age_spline.xmax();
}

void lumr_mass_Spline_age::interpolate_lumr_mass_age(double mass, double age, double &lum, double &vmag, double &radius)
{
	set_age(age);
	interpolate_lumr(mass,lum,vmag,radius);
}

void lumr_mass_Spline_age::interpolate_lumr(double mass, double &lum, double &vmag, double &radius)
{
	double max_rgb_age_at_fixed_mass, max_hb_agb_age_at_fixed_mass, min_hb_agb_age_at_fixed_mass;
	double lum_at_max_rgb_age, vmag_at_max_rgb_age, radius_at_max_rgb_age;
	double lum_at_max_hb_agb_age, lum_at_min_hb_agb_age;
	double min_rgb_mass;
	if (max_rgb_age_spline.xmin() > mass)
	{
		max_rgb_age_at_fixed_mass = 1e30;
		max_hb_agb_age_at_fixed_mass = 1e30;
		min_hb_agb_age_at_fixed_mass = 1e30;
		lum_at_max_rgb_age = 1e30;
		vmag_at_max_rgb_age = 1e30;
		radius_at_max_rgb_age = 1e30;
		lum_at_max_hb_agb_age = 1e30;
		lum_at_min_hb_agb_age = 1e30;
	}
	else
	{
		max_rgb_age_at_fixed_mass = max_rgb_age_spline.splint(mass);
		max_hb_agb_age_at_fixed_mass = max_hb_agb_age_spline.splint(mass);
		min_hb_agb_age_at_fixed_mass = min_hb_agb_age_spline.splint(mass);
		vmag_at_max_rgb_age = vmag_at_max_rgb_age_spline.splint(mass);
		lum_at_max_rgb_age = lum_at_max_rgb_age_spline.splint(mass);
		radius_at_max_rgb_age = radius_at_max_rgb_age_spline.splint(mass);

		//lum_at_max_rgb_age_spline.printall(300);
		//die();
		lum_at_max_hb_agb_age = lum_at_max_hb_agb_age_spline.splint(mass);
		lum_at_min_hb_agb_age = lum_at_min_hb_agb_age_spline.splint(mass);
	}
	vector<double> lum_age_interpolation_vec;
	vector<double> vmag_age_interpolation_vec;
	vector<double> lum_hb_agb_age_interpolation_vec;
	vector<double> r_age_interpolation_vec;
	dvector vmag_age_interpolation;
	dvector lum_age_interpolation;
	dvector lum_hb_agb_age_interpolation;
	dvector r_age_interpolation;
	if (fixed_age < age_vals[0]) die("required fixed_age too young!");
	else if (fixed_age < max_rgb_age_at_fixed_mass)
	{
		vector<double> age_vals_at_fixed_mass_vec;
		vector<double> age_hb_agb_vals_at_fixed_mass_vec;
		dvector age_vals_at_fixed_mass;
		dvector age_hb_agb_vals_at_fixed_mass;
		for (int i=0; i < n_age; i++)
		{
			if (age_vals[i] < max_rgb_age_at_fixed_mass)
			{
				age_vals_at_fixed_mass_vec.push_back(age_vals[i]);
				vmag_age_interpolation_vec.push_back(lumr_point[i].vmag_interpolate(mass));
				lum_age_interpolation_vec.push_back(lumr_point[i].lum_interpolate(mass));
				r_age_interpolation_vec.push_back(lumr_point[i].r_interpolate(mass));
			}
		}
		if (max_rgb_age_at_fixed_mass < age_vals[n_age-1])
		{
			age_vals_at_fixed_mass_vec.push_back(max_rgb_age_at_fixed_mass);
			vmag_age_interpolation_vec.push_back(vmag_at_max_rgb_age);
			lum_age_interpolation_vec.push_back(lum_at_max_rgb_age);
			r_age_interpolation_vec.push_back(radius_at_max_rgb_age);
		}

		age_vals_at_fixed_mass.input(age_vals_at_fixed_mass_vec.size());
		lum_age_interpolation.input(lum_age_interpolation_vec.size());
		vmag_age_interpolation.input(vmag_age_interpolation_vec.size());
		r_age_interpolation.input(r_age_interpolation_vec.size());
		for (int i=0; i < age_vals_at_fixed_mass_vec.size(); i++)
		{
			age_vals_at_fixed_mass[i] = age_vals_at_fixed_mass_vec[i];
			lum_age_interpolation[i] = lum_age_interpolation_vec[i];
			vmag_age_interpolation[i] = vmag_age_interpolation_vec[i];
			r_age_interpolation[i] = r_age_interpolation_vec[i];
		}

		lum_age_spline_at_fixed_mass.input(age_vals_at_fixed_mass,lum_age_interpolation);
		vmag_age_spline_at_fixed_mass.input(age_vals_at_fixed_mass,vmag_age_interpolation);
		lum = lum_age_spline_at_fixed_mass.splint(fixed_age);
		vmag = vmag_age_spline_at_fixed_mass.splint(fixed_age);
		r_age_spline_at_fixed_mass.input(age_vals_at_fixed_mass,r_age_interpolation);
		radius = r_age_spline_at_fixed_mass.splint(fixed_age);
	}
	else { lum = -1e30; vmag = -1e30; radius = radius_at_max_rgb_mass; }
}


void lumr_mass_Spline_age::plot_lumr(int npoints_mass, int npoints_age)
{
	double mass, minmass, maxmass, mass_step;
	double age, min_age, max_age, age_step;
	minmass = mass_vals[0][0];
	maxmass = mass_vals[0][mass_vals[0].size()-1];
	min_age = age_vals[1];
	max_age = age_vals[age_vals.size()-4];
	mass_step = (maxmass-minmass)/(npoints_mass-1);
	age_step = (max_age-min_age)/(npoints_age-1);
	int i,j;
	double lum, radius, vmag;
	
	for (i=0, age=min_age; i < npoints_age; i++, age += age_step)
	{
		for (j=0, mass=minmass; j < npoints_mass; j++, mass += mass_step)
		{
			interpolate_lumr_mass_age(mass,age,lum,vmag,radius);
			if (lum != -1e30) { cout << age << " " << mass << " " << lum << " " << radius << endl; }
		}
	}
}

/**************************** age_metallicity uses metallicity and age data *******************************/

/*
void lumr_Likelihood_Spline_magtime::input(string spline_label)
{
	double time, mag, loglumr, like;
	vector<double> time_vec, mag_vec;
	string splinedir = "logdvhists.magtime/" + spline_label + "/";

	string timeval_filename = splinedir + "timevals";
	ifstream timeval_file(timeval_filename.c_str());
	while (timeval_file >> time)
		time_vec.push_back(time);
	ntimes = time_vec.size();
	timevals.input(ntimes);
	int i;
	for (i=0; i < ntimes; i++) timevals[i] = time_vec[i];

	string magval_filename = splinedir + "magvals";
	ifstream magval_file(magval_filename.c_str());
	while (magval_file >> mag)
		mag_vec.push_back(mag);
	nmags = mag_vec.size();
	magvals.input(nmags);
	int j;
	for (j=0; j < nmags; j++) magvals[j] = mag_vec[j];

	string magtimefilename, timestring, magstring;
	stringstream timestr0, magstr0;
	timestr0 << timevals[0];
	timestr0 >> timestring;
	magstr0 << magvals[0];
	magstr0 >> magstring;
	magtimefilename = splinedir + "t" + timestring + ".mag" + magstring;
	ifstream magtimelikefile(magtimefilename.c_str());
	vector<double> loglumr_vec;
	while (magtimelikefile >> loglumr >> like)
		loglumr_vec.push_back(loglumr);
	n_lumr = loglumr_vec.size();
	magtimelikefile.close();
	loglumr_vals.input(n_lumr);
	for (i=0; i < n_lumr; i++) loglumr_vals[i] = loglumr_vec[i];

	lumr_point = new lumr_points_magtime[n_lumr];
	for (i=0; i < n_lumr; i++)
	{
		lumr_point[i].time_vals.input(timevals);
		lumr_point[i].mag_vals.input(magvals);
		lumr_point[i].likevals.input(ntimes,nmags);
	}

	int lumr_i;
	for (i=0; i < ntimes; i++)
	{
		for (j=0; j < nmags; j++)
		{
			lumr_i=0;
		
			magtimefilename=timestring=magstring="";
			stringstream timestr, magstr;
			timestr << timevals[i];
			timestr >> timestring;
			magstr << magvals[j];
			magstr >> magstring;
			magtimefilename = splinedir + "t" + timestring + ".mag" + magstring;
			magtimelikefile.open(magtimefilename.c_str());
			while (magtimelikefile >> loglumr >> like)
			{
				lumr_point[lumr_i].likevals[i][j] = like;
				lumr_i++;
			}
			magtimelikefile.close();
		}
	}

	for (i=0; i < n_lumr; i++)
		lumr_point[i].create_magtimespline();

	//for (i=0; i < n_lumr; i++)
	//{
		//cout << loglumr_vals[i] << " " << lumr_point[i].interpolate(1,1) << endl;
	//}
	//die();
}

lumr_Likelihood_Spline_magtime::~lumr_Likelihood_Spline_magtime()
{
	if (lumr_point != NULL)
		delete[] lumr_point;
}

void lumr_Likelihood_Spline_magtime::set_magtime(double time, double mag)
{
	fixed_time = time;
	fixed_mag = mag;
	dvector likelihood_magtime_interpolation(n_lumr);
	for (int i=0; i < n_lumr; i++)
	{
		likelihood_magtime_interpolation[i] = lumr_point[i].interpolate(time,mag);
	}
	lumr_likelihood_spline_at_fixed_magtime.input(loglumr_vals,likelihood_magtime_interpolation);
}

*/

