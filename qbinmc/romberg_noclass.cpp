#include "romberg_noclass.h"
#include "errors.h"
#include <cmath>

#define FUNC(x) (*func)(x)

double romberg(double (*func)(const double), const double a, const double b, const double eps, const int k)
{
	const int jmax = 20;
	if (k > jmax) die("k must be less than or equal to max iterations (%i) in romberg_open", jmax);
	double ss, dss;
	double *s, *hsq;

	// hsq[j] is the (squared) stepsize of the j'th iteration, since the error is a function of h^2
	hsq = new double[jmax+1];
	s = new double[jmax];

	hsq[0] = 1.0;
	for (int j=1; j <= jmax; j++)
	{
		s[j-1] = trapzd(func,a,b,j);
		if (j >= k) {
			RombergPolyExtrapolate(hsq+j-k, s+j-k, k, 0.0, ss, dss);
			if (fabs(dss) <= eps*fabs(ss)) {
				delete[] s;
				delete[] hsq;
				return ss;
			}
		}
		hsq[j] = 0.25 * hsq[j-1]; // number of steps is doubled with each iteration, so h_new = h_old / 2
	}
	die("Too many iterations in routine romberg\n\ncalculated error = %g\nrequired accuracy = %g\nmax iterations = %i", fabs(dss), eps*fabs(ss), jmax);
	return 0.0;
}

double romberg_nocrash(double (*func)(const double), const double a, const double b, const double eps, const int k)
{
	const int jmax = 20;
	if (k > jmax) die("k must be less than or equal to max iterations (%i) in romberg_open", jmax);
	double ss, dss;
	double *s, *hsq;

	// hsq[j] is the (squared) stepsize of the j'th iteration, since the error is a function of h^2
	hsq = new double[jmax+1];
	s = new double[jmax];

	hsq[0] = 1.0;
	for (int j=1; j <= jmax; j++)
	{
		s[j-1] = trapzd(func,a,b,j);
		if (j >= k) {
			RombergPolyExtrapolate(hsq+j-k, s+j-k, k, 0.0, ss, dss);
			if (fabs(dss) <= eps*fabs(ss)) {
				delete[] s;
				delete[] hsq;
				return ss;
			}
		}
		hsq[j] = 0.25 * hsq[j-1]; // number of steps is doubled with each iteration, so h_new = h_old / 2
	}
	warn("Reached max number of iterations in routine romberg\n\ncalculated error = %g\nrequired accuracy = %g\nmax iterations = %i", fabs(dss), eps*fabs(ss), jmax);
	return ss;
}

double romberg(double (*func)(const double), const double a, const double b, const double eps, const int k, const double min_error)
{
	const int jmax = 20;
	if (k > jmax) die("k must be less than or equal to max iterations (%i) in romberg_open", jmax);
	double ss, dss;
	double *s, *hsq;
	double err, ferr;

	// hsq[j] is the (squared) stepsize of the j'th iteration, since the error is a function of h^2
	hsq = new double[jmax+1];
	s = new double[jmax];

	hsq[0] = 1.0;
	for (int j=1; j <= jmax; j++)
	{
		s[j-1] = trapzd(func,a,b,j);
		if (j >= k) {
			RombergPolyExtrapolate(hsq+j-k, s+j-k, k, 0.0, ss, dss);
			//err = ((ferr=eps*fabs(ss)) < min_error) ? min_error : ferr;
			err = (fabs(ss) < min_error) ? eps*fabs(ss) : min_error;
			if (fabs(dss) <= err) {
				delete[] s;
				delete[] hsq;
				return ss;
			}
		}
		hsq[j] = 0.25 * hsq[j-1]; // number of steps is doubled with each iteration, so h_new = h_old / 2
	}
	die("Too many iterations in routine romberg\n\ncalculated error = %g\nrequired accuracy = %g\nmax iterations = %i", fabs(dss), eps*fabs(ss), jmax);
	return 0.0;
}

double romberg_open(double (*func)(const double), const double a, const double b, const double eps, const int k)
{
	const int jmax = 20;
	if (k > jmax) die("k must be less than or equal to max iterations (%i) in romberg_open", jmax);
	double ss, dss;
	double *s, *hsq;

	// hsq[j] is the (squared) stepsize of the j'th iteration, since the error goes like h^2
	hsq = new double[jmax+1];
	s = new double[jmax];

	hsq[0] = 1.0;
	for (int j=1; j <= jmax; j++)
	{
		s[j-1] = midpnt(func,a,b,j);
		if (j >= k) {
			RombergPolyExtrapolate(hsq+j-k, s+j-k, k, 0.0, ss, dss);
			if (fabs(dss) <= eps*fabs(ss)) {
				delete[] s;
				delete[] hsq;
				return ss;
			}
		}
		hsq[j] = hsq[j-1]/9.0; // number of steps is tripled with each iteration, so h_new = h_old / 3
	}
	die("Too many iterations in routine romberg_open\n\ncalculated error = %g\nrequired accuracy = %g\nmax iterations = %i", fabs(dss), eps*fabs(ss), jmax);
	return 0.0;
}

double romberg_improper(double (*func)(const double), const double a, const double b, const double eps, const int k)
{
	const int jmax = 20;
	if (k > jmax) die("k must be less than or equal to max iterations (%i) in romberg_open", jmax);
	double ss, dss;
	double *s, *hsq;

	// hsq[j] is the (squared) stepsize of the j'th iteration, since the error goes like h^2
	hsq = new double[jmax+1];
	s = new double[jmax];

	hsq[0] = 1.0;
	for (int j=1; j <= jmax; j++)
	{
		s[j-1] = midinf(func,a,b,j);
		if (j >= k) {
			RombergPolyExtrapolate(hsq+j-k, s+j-k, k, 0.0, ss, dss);
			if (fabs(dss) <= eps*fabs(ss)) {
				delete[] s;
				delete[] hsq;
				return ss;
			}
		}
		hsq[j] = hsq[j-1]/9.0; // number of steps is tripled with each iteration, so h_new = h_old / 3
	}
	die("Too many iterations in routine romberg_open\n\ncalculated error = %g\nrequired accuracy = %g\nmax iterations = %i", fabs(dss), eps*fabs(ss), jmax);
	return 0.0;
}

double trapzd(double (*func)(double), const double a, const double b, const int n)
{
	double x, tnm, sum, del;
	static double s;
	int it, j;

	if (n == 1) {
		return (s = 0.5 * (b-a) * (FUNC(a)+FUNC(b)));
	} else {
		for (it=1, j=1; j < n-1; j++) it <<= 1;
		tnm = it;
		del = (b-a)/tnm;
		x = a + 0.5*del;
		for (sum=0.0, j=0; j < it; j++, x += del) sum += FUNC(x);
		s = 0.5*(s + (b-a)*sum/tnm);
		return s;
	}
}

double trapezoid_rule(double (*func)(double), const double a, const double b, const int npoints)
{
	double x, sum=0, step;
	double s;
	int j;

	step = (b-a)/(npoints-1);
	sum = 0.5*(FUNC(a)+FUNC(b));
	for (j=1, x=a+step; j < npoints-1; j++, x += step) { sum += FUNC(x); }
	s = (b-a)*sum/(npoints-1);
	return s;
}

double riemann_sum(double (*func)(double), const double a, const double b, const int npoints)
{
	double x, sum=0, step;
	double s;
	int j;

	step = (b-a)/(npoints-1);
	for (j=1, x=a+step; j < npoints-1; j++, x += step) { sum += FUNC(x); }
	s = sum*npoints*step;
	return s;
}

double qtrap(double (*func)(double), const double a, const double b, const double EPS)
{
	const int JMAX=20;
	int j;
	double s, olds=0.0;

	for (j=0; j < JMAX; j++) {
		s = trapzd(func,a,b,j+1);
		if (j > 5)
			if ((fabs(s-olds) < EPS*fabs(olds)) or (s == 0.0 and olds == 0.0)) return s;
		olds=s;
	}
	die("Too many steps in qtrap");
	return 0; // never get here
}


double midpnt(double (*func)(double), const double a, const double b, const int n)
{
	double x,tnm,sum,del,ddel;
	static double s;
	int it,j;

	if (n == 1) {
		return (s = (b-a)*FUNC(0.5*(a+b)));
	} else {
		for (it=1, j=1; j < n-1; j++) it *= 3;
		tnm = it;
		del = (b-a)/(3.0*tnm);
		ddel = del + del;
		x = a + 0.5*del;
		sum = 0.0;
		for (j=0; j < it; j++) {
			sum += FUNC(x);
			x += ddel;
			sum += FUNC(x);
			x += del;
		}
		s = (s + (b-a)*sum/tnm)/3.0;
		return s;
	}
}

double midinf(double (*func)(double), const double aa, const double bb, const int n)
{
	double a,b,x,tnm,sum,del,ddel,mid;
	static double s;
	int it,j;

	b=1.0/aa;
	a=1.0/bb;
	if (n == 1) {
		mid = 0.5*(a+b);
		return (s = (b-a)*(*func)(1.0/mid)/mid*mid);
	} else {
		for (it=1, j=1; j < n-1; j++) it *= 3;
		tnm = it;
		del = (b-a)/(3.0*tnm);
		ddel = del + del;
		x = a + 0.5*del;
		sum = 0.0;
		for (j=0; j < it; j++) {
			sum += (*func)(1.0/x)/(x*x);
			x += ddel;
			sum += (*func)(1.0/x)/(x*x);
			x += del;
		}
		s = (s + (b-a)*sum/tnm)/3.0;
		return s;
	}
}

void RombergPolyExtrapolate(double xa[], double ya[], const int n, const double x, double &y, double &dy)
{
	double *c, *d;
	c = new double[n];
	d = new double[n];
	for (int i=0; i < n; i++)
	{
		c[i] = ya[i];
		d[i] = ya[i];
	}

	double w;
	y = ya[n-1];
	for (int m=0; m < n-1; m++)
	{
		for (int i=0; i < n-m-1; i++)
		{
			w = (c[i+1] - d[i]) / (xa[i] - xa[i+m+1]);
			c[i] = w * (xa[i]-x);
			d[i] = w * (xa[i+m+1]-x);
		}
		y += d[n-m-2];
	}
	dy = d[0];
	delete[] c;
	delete[] d;
	return;
}

