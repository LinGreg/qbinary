
inline void SWAP(double &a, double &b)
	{double dum=a; a=b; b=dum;}
inline const double SIGN(const double &a, const double &b)
	{return b >= 0 ? (a >= 0 ? a : -a) : (a >= 0 ? -a : a);}
inline double MAX(const double &a, const double &b) { return (a > b ? a : b); }
inline double MIN(const double &a, const double &b) { return (a < b ? a : b); }

class BracketMethod
{
	protected:
	double ax,bx,cx,fa,fb,fc;
	inline void shft2(double &a, double &b, const double c) { a=b; b=c; }
	inline void shft3(double &a, double &b, double &c, const double d) { a=b; b=c; c=d; }
	inline void mov3(double &a, double &b, double &c, const double d, const double e, const double f) { a=d; b=e; c=f; }

	public:
	void set_interval(const double a, const double b) { ax=a; bx=b; }
	void get_bracket(double &a, double &b, double &c) { a=ax; b=bx; c=cx; }
	template <class T>
	void bracket(const double a, const double b, T &func)
	{
		const double GOLD=1.618034, GLIMIT=100.0, TINY=1.0e-20;
		ax = a; bx = b;
		double fu;
		fa = func(ax);
		fb = func(bx);
		if (fb > fa) {
			SWAP(ax,bx);
			SWAP(fb,fa);
		}
		cx = bx + GOLD*(bx-ax);
		fc = func(cx);

		//double r,q,u,ulim;
		while (fb > fc)
		{
			double r = (bx-ax)*(fb-fc);
			double q = (bx-cx)*(fb-fa);
			double u = bx - ((bx-cx)*q-(bx-ax)*r)/(2.0*SIGN(MAX(abs(q-r),TINY),q-r));
			double ulim = bx + GLIMIT*(cx-bx);
			if ((bx-u)*(u-cx) > 0.0) {
				fu = func(u);
				if (fu < fc) {
					ax = bx;
					bx = u;
					fa = fb;
					fb = fu;
					return;
				} else if (fu > fb) {
					cx = u;
					fc = fu;
					return;
				}
				u = cx + GOLD*(cx-bx);
				fu = func(u);
			} else if ((cx-u)*(u-ulim) > 0.0) {
				fu = func(u);
				if (fu < fc) {
					shft3(bx,cx,u,u+GOLD*(u-cx));
					shft3(fb,fc,fu,func(u));
				}
			} else if ((u-ulim)*(ulim-cx) >= 0.0) {
				u = ulim;
				fu = func(u);
			} else {
				u = cx + GOLD*(cx-bx);
				fu = func(u);
			}
			shft3(ax,bx,cx,u);
			shft3(fa,fb,fc,fu);
		}
	}
};

class GoldenMeanMethod : public BracketMethod
{
	private:
	double xmin, fmin;
	const double tol;

	public:
	GoldenMeanMethod(const double toll = 3.0e-8) : tol(toll) {}
	double get_fmin(void) { return fmin; }
	template <class T>
	double minimize(T &func)
	{
		const double R = 0.61803399, C = 1.0 - R;	// golden ratios
		double x1, x2, x0, x3;
		x0 = ax;
		x3 = cx;
		if (abs(cx-bx) > abs(bx-ax)) {
			x1 = bx;
			x2 = bx + C*(cx-bx);
		} else {
			x2 = bx;
			x1 = bx - C*(bx-ax);
		}
		double f1 = func(x1);
		double f2 = func(x2);
		while (abs(x3-x0) > tol*(abs(x1) + abs(x2)))
		{
			if (f2 < f1) { 
				shft3(x0,x1,x2,R*x2+C*x3);
				shft2(f1,f2,func(x2));
			} else {
				shft3(x3,x2,x1,R*x1+C*x0);
				shft2(f2,f1,func(x1));
			}
		}
		if (f1 < f2) {
			xmin = x1;
			fmin = f1;
		} else {
			xmin = x2;
			fmin = f2;
		}
		return xmin;
	}
};

class BrentsMinMethod : public BracketMethod
{
	private:
	static const int ITMAX = 100;
	static const double CGOLD = 0.3819660;
	static const double ZEPS = 1.0e-10;

	double xmin,fmin;
	const double tol;

	public:
	BrentsMinMethod(const double toll=3.0e-8) : tol(toll) {}
	double get_fmin(void) { return fmin; }

	template <class T>
	double minimize(T &func)
	{
		double a,b,d=0.0,etemp,fu,fv,fw,fx;
		double p,q,r,tol1,tol2,u,v,w,x,xm;
		double e=0.0;
		
		a = MIN(ax,cx);
		b = MAX(ax,cx);
		x=w=v=bx;
		fw=fv=fx=func(x);
		for (int iter=0; iter < ITMAX; iter++)
		{
			xm=0.5*(a+b);
			tol2 = 2.0 * ((tol1=tol*abs(x)) + ZEPS);
			if (abs(x-xm) <= (tol2-0.5*(b-a))) {
				fmin = fx;
				return xmin = x;
			}
			if (abs(e) > tol1) {
				r = (x-w)*(fx-fv);
				q = (x-v)*(fx-fw);
				p = (x-v)*q - (x-w)*r;
				q = 2.0*(q-r);
				if (q > 0.0) p = -p;
				q = abs(q);
				etemp = e;
				e = d;
				if (abs(p) >= abs(0.5*q*etemp) or p <= q*(a-x) or p >= q*(b-x))
					d = CGOLD*(e=(x >= xm ? a-x : b-x));
				else {
					d = p/q;
					u = x + d;
					if (u-a < tol2 or b-u < tol2)
						d = SIGN(tol1,xm-x);
				}
			} else {
				d = CGOLD*(e=(x >= xm ? a-x : b-x));
			}
			u = (abs(d) >= tol1 ? x+d : x + SIGN(tol1,d));
			fu = func(u);
			if (fu <= fx) {
				if (u >= x) a=x; else b=x;
				shft3(v,w,x,u);
				shft3(fv,fw,fx,fu);
			} else {
				if (u < x) a=u; else b=u;
				if (fu <= fw or w == x) {
					v = w;
					w = u;
					fv = fw;
					fw = fu;
				} else if (fu <= fv or v == x or v == w) {
					v = u;
					fv = fu;
				}
			}
		}
		die("Too many iterations in Brent's Method (for minimizing)");
	}
};

template <class T>
class f1dim;

template <class T>
class LineMethod
{
	protected:
	dvector p;
	dvector xi;
	T &func;
	int n;

	public:
	LineMethod(T &funcc) : func(funcc) {}
	double linemin()
	{
		double ax, xx, xmin;
		n = p.size();
		f1dim<T> f1dim(p,xi,func);
		ax = 0.0;
		xx = 0.1;
		//xx = 1.0;
		BrentsMinMethod brent;
		brent.bracket(ax,xx,f1dim);
		xmin=brent.minimize(f1dim);
		for (int j=0; j < n; j++)
		{
			xi[j] *= xmin;
			p[j] += xi[j];
		}
		return (brent.get_fmin());
	}
};

template <class T>
class f1dim
{
	private:
	const dvector& p;
	const dvector& xi;
	int n;
	T &func;
	dvector xt;

	public:
	f1dim(const dvector& pp, const dvector& xii, T &funcc) : p(pp), xi(xii), n(pp.size()), func(funcc), xt(n) {}
	double operator() (const double x)
	{
		for (int j=0; j<n; j++)
			xt[j] = p[j] + x*xi[j];
		return func(xt);
	}
};

template <class T>
class Powell : LineMethod<T>
{
	private:
	int iter;
	double fret;
	using LineMethod<T>::func;
	using LineMethod<T>::linemin;
	using LineMethod<T>::p;
	using LineMethod<T>::xi;
	const double ftol;

	public:
	Powell(T &func, const double ftoll=3.0e-8) : LineMethod<T>(func), ftol(ftoll) {}
	void set_precision(const double ftoll) { ftol = ftoll; }
	dvector minimize(const dvector& pp)
	{
		int n = pp.size();
		dmatrix ximat(n,n);
		ximat = 0;
		for (int i=0; i < n; i++) ximat[i][i] = 1.0;
		return (minimize(pp,ximat));
	}
	dvector minimize(const dvector& pp, const dmatrix& ximat)
	{
		const int ITMAX = 200;
		const double TINY = 1.0e-25;
		double fptt;
		int n = pp.size();
		p = pp;
		dvector pt(n), ptt(n);
		xi.resize(n);
		fret = func(p);
		for (int j=0; j < n; j++) pt[j] = p[j];
		for (iter=0; ; ++iter)
		{
			double fp = fret;
			int ibig = 0;
			double del = 0.0;
			for (int i=0; i < n; i++)
			{
				for (int j=0; j < n; j++) xi[j] = ximat[j][i];
				fptt = fret;
				fret = linemin();
				if (fptt-fret > del) {
					del = fptt - fret;
					ibig = i + 1;
				}
			}
			if (2.0*(fp-fret) <= ftol*(abs(fp) + abs(fret)) + TINY)
				return p;
			if (iter == ITMAX) die("powell exceeding maximum iterations.");
			for (int j=0; j < n; j++)
			{
				ptt[j] = 2.0*p[j] - pt[j];
				xi[j] = p[j] - pt[j];
				pt[j] = p[j];
			}
			fptt = func(ptt);
			if (fptt < fp)
			{
				double t = 2.0*(fp-2.0*fret+fptt)*SQR(fp-fret-del) - del*SQR(fp-fptt);
				if (t < 0.0)
				{
					fret = linemin();
					for (int j=0; j < n; j++) {
						ximat[j][ibig-1] = ximat[j][n-1];
						ximat[j][n-1] = xi[j];
					}
				}
			}
		}
	}
};

