#include "vector.h"
#include "errors.h"

#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp;

void sort(int n, double arr[])
{
	const int M = 7;
	const int nstack = 50;

	int i, j, k;
	int ir = n - 1, l = 0;
	int jstack = -1;
	double a, temp;

	int *istack = new int[nstack];
	for (;;) {
		if (ir-l < M) {
			for (j=l+1; j <= ir; j++) {
				a = arr[j];
				for (i = j-1; i >= l; i--) {
					if (arr[i] <= a) break;
					arr[i+1] = arr[i];
				}
				arr[i+1] = a;
			}
			if (jstack < 0) {
				delete[] istack;
				return;
			}
			ir = istack[jstack--];
			l = istack[jstack--];
		} else {
			k = (l+ir) >> 1;
			SWAP(arr[k],arr[l+1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir])
			}
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir])
			}
			if (arr[l] > arr[l+1]) {
				SWAP(arr[l],arr[l+1])
			}
			i = l + 1;
			j = ir;
			a = arr[l+1];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j])
			}
			arr[l+1] = arr[j];
			arr[j] = a;
			jstack += 2;
			if (jstack >= nstack) die("nstack too small in routine sort2");
			if (ir-i+1 >= j-l) {
				istack[jstack] = ir;
				istack[jstack-1] = i;
				ir = j - 1;
			} else {
				istack[jstack] = j - 1;
				istack[jstack-1] = l;
				l = i;
			}
		}
	}
}

void sort(dvector &arr)
{
	const int M = 7;
	const int nstack = 50;
	int n = arr.size();

	int i, j, k;
	int ir = n - 1, l = 0;
	int jstack = -1;
	double a, temp;

	int *istack = new int[nstack];
	for (;;) {
		if (ir-l < M) {
			for (j=l+1; j <= ir; j++) {
				a = arr[j];
				for (i = j-1; i >= l; i--) {
					if (arr[i] <= a) break;
					arr[i+1] = arr[i];
				}
				arr[i+1] = a;
			}
			if (jstack < 0) {
				delete[] istack;
				return;
			}
			ir = istack[jstack--];
			l = istack[jstack--];
		} else {
			k = (l+ir) >> 1;
			SWAP(arr[k],arr[l+1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir])
			}
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir])
			}
			if (arr[l] > arr[l+1]) {
				SWAP(arr[l],arr[l+1])
			}
			i = l + 1;
			j = ir;
			a = arr[l+1];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j])
			}
			arr[l+1] = arr[j];
			arr[j] = a;
			jstack += 2;
			if (jstack >= nstack) die("nstack too small in routine sort2");
			if (ir-i+1 >= j-l) {
				istack[jstack] = ir;
				istack[jstack-1] = i;
				ir = j - 1;
			} else {
				istack[jstack] = j - 1;
				istack[jstack-1] = l;
				l = i;
			}
		}
	}
}

void sort(int n, double arr[], double brr[])
{
	const int M = 7;
	const int nstack = 50;

	int i, j, k;
	int ir = n - 1, l = 0;
	int jstack = -1;
	double a, b, temp;

	int *istack = new int[nstack];
	for (;;) {
		if (ir-l < M) {
			for (j=l+1; j <= ir; j++) {
				a = arr[j];
				b = brr[j];
				for (i = j-1; i >= l; i--) {
					if (arr[i] <= a) break;
					arr[i+1] = arr[i];
					brr[i+1] = brr[i];
				}
				arr[i+1] = a;
				brr[i+1] = b;
			}
			if (jstack < 0) {
				delete[] istack;
				return;
			}
			ir = istack[jstack--];
			l = istack[jstack--];
		} else {
			k = (l+ir) >> 1;
			SWAP(arr[k],arr[l+1])
			SWAP(brr[k],brr[l+1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir])
				SWAP(brr[l],brr[ir])
			}
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir])
				SWAP(brr[l+1],brr[ir])
			}
			if (arr[l] > arr[l+1]) {
				SWAP(arr[l],arr[l+1])
				SWAP(brr[l],brr[l+1])
			}
			i = l + 1;
			j = ir;
			a = arr[l+1];
			b = brr[l+1];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j])
				SWAP(brr[i],brr[j])
			}
			arr[l+1] = arr[j];
			arr[j] = a;
			brr[l+1] = brr[j];
			brr[j] = b;
			jstack += 2;
			if (jstack >= nstack) die("nstack too small in routine sort2");
			if (ir-i+1 >= j-l) {
				istack[jstack] = ir;
				istack[jstack-1] = i;
				ir = j - 1;
			} else {
				istack[jstack] = j - 1;
				istack[jstack-1] = l;
				l = i;
			}
		}
	}
}

void sort(dvector &arr, dvector &brr)
{
	const int M = 7;
	const int nstack = 50;
	int n = arr.size();

	int i, j, k;
	int ir = n - 1, l = 0;
	int jstack = -1;
	double a, b, temp;

	int *istack = new int[nstack];
	for (;;) {
		if (ir-l < M) {
			for (j=l+1; j <= ir; j++) {
				a = arr[j];
				b = brr[j];
				for (i = j-1; i >= l; i--) {
					if (arr[i] <= a) break;
					arr[i+1] = arr[i];
					brr[i+1] = brr[i];
				}
				arr[i+1] = a;
				brr[i+1] = b;
			}
			if (jstack < 0) {
				delete[] istack;
				return;
			}
			ir = istack[jstack--];
			l = istack[jstack--];
		} else {
			k = (l+ir) >> 1;
			SWAP(arr[k],arr[l+1])
			SWAP(brr[k],brr[l+1])
			if (arr[l] > arr[ir]) {
				SWAP(arr[l],arr[ir])
				SWAP(brr[l],brr[ir])
			}
			if (arr[l+1] > arr[ir]) {
				SWAP(arr[l+1],arr[ir])
				SWAP(brr[l+1],brr[ir])
			}
			if (arr[l] > arr[l+1]) {
				SWAP(arr[l],arr[l+1])
				SWAP(brr[l],brr[l+1])
			}
			i = l + 1;
			j = ir;
			a = arr[l+1];
			b = brr[l+1];
			for (;;) {
				do i++; while (arr[i] < a);
				do j--; while (arr[j] > a);
				if (j < i) break;
				SWAP(arr[i],arr[j])
				SWAP(brr[i],brr[j])
			}
			arr[l+1] = arr[j];
			arr[j] = a;
			brr[l+1] = brr[j];
			brr[j] = b;
			jstack += 2;
			if (jstack >= nstack) die("nstack too small in routine sort2");
			if (ir-i+1 >= j-l) {
				istack[jstack] = ir;
				istack[jstack-1] = i;
				ir = j - 1;
			} else {
				istack[jstack] = j - 1;
				istack[jstack-1] = l;
				l = i;
			}
		}
	}
}

#undef SWAP
