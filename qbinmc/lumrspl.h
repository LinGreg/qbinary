#ifndef LUMRSPL_H
#define LUMRSPL_H

#include <vector>
#include "spline.h"
#include "vector.h"
#include "matrix.h"
using namespace std;

struct lumr_points_mass
{
	dvector lumvals;
	dvector vmagvals;
	dvector rvals;
	dvector mass_vals;
	Spline lum_mass;
	Spline vmag_mass;
	Spline r_mass;
	void create_lum_mass_spline(void) { lum_mass.input(mass_vals,lumvals); }
	void create_vmag_mass_spline(void) { vmag_mass.input(mass_vals,vmagvals); }
	void create_r_mass_spline(void) { r_mass.input(mass_vals,rvals); }
	double vmag_interpolate(double mass) { return vmag_mass.splint(mass); }
	double lum_interpolate(double mass) { return lum_mass.splint(mass); }
	double r_interpolate(double mass) { return r_mass.splint(mass); }
	void lum_printall(int nn) { lum_mass.printall(nn); }
	void r_printall(int nn) { r_mass.printall(nn); }
	lumr_points_mass() {}
	~lumr_points_mass() {}
};

struct lumr_mass_Spline_age
{
	double fixed_age;
	bool include_hb_agb_stars;

	int n_age;
	Spline min_hb_agb_age_mass_spline;
	Spline max_hb_agb_age_mass_spline;
	Spline max_age_mass_spline;

	Spline max_rgb_age_spline;
	Spline max_rgb_mass_spline;
	Spline radius_at_max_rgb_age_spline;
	Spline vmag_at_max_rgb_age_spline;
	Spline lum_at_max_rgb_age_spline;
	Spline radius_at_max_rgb_mass_spline;
	Spline vmag_at_max_rgb_mass_spline;
	Spline lum_at_max_rgb_mass_spline;
	double max_rgb_mass;
	double radius_at_max_rgb_mass;
	double vmag_at_max_rgb_mass;
	double lum_at_max_rgb_mass;

	Spline min_hb_agb_age_spline;
	Spline min_hb_agb_mass_spline;
	Spline vmag_at_min_hb_agb_age_spline;
	Spline vmag_at_min_hb_agb_mass_spline;
	Spline lum_at_min_hb_agb_age_spline;
	Spline lum_at_min_hb_agb_mass_spline;
	double min_hb_agb_mass;
	double vmag_at_min_hb_agb_mass;
	double lum_at_min_hb_agb_mass;

	Spline max_hb_agb_age_spline;
	Spline max_hb_agb_mass_spline;
	Spline vmag_at_max_hb_agb_age_spline;
	Spline vmag_at_max_hb_agb_mass_spline;
	Spline lum_at_max_hb_agb_age_spline;
	Spline lum_at_max_hb_agb_mass_spline;
	double max_hb_agb_mass;
	double vmag_at_max_hb_agb_mass;
	double lum_at_max_hb_agb_mass;

	dvector age_vals;
	dvector *mass_vals;
	dvector *hb_agb_mass_vals;
	lumr_points_mass *lumr_point;
	lumr_points_mass *lumr_hb_agb_point;
	Spline vmag_age_spline_at_fixed_mass;
	Spline lum_age_spline_at_fixed_mass;
	Spline lum_age_spline_hb_agb_at_fixed_mass;
	Spline vmag_age_spline_hb_agb_at_fixed_mass;
	Spline r_age_spline_at_fixed_mass;

	Spline vmag_rgb_at_fixed_age_spline;
	Spline lum_rgb_at_fixed_age_spline;
	Spline radius_rgb_at_fixed_age_spline;
	Spline vmag_hb_agb_at_fixed_age_spline;
	Spline lum_hb_agb_at_fixed_age_spline;

	public:
	lumr_mass_Spline_age() {
		dvector a(3), b(3); a[0]=0;a[1]=1;a[2]=3;b[0]=1;b[1]=2;b[2]=3;
		lumr_point=NULL; mass_vals = NULL; hb_agb_mass_vals = NULL; lumr_hb_agb_point=NULL; include_hb_agb_stars = false; }
	~lumr_mass_Spline_age();
	void input(double metallicity);
	void set_age(double age);
	double interpolate_lum_at_fixed_age(double mass);
	double interpolate_vmag_at_fixed_age(double mass);
	double interpolate_radius_at_fixed_age(double mass);
	void create_lum_r_splines_at_fixed_age(double age);
	void interpolate_lumr(double mass, double &lum, double &vmag, double &radius);
	void interpolate_lumr_mass_age(double mass, double age, double &lum, double &vmag, double &radius);

	double plot_r(int npoints) { radius_rgb_at_fixed_age_spline.printall(npoints); }
	double min_vmag() { return vmag_rgb_at_fixed_age_spline.y_at_xmin(); }
	double max_vmag() { return vmag_rgb_at_fixed_age_spline.y_at_xmax(); }
	double min_mass(void) { return lum_rgb_at_fixed_age_spline.xmin(); }
	double min_rgb_mass_at_fixed_age(void) { return lum_rgb_at_fixed_age_spline.xmin(); }
	double max_rgb_mass_at_fixed_age(void) { return lum_rgb_at_fixed_age_spline.xmax(); }
	double min_hb_agb_vmag() { return vmag_hb_agb_at_fixed_age_spline.y_at_xmin(); }
	double max_hb_agb_vmag() { return vmag_hb_agb_at_fixed_age_spline.y_at_xmax(); }
	double max_hb_agb_mass_at_fixed_age(void) { return lum_hb_agb_at_fixed_age_spline.xmax(); }
	double min_hb_agb_mass_at_fixed_age(void) { return lum_hb_agb_at_fixed_age_spline.xmin(); }
	void plot_lumr(int npoints_mass, int npoints_age);
	void plot_lum_r_splines(int npoints, string lumfilename, string lumfilename_hb_agb, string rfilename)
	{
		lum_rgb_at_fixed_age_spline.printall(npoints,lumfilename);
		lum_hb_agb_at_fixed_age_spline.printall(npoints,lumfilename_hb_agb);
		radius_rgb_at_fixed_age_spline.printall(npoints,rfilename); 
	}
};

/*
struct lumr_points_metallicity_age
{
	dmatrix likevals;
	dvector age_vals;
	dvector metallicity_vals;
	Spline2D lumr_metallicity_age;
	void create_metallicity_agespline(void) { lumr_metallicity_age.input(age_vals,metallicity_vals,likevals); }
	double interpolate(double age, double metallicity) { return lumr_metallicity_age.splint(age,metallicity); }
	double zval(int i, int j) { return lumr_metallicity_age.zval(i,j); }
	lumr_points_metallicity_age() {}
	~lumr_points_metallicity_age() {}
};

class lumr_mass_Spline_metallicity_age
{
	int n_lumr;
	int n_age, nmetallicitys;
	dvector agevals, metallicityvals;
	dvector mass_vals;
	lumr_points_metallicity_age *lumr_point;
	double fixed_age, fixed_metallicity;
	Spline lumr_mass_spline_at_fixed_metallicity_age;

	public:
	lumr_mass_Spline_metallicity_age() { lumr_point=NULL; }
	~lumr_mass_Spline_metallicity_age();
	void input(string spline_label);
	void set_metallicity_age(double age, double metallicity);
	double interpolate(double lumr) { return lumr_mass_spline_at_fixed_metallicity_age.splint(lumr); }
	double plot_likelihood(int npoints) { lumr_mass_spline_at_fixed_metallicity_age.printall(npoints); }
	double plot_likelihood(int npoints, string filename) { lumr_mass_spline_at_fixed_metallicity_age.printall(npoints,filename); }
	double lumr_min(void) { return mass_vals[0]; }
	double lumr_max(void) { return mass_vals[n_lumr-1]; }
};
*/

#endif // LUMRSPL_H
