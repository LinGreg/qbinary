#ifndef RANDOM_H
#define RANDOM_H
#include <iostream>
#include <cstdio>
#include <cmath>
#include "GregsMathHdr.h"

using namespace std;

class Cholesky
{
	private:
		int n;
		double **el;
		
	public:
		Cholesky(const int nin) : n(nin)
		{
			el = matrix <double> (n, n);
		}
		
		Cholesky(double **a, const int nin) : n(nin)
		{
			el = matrix <double> (n, n);
			double sum = 0;
			int i, j, k;
			
			for (i = 0; i < n; i++)
				for (j = 0; j < n; j++)
					el[i][j] = a[i][j];

			for (i = 0; i < n; i++)
			{
				for (j = i; j < n; j++)
				{
					for(sum = el[i][j], k = i - 1; k >= 0; k--)
						sum -= el[i][k]*el[j][k];
					if(i ==j)
					{
						if(sum <= 0.0)
						{
							cout << "Cholesky failed " << sum << endl;
							getchar();
						}
						el[i][i] = sqrt(sum);
					}
					else
						el[j][i] = sum/el[i][i];
				}
			}
			for (i = 0; i < n; i++)
				for (j = 0; j < i; j++)
					el[j][i] = 0.0;
		}
		
		bool EnterMatM(double **a, const int min)
		{
			double sum = 0;
			int i, j, k;
			//ofstream out1("cov.dat");
			for (i = 0; i < n; i++)
			{
				for (j = 0; j < n; j++)
				{
					el[i][j] = a[i][j];
					//out1 << el[i][j] << "   " << endl;
				}
				//out1 << endl;
			}
			
// 			if (n >= min)
// 			{
// 				k = min -1;
// 				int l = n - min + 1;
// 				for (i = 0; i < l; i++)
// 				{
// 					for (j = k+i; j < n; j++)
// 						el[i][j] = el[j][i] = 0.0;
// 				}
// 			}
			
			for (i = 0; i < n; i++)
			{
				for (j = i; j < n; j++)
				{
					for(sum = el[i][j], k = i - 1; k >= 0; k--)
						sum -= el[i][k]*el[j][k];
					if(i ==j)
					{
						if(sum <= 0.0)
							return true;
						el[i][i] = sqrt(sum);
					}
					else
						el[j][i] = sum/el[i][i];
				}
			}
			for (i = 0; i < n; i++)
				for (j = 0; j < i; j++)
					el[j][i] = 0.0;
				
// 						ofstream out2("cov2.dat");
// 			double **ct = matrix <double> (n, n, 0.0);
// 			for (i = 0; i < n; i++)
// 				for (j = 0; j < n; j++)
// 					for (k = 0; k < n; k++)
// 						ct[i][j] += el[i][k]*el[j][k];
// 			for (i = 0; i < n; i++)
// 			{
// 				for (j = 0; j < n; j++)
// 					out2 << ct[i][j] << "   ";
// 				out2 << endl;
// 			}
// 			cout << "finished" << endl;
//			getchar();
				
			return false;
		}
		
		bool EnterMat(double **a)
		{
			bool good = true;
			double sum = 0;
			int i, j, k;
			
			for (i = 0; i < n; i++)
			{
				for (j = 0; j < n; j++)
				{
					el[i][j] = a[i][j];
				}
			}
			
			for (i = 0; i < n; i++)
			{
				for (j = i; j < n; j++)
				{
					for(sum = el[i][j], k = i - 1; k >= 0; k--)
						sum -= el[i][k]*el[j][k];
					if(i ==j)
					{
						if(sum <= 0.0)
						{good = false;  cout << "Cholesky failed " << sum << endl;}
						el[i][i] = sqrt(sum);
					}
					else
						el[j][i] = sum/el[i][i];
				}
			}
			for (i = 0; i < n; i++)
				for (j = 0; j < i; j++)
					el[j][i] = 0.0;
				
			return good;
		}
		
		void EnterMat(double **a, int nin)
		{
			del <double> (el, n);
			n = nin;
			el = matrix <double> (n, n);
			EnterMat(a);
		}
		
		void ElMult (double *y, double *b)
		{
			int i, j;
			for(i = 0; i < n; i++)
			{
				b[i] = 0.0;
				for (j = 0; j <= i; j++)
					b[i] += el[i][j]*y[j];
			}
		}
		
		void ElMult (double *y)
		{
			int i, j;
			double *b = new double[n];
			for(i = 0; i < n; i++)
			{
				b[i] = 0.0;
				for (j = 0; j <= i; j++)
				{
					b[i] += el[i][j]*y[j];
				}
			}
			for (i = 0; i < n; i++)
			{
				y[i] = b[i];
			}
			delete[] b;
		}
		
		void Solve (double *b, double *x)
		{
			int i, k;
			double sum;
			
			for (i = 0; i < n; i++)
			{
				for (sum = b[i], k=i-1; k >=0; k--)
					sum -= el[i][k]*x[k];
				x[i]=sum/el[i][i];
			}
			
			for (i = n-1; i >=0; i--)
			{
				for (sum = x[i], k=i+1; k<n; k++)
					sum -= el[k][i]*x[k];
				x[i]=sum/el[i][i];
			}
		}
		
		double Square(double *y, double *y0)
		{
			int i, j;
			double sum;
			double *x = new double[n];
			
			for (i = 0; i < n; i++)
			{
				for (sum = (y[i]-y0[i]), j=0; j < i; j++)
					sum -= el[i][j]*x[j];
				x[i]=sum/el[i][i];
			}
			
			sum = 0.0;
			for (i = 0; i < n; i++)
				sum += x[i]*x[i];
			
			delete[] x;
			
			return sum;
		}
		
		double Square(double *y, double *y0, int *map)
		{
			int i, j;
			double sum;
			double *x = new double[n];
			
			for (i = 0; i < n; i++)
			{
				for (sum = (y[map[i]]-y0[i]), j=0; j < i; j++)
					sum -= el[i][j]*x[j];
				x[i]=sum/el[i][i];
			}
			
			sum = 0.0;
			for (i = 0; i < n; i++)
				sum += x[i]*x[i];
			
			delete[] x;
			
			return sum;
		}
		
		void Inverse(double **ainv)
		{
			double sum;
			
			for (int i = 0; i < n; i++)
				for (int j = 0; j <= i; j++)
				{
					sum = (i == j ? 1.0 : 0.0);
					for (int k = i-1; k >= j; k--)
						sum -= el[i][k]*ainv[j][k];
					ainv[j][i] = sum/el[i][i];
				}

			for (int i = n - 1; i >= 0; i--)
				for (int j = 0; j <= i; j++)
				{
					sum = (i < j ? 0.0 : ainv[j][i]);
					for (int k = i + 1; k < n; k++)
						sum -= el[k][i]*ainv[j][k];
					ainv[i][j] = ainv[j][i] = sum/el[i][i];
				}
		}

		double DetSqrt()
		{
			double temp = 1.0;
			for (int i = 0; i < n; i++)
				temp *= el[i][i];
			return temp;
		}
		~Cholesky()
		{
			del <double> (el, n);
		}
};

class Ran
{
	private:
		unsigned long long int u, v, w;
		
	public:
		Ran(unsigned long long int j) : v(4101842887655102017LL), w(1)
		{
			u = j ^ v; int64();
			v = u; int64(); int64();
			w = v; int64(); int64();
		}
		inline unsigned long long int int64()
		{
			u = u * 2862933555777941757LL + 7046029254386353087LL;
			v ^= v >> 17; v ^= v << 31; v ^= v >> 8;
			w = 4294957665U*(w & 0xffffffff) + (w >> 32);
			unsigned long long int x = u ^ (u << 21); x ^= x >> 35; x ^= x << 4;
			return (x + v) ^ w;
		}
		inline double Doub(){return 5.42101086242752217E-20 * int64();}
		inline unsigned int int32(){return (unsigned int)int64();}
};

class ExponDev : public Ran
{
	private:
		double beta;
	public:
		ExponDev(double din, unsigned long long ii) : Ran(ii), beta(din){}
		double Dev()
		{
			double u;
			do
			{
				u = Doub();
			}
			while(u == 0);
			
			return -log(u)/beta;
		}
};

class NormalDev : public Ran
{
	private:
		double mu, sig;
		
	public:
		NormalDev(double mmu, double ssig, unsigned long long i) : Ran(i), mu(mmu), sig(ssig){}
		double  Dev()
		{
			double u, v, x, y, q;
			do
			{
				u = Doub();
				v = 1.7156*(Doub() - 0.5);
				x = u - 0.449871;
				y = fabs(v) + 0.386595;
				q = x*x + y*(0.19600*y-0.25472*x);
			}
			while(q > 0.27597 && (q > 0.27846 || v*v > -4.0*log(u)*u*u));
			
			return mu + sig*v/u;
		}
};

class BasicDevs : public Ran
{
	private:
		
	public:
		BasicDevs(unsigned long long i) : Ran(i) {}

		double  Dev()
		{
			double u, v, x, y, q;
			do
			{
				u = Doub();
				v = 1.7156*(Doub() - 0.5);
				x = u - 0.449871;
				y = fabs(v) + 0.386595;
				q = x*x + y*(0.19600*y-0.25472*x);
			}
			while(q > 0.27597 && (q > 0.27846 || v*v > -4.0*log(u)*u*u));
			
			return v/u;
		}
		
		double ExpDev()
		{
			double u;
			do
			{
				u = Doub();
			}
			while(u == 0);
			
			return -log(u);
		}
};

class MultiNormalDev : public Ran, public Cholesky
{
	private:
		int mm;
		double f;
		double *spt;
		
	public:
		MultiNormalDev (double **vvar, double fin, unsigned long long int j, int nin) : Ran(j), Cholesky(vvar, nin), mm(nin)
		{
			spt = matrix <double> (mm);
		}
		void Dev(double *pt, double *mean)
		{
			double u, v, x, y, q;
			for (int i = 0; i < mm; i++)
			{
				do
				{
					u = Doub();
					v = 1.7156*(Doub() - 0.5);
					x = u - 0.449871;
					y = fabs(v) + 0.386595;
					q = x*x + y*(0.19600*y-0.25472*x);
				}
				while(q > 0.27597 && (q > 0.27846 || v*v > -4.0*log(u)*u*u));
				spt[i] = v/u;
			}
			ElMult(spt, pt);
			for (int i = 0; i < mm; i++)
				pt[i] = pt[i] + mean[i];
		}
		void Dev(double **cvar, double *pt, double *mean)
		{
			double u, v, x, y, q;
			for (int i = 0; i < mm; i++)
			{
				do
				{
					u = Doub();
					v = 1.7156*(Doub() - 0.5);
					x = u - 0.449871;
					y = fabs(v) + 0.386595;
					q = x*x + y*(0.19600*y-0.25472*x);
				}
				while(q > 0.27597 && (q > 0.27846 || v*v > -4.0*log(u)*u*u));
				spt[i] = v/u;
			}
			EnterMat(cvar);
			ElMult(spt, pt);
			for (int i = 0; i < mm; i++)
				pt[i] = pt[i] + mean[i];
		}
		~MultiNormalDev()
		{
			del <double> (spt);
		}
};

class RandomBasis : public BasicDevs
{
	private:
		double **rotVec;
		
	protected:
		int num;
		double **currentVec;
		double **endVec;

	public:
		RandomBasis(int nin, unsigned long long iin) : BasicDevs(iin), num(nin)
		{
			rotVec = matrix <double> (nin, nin);
			RandRot();
			endVec = currentVec + num;
		}
		
		void ChangeDim(const int nin)
		{
			del <double> (rotVec, num);
			num = nin;
			rotVec = matrix <double> (nin, nin);
			RandRot();
			endVec = currentVec + num;
		}
			
		void RandRot()
		{
			double temp;
			double vec[num];
			int i, j, k;
				
			for (i = 0; i < num; i++)
			{
				for (j = 0; j < num; j++)
					vec[j] = Dev();
				for (j = 0; j < i; j++)
				{
					temp = 0.0;
					for (k = 0; k < num; k++)
						temp += vec[k]*rotVec[j][k];
					for (k = 0; k < num; k++)
						vec[k] -= temp*rotVec[j][k];
				}
				temp = 0.0;
				for (j = 0; j < num; j++)
					temp += vec[j]*vec[j];
				temp = sqrt(temp);
				for (j = 0; j < num; j++)
					rotVec[i][j] = vec[j]/temp;
			}
			
			currentVec = rotVec;
		}
			
		double RanMult(double **cin)
		{
			int i, j;
			double temp = 0.0;
			
			for (i = 0; i < num; i++)
			{
				for (j = 0; j < num; j++)
				{
					temp += (*currentVec)[i]*cin[i][j]*(*currentVec)[j];
				}
			}
			return temp;
		}
			
		void RanMult(const double in, double *out)
		{
			int i;
			double *ptr = *currentVec;
			
			for (i = 0; i < num; i++)
			{
				*out++ = in*(*ptr++);
					
			}
			return;
		}
			
		void RanMult(double *in, const double w, double *out)
		{
			int i;
			double *ptr = *currentVec;
			
			for (i = 0; i < num; i++)
			{
				*out++ = *in++ + w*(*ptr++);
			}
			return;
		}
			
		double Mag(double *a, double *a0)
		{
			double temp = 0.0;
			int i;
			double *ptr = *currentVec;
			
			for (i = 0; i < num; i++)
				temp += (*a++-*a0++)*(*ptr++);
				
			return temp;
		}
			
		void Adjust(double *a, const double lim, const int iin)
		{
			double temp = (lim - a[iin])/(*currentVec)[iin];
			double *ptr = *currentVec;
			
			for (int i = 0; i < num; i++)
				a[i] += temp*(*ptr++);
			return;
		}
			
		virtual void operator ++ (int)
		{
			if (++currentVec == endVec)
			{
				RandRot();
			}
		}
		
		virtual ~RandomBasis()
		{
			del <double> (rotVec, num);
		}
};

class TransformRandomBasis : public RandomBasis, public Cholesky
{
	private:

	public:
		TransformRandomBasis(double **vvar, int nin, unsigned long long iin) : RandomBasis(nin, iin), Cholesky(vvar, nin)
		{
			double **ptr = currentVec;
			for (int i = 0; i < num; i++)
			{
				ElMult(*ptr++);
			}
		}
	
		void operator ++ (int)
		{
			if (++currentVec == endVec)
			{
				RandRot();
				double **ptr = currentVec;
				for (int i = 0; i < num; i++)
				{
					ElMult(*ptr++);
				}
			}
		}
};

class MultiNormDev : public RandomBasis, public Cholesky
{
	private:
		double fac;
		
	public:
		MultiNormDev(int nin, double din, unsigned long long iin) : RandomBasis(nin, iin), Cholesky(nin), fac(din)
		{
		}
		
		MultiNormDev(double **vvar, const int nin, double din, unsigned long long iin) : RandomBasis(nin, iin), Cholesky(vvar, nin), fac(din)
		{
		}
		
		void MultiDev(double *pin, double *p0)
		{
			int i;
			double dist = 0.0;

			if(Doub() < 0.33)
				dist = ExpDev();
			else
			{
				double temp;
				for (i = 0; i < 2; i++)
				{
					temp = Dev();
					dist += temp*temp;
				}
				dist = sqrt(dist/2.0);
			}

			ElMult(*currentVec, pin);
			for (i = 0; i < num; i++)
			{
				pin[i] = fac*dist*pin[i] + p0[i];
			}
			(*this)++;

			return;
		}
		
		void MultiDev(double **cvar, double *pin, double *p0)
		{
			int i;
			double dist = 0.0;

			if(Doub() < 0.33)
				dist = ExpDev();
			else
			{
				double temp;
				for (i = 0; i < 2; i++)
				{
					temp = Dev();
					dist += temp*temp;
				}
				dist = sqrt(dist/2.0);
			}

			EnterMat(cvar);
			ElMult(*currentVec, pin);
			for (i = 0; i < num; i++)
			{
				pin[i] = fac*dist*pin[i] + p0[i];
			}
			(*this)++;

			return;
		}
		
		void EllipseDev(double *pin, double *p0, double fin)
		{
			int i;
			double dist = 0.0;

			dist = pow(Doub(), 1.0/num);
			
			ElMult(*currentVec, pin);
			for (i = 0; i < num; i++)
			{
				pin[i] = fin*dist*pin[i] + p0[i];
			}
			(*this)++;

			return;
		}
		
		void EllipseDev(double **cvar, double *pin, double *p0, double fin)
		{
			int i;
			double dist = 0.0;

			dist = pow(Doub(), 1.0/num);
			
			EnterMat(cvar);
			ElMult(*currentVec, pin);
			for (i = 0; i < num; i++)
			{
				pin[i] = fin*dist*pin[i] + p0[i];
			}
			(*this)++;

			return;
		}
};

#endif
