default: mkdist
GCC   := g++ -w -O3

mkdist_objects = mkdist.o mcmceval.o GregsMathHdr.o errors.o

mkdist: $(mkdist_objects)
	$(GCC) -o mkdist $(mkdist_objects) -lm

errors.o: errors.cpp errors.h
	$(GCC) -c errors.cpp

GregsMathHdr.o: GregsMathHdr.cpp GregsMathHdr.h
	$(GCC) -c GregsMathHdr.cpp

mcmceval.o: mcmceval.cpp mcmceval.h GregsMathHdr.h random.h errors.h
	$(GCC) -c mcmceval.cpp

mkdist.o: mkdist.cpp mcmceval.h errors.h
	$(GCC) -c mkdist.cpp

clean:
	rm mkdist $(mkdist_objects)

clmain:
	rm mkdist.o

vim_run:
	mkdist

